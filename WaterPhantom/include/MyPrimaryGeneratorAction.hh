#ifndef MyPrimaryGeneratorAction_h
#define MyPrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4SystemOfUnits.hh"
//#include "G4ParticleDefinition.hh"
#include <CLHEP/Random/RandGaussQ.h>

class MyPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    MyPrimaryGeneratorAction();
    ~MyPrimaryGeneratorAction();
    
    //////////////
    // Required //
    //////////////
    void GeneratePrimaries(G4Event* anEvent);
  private:
     G4ParticleGun* fParticleGun;
    CLHEP::RandGauss* fRandGauss;

    G4double fBeamProfileX; //mm
	G4double fBeamProfileY; //mm
	G4double fBeamDivergenceX; //radian
	G4double fBeamDivergenceY; //radian

};

#endif // MyPrimaryGeneratorAction_h 
