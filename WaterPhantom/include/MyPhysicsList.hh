#ifndef MyPhysicsList_h
#define MyPhysicsList_h 1

//#include "G4VUserPhysicsList.hh"
//#include "G4ParticleTypes.hh"
//#include "G4ParticleTable.hh"
//#include "G4ProcessManager.hh"
//#include "G4MultipleScattering.hh"
//#include "G4hIonisation.hh"

#include "G4VModularPhysicsList.hh"
#include "G4EmStandardPhysics.hh"
/// Modular physics list
///
/// It includes the folowing physics builders
/// - G4DecayPhysics
/// - G4RadioactiveDecayPhysics
/// - G4EmStandardPhysics


//class MyPhysicsList: public G4VUserPhysicsList
class MyPhysicsList : public G4VModularPhysicsList
{
  public:
    MyPhysicsList();
    virtual ~MyPhysicsList();
    
    //////////////
    // Required //
    //////////////
    //void ConstructParticle();
    //void ConstructProcess();
    virtual void SetCuts();
};

#endif
