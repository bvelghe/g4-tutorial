#ifndef MyDetectorConstruction_h
#define MyDetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"

#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"

#include "G4ThreeVector.hh"
#include "WaterPhantom.hh"

#include <vector>

class MyDetectorConstruction : public G4VUserDetectorConstruction 
{
	public:
    MyDetectorConstruction();
    ~MyDetectorConstruction();
    void SetProperties();
    //////////////
    // Required //
    //////////////
    G4VPhysicalVolume* Construct();
    //FIXME HACK
    G4ThreeVector GetSensorPosition();
    private:
      G4NistManager * fNistMan;

      
      G4Box * fworldSolidVolume;
      G4LogicalVolume * fworldLogicalVolume;
      G4VPhysicalVolume * fworldPhysicalVolume;

      WaterPhantom * fWaterPhantom;
};

#endif // MyDetectorConstruction_h
