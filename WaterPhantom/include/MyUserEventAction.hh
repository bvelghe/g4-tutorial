#ifndef MyUserEventAction_h
#define MyUserEventAction_h 1

#include "G4UserEventAction.hh"
#include "MySensorHit.hh"
#include "G4Event.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4UnitsTable.hh"
#include "MyUserRunAction.hh"

class MyUserEventAction : public G4UserEventAction
{
	public:
           MyUserEventAction(MyUserRunAction *runAction);
           ~MyUserEventAction();
           void BeginOfEventAction(const G4Event*);
           void EndOfEventAction(const G4Event*);
          
	private:
		G4int fHitsCollectionID;
		MySensorHitsCollection* fHitsCollection;
        G4SDManager * fSDman;
        MyUserRunAction * fRunAction;
};

#endif // MyUserEventAction_h
