#ifndef __WATERPHANTOM_HH__
#define __WATERPHANTOM_HH__

// Materials 
//   - Box: Polymethyl methacrylate (PMMA) 
//   - Sensor: Silicium 

#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Box.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"

#include "G4SystemOfUnits.hh"
#include "MyDetectorMessenger.hh"
#include "G4SDManager.hh"
#include "MySensorSD.hh"
#include "G4RunManager.hh"
class MyDetectorMessenger; //FIXME

class WaterPhantom : public G4VUserDetectorConstruction {
  public:  	
    WaterPhantom(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy);
    ~WaterPhantom();	
    //Pure virtual 
    G4VPhysicalVolume* Construct();
    void SetProperties(); 
    bool MoveSensor(G4ThreeVector pos);
    G4ThreeVector GetSensorPosition();
  private:
    MyDetectorMessenger * fMessenger;

    G4LogicalVolume* fMotherLogicalVolume; 
    G4ThreeVector fPosition;
    G4int fCopy;

    MySensorSD* fSensorSD;
    G4String fSensorName;
    ///////////////////
    // Water phantom //
    ///////////////////
    G4Box * fWaterPhantomSolidVolume;
    G4Box * fWallSolidVolume;
    G4Box * fWaterSolidVolume;
    
    G4LogicalVolume * fWaterPhantomLogicalVolume;
    G4LogicalVolume * fWallLogicalVolume;
    G4LogicalVolume * fWaterLogicalVolume;

    G4VPhysicalVolume * fWaterPhantomPhysicalVolume;
    G4VPhysicalVolume * fWallPhysicalVolume;
    G4VPhysicalVolume * fWaterPhysicalVolume;

    G4VisAttributes * fWallVisAtt;
    G4VisAttributes * fWaterVisAtt;

    //////////////////
    // Dummy sensor //
    //////////////////
    G4Box * fSensorSolidVolume;

    G4LogicalVolume * fSensorLogicalVolume;

    G4VPhysicalVolume * fSensorPhysicalVolume;

    G4VisAttributes fSensorVisAtt;

};

#endif // __WATERPHANTOM_HH__
