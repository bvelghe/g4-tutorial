#ifndef __MyDetectorMessenger_hh__
#define __MyDetectorMessenger_hh__

#include "G4UImessenger.hh"
#include "WaterPhantom.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIdirectory.hh"

class WaterPhantom; //FIXME

class MyDetectorMessenger : public G4UImessenger
{
  public:
    MyDetectorMessenger(WaterPhantom * Det);
    //MyDetectorMessenger();
    virtual ~MyDetectorMessenger();
    virtual void SetNewValue(G4UIcommand*, G4String);
  private:
    WaterPhantom * fWaterPhantom;
    G4UIdirectory*           fDetDirectory;
    G4UIcmdWith3VectorAndUnit* fSetSensorPos;

};
#endif // __MyDetectorMessenger_hh__
