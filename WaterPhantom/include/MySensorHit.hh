#ifndef MySensorHit_h
#define MySensorHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"

class MySensorHit : public G4VHit {
	public:
		MySensorHit();
		~MySensorHit();
		//
		MySensorHit(const MySensorHit&);
		const MySensorHit& operator=(const MySensorHit&);
	  	G4int operator==(const MySensorHit&) const;
		//
		//void Draw();
		//void Print();
		//
		inline void Energy(G4double energy) { fEnergy = energy;} 
		inline void AddEnergy(G4double energy) { fEnergy += energy;}
        //
	    G4double Energy(void) { return fEnergy;}
	    //
	        
	private:
		G4double fEnergy;
};

//Hits storage 
typedef G4THitsCollection<MySensorHit> MySensorHitsCollection;

#endif // MySensorHit_h
