#ifndef MySensorSD_h
#define MySensorSD_h 1

#include "G4VSensitiveDetector.hh"
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "MySensorHit.hh"

class MySensorSD : public G4VSensitiveDetector {
	public:
		MySensorSD(G4String,G4int);
		~MySensorSD();
		void Initialize(G4HCofThisEvent*);
		G4bool ProcessHits(G4Step*, G4TouchableHistory*);
	private:
		G4int fCopy;
		G4int fHCID; //Hits collection ID
        G4int fCell;
		MySensorHitsCollection * fHitsCollection; // see MySensorHit.hh
};

#endif // MySensorSD_h
