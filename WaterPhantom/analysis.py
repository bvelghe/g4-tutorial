def mean_energy(f): 
	acc = 0.0
	nhits = 0
	fh = open(f)
	for line in fh:
		if(line[0] == '#'):
			continue
		nhits += 1
		acc += float(line)
	fh.close()
	if(nhits !=0):
		return (acc/nhits)
	else: return 0.0

filelist = ['output_run'+str(i)+'.dat' for i in range(0,40)]
for f in filelist:
	print mean_energy(f)

