#include "MyDetectorMessenger.hh"


MyDetectorMessenger::MyDetectorMessenger(WaterPhantom* Det) : G4UImessenger(),
   fWaterPhantom(Det) {
   
   fDetDirectory = new G4UIdirectory("/phantom/");
   fDetDirectory->SetGuidance("Water phantom control");

    /////////////////////////////////
    // Control the sensor position //
    /////////////////////////////////

   fSetSensorPos = new G4UIcmdWith3VectorAndUnit("/phantom/setSensorPos",this);
   fSetSensorPos->SetGuidance("Set the sensor postion");
   fSetSensorPos->SetGuidance("The sensor must be inside the phantom");
   fSetSensorPos->SetParameterName("X","Y","Z",false);
   fSetSensorPos->SetUnitCategory("Length");
   fSetSensorPos->AvailableForStates(G4State_PreInit,G4State_Idle);

}



MyDetectorMessenger::~MyDetectorMessenger() {
    delete fDetDirectory;
    delete fSetSensorPos;
}

void MyDetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue) {
 if( command == fSetSensorPos ) {
    fWaterPhantom->MoveSensor(fSetSensorPos->GetNew3VectorValue(newValue));
  }
}
