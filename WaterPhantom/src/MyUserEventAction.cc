#include "MyUserEventAction.hh"


//FIXME : iCopy ?

MyUserEventAction::MyUserEventAction(MyUserRunAction *runAction) {
	fHitsCollectionID = -1;
	fHitsCollection = 0;
    fSDman = G4SDManager::GetSDMpointer();
    fRunAction = runAction;
}
MyUserEventAction::~MyUserEventAction() {;}

void MyUserEventAction::BeginOfEventAction(const G4Event* event) {

	if(fHitsCollectionID < 0) fHitsCollectionID = fSDman->GetCollectionID("Sensor_HitsCollection_0");
 
    /////////////////////////////
    // Display run progression //
    /////////////////////////////	
    G4int eventID = event->GetEventID();
	if(eventID%1000 == 0) {
        G4cout << "# >>> Event " << event->GetEventID() << G4endl;
    }
}


void MyUserEventAction::EndOfEventAction(const G4Event* event) {
      
	if(fHitsCollectionID < 0) return;
 	G4HCofThisEvent* HCTE = event->GetHCofThisEvent();
	if(HCTE) {
        fHitsCollection = (MySensorHitsCollection*)(HCTE->GetHC(fHitsCollectionID));
		G4int NbHits = 0;

		NbHits = fHitsCollection->entries();	
		for (G4int i = 0; i < NbHits; i++)
        {
            MySensorHit *hit =  (*fHitsCollection)[i];
            fRunAction->StoreHit(hit->Energy());
        }		
    }	
}

