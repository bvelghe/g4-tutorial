#include "WaterPhantom.hh"

WaterPhantom::WaterPhantom(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy) {
    fMotherLogicalVolume = motherLogicalVolume;
	fPosition = position;
	fCopy = copy;
    fMessenger = new MyDetectorMessenger(this);
}

WaterPhantom::~WaterPhantom() {
    delete fMessenger;
}

G4VPhysicalVolume* WaterPhantom::Construct() {
    ///////////////////
    // Water phantom //
    ///////////////////

    fWaterPhantomSolidVolume = new G4Box("WaterPhantom",15.0*cm,15.0*cm,15.0*cm); //300mm
    fWallSolidVolume = new G4Box("Wall",15.0*cm,15.0*cm,0.06*cm); //1.2mm
    fWaterSolidVolume = new G4Box("Water",15.0*cm,15.0*cm,14.94*cm); // 298.8mm
    
    fWaterPhantomLogicalVolume = new G4LogicalVolume(fWaterPhantomSolidVolume,
                                G4Material::GetMaterial("G4_Galactic"),
                                "WaterPhantom",
                                0,
                                0,
                                0);

    fWallLogicalVolume = new G4LogicalVolume(fWallSolidVolume,
                                G4Material::GetMaterial("PMMA"),
                                "Wall",
                                0,
                                0,
                                0);

    fWaterLogicalVolume =  new G4LogicalVolume(fWaterSolidVolume,
                                G4Material::GetMaterial("G4_WATER"),
                                "Water",
                                0,
                                0,
                                0);

    fWaterPhantomPhysicalVolume = new G4PVPlacement(0,
                                fPosition,
                                fWaterPhantomLogicalVolume, 
                                "WaterPhantom",
                                fMotherLogicalVolume,
                                false
                                ,0);   

    fWallPhysicalVolume = new G4PVPlacement(0,
                                G4ThreeVector(0.,0.,-0.5*29.88*cm),
                                fWallLogicalVolume,
                                "Wall",
                                fWaterPhantomLogicalVolume,
                                false
                                ,0); 
 
    fWaterPhysicalVolume = new G4PVPlacement(0,
                                G4ThreeVector(0.,0.,0.5*0.12*cm), 
                                fWaterLogicalVolume,
                                "Water",
                                fWaterPhantomLogicalVolume,
                                false
                                ,0); 


    //////////////////
    // Dummy sensor //
    //////////////////
    fSensorSolidVolume = new G4Box("Sensor",0.25*cm,0.25*cm,0.1*cm);
    
    fSensorLogicalVolume = new G4LogicalVolume(fSensorSolidVolume,
                                G4Material::GetMaterial("G4_Si"),
                                "Sensor",
                                0,
                                0,
                                0);

    fSensorPhysicalVolume = new G4PVPlacement(0,
                                //G4ThreeVector(0.,0.,0.5*(1*cm - 29*cm) + 0.5*0.5*cm),
                                G4ThreeVector(0.,0.,0.),
                                fSensorLogicalVolume,
                                "Sensor",
                                fWaterLogicalVolume,
                                false
                                ,0); 
    
		
		/////////////////////
    // Sensitve Volume //
    /////////////////////
    //FIXME Clean the code 
    // Name of the detector 
	std::ostringstream oss;
    oss << fCopy;
	fSensorName = "/Sensor_"+oss.str();
	
	G4SDManager* SDmanager = G4SDManager::GetSDMpointer();
	fSensorSD = new MySensorSD(fSensorName,fCopy);
    fSensorLogicalVolume->SetSensitiveDetector(fSensorSD);
	SDmanager->AddNewDetector(fSensorSD);

    return fWaterPhantomPhysicalVolume;                        
}

bool WaterPhantom::MoveSensor(G4ThreeVector pos) {

    if(!fSensorPhysicalVolume) return false;
    ////////////    
    // Checks //
    ////////////
    G4cout << "Move sensor to " << pos << G4endl;
    G4double z_min = 0.5*(0.12*cm - 29.88*cm) + 0.5*0.5*cm;   
    //G4double z_max = 0.5*(1*cm + 29.9*cm) - 0.5*0.5*cm; //FIXME NEED UPDATE
    if(pos.z() < 0.0 || pos.z() > 29.38*cm) return false;
    pos.setZ(z_min + pos.z());
    G4cout << "Debug: " << pos << G4endl;   

    fSensorPhysicalVolume->SetTranslation(pos);
    // Update geometry
    G4RunManager::GetRunManager()->GeometryHasBeenModified();
    return true;                         
}

G4ThreeVector WaterPhantom::GetSensorPosition() {
    G4double z_min = 0.5*(0.12*cm - 29.88*cm) + 0.5*0.5*cm;   
    G4ThreeVector pos = fSensorPhysicalVolume->GetTranslation();
    pos.setZ(pos.z() - z_min);
    return pos;
}

void WaterPhantom::SetProperties() {
    fWallVisAtt = new G4VisAttributes(G4Colour(0.0,1.0,0.0)); //G4Colour (red, green, blue, alpha=1)
    fWaterVisAtt = new G4VisAttributes(G4Colour(0.0,0.0,1.0));
    fSensorVisAtt = new G4VisAttributes(G4Colour(1.0,1.0,0.0));

		fWallLogicalVolume->SetVisAttributes(fWallVisAtt);
    fWaterLogicalVolume->SetVisAttributes(fWaterVisAtt); 
    fSensorLogicalVolume->SetVisAttributes(fSensorVisAtt);

	fWaterPhantomLogicalVolume->SetVisAttributes(G4VisAttributes::Invisible);
}
