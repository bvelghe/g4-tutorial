#include "MyPrimaryGeneratorAction.hh"

//////////////////////////////
//	Génération du faisceau	//
//////////////////////////////

MyPrimaryGeneratorAction::MyPrimaryGeneratorAction() {
    G4int n_particle = 1;
	fParticleGun = new G4ParticleGun(n_particle);
	
	//Random engine
	fRandGauss = new CLHEP::RandGauss(CLHEP::HepRandom::getTheEngine());

    G4ParticleTable * particleTable = G4ParticleTable::GetParticleTable();
	
    ///////////////////////////
	// Paramètre du faisceau //
    ///////////////////////////

	fBeamProfileX = 5.0; //mm
	fBeamProfileY = 5.0; //mm
	fBeamDivergenceX = 0.001; //radian
	fBeamDivergenceY = 0.001; //radian
	
  //G4ParticleDefinition * proton = particleTable->FindParticle("proton"); //FIXME geantino
  G4ParticleDefinition * proton = particleTable->FindParticle("gamma"); //FIXME geantino
	fParticleGun->SetParticleDefinition(proton);

}

MyPrimaryGeneratorAction::~MyPrimaryGeneratorAction() {
    delete fParticleGun;
}

void MyPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
 
	
	//Beam divergence
	G4double x_kick = atan(fRandGauss->fire(0,fBeamDivergenceX)); //m
	G4double y_kick = atan(fRandGauss->fire(0,fBeamDivergenceY)); //m
	//Beam spatial extension (Gaussian)
	G4double x_offset = fRandGauss->fire(0.0,fBeamProfileX); //mm
	G4double y_offset = fRandGauss->fire(0.0,fBeamProfileY); //mm
		
    //Beam energy (Gaussian)
    //G4double E_offset = fRandGauss->fire(0.0,1*MeV); //1 MeV DEMO FIXME
    G4double E_offset = 0.0;

	fParticleGun->SetParticleMomentumDirection(G4ThreeVector(x_offset*mm+x_kick*m,y_offset*mm+y_kick*m,+1.0*m)); 
	fParticleGun->SetParticlePosition(G4ThreeVector(x_offset*mm,y_offset*mm,0*m));
	fParticleGun->GeneratePrimaryVertex(anEvent);
	fParticleGun->SetParticleEnergy(E_offset+60*MeV); //60 MeV
}
