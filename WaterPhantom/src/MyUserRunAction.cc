#include "MyUserRunAction.hh"



MyUserRunAction::MyUserRunAction(MyDetectorConstruction * det)
{
    fDetector = det;
    fRunManager = G4RunManager::GetRunManager();
    //(G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction()
}


MyUserRunAction::~MyUserRunAction()
{;}

bool MyUserRunAction::StoreEvent(G4int event_id) {
	return true;
}


bool MyUserRunAction::StoreHit(G4double energy) {
    //FIXME Slow ...    
    if(fOutFile.is_open()) {
        fOutFile <<  energy/MeV << G4endl;
    }
	return true;
}


void MyUserRunAction::BeginOfRunAction(const G4Run* aRun)
{
  //fRunManager->GeometryHasBeenModified(); //<<<---  
  fRunID = aRun->GetRunID();
  G4cout << "### Run " << fRunID << " start." << G4endl;
  G4cout << "# Sensor position: " << fDetector->GetSensorPosition() << G4endl;
  G4cout << "# Beam energy: " << "60 MeV" << G4endl; //FIXME
  G4RunManager::GetRunManager()->SetRandomNumberStore(true);
  //Open output file
  std::ostringstream os; os << fRunID;
  G4String filename = "output_run"+os.str()+".dat";
  G4cout << "# Output in " << filename << G4endl;
  fOutFile.open(filename.c_str());
  if(!fOutFile.is_open()) {
    //FIXME
  } else {
    fOutFile << "### Run " << fRunID << " start." << G4endl;
    fOutFile << "# Sensor position: " << fDetector->GetSensorPosition() << G4endl;
    fOutFile << "# Beam energy: " << "60 MeV" << G4endl; //FIXME
    fOutFile << "# Energy (MeV)" << G4endl;
  }
}

void MyUserRunAction::EndOfRunAction(const G4Run*)
{
  G4cout << "### Run " << fRunID << " end." << G4endl;
  //Close output file
  if(fOutFile.is_open()) {
    fOutFile.close(); 
  }
}
