#include "MySensorHit.hh"

MySensorHit::MySensorHit() {
	fEnergy = 0;
}
MySensorHit::~MySensorHit() {}

/////////////////////////
// Not yet implemented //
/////////////////////////

//void MySensorHit::Draw() {}
//void MySensorHit::Print() {}

// Shallow copy is sufficicent
MySensorHit::MySensorHit(const MySensorHit& right)
{
	fEnergy = right.fEnergy;

}

const MySensorHit& MySensorHit::operator=(const MySensorHit& right)
{
	if (this == &right) return *this; //Check for self-assignment
	fEnergy = right.fEnergy;


	return *this;
}

G4int MySensorHit::operator==(const MySensorHit& right) const
{
    return (this==&right) ? 1 : 0;
}
