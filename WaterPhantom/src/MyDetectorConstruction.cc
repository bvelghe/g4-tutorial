#include "MyDetectorConstruction.hh"

MyDetectorConstruction::MyDetectorConstruction() {
	/////////////////////
	// Build materials //
	/////////////////////
    
    G4double density;
    std::vector<G4String> PMMA_elm;
    PMMA_elm.push_back("H");
    PMMA_elm.push_back("C");
    PMMA_elm.push_back("O");
    std::vector<G4int> PMMA_nbAtoms;
    PMMA_nbAtoms.push_back(5);
    PMMA_nbAtoms.push_back(8);
    PMMA_nbAtoms.push_back(2);

	fNistMan = G4NistManager::Instance();
 	fNistMan->FindOrBuildMaterial("G4_Galactic");
    fNistMan->FindOrBuildMaterial("G4_WATER");
    fNistMan->FindOrBuildMaterial("G4_Si");
    fNistMan->ConstructNewMaterial("PMMA",PMMA_elm,PMMA_nbAtoms, density=1190*kg/m3);
}
MyDetectorConstruction::~MyDetectorConstruction() {

}
  
G4VPhysicalVolume* MyDetectorConstruction::Construct() {
    ///////////
    // World //
    ///////////
    fworldSolidVolume = new G4Box("World",1.0*m,1.0*m,1.0*m);
    
    fworldLogicalVolume = new G4LogicalVolume(fworldSolidVolume,
                                G4Material::GetMaterial("G4_Galactic"),
                                "World",
                                0,
                                0,
                                0);
                                
    fworldPhysicalVolume = new G4PVPlacement(0,
                                G4ThreeVector(0.,0.,0.),
                                fworldLogicalVolume,
                                "World",
                                0,
                                false
                                ,0);  

    ///////////////////
    // Water phantom //
    ///////////////////
    fWaterPhantom = new WaterPhantom(fworldLogicalVolume,G4ThreeVector(0.,0.,0.5*m),0);
    fWaterPhantom->Construct();
	fWaterPhantom->SetProperties();
    
    SetProperties();
    return fworldPhysicalVolume;                        
}

// HACK FIXME FIXME FIXME

G4ThreeVector MyDetectorConstruction::GetSensorPosition() {
    if(fWaterPhantom) return fWaterPhantom->GetSensorPosition();
    else return G4ThreeVector(0.,0.,0.);
}

void MyDetectorConstruction::SetProperties() {	
	 //World volume is invisible !
	fworldLogicalVolume->SetVisAttributes(G4VisAttributes::Invisible);
}
