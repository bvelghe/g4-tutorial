# UBC PHYS 531 - Radiation Detectors - Geant4 Simulation of a Small TPC

The _Monte Carlo_ (MC) simulation of the detector setup is an important tool in experimental high energy physics. Typically, the interaction of the particles with the complete detectors geometry and responses are modeled.
The MC simulation can be used to optimize the detector layout and study the _signal_ and _backgrouds_ in ideal conditions. 

We prepared a virtual machine (VM) to allow you to run a simple simulation of the TPC used in the lab. 
The parameters of the simulation were chosen to match the actual setup. This will allow you to compare your measurements with the output of the simulation.

The VM provides a ready-to-use environement with:

 - [Ubuntu 16.04 LTS](https://www.ubuntu.com/): an easy to use Linux distribution,
 - [Geant4 4.10](https://geant4.cern.ch/): a C++ framework to simulate the interaction of particles with matter,
 - [ROOT 6.08](https://root.cern.ch/): a C++ data analysis toolkit wildly used in high energy physics.

This document will guide you through the installation process and gives you a simple tour of the simulation. Finally, some tips are given on how to modify the simulation.

## How to install the VM?
The image can be downloaded from [TO BE DETERMINED](TO BE DETERMINED).

 - First, if it is not installed on your computer, you need to install [VirtualBox](https://www.virtualbox.org/), it runs on Linux, MacOSX and Windows,
 - Import the VM image:
 ![import VM](../screenshots/import_vm.png)

Once the VM image is installed you can start the machine by clicking on the Start button. After a few moment, you will be presented with a login prompt:
![login screen](../screenshots/login_screen.png)

Use the password `g4` to login in. The the username is `g4`.

## Geant4?
[Geant4](https://geant4.cern.ch/) (G4) is a standard toolkit to simulate the interaction of particles with matter. Most of the high energy experiments are using this package to model their setup.
In a nutshell, the user has to provides the geometry of the detector, the type of particles and their properties (momentum, direction, etc), and the physics processes to simulate (electromagnetic interactions, etc.).
The package then takes care of propagating the particles inside the geometry in _steps_. At each _step_ it computes the energy deposited and, possibly, generate new particles (e.g. hadronic showers, etc.).

The user is responsible for translating the energy deposited in a certain volume into physical variable of interest. This stage is often call the _digitalization_.

For simple setup, like this one, G4 provides a _scoring_ interface which allow to extract some quantities directly from the simulation without the need of writing a digitalization. Link to the documentation are given below. 

## How to compile the Geant4 model?
Before using the model, and after every modification to the source code, we need to comple it.
The code is stored in the `~/g4tutoriel/AlphaTPC` directory. To compile it follow the steps:
 - Open a terminal:
 ![start terminal](../screenshots/start_terminal.png)
 - Navigate to the `~/g4tutoriel` directory:
```
cd ~/g4tutoriel
```
 - Source the `env.sh` script to source the Geant4 environment:
 ```
source env.sh
 ```
 - Create a build directory:
```
mkdir build
```
 - Inside the build directory, setup the build:  
```
cd build
cmake ../AlphaTPC
```
 - Compile the project:
```
make
```

That's it, you should now have a AlphaTPC executable inside the `build` directory.

## How to run the simulation?
As we explained above, for this simple example the readout is not simulated, instead we use the Geant4 scoring interface to extract physical quantities.

Start the simulation by typing `./AlphaTPC`, one is presented with a shell.
To quit, simply enter `exit`.

A type run could be:
```
Idle> /score/create/boxMesh boxMesh_1
Idle> /score/mesh/boxSize 1 1 1 m
Idle> /score/mesh/nBin 10 10 100
Idle> /score/quantity/energyDeposit edep MeV
Idle> /score/close 
Idle> /run/beamOn 100000
Idle> /score/dumpQuantityToFile boxMesh_1 edep edep.csv
Idle> exit
```

This will produce a file, `edep.csv`, with the result of the simulation of 100000 alpha particles.

```
# mesh name: boxMesh_1
# primitive scorer name: edep
# iX, iY, iZ, total(value) [MeV], total(val^2), entry
0,0,0,0,0,0
0,0,1,0,0,0
0,0,2,0,0,0
0,0,3,0,0,0
0,0,4,0,0,0
0,0,5,0,0,0
0,0,6,0,0,0
...
```

Going line-by-line:

 - `/score/create/boxMesh boxMesh_1` creates a new mesh called `boxMesh_1`, you can create multiple mesh with different geometries,
 - `/score/mesh/boxSize 10 10 10 cm` sets the dimensions of the mesh (X,Y and Z directions),
 - `/score/mesh/nBin 10 10 100` sets the granularity of the mesh, i.e. the numbers of bins in the X,Y and Z directions.
 - `/score/quantity/energyDeposit edep MeV` sets the quantity to extract and the units, check the documenting for other quantities, `edep` is a arbitrary name to identify the quantity, you can have more than one attached to a mesh,
- `/score/close` close the configuration of the mesh `boxMesh_1`,
- `/run/beamOn 100000` simulates 100,000 independent particles ,
- `/score/dumpQuantityToFile boxMesh_1 edep edep.csv`, save the `edep` quantity measured on the mesh `boxMesh_1` to the file `edep.csv`,
- `exit` quits the simulation.

You can use any tool to plot and analyze the results, Excel, ROOT, python, etc.

## Typical Result 
![Energy spectrum](alpha_energy_spec.png)

## Documentation

### General Geant4 documention 
- https://geant4.web.cern.ch/geant4/support/userdocuments.shtml

### Scoring 
In the previous example we score the energy deposition, Geant4 can provide more quantities, see the following web pages for more information:
 - https://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/ForApplicationDeveloper/html/ch04s08.html
 - https://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/ForApplicationDeveloper/html/AllResources/Control/UIcommands/_score_.html

## Development notes

You are free to modify the simulation, for instance one can change the type or energy of the particles, the gas mixture, etc.  

### Structure 
The simple simulation is made of three main parts
 
 - Physics: `src/PhysicsList.cc`,
 - Geometry: `src/DetectorConstruction.cc`,
 - Particle generation: `src/PrimaryGeneratorAction.cc`.

Everything is glued together in `main.cc`, the entry point of the simulation.

The particle type and energy are defined in `src/PrimaryGeneratorAction.cc`. The default values are 5.4 MeV alphas.
Similarly, the gas type is defined in `src/DetectorConstruction.cc`. The default value is a 90/10 mixture of Ar/CH4 at STP.

### Materials 
We use the NIST database whenever possible.
The `/material/nist/listMaterials` command to print the standard materials. Some exemples:
 
 - `G4_Si`: Silicon,
 - `G4_SODIUM_IODIDE`: NaI, inorganic scintillator,
 - `G4_PLASTIC_SC_VINYLTOLUENE`: Type plastic typicaly used for scintillators.

### Debuging 
This section is for information only.
One can check for memory leaks with `valgrind`:
```
valgrind --suppressions=/home/g4/root/etc/valgrind-root.supp --leak-check=full ./AlphaTPC
``` 
