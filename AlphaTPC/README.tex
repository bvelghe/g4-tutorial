\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{textcomp}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb} % Lesssim
\usepackage{wrapfig}
\usepackage{multirow}
\usepackage{rotating} % Sideway table
\usepackage{color}
\usepackage{hyperref}
\usepackage{numprint}
\usepackage{breakurl}
\usepackage[normalem]{ulem} % \sout
\usepackage[perpage,symbol]{footmisc} % Better footnotes
\date{\today}
\author{Bob Velghe\thanks{\href{mailto:bob.velghe@triumf.ca}{bob.velghe@triumf.ca}}}
\title{UBC PHYS 531 - Radiation Detectors - Geant4 Simulation of a Small TPC}
\begin{document}
\maketitle	

\section{Introduction}\label{ubc-phys-531---radiation-detectors---geant4-simulation-of-a-small-tpc}

The \emph{Monte Carlo} (MC) simulation of the detector setup is an important tool in experimental high energy physics. 
Typically, the interaction of the particles with the complete detectors geometry and responses are modeled. 
The MC simulation can be used to optimize the detector layout and study the \emph{signal} and \emph{backgrounds} in ideal conditions.

We prepared a virtual machine (VM) to allow you to run a simple simulation of the TPC used in the lab. 
The parameters of the simulation were chosen to match the actual setup. This will allow you to compare your measurements with the output of the simulation.

The VM provides a ready-to-use environment with:
\begin{itemize}
\item
  \href{https://www.ubuntu.com/}{Ubuntu 16.04 LTS}: an easy to use Linux distribution,
\item
  \href{https://geant4.cern.ch/}{Geant4 4.10}: a C++ framework to simulate the interaction of particles with matter,
\item
  \href{https://root.cern.ch/}{ROOT 6.08}: a C++ data analysis toolkit wildly used in high-energy physics.
\end{itemize}

We will simulate a $10 \times 10 \times 10$ cm box filled with HeCO\textsubscript{2} gas. 
By convention, the box is centered at $(0,0,0)$.
A collimated 5.49 MeV alpha source is placed at $\left(0,0,-4~\mathrm{cm}\right)$. 
The particles are emitted in the $+Z$ direction.

This document will guide you through the installation process and gives you a simple tour of the simulation. Finally, some tips are given on how to modify the simulation.

\subsection{How to install the VM?}\label{how-to-install-the-vm}

The image can be downloaded from 

\href{https://files.workspace.ubc.ca/MyDevice/s/570/0f1bb978‐0f54‐4a50‐8e6d‐d66c5ab4c41a}{https://files.workspace.ubc.ca/MyDevice/s/570/0f1bb978‐0f54‐4a50‐8e6d‐d66c5ab4c41a} 

using the password provided by DB.

\begin{itemize}
\item
  First, if it is not installed on your computer, you need to install \href{https://www.virtualbox.org/}{VirtualBox}, it runs on Linux, MacOSX and Windows,
\item
	Import the VM image: \includegraphics[width=0.5\textwidth]{screenshots/import_vm.png}
\item
  Once the VM image is installed, you can start the machine by clicking on the Start button.
After a few moments, you will be presented with a login prompt: \includegraphics[width=0.5\textwidth]{screenshots/login_screen.png}
\end{itemize}

Use the password \texttt{g4} to login in. The username is \texttt{g4}.

\subsection{Geant4?}\label{geant4}

\href{https://geant4.cern.ch/}{Geant4} (G4) is a standard toolkit to simulate the interaction of particles with matter.
Most of the high-energy experiments are using this package to model their setup. 
In a nutshell, the user has to provide the geometry of the detector, the type of particles and their properties (momentum, direction, etc.), and the physics processes to simulate (electromagnetic interactions, etc.).
The package then takes care of propagating the particles inside the geometry in \emph{steps}.
At each step it computes the energy deposited and, possibly, generate new particles (e.g.~hadronic showers, etc.).

The user is responsible for translating the energy deposited in a certain volume into physical variable of interest.
This stage is often called the \emph{digitalization}.

For simple setup, like this one, G4 provides a \emph{scoring} interface which allows to extract some quantities directly from the simulation without the need of writing a digitalization. 
Links to the documentation are given below.

\subsection{How to compile the Geant4 model?}\label{how-to-compile-the-geant4-model}

Before using the model, and after every modification to the source code, we need to compile it. 
The code is stored in the \texttt{\textasciitilde{}/g4tutoriel/AlphaTPC} directory. 
To compile it follows the steps: 
\begin{itemize}
\item 
		Open a terminal:
		\includegraphics[width=0.5\textwidth]{screenshots/start_terminal.png} 
\item Navigate to the \texttt{\textasciitilde{}/g4tutoriel} directory:
	\begin{verbatim}
	cd ~/g4tutoriel
	\end{verbatim}
\item
  Source the \texttt{env.sh} script to set up the Geant4 environment:
	\begin{verbatim}
  source env.sh
	\end{verbatim}
\item
  Create a build directory:
\begin{verbatim}
mkdir build
\end{verbatim}
\item
  Inside the build directory, set up the build using \texttt{cmake}:
\begin{verbatim}
cd build
cmake ../AlphaTPC
\end{verbatim}
\item
  Compile the project:
\begin{verbatim}
make
\end{verbatim}
\end{itemize}

That's it, you should now have a \texttt{AlphaTPC} executable inside the \texttt{build} directory.

\subsection{How to run the simulation?}\label{how-to-run-the-simulation}
\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{screenshots/dedx.pdf}
\caption{Typical energy spectrum for 5.49 MeV alphas in a HeCO\textsubscript{2} mixture. The collimated source is placed at $Z = 1$cm.}
\label{fig:typical_result}
\end{figure}
As we explained above, for this simple example the readout is not simulated, instead we use the Geant4 scoring interface to extract physical quantities. 
We will now explore a simple example.

Start the simulation by typing \texttt{./AlphaTPC}, one is presented with a shell. 
To quit, simply enter \texttt{exit}.

A typical run could be:
\begin{verbatim}
Idle> /score/create/boxMesh boxMesh_1
Idle> /score/mesh/boxSize 5 5 5 cm
Idle> /score/mesh/nBin 1 1 1000
Idle> /score/quantity/energyDeposit edep MeV
Idle> /score/close 
Idle> /run/beamOn 1000
Idle> /score/dumpQuantityToFile boxMesh_1 edep edep.csv
Idle> exit
\end{verbatim}
This will produce a file, \texttt{edep.csv}, with the result of the simulation of 1,000 alpha particles.
Note that the results are averages, the scoring interface doesn't allow to access individual events.
More advanced interfaces (see \texttt{G4VSensitiveDetector} for instance) allow to store detailed information on a particle-by-particle basis.

The file reads
\begin{verbatim} 
# mesh name: boxMesh_1
# primitive scorer name: edep
# iX, iY, iZ, total(value) [MeV], total(val^2), entry
0,0,0,0,0,0
0,0,1,0,0,0
0,0,2,0,0,0
0,0,3,0,0,0
0,0,4,0,0,0
0,0,5,0,0,0
0,0,6,0,0,0
...
\end{verbatim}
The three first columns are the bin indexes. In our example, we have one bin along the $X$ and $Y$ axis and 1,000 bins along the $Z$ axis. 
There is one line per cell.
The next three columns are the sum of the energy deposits, the sum of the energy deposits squared and the number of energy deposits in a particular cell. 
That is, to get the average energy deposit in a cell one has to divide the \emph{total} column by the \emph{entry} column. 

Now, going through the commands line-by-line:
\begin{itemize}
\item
  \texttt{/score/create/boxMesh\ boxMesh\_1}, creates a new mesh called
  \texttt{boxMesh\_1}, you can create multiple mesh with different
  geometries,
\item
  \texttt{/score/mesh/boxSize\ 5\ 5\ 5\ cm}, sets the \textbf{half}-dimensions\footnote{That is, this will create a $10 \times 10 \times 10$ cm mesh.} of the mesh (X,Y and Z directions), 
\item
  \texttt{/score/mesh/nBin\ 1\ 1\ 1000}, sets the granularity of the
  mesh, i.e.~the numbers of bins in the X,Y and Z directions.
\item
  \texttt{/score/quantity/energyDeposit\ edep\ MeV}, sets the quantity to
  extract and the units, check the documenting for other quantities,
  \texttt{edep} is a arbitrary name to identify the quantity, you can
  have more than one attached to a mesh,
\item
  \texttt{/score/close}, close the configuration of the mesh
  \texttt{boxMesh\_1},
\item
  \texttt{/run/beamOn\ 1000}, simulates 1,000 independent particles,
\item
  \texttt{/score/dumpQuantityToFile\ boxMesh\_1\ edep\ edep.csv}, save
  the \texttt{edep} quantity measured on the mesh \texttt{boxMesh\_1} to
  the file \texttt{edep.csv},
\item
  \texttt{exit}, quits the simulation.
\end{itemize}

You can use any tool to plot and analyze the results, Excel, ROOT, python, etc. 
We provide a ROOT macro (see \texttt{bragg\_peak.C} in the \texttt{AlphaTPC} directory) that will produce the Figure \ref{fig:typical_result}.
Guidance on how to run the macro is provided in the file.

The commands can be grouped in a file, which is convenient when running an example multiple time, see for instance \texttt{run.mac} in the \texttt{AlphaTPC} directory.

\subsection{Where to Find Documentation}\label{documentation}
The \texttt{Geant4} document can be found online:
\begin{itemize}
  \item
    General Geant4 Documention
    
    \url{https://geant4.web.cern.ch/geant4/support/userdocuments.shtml}
	\item	
		Scoring overview
		
		\url{https://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/ForApplicationDeveloper/html/ch04s08.html}
	\item
		Scoring commands
		
		\url{https://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/ForApplicationDeveloper/html/AllResources/Control/UIcommands/_score_.html}
\end{itemize}

Similarly, see the \href{https://root.cern.ch/}{https://root.cern.ch/} for the ROOT documentation.

\subsection{Development notes}\label{development-notes}
You are free to modify the simulation, for instance one can change the type or energy of the particles, the gas mixture, etc.
The simulation is made of three main parts:
\begin{itemize}
\item
  the physics definition: \texttt{src/PhysicsList.cc},
\item
  the geometry and material definition: \texttt{src/DetectorConstruction.cc},
\item
  the particle generation: \texttt{src/PrimaryGeneratorAction.cc}.
\end{itemize}
Everything is glued together in \texttt{main.cc}, the entry point of the simulation.
Do not hesitate to look at the source code, we tried to comment the important parts.
 
After every modification one has to compile the code, see Section \ref{how-to-compile-the-geant4-model} for details.
 
%\subsubsection{Materials}\label{materials}
%
%We use the NIST database whenever possible. The \texttt{/material/nist/listMaterials} command to print the standard
%materials. 
%Some examples:
%\begin{itemize}
%\item
%  \texttt{G4\_Si}: Silicon,
%\item
%  \texttt{G4\_SODIUM\_IODIDE}: NaI, inorganic scintillator,
%\item
%  \texttt{G4\_PLASTIC\_SC\_VINYLTOLUENE}: Type plastic typically used for
%  scintillators.
%\end{itemize}

%\subsubsection{Debugging}\label{debuging}
%This section is for information only. One can check for memory leaks
%with \texttt{valgrind}:
%\begin{verbatim}
%valgrind --suppressions=/home/g4/root/etc/valgrind-root.supp --leak-check=full ./AlphaTPC
%\end{verbatim}
\end{document}
