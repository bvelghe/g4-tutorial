#include "DetectorConstruction.hh"

DetectorConstruction::DetectorConstruction() {
	/////////////////////
	// Build materials //
	/////////////////////
	fNistMan = G4NistManager::Instance();
	fNistMan->FindOrBuildMaterial("G4_AIR");
}

DetectorConstruction::~DetectorConstruction() {
}

G4VPhysicalVolume* DetectorConstruction::Construct() {
	// Our setup is very simple, will will simply use the world volume.
	// In more realistic example, one define a big "world" volume and define the geometry using sub-volumes placed inside.
	//
	// It is often best to define sub-componant in separate classes and create instences as needed.
	// For instance, lets supose that we need to model four wire chambers and a big sodium iodide crystal.
	// In that case, one could create WireChamber and NaICrystal classes describing the detailed geometry of the objects.
	// in the DetectorConstruction class, one would simply create four instance of the WireChamber class and 
	// one instance of the NaICrystal, i.e.
	//
	// WireChamber * fWireChamber[4];
	// NaICrystal * fNaICrystal;
	// fWireChamber[0] = new WireChamber(position_0,0);
	// fWireChamber[1] = new WireChamber(position_1,1);
	// etc.
	
	/////////////////////////////
	// Define the world volume //
	/////////////////////////////
	fworldSolidVolume = new G4Box("World",1.0*m,1.0*m,1.0*m);

	fworldLogicalVolume = new G4LogicalVolume(fworldSolidVolume,
			G4Material::GetMaterial("G4_AIR"),
			"World",
			0,
			0,
			0);

	fworldPhysicalVolume = new G4PVPlacement(0,
			G4ThreeVector(0.,0.,0.),
			fworldLogicalVolume,
			"World",
			0,
			false
			,0);  

	return fworldPhysicalVolume;                        
}

void DetectorConstruction::SetProperties() {	
	// Set the world volume invisible
	fworldLogicalVolume->SetVisAttributes(G4VisAttributes::Invisible);
}
