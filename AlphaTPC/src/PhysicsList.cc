#include "PhysicsList.hh"


PhysicsList::PhysicsList() {

	// EM physics
	RegisterPhysics(new G4EmStandardPhysics());
}
PhysicsList::~PhysicsList() {}

/*
   void PhysicsList::ConstructParticle() {
// In this method, static member functions should be called
// for all particles which you want to use.
// This ensures that objects of these particle types will be
// created in the program. 
// G4Geantino::GeantinoDefinition();
//G4Proton::ProtonDefinition();


}
 */
/*
   void PhysicsList::ConstructProcess() {
// Transports particles through the detector geometry
AddTransportation();

////////////
// Proton //
////////////    
G4ParticleTable * particleTable = G4ParticleTable::GetParticleTable();
G4cout << particleTable << G4endl;
G4ParticleDefinition * proton = particleTable->FindParticle("proton");
G4ProcessManager * processManager = proton->GetProcessManager();
//processManager->AddProcess(new G4MultipleScattering(), -1, 1, 1);
processManager->AddProcess(new G4hIonisation(),-1, 2, 2);
}
 */
void PhysicsList::SetCuts() {
	//  SetCutsWithDefault();
	G4VUserPhysicsList::SetCuts();
}
