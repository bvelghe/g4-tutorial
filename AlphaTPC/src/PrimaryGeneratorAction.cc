#include "PrimaryGeneratorAction.hh"

//////////////////////////////
//	Génération du faisceau	//
//////////////////////////////

PrimaryGeneratorAction::PrimaryGeneratorAction() {
	fSingleParticleSource = new G4SingleParticleSource();
	fSingleParticleSource->SetParticleDefinition(G4Gamma::Definition());
	fSingleParticleSource->GetEneDist()->SetMonoEnergy(511.0*keV);
	fSingleParticleSource->GetAngDist()->SetParticleMomentumDirection(G4ThreeVector(0.0,0.0,1.0));
	fSingleParticleSource->GetPosDist()->SetCentreCoords(G4ThreeVector(0.0,0.0,-99.5*cm)); //FIXME: See DetectorConstruction.cc
}

PrimaryGeneratorAction::~PrimaryGeneratorAction() {
    delete fSingleParticleSource;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
    fSingleParticleSource->GeneratePrimaryVertex(anEvent);
}

