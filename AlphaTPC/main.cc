//#include "G4UIQt.hh"


#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "PrimaryGeneratorAction.hh"

#include "G4UImanager.hh"
#include "G4UIExecutive.hh"
#include "G4VisExecutive.hh"
#include "G4RunManager.hh"
#include "G4ScoringManager.hh"  

int main(int argc, char * argv[]) 
{

	G4RunManager* runManager = new G4RunManager();

	DetectorConstruction * detector = new DetectorConstruction();
	runManager->SetUserInitialization(detector);	

	G4VUserPhysicsList* physics = new PhysicsList();
	runManager->SetUserInitialization(physics);

	G4VUserPrimaryGeneratorAction* generator_action = new PrimaryGeneratorAction();
	runManager->SetUserAction(generator_action);


	//////////////////////////
	// Start the event loop //
	//////////////////////////

	runManager->Initialize();

	//////////////////////////////////////
	// User interface and visualization // 	
	//////////////////////////////////////

	G4VisManager* visManager = new G4VisExecutive;
	visManager->Initialize();

	// Get the pointer to the User Interface manager
	G4UImanager * UImanager = G4UImanager::GetUIpointer(); 
	G4UIExecutive * ui = 0;
	if (argc!=1)   // batch mode  
	{
		G4String command = "/control/execute ";
		G4String fileName = argv[1];
		UImanager->ApplyCommand(command+fileName);
	}
	else           // interactive mode : define UI session  
	{
		ui = new G4UIExecutive(argc,argv);
		ui->SessionStart();
	}
	////////////////////
	// Clean and exit //
	////////////////////

	if(ui != 0) delete ui;	

	delete visManager;
	delete runManager; // Also delete the user intializations/actions
	
	return 0;
}
