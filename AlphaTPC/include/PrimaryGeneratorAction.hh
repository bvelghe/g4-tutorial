#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4GeneralParticleSource.hh"
#include "G4SystemOfUnits.hh"
#include "G4Gamma.hh"

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    PrimaryGeneratorAction();
    ~PrimaryGeneratorAction();
    //////////////
    // Required //
    //////////////
    void GeneratePrimaries(G4Event* anEvent);
  private:
     G4SingleParticleSource * fSingleParticleSource;
};

#endif // PrimaryGeneratorAction_h 
