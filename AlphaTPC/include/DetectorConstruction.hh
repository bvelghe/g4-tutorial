#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"

#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh" 

#include "G4SystemOfUnits.hh" 

#include "G4VisAttributes.hh"

class DetectorConstruction : public G4VUserDetectorConstruction 
{
	public:
		DetectorConstruction();
		~DetectorConstruction();
		void SetProperties();
		G4VPhysicalVolume* Construct();

	private:
		G4NistManager * fNistMan;

		G4Box * fworldSolidVolume;
		G4LogicalVolume * fworldLogicalVolume;
		G4VPhysicalVolume * fworldPhysicalVolume;

};

#endif // DetectorConstruction_h
