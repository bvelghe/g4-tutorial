// This is a ROOT macro file
// Usage: $ root -l 'bragg_peak.C("INPUT_FILE")'  
// (-l -> do not show splash screen)

// # iX, iY, iZ, total(value) [MeV], total(val^2), entry

int bragg_peak(const char * input_file) {
	ifstream fh;
	fh.open(input_file);
	if(!fh.good()) {
		std::cout << "Unable to open " << input_file << std::endl; 
		return -1;
	}

	
	// Some logic to skip comment lines
	// !Fragile! 
	char token;
	do {
		fh.read(&token,1);
		fh.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	} while(token == '#' && fh.good());

	char delem; // Take care of the comma
	int iX, iY, iZ, entry;
	double total, total_sq;
	
	std::vector<float> v_zpos; //cm
	std::vector<float> v_avg_edep;
	std::vector<float> v_var_edep; 

	while(fh >> iX >> delem >> iY >> delem >> iZ >> delem >> total >> delem >> total_sq >> delem >> entry) {
		if(entry == 0) continue; // Skip if no entries.
		// ! Depends on the mesh definition !
		// The Z bin size is 0.01 cm
		// Units are (MeV / 0.01 cm), multiply by 100 to obtain MeV/cm 

		v_zpos.push_back(iZ*1e-2); // iZ is the bin number, multiple by 0.01 to get cm (!depends on mesh definition!)
		
		double avg_edep = total/entry; // Average energy dep. in the bin iZ
		double var_edep = total_sq/entry - (total/entry)*(total/entry);

		v_avg_edep.push_back(avg_edep*1e2); 
		
		// St. dev of the mean ... 
		v_var_edep.push_back(sqrt(var_edep)*1e2/sqrt(entry)); 
	}
	
	TGraphErrors * g_edep_z = new TGraphErrors(v_zpos.size(),&v_zpos[0],&v_avg_edep[0],0,&v_var_edep[0]);  
	TCanvas * c1 = new TCanvas("c1","Energy deposition");
	// The graph will be drawn in the current canvas [c1] (hidden state)
	g_edep_z->SetTitle(";Z (cm); <dE/dx> (MeV/cm)"); 
	g_edep_z->Draw("AP"); 

	// Do not delete the object instances (ROOT interactive session)
	//delete c1;
	//delete g_edep_z;	
	
	fh.close();
	return 0;
}
