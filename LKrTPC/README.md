## Visualisation
![Setup Geometry](setup.png)

To obtain the image above, type to following commands:
    
    Idle> /vis/scene/create
    Idle> /vis/open OGL
    Idle> /vis/scene/add/trajectories
    Idle> /run/beamOn 1
    Idle> exit

## Materials 
We are using the NIST database whenever possible.
See `/material/nist/listMaterials` 
 
 - `G4_Si`
 - `G4_SODIUM_IODIDE`
 - `G4_PLASTIC_SC_VINYLTOLUENE` 

valgrind --suppressions=/home/g4/root/etc/valgrind-root.supp --leak-check=full ./LKrTPC test.mac

## General Notes 
 - The geometry parameters are declared in each detector `Construct()` method. In the future, it should be moved to a central repositrory. We prefix the variables with a `f` to indicate that, ideally, the variables should be dynamically filled (and therefore *maybe* promoted as class members).  

## TODO
 - Fix the CMakefile.txt in the RootIO directory, clarify the need for the ROOT dictionaries
 - Sum of the TPCHit? In TPCRootIO or in TPCSD class?
 - Check to hit collection ID at each event? Try to avoid that!
 - What to do when no hit in an event? --> TClonesArray of hits and not just the energy sum for the event?
