#include "UserTrackInformation.hh"

G4ThreadLocal G4Allocator<UserTrackInformation> * aUserTrackInformationAllocator = 0;

UserTrackInformation::UserTrackInformation() : G4VUserTrackInformation() {
	fComptonScatteringFlag = false;
	fPhotoElectricFlag = false;
}

UserTrackInformation::UserTrackInformation(const UserTrackInformation *aTrackInfo) : G4VUserTrackInformation() {
	fComptonScatteringFlag = aTrackInfo->fComptonScatteringFlag;
	fPhotoElectricFlag = aTrackInfo->fPhotoElectricFlag;
}


UserTrackInformation::~UserTrackInformation() {
}

UserTrackInformation & UserTrackInformation::operator=(const UserTrackInformation & aTrackInfo) {
	fComptonScatteringFlag = aTrackInfo.fComptonScatteringFlag;
	fPhotoElectricFlag = aTrackInfo.fPhotoElectricFlag;
	return *this;
}
