#include "RootIO.hh"

RootIO::RootIO() : m_out_file(0) {
	m_header_io = new HeaderRootIO();
	m_crystal_io = new CrystalRootIO();
	m_lkr_active_volume_io = new LKrActiveVolumeRootIO();
	m_sipm_io = new SiPMRootIO();
}

bool RootIO::Open(std::string output_file) {
	m_out_file = new TFile(output_file.c_str(),"RECREATE");
	if(m_out_file == 0) return false; // Should never happen
	if(m_out_file->IsZombie()) {
		std::cout << "Unable to open the file " << output_file << " for writing" << std::endl;
		return false;
	}
	return true;
}

bool RootIO::StartRun(long run_id,std::string output_file) {
	if(Open(output_file) != true) return false; // Unable to start a run	
	// Create EventHeader tree
	m_header_tree = new TTree("Header","Event Metadata Tree");
	
	// Create sub-detectors tree
	// FIXME: Bad arch. --> Move this to [Detector]RootIO
	m_crystal_tree = new TTree("NaI_Crystal","NaI Crystal Event Tree"); 
	m_lkr_active_volume_tree = new TTree("LKr_Active_Volume","LKr Active Volume Event Tree");
	m_sipm_tree = new TTree("SiPMs","SiPMs Event Tree");
	m_header_io->Initialize(m_header_tree);
	m_crystal_io->Initialize(m_crystal_tree);
	m_lkr_active_volume_io->Initialize(m_lkr_active_volume_tree);
	m_sipm_io->Initialize(m_sipm_tree);

	return true;
}

void RootIO::EndRun() {
	Close();
}

void RootIO::SaveEvent(const G4Event * event) {
    m_header_io->SaveEvent(event);
	m_crystal_io->SaveEvent(event);
	m_lkr_active_volume_io->SaveEvent(event);
	m_sipm_io->SaveEvent(event);
}

void RootIO::Close() {
	if(m_out_file == 0) return; // INOP
	if(m_out_file->IsOpen()) {
		m_out_file->Write();
		m_out_file->Close(); 	
	}
	delete m_out_file;
	m_out_file = 0;
}

RootIO::~RootIO() {
	Close();
}

