#include "SiPM.hh"
#include "G4NistManager.hh" 

SiPM::SiPM(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, Orientation orientation, G4int copy) :
    G4VUserDetectorConstruction(),
	fMotherLogicalVolume(motherLogicalVolume), 
	fCopy(copy),
	fPosition(position), 
	fOrientation(orientation)
{


}

SiPM::~SiPM() {
}

G4VPhysicalVolume* SiPM::Construct() {
	std::ostringstream oss;
	oss << fCopy;

	const G4int N = 60;

	G4double PhotonEnergy[N] = {6.81*eV,6.90*eV,7.03*eV,7.14*eV,7.26*eV,7.41*eV,7.50*eV,7.61*eV,7.67*eV,7.71*eV,7.77*eV,7.79*eV,7.82*eV,7.86*eV,7.90*eV,7.92*eV,7.94*eV,7.97*eV,8.00*eV,8.02*eV,8.05*eV,8.06*eV,8.10*eV,8.11*eV,8.15*eV,8.18*eV,8.21*eV,8.24*eV,8.27*eV,8.30*eV,8.35*eV,8.42*eV,8.45*eV,8.53*eV,8.54*eV,8.57*eV,8.60*eV,8.62*eV,8.66*eV,8.68*eV,8.70*eV,8.73*eV,8.76*eV,8.80*eV,8.81*eV,8.85*eV,8.87*eV,8.90*eV,8.92*eV,8.96*eV,9.00*eV,9.01*eV,9.08*eV,9.12*eV,9.23*eV,9.32*eV,9.41*eV,9.54*eV,9.66*eV,9.84*eV};

	G4double Rindex[N] = {1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32};


	G4MaterialPropertiesTable * SiPM_Mat_Prop = new G4MaterialPropertiesTable();
	SiPM_Mat_Prop->AddProperty("RINDEX",PhotonEnergy,Rindex,N);

	G4NistManager * NistMan = G4NistManager::Instance();
	G4Material * SiPM_Mat = NistMan->FindOrBuildMaterial("G4_SILICON_DIOXIDE");
	SiPM_Mat->SetMaterialPropertiesTable(SiPM_Mat_Prop);

	// FIXME: [IMPROVEMENT] move the declaration to the header file and user a GeometryParameter(s) class.
	G4double fModuleXLength = 1.5*cm;
	G4double fModuleYLength = 1.5*cm;
	G4double fModuleZLength = 2.0*mm;

	
	G4String volume_name = "SiPM_Module_"+oss.str();	
	fSiPMSolidVolume = new G4Box(volume_name,0.5*fModuleXLength,0.5*fModuleYLength,0.5*fModuleZLength);
	
	fSiPMLogicalVolume = new G4LogicalVolume(fSiPMSolidVolume,
		G4Material::GetMaterial("G4_SILICON_DIOXIDE"),
		volume_name,
		0, // G4FieldManager * (def. 0)
		0, // G4VSensitiveDetector * (def. 0)
		0, // G4UserLimits * (def. 0)
		true); // optimise (def. true)

	// Looking in the beam direction
	G4RotationMatrix * rot_mat = new G4RotationMatrix();
	switch(fOrientation) {
		case Orientation::kLeft:
			rot_mat->rotateY(90.0*deg);
			break;
		case Orientation::kRight:
			rot_mat->rotateY(270.0*deg);
			break;
		case Orientation::kTop:
			rot_mat->rotateX(90.0*deg);
			break;
		case Orientation::kBottom:
			rot_mat->rotateX(270.0*deg);
			break;
		default:
			break; //INOP
	}


	fSiPMPhysicalVolume = new G4PVPlacement(rot_mat,
		fPosition,
		fSiPMLogicalVolume,
		volume_name,
		fMotherLogicalVolume,
		false
		,0); 

	// Use fCopy identify the SiPM
	fSiPMPhysicalVolume->SetCopyNo(fCopy);



	
	return fSiPMPhysicalVolume;
}

void SiPM::ConstructSDandField() {
    std::ostringstream oss;
    oss << fCopy;

    // Setup the sensitive volume
    G4String sensor_SDname = "/SiPM_"+oss.str();

    SiPMSD * SiPM_SD = new SiPMSD(sensor_SDname,fCopy);
    G4SDManager::GetSDMpointer()->AddNewDetector(SiPM_SD);
    SetSensitiveDetector(fSiPMLogicalVolume->GetName(), SiPM_SD);

}


void SiPM::SetProperties() {
	fSiPMVisAttributes = new G4VisAttributes(G4Colour(1.0,1.0,0.0));
	fSiPMLogicalVolume->SetVisAttributes(fSiPMVisAttributes);
}
