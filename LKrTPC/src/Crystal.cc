#include "Crystal.hh"

Crystal::Crystal(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy) :
    G4VUserDetectorConstruction(),
	fMotherLogicalVolume(motherLogicalVolume), 
	fPosition(position), 
	fCopy(copy) {
	}

Crystal::~Crystal() {
}

G4VPhysicalVolume* Crystal::Construct() {
	std::ostringstream oss;
	oss << fCopy;
	

	// FIXME: [IMPROVEMENT] move the declaration to the header file and user a GeometryParameter(s) class.
	G4double fCrystalRadius = 5.4*cm;
	G4double fCrystalZLength = 10.0*cm;

	G4String volume_name = "Crystal_"+oss.str();	
	fCrystalSolidVolume = new G4Tubs(volume_name,0.0,fCrystalRadius,0.5*fCrystalZLength,0,2*CLHEP::pi*radian);
	
	fCrystalLogicalVolume = new G4LogicalVolume(fCrystalSolidVolume,
		G4Material::GetMaterial("G4_SODIUM_IODIDE"),
		volume_name,
		0, // G4FieldManager * (def. 0)
		0, // G4VSensitiveDetector * (def. 0)
		0, // G4UserLimits * (def. 0)
		true); // optimise (def. true)
	
	fCrystalPhysicalVolume = new G4PVPlacement(0,
		fPosition,
		fCrystalLogicalVolume,
		volume_name,
		fMotherLogicalVolume,
		false
		,0); 




	return fCrystalPhysicalVolume;
}

void Crystal::ConstructSDandField() {
    G4cout << "Crystal::ConstructSDandField() invoked\n";
    G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
    // Setup the sensitive volume
    std::ostringstream oss;
    oss << fCopy;
    G4String sensor_SDname = "/NaI_"+oss.str();

    CrystalSD * CR_SD = new CrystalSD(sensor_SDname,fCopy);
    G4SDManager::GetSDMpointer()->AddNewDetector(CR_SD);
    SetSensitiveDetector(fCrystalLogicalVolume->GetName(), CR_SD);

}

void Crystal::SetProperties() {
	fCrystalVisAttributes = new G4VisAttributes(G4Colour(0.0,0.0,1.0));
	fCrystalLogicalVolume->SetVisAttributes(fCrystalVisAttributes);
}
