#include "UserActionInitialization.hh"
#include "TROOT.h"

UserActionInitialization::UserActionInitialization() : G4VUserActionInitialization() {
    ROOT::EnableThreadSafety();
}

UserActionInitialization::~UserActionInitialization() {

}

void UserActionInitialization::Build() const {

    // Instances are deleted automatically when G4RunManager is deleted.
    auto * generator_action = new PrimaryGeneratorAction();
    SetUserAction(generator_action);

    // Both here and in BuildForMaster, I don't like this design.
    auto * run_action = new UserRunAction();
    SetUserAction(run_action);

    auto * event_action = new UserEventAction();
    SetUserAction(event_action);

    auto * tracking_action = new UserTrackingAction();
    SetUserAction(tracking_action);

    auto * stepping_action = new LKrUserSteppingAction();
    SetUserAction(stepping_action);


}

void UserActionInitialization::BuildForMaster() const {
    auto * run_action = new UserRunAction();
        SetUserAction(run_action);
    }