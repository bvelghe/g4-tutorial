#include "UserEventAction.hh"


//FIXME : iCopy ?

// Note: G4Track and G4Step are transient classes; they are not available at the end of the event.
// G4VTrajectory and G4VTrajectoryPoint are the only ones an user may employ of end-of-event analysis or for persistency.

UserEventAction::UserEventAction() : G4UserEventAction(), m_hits_collection_ID(-1) {
	m_SD_manager = G4SDManager::GetSDMpointer();
}

UserEventAction::~UserEventAction() {;}

void UserEventAction::BeginOfEventAction(const G4Event* event) {

	/////////////////////////////
	// Display run progression //
	/////////////////////////////	
	G4int eventID = event->GetEventID();
	if(eventID%100 == 0) {
		G4cout << "# > Event " << event->GetEventID() << G4endl;
	}
}


void UserEventAction::EndOfEventAction(const G4Event* event) {
	
	///////////
	// Debug //
	///////////	
	//G4cout << " ---- END ---- " << G4endl; 
	/* --> Move to RootIO
	if(m_hits_collection_ID < 0) m_hits_collection_ID = m_SD_manager->GetCollectionID("LKrActiveVolume_HitsCollection_0");
	if(m_hits_collection_ID < 0) return;


	double energy_sum = 0.0;
	G4ThreeVector pos_avg = G4ThreeVector(0.0,0.0,0.0); // We average the E_dep position in ..SD.cc
	G4HCofThisEvent* HCTE = event->GetHCofThisEvent();
	if(HCTE) {
        	m_hits_collection = (LKrActiveVolumeG4HitsCollection*)(HCTE->GetHC(m_hits_collection_ID));
		G4int NbHits = m_hits_collection->entries();
		for(int idx = 0; idx < NbHits; idx++) {
			energy_sum += (*m_hits_collection)[idx]->Energy()/MeV;
			pos_avg += (*m_hits_collection)[idx]->Position()/mm;
		}
	       
		//G4cout << "*** UEA ***" << pos_avg << " " << NbHits << G4endl;
	}	
	*/
	//////////////////
	// End of Debug //
	//////////////////
	
	
	
	
	
	
	
	
	
	if(false) {	
	G4TrajectoryContainer * trajectoryContainer = event->GetTrajectoryContainer();
	
	G4int n_traj = 0;
	if(trajectoryContainer) n_traj = trajectoryContainer->entries();
	//if(G4VVisManager::GetConcreteInstance()) {
		for(G4int i = 0 ; i < n_traj ; i++) {

			UserTrajectory * traj = (UserTrajectory*)((*trajectoryContainer)[i]);
			G4cout << "Trajectory # " << i << " [" << traj->GetPointEntries() << "]" << G4endl;
			G4cout << traj->GetParticleName() << G4endl;
			G4cout << traj->GetParentID() << G4endl;
			// Deleted?
			//for(G4int j = 0; traj->GetPointEntries(); j++) {
			//	UserTrajectoryPoint * traj_pt = (UserTrajectoryPoint*)traj->GetPoint(j);
			//}
		}
	//}
	}


}
