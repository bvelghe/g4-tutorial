#include "SiPMRootIO.hh" 

SiPMRootIO::SiPMRootIO() : m_ready(false), m_tree(0), m_event(0), m_branch(0) {
	for(int icopy = 0; icopy < c_n_sipm; icopy++) {
		m_hits_collection_ID[icopy] = -1;
		m_hits_collection[icopy] = 0;
	}
	m_SD_manager = G4SDManager::GetSDMpointer();

}

bool SiPMRootIO::Initialize(TTree * tree) {
    m_tree = tree;
    m_event = new SiPMEvent();
    m_branch = m_tree->Branch("Hits",&m_event);
    m_ready = true;

    return true;
}

void SiPMRootIO::SaveEvent(const G4Event * event) {
	if(m_ready == false) return;
	m_event->Reset(); // This is important, clean up the vector

	// FIXME Clean this up ! Do I need to check the m_SD_manager->GetCollectionID at each event?
	for(int icopy = 0; icopy < c_n_sipm; icopy++) {
		std::ostringstream oss;
		oss << icopy;
		if(m_hits_collection_ID[icopy] < 0) m_hits_collection_ID[icopy] = m_SD_manager->GetCollectionID("SiPM_HitsCollection_"+oss.str());
		if(m_hits_collection_ID[icopy] < 0) return;

		G4HCofThisEvent* HCTE = event->GetHCofThisEvent();
		if(HCTE) {
			m_hits_collection[icopy] = (SiPMG4HitsCollection*)(HCTE->GetHC(m_hits_collection_ID[icopy]));
			G4int NbHits = m_hits_collection[icopy]->entries();
			if(NbHits <= 0) continue;
			for(int idx = 0; idx < NbHits; idx++) {
				G4double wavelength = (*m_hits_collection[icopy])[idx]->WaveLength();
				// Digitalization goes here
				SiPMHit hit;
				hit.SetStationID(icopy);
				//hit.SetWaveLength(energy_sum + energy_smearing);
				hit.SetTime(0.0);
				m_event->AddHit(hit);	


			}
		}	
	}
	
	// Store this event
	m_tree->Fill();
}


SiPMRootIO::~SiPMRootIO() {
	if(m_event != 0) {
		delete m_event;
		m_event = 0;
	}
}
