#include "CrystalRootIO.hh" 

CrystalRootIO::CrystalRootIO() : m_ready(false), m_tree(0), m_event(0), m_branch(0) {
	m_hits_collection_ID = -1;
	m_hits_collection = 0;
	m_SD_manager = G4SDManager::GetSDMpointer();

}

bool CrystalRootIO::Initialize(TTree * tree) {
	m_tree = tree;
	m_event = new CrystalEvent();
	m_branch = m_tree->Branch("Hits",&m_event);
	m_ready = true;
	

	return true;
}

void CrystalRootIO::SaveEvent(const G4Event * event) {
	if(m_ready == false) return;
	// Digitalization goes here

	// FIXME Clean this up ! Do I need to check the m_SD_manager->GetCollectionID at each event?
	if(m_hits_collection_ID < 0) m_hits_collection_ID = m_SD_manager->GetCollectionID("Crystal_HitsCollection_0");
	if(m_hits_collection_ID < 0) return;

	double energy_sum = 0.0;	
	G4HCofThisEvent* HCTE = event->GetHCofThisEvent();
	if(HCTE) {
        	m_hits_collection = (CrystalG4HitsCollection*)(HCTE->GetHC(m_hits_collection_ID));
		G4int NbHits = m_hits_collection->entries();
    		m_event->SetNHits(NbHits);
		for(int idx = 0; idx < NbHits; idx++) {
			energy_sum += (*m_hits_collection)[idx]->Energy()/MeV;
		} 
	}	
	

	m_event->SetEnergy(energy_sum);
	m_event->SetTime(0.0);
	
	// Store this event
	m_tree->Fill();
}


CrystalRootIO::~CrystalRootIO() {
	if(m_event != 0) {
		delete m_event;
		m_event = 0;
	}
}
