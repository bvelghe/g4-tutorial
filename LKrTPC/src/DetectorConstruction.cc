#include "DetectorConstruction.hh"

DetectorConstruction::DetectorConstruction() : G4VUserDetectorConstruction() {
	/////////////////////
	// Build materials //
	/////////////////////
	fNistMan = G4NistManager::Instance();
	fNistMan->FindOrBuildMaterial("G4_Galactic");
	fNistMan->FindOrBuildMaterial("G4_AIR");
	fNistMan->FindOrBuildMaterial("G4_STAINLESS-STEEL"); // (density = 8 g/cm^3)
	fNistMan->FindOrBuildMaterial("G4_SODIUM_IODIDE");
	fNistMan->FindOrBuildMaterial("G4_lKr");
	fNistMan->FindOrBuildMaterial("G4_SILICON_DIOXIDE"); // For SiPM, find better 
}

DetectorConstruction::~DetectorConstruction() {
}



G4VPhysicalVolume* DetectorConstruction::Construct() {
	/////////////////////////////
	// Define the world volume //
	/////////////////////////////
	fWorldSolidVolume = new G4Box("World",1.0*m,1.0*m,1.0*m);

	fWorldLogicalVolume = new G4LogicalVolume(fWorldSolidVolume,
			G4Material::GetMaterial("G4_AIR"),
			"World",
			0,
			0,
			0);

	fWorldPhysicalVolume = new G4PVPlacement(0,
			G4ThreeVector(0.,0.,0.),
			fWorldLogicalVolume,
			"World",
			0,
			false
			,0);  

	///////////////////////////
	// Instantiate the setup //
	///////////////////////////


	// Note: Variables are f-prefixed anyway since they should eventually be loaded from a central source (and promoted as class members).
	// All the pos. refer to the *center* of the volume, origin is at -Z pos. of the world volume (see globalZOffset)

	G4double fExternalWindowThickness = 0.25*mm;
	G4double fInsulationVacuumThickness = 2.0*cm;
	G4double fInternalWindowThickness = 0.25*mm;
	G4double fPassiveLKrThickness = 3.0*cm;

	// Source is at Z=0m
	G4double fNaICrystalZPos = -24.0*cm;
	G4double fExternalWindowZRefPos = +0.25*cm; // Position of the external face

	G4double external_window_zpos = fExternalWindowZRefPos+0.5*fExternalWindowThickness;
	G4double insulation_zpos = external_window_zpos+0.5*(fExternalWindowThickness+fInsulationVacuumThickness);
	G4double internal_window_zpos = insulation_zpos+0.5*(fInsulationVacuumThickness+fInternalWindowThickness);
	G4double passive_lkr_zpos = internal_window_zpos+0.5*(fInternalWindowThickness+fPassiveLKrThickness);
	G4double active_lkr_zpos = passive_lkr_zpos+0.5*(fPassiveLKrThickness+4.0*cm); // Note: 4 cm hidden in LkrActiveVolume class, would be solved by an central geometry def. class.


	// Passive volume -- Too lazy to move them into classes 
	fExternalWindowSolidVolume = new G4Box("External_Window",2.5*cm,2.5*cm,0.5*fExternalWindowThickness);

	fExternalWindowLogicalVolume = new G4LogicalVolume(fExternalWindowSolidVolume,
			G4Material::GetMaterial("G4_STAINLESS-STEEL"),
			"External_Window",
			0,
			0,
			0);
	
	fExternalWindowPhysicalVolume = new G4PVPlacement(0,
			G4ThreeVector(0.,0.,external_window_zpos),
			fExternalWindowLogicalVolume,
			"External_Window",
			fWorldLogicalVolume,
			false
			,0);  



	fInsulationVacuumSolidVolume = new G4Box("Insulation_Vacuum",2.5*cm,2.5*cm,0.5*fInsulationVacuumThickness);

	fInsulationVacuumLogicalVolume = new G4LogicalVolume(fInsulationVacuumSolidVolume,
			G4Material::GetMaterial("G4_Galactic"),
			"Insulation_Vacuum",
			0,
			0,
			0);

	fInsulationVacuumPhysicalVolume = new G4PVPlacement(0,
			G4ThreeVector(0.,0.,insulation_zpos),
			fInsulationVacuumLogicalVolume,
			"Insulation_Vacuum",
			fWorldLogicalVolume,
			false
			,0);  


	fInternalWindowSolidVolume = new G4Box("Internal_Window",2.5*cm,2.5*cm,0.5*fInternalWindowThickness);

	fInternalWindowLogicalVolume = new G4LogicalVolume(fInternalWindowSolidVolume,
			G4Material::GetMaterial("G4_STAINLESS-STEEL"),
			"Internal_Window",
			0,
			0,
			0);

	fInternalWindowPhysicalVolume = new G4PVPlacement(0,
			G4ThreeVector(0.,0.,internal_window_zpos),
			fInternalWindowLogicalVolume,
			"Internal_Window",
			fWorldLogicalVolume,
			false
			,0);  


	fPassiveLKrSolidVolume = new G4Box("Passive_LKr",2.5*cm,2.5*cm,0.5*fPassiveLKrThickness);

	fPassiveLKrLogicalVolume = new G4LogicalVolume(fPassiveLKrSolidVolume,
			G4Material::GetMaterial("G4_lKr"),
			"Passive_LKr",
			0,
			0,
			0);


	fPassiveLKrPhysicalVolume = new G4PVPlacement(0,
			G4ThreeVector(0.,0.,passive_lkr_zpos),
			fPassiveLKrLogicalVolume,
			"Passive_LKr",
			fWorldLogicalVolume,
			false
			,0);  


	// FIXME: ... Add the cathode here ...	

	
	fNaICrystal = new Crystal(fWorldLogicalVolume,G4ThreeVector(0.0,0.0,fNaICrystalZPos),0);
	fNaICrystal->Construct();
	fNaICrystal->SetProperties();


	fLKrActiveVolume = new LKrActiveVolume(fWorldLogicalVolume,G4ThreeVector(0.0,0.0,active_lkr_zpos),0);
	
	G4cout << "Anode is at Z +" << (active_lkr_zpos+2.0*cm)/mm << " mm" << G4endl;
	G4VPhysicalVolume * lkr_active_physical_volume = fLKrActiveVolume->Construct();
	fLKrActiveVolume->SetProperties();

	// Eight SiPMs
	G4double sipm_z_front = active_lkr_zpos-15*mm+0.5*15*mm;
	G4double sipm_z_back = active_lkr_zpos+15*mm-0.5*15*mm;

	G4double sipm_zpos[2] = {sipm_z_front,sipm_z_back};
	G4double sipm_ypos[8] = {2.0*cm+0.5*2.0*mm,-2.0*cm-0.5*2.0*mm};
	G4double sipm_xpos[8] = {2.0*cm+0.5*2.0*mm,-2.0*cm-0.5*2.0*mm};

	fSiPM[0] = new SiPM(fWorldLogicalVolume,G4ThreeVector(sipm_xpos[0],-15*mm+0.5*15*mm,sipm_zpos[1]),Orientation::kLeft,0);
	fSiPM[1] = new SiPM(fWorldLogicalVolume,G4ThreeVector(sipm_xpos[0],+15*mm-0.5*15*mm,sipm_zpos[0]),Orientation::kLeft,1);

	fSiPM[2] = new SiPM(fWorldLogicalVolume,G4ThreeVector(sipm_xpos[1],-15*mm+0.5*15*mm,sipm_zpos[0]),Orientation::kRight,2);
	fSiPM[3] = new SiPM(fWorldLogicalVolume,G4ThreeVector(sipm_xpos[1],+15*mm-0.5*15*mm,sipm_zpos[1]),Orientation::kRight,3);
	
	fSiPM[4] = new SiPM(fWorldLogicalVolume,G4ThreeVector(-15*mm+0.5*15*mm,sipm_xpos[0],sipm_zpos[0]),Orientation::kTop,4);
	fSiPM[5] = new SiPM(fWorldLogicalVolume,G4ThreeVector(+15*mm-0.5*15*mm,sipm_xpos[0],sipm_zpos[1]),Orientation::kTop,5);
	fSiPM[6] = new SiPM(fWorldLogicalVolume,G4ThreeVector(-15*mm+0.5*15*mm,sipm_xpos[1],sipm_zpos[1]),Orientation::kBottom,6);
	fSiPM[7] = new SiPM(fWorldLogicalVolume,G4ThreeVector(+15*mm-0.5*15*mm,sipm_xpos[1],sipm_zpos[0]),Orientation::kBottom,7);
	
	G4OpticalSurface * sipm_opsurf = new G4OpticalSurface("sipm_opsurf");
	/* Those are the default values - make them explicit */
	sipm_opsurf->SetType(dielectric_dielectric);
	sipm_opsurf->SetFinish(polished);
	sipm_opsurf->SetModel(glisur);

	/* Perfectly "absorptive" surface ! arXiv:1612.05162 for dielectric-metal */	
	const G4int N = 2;
	G4double PhotonEnergy[N] = {6*eV,10*eV};
	G4double Reflectivity[N] = {0.0,0.0}; // <-- Important
	G4double Efficiency[N] = {1.0,1.0}; // <-- FIXME what is this??? Read the code, only useful for DelectricLUTDAVIS() ? NO also for detection!!!! See G4OpBoundaryProcessStatus
	G4double Rindex[N] = {1.0,1.0};


	G4MaterialPropertiesTable * sipm_mprop = new G4MaterialPropertiesTable();
	sipm_mprop->AddProperty("EFFICIENCY",PhotonEnergy,Efficiency,N);
	sipm_mprop->AddProperty("REFLECTIVITY",PhotonEnergy,Reflectivity,N);
	sipm_mprop->AddProperty("RINDEX",PhotonEnergy,Rindex,N);
	/* Note if RINDEX is not defined the op photons are probably killed anyway */

	sipm_opsurf->SetMaterialPropertiesTable(sipm_mprop);

	// Note: G4LogicalSkinSurface -> Logical volume entirely surrounded by this surface
	//       G4LogicalBorderSurface ordered pair of physical volume touching at the surface

	// FIXME: Add Rayleigh scatt to the bulk ?


	sipm_opsurf->DumpInfo();	
	sipm_mprop->DumpTable();
	std::ostringstream oss;
	for(int i = 0; i < 8; i++) {
		oss << i;
		G4VPhysicalVolume * sipm_physical_volume = fSiPM[i]->Construct();
		fSiPM[i]->SetProperties();
		G4String volume_name = "SiPM_Surface_"+oss.str();	
		fSiPMSurface[i] = new G4LogicalBorderSurface(volume_name,lkr_active_physical_volume,sipm_physical_volume,sipm_opsurf);
		oss.clear();
	}
	G4OpticalSurface * active_vol_opsurf = new G4OpticalSurface("active_vol_opsurf");
	/* Those are the default values */
	active_vol_opsurf->SetType(dielectric_metal); // * Not used with glisur?*
	active_vol_opsurf->SetFinish(polished);
	active_vol_opsurf->SetModel(glisur);
	
	G4MaterialPropertiesTable * active_vol_mprop = new G4MaterialPropertiesTable();
	//active_vol_mprop->AddProperty("EFFICIENCY",PhotonEnergy,Efficiency,N);
	
	G4double ReflectivityWall[N] = {0.5,0.5}; /* 50% REF */ 
	active_vol_mprop->AddProperty("REFLECTIVITY",PhotonEnergy,ReflectivityWall,N);
	active_vol_mprop->AddProperty("RINDEX",PhotonEnergy,Rindex,N);

	/* FIXME RINDEX is not defined, the op photons are probably killed anyway,
	 * read G4OpBoundaryProcess and G4OpticalSurface */
	active_vol_opsurf->SetMaterialPropertiesTable(active_vol_mprop);


	active_vol_opsurf->DumpInfo();
	active_vol_mprop->DumpTable();

	G4String volume_name = "LKr_Surface_"+oss.str();	
	//Rename this
	fLKrSurface = new G4LogicalBorderSurface(volume_name,lkr_active_physical_volume,fWorldPhysicalVolume,active_vol_opsurf);
	fCathodeSurface = new G4LogicalBorderSurface(volume_name,lkr_active_physical_volume,fPassiveLKrPhysicalVolume,active_vol_opsurf);

	// Delete this all the stuff I just constructed?

	SetProperties();
	return fWorldPhysicalVolume;                        
}

void DetectorConstruction::ConstructSDandField() {
    fNaICrystal->ConstructSDandField();
    fLKrActiveVolume->ConstructSDandField();
    for(int i = 0; i < 8; i++) {
        fSiPM[i]->ConstructSDandField();
    }
}

void DetectorConstruction::SetProperties() {	
	// Set the world volume invisible
	fWorldLogicalVolume->SetVisAttributes(G4VisAttributes::Invisible);



	fExternalWindowVisAttributes = new G4VisAttributes(G4Colour(0.5,1.0,1.0));
	fExternalWindowLogicalVolume->SetVisAttributes(fExternalWindowVisAttributes);

	fInsulationVacuumVisAttributes = new G4VisAttributes(G4Colour(1.0,1.0,1.0));
	fInsulationVacuumLogicalVolume->SetVisAttributes(fInsulationVacuumVisAttributes);
	
	fInternalWindowVisAttributes = new G4VisAttributes(G4Colour(0.0,1.0,1.0));
	fInternalWindowLogicalVolume->SetVisAttributes(fInternalWindowVisAttributes);

	fPassiveLKrVisAttributes = new G4VisAttributes(G4Colour(0.0,1.0,0.0));
	fPassiveLKrLogicalVolume->SetVisAttributes(fPassiveLKrVisAttributes);

}
