#include "GammaSource.hh"

GammaSource::GammaSource() {}
GammaSource::~GammaSource() {}

void GammaSource::GeneratePrimaryVertex(G4Event * event) {
	
	// Where vertex is deleted?	
	G4PrimaryVertex * vertex = new G4PrimaryVertex(particle_position,particle_time);

	bool geom_acc = false;
	// do ... while
	G4ThreeVector ref_axis = G4ThreeVector(0.0,0.0,1.0);
	G4ThreeVector direction; 
	while(geom_acc == false) {	
		direction = G4RandomDirection();
		if(ref_axis.angle(direction) < 0.10) geom_acc = true;
	}	
	
	// Perfect collimator ! 
	//direction = ref_axis;

	for(G4int i = 0;i < 2; i++) {
		G4PrimaryParticle * particle = new G4PrimaryParticle(22); // 22 = gamma
		// Mass and charge are fixed by the constructor 
		//particle->SetPolarization();
		particle->SetKineticEnergy(511.0*keV);
		// Not ideal ...
		
		particle->SetMomentumDirection(direction);
		particle->SetWeight(1.0);

		vertex->SetPrimary(particle);
		direction = -1.0*direction;
	}
	
	G4PrimaryParticle * particle = new G4PrimaryParticle(22); // 22 = gamma
	particle->SetKineticEnergy(1.2*MeV);
	particle->SetMomentumDirection(G4RandomDirection());
	particle->SetWeight(1.0);
	vertex->SetPrimary(particle);
	
	
	event->AddPrimaryVertex(vertex);
}
