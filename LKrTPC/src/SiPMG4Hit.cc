#include "SiPMG4Hit.hh"

G4ThreadLocal G4Allocator<SiPMG4Hit>* SiPMG4HitAllocator = 0;


SiPMG4Hit::SiPMG4Hit() {
	fWaveLength = -1.0;
}
SiPMG4Hit::~SiPMG4Hit() {}

/////////////////////////
// Not yet implemented //
/////////////////////////

//void SiPMG4Hit::Draw() {}
//void SiPMG4Hit::Print() {}

// Shallow copy is sufficicent
SiPMG4Hit::SiPMG4Hit(const SiPMG4Hit& right)
{
	fWaveLength = right.fWaveLength;

}

const SiPMG4Hit& SiPMG4Hit::operator=(const SiPMG4Hit& right)
{
	if (this == &right) return *this; //Check for self-assignment
	fWaveLength = right.fWaveLength;
	return *this;
}

G4int SiPMG4Hit::operator==(const SiPMG4Hit& right) const
{
    return (this==&right) ? 1 : 0;
}
