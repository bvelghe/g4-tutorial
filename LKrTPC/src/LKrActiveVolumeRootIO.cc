#include "LKrActiveVolumeRootIO.hh" 

LKrActiveVolumeRootIO::LKrActiveVolumeRootIO() : m_ready(false), m_tree(0), m_event(0), m_branch(0) {
	m_hits_collection_ID = -1;
	m_hits_collection = 0;
	m_SD_manager = G4SDManager::GetSDMpointer();

}

bool LKrActiveVolumeRootIO::Initialize(TTree * tree) {
	m_tree = tree;
	m_event = new AnodeEvent();
	m_branch = m_tree->Branch("Hits",&m_event);
	m_ready = true;

	return true;
}

void LKrActiveVolumeRootIO::SaveEvent(const G4Event * event) {
	if(m_ready == false) return;
	m_event->Reset(); // This is important, clean up the vector
	// FIXME Clean this up ! Do I need to check the m_SD_manager->GetCollectionID at each event?
	if(m_hits_collection_ID < 0) m_hits_collection_ID = m_SD_manager->GetCollectionID("LKrActiveVolume_HitsCollection_0");
	if(m_hits_collection_ID < 0) return;

	G4HCofThisEvent* HCTE = event->GetHCofThisEvent();
	if(HCTE) {
        	m_hits_collection = (LKrActiveVolumeG4HitsCollection*)(HCTE->GetHC(m_hits_collection_ID));
		G4int NbHits = m_hits_collection->entries();
		for(int idx = 0; idx < NbHits; idx++) {
			G4double energy = (*m_hits_collection)[idx]->Energy()/MeV;
			G4ThreeVector pos = (*m_hits_collection)[idx]->Position()/mm;
			G4bool compton_flag = (*m_hits_collection)[idx]->ComptonFlag();
			// FIXME Typo
			G4bool pe_flag = (*m_hits_collection)[idx]->PhotoElectronFlag();
			
			// Digitalization goes here
			G4double drift_velo = 1; // Dummy conversion variable
			G4double time = pos.z()*drift_velo; 

			AnodeHit hit;
			hit.SetEnergy(energy);
			hit.SetTime(time);
			TVector3 rpos(pos.x(),pos.y(),pos.z());
			hit.SetPosition(rpos);
			hit.SetComptonFlag(compton_flag);
			hit.SetPhotoElectricFlag(pe_flag);
			m_event->AddHit(hit);	


		}
	}	
	
	// Store this event
	m_tree->Fill();
}


LKrActiveVolumeRootIO::~LKrActiveVolumeRootIO() {
	if(m_event != 0) {
		delete m_event;
		m_event = 0;
	}
}
