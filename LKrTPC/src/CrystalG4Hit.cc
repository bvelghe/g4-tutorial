#include "CrystalG4Hit.hh"

G4ThreadLocal G4Allocator<CrystalG4Hit>* CrystalG4HitAllocator = 0;


CrystalG4Hit::CrystalG4Hit() {
	fEnergy = 0;
}
CrystalG4Hit::~CrystalG4Hit() {}

/////////////////////////
// Not yet implemented //
/////////////////////////

//void CrystalG4Hit::Draw() {}
//void CrystalG4Hit::Print() {}

// Shallow copy is sufficicent
CrystalG4Hit::CrystalG4Hit(const CrystalG4Hit& right)
{
	fEnergy = right.fEnergy;

}

const CrystalG4Hit& CrystalG4Hit::operator=(const CrystalG4Hit& right)
{
	if (this == &right) return *this; //Check for self-assignment
	fEnergy = right.fEnergy;
	return *this;
}

G4int CrystalG4Hit::operator==(const CrystalG4Hit& right) const
{
    return (this==&right) ? 1 : 0;
}
