#include "LKrActiveVolumeG4Hit.hh"

LKrActiveVolumeG4Hit::LKrActiveVolumeG4Hit() {
	fEnergy = 0;
	fComptonFlag = false;
	fPosition = G4ThreeVector(std::nan(""),std::nan(""),std::nan(""));
}
LKrActiveVolumeG4Hit::~LKrActiveVolumeG4Hit() {}

/////////////////////////
// Not yet implemented //
/////////////////////////

//void LKrActiveVolumeG4Hit::Draw() {}
//void LKrActiveVolumeG4Hit::Print() {}

// Shallow copy is sufficicent
LKrActiveVolumeG4Hit::LKrActiveVolumeG4Hit(const LKrActiveVolumeG4Hit& right)
{
	fEnergy = right.fEnergy;
	fComptonFlag = right.fComptonFlag;
	fPosition = right.fPosition;
}

const LKrActiveVolumeG4Hit& LKrActiveVolumeG4Hit::operator=(const LKrActiveVolumeG4Hit& right)
{
	if (this == &right) return *this; //Check for self-assignment
	fEnergy = right.fEnergy;
	fComptonFlag = right.fComptonFlag;
	fPosition = right.fPosition;
	return *this;
}

G4int LKrActiveVolumeG4Hit::operator==(const LKrActiveVolumeG4Hit& right) const
{
    return (this==&right) ? 1 : 0;
}
