#include "UserRunAction.hh"

UserRunAction::UserRunAction() : G4UserRunAction(), m_root_io(nullptr) {
    // !!! IsMaster not yet initialized here !!!
}
UserRunAction::~UserRunAction() {

}

G4Run * UserRunAction::GenerateRun() {

    // This is called before BeginOfRunAction
    if(IsMaster()) {
        // Called at the very end /
        auto run = new Run();
        return run;
    } else {
        m_root_io = new RootIO();
        auto run = new Run(m_root_io);
        return run;
    }

}



void UserRunAction::BeginOfRunAction(const G4Run *aRun) {
    ///////////////////////
    // ROOT Input/Output //
    ///////////////////////
    // ROOT does not support MT, one file / thread is the only solution at the moment
    // Note: Could also use the singleton pattern

    if(IsMaster()) {

    } else {
        std::ostringstream os;
        os << "test_" << G4Threading::G4GetThreadId() << ".root";

        m_root_io->StartRun(aRun->GetRunID(), os.str());
    }

}
void UserRunAction::EndOfRunAction(const G4Run *aRun) {

    if(IsMaster()) {

    } else {
        m_root_io->EndRun();
        delete m_root_io;
    }
}
