#include "SiPMSD.hh"
#include "G4UnitsTable.hh"
#include "G4SDManager.hh"
#include "G4VProcess.hh"
#include "G4Step.hh"

SiPMSD::SiPMSD(G4String name,G4int icopy) : G4VSensitiveDetector(name) {
	fCopy = icopy;
	fHCID = -1;
	std::ostringstream os; os << icopy;
  	collectionName.insert("SiPM_HitsCollection_" + os.str());
}	


SiPMSD::~SiPMSD() {
}

//Called at the beginning of every new event
void SiPMSD::Initialize(G4HCofThisEvent* HCTE) {

    fHitsCollection = new SiPMG4HitsCollection(SensitiveDetectorName, collectionName[0]);
	if (fHCID < 0)
	{
		fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
	}
	HCTE->AddHitsCollection(fHCID, fHitsCollection);
}

//Called for every step in the sensitve volume (mandatory)
G4bool SiPMSD::ProcessHits(G4Step* step, G4TouchableHistory*)	{
    return true;
}

// Dirty hack ??? G4 LXe exemple
G4bool SiPMSD::ProcessHits(const G4Step* step, G4TouchableHistory*)	{

  	G4double energy = step->GetTotalEnergyDeposit();
	G4double wavelength = (CLHEP::h_Planck*CLHEP::c_light)/(energy*CLHEP::eV);
	

	SiPMG4Hit* hit = new SiPMG4Hit();
	hit->WaveLength(wavelength);

	fHitsCollection->insert(hit);

	return true;
} 
