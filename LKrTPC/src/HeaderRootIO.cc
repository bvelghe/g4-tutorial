#include "HeaderRootIO.hh" 

HeaderRootIO::HeaderRootIO() : m_ready(false), m_tree(0), m_event(0), m_branch(0) {}

bool HeaderRootIO::Initialize(TTree * tree) {
	m_tree = tree;
	m_event = new EventHeader();
	m_branch = m_tree->Branch("Metadata",&m_event);
	m_ready = true;
	return true;
}

void HeaderRootIO::SaveEvent(const G4Event * event) {
	if(m_ready == false) return;
	// Digitalization goes here

	// Store this event
	m_tree->Fill();
}


HeaderRootIO::~HeaderRootIO() {
	if(m_event != 0) {
		delete m_event;
		m_event = 0;
	}
}
