#include "UserTrackingAction.hh"
#include "UserTrajectory.hh"

UserTrackingAction::UserTrackingAction() : G4UserTrackingAction(), fUserTrajectory(0) {
}
UserTrackingAction::~UserTrackingAction() {}

void UserTrackingAction::PreUserTrackingAction(const G4Track * track) {

	
	// Construct a concrete trajectory class object here
// Note: The customised trajectory point *must* be constructed in the AppendStep() method of the trajectory class.
//
//
// Operator new and delete must be provided if G4Trajectory declares any new class variables.
//
	// Create trajectories only for primaries
	if(track->GetParentID() == 0) {
		fUserTrajectory = new UserTrajectory(track);
		fpTrackingManager->SetStoreTrajectory(true);
		fpTrackingManager->SetTrajectory(fUserTrajectory); // inherited 
	} else {
		fpTrackingManager->SetStoreTrajectory(false);
	}
	// Add user infos ...
	fpTrackingManager->SetUserTrackInformation(new UserTrackInformation()); // Who is deleting this?
}

void UserTrackingAction::PostUserTrackingAction(const G4Track * track) {
	// Memory: Responsability of the Tracking Manager?
	UserTrajectory * trajectory = (UserTrajectory*)fpTrackingManager->GimmeTrajectory();
	if(trajectory) {
		for(G4int i = 0 ; i < trajectory->GetPointEntries() ; i++) {
			UserTrajectoryPoint * trajectory_point = (UserTrajectoryPoint*)trajectory->GetPoint(i);
			//G4cout << "[" << i << "] " << trajectory_point->GetEnergy() << " " << trajectory_point->GetPosition() << G4endl;
		}
	}
	
	
	//if(fUserTrajectory) {
	//	delete fUserTrajectory;
	//	fUserTrajectory = 0; // <-- Important!
	//}


	// Pass the track information to the secondaries ...
	G4TrackVector * secondaries = fpTrackingManager->GimmeSecondaries();
	if(secondaries) {
		UserTrackInformation * info = (UserTrackInformation*)(track->GetUserInformation());

		for(size_t i = 0 ; i < secondaries->size() ; ++i) {
			UserTrackInformation * sec_info = new UserTrackInformation(info);
			(*secondaries)[i]->SetUserInformation(sec_info);
		}
	}
}

