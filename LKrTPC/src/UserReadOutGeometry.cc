#include "UserReadOutGeometry.hh"

UserReadOutGeometry::UserReadOutGeometry() : fROPhysicalVolume(0) { 
	
	//fNistMan = G4NistManager::Instance();
	//fNistMan->FindOrBuildMaterial("G4_Galactic");
	fROSolidVolume = new G4Box("RO_World",1.0*m,1.0*m,1.0*m);

	fROLogicalVolume = new G4LogicalVolume(fROSolidVolume,
			G4Material::GetMaterial("G4_Galactic"),
			"RO_World",
			0,
			0,
			0);

	fROPhysicalVolume = new G4PVPlacement(0,
			G4ThreeVector(0.,0.,0.),
			fROLogicalVolume,
			"RO_World",
			0,
			false
			,0);  


}

UserReadOutGeometry::~UserReadOutGeometry() {
	delete fROPhysicalVolume;
}

void UserReadOutGeometry::SetZPos(G4double z_pos) {
	
}

G4VPhysicalVolume* UserReadOutGeometry::Build() {
	return nullptr;
}
