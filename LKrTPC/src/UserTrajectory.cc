#include "UserTrajectory.hh"

UserTrajectory::UserTrajectory() : fPositionRecord(0), fTrackID(0), fParentID(0), fPDGEncoding(0), fPDGCharge(.0), fInitialKineticEnergy(.0), fParticleName(""), fInitialMomentum(G4ThreeVector() )
{}

UserTrajectory::UserTrajectory(const G4Track * track) {
	// Store info here
	const G4ParticleDefinition * fParticleDefinition = track->GetParticleDefinition();

	fParticleName = fParticleDefinition->GetParticleName();
	fPDGCharge = fParticleDefinition->GetPDGCharge();
	fPDGEncoding = fParticleDefinition->GetPDGEncoding();
	
	
	fTrackID = track->GetTrackID();
	fParentID = track->GetParentID();
	fInitialKineticEnergy = track->GetKineticEnergy();
	fInitialMomentum = track->GetMomentum();
	fInitialPosition = track->GetPosition();

	fPositionRecord = new std::vector< UserTrajectoryPoint *>;

	// Add the first trajectory point
	fPositionRecord->push_back(new UserTrajectoryPoint());
}

UserTrajectory::~UserTrajectory() {

	if(fPositionRecord) {
		// Why not delete[], custom delete operator?
		for(size_t i = 0 ; i < fPositionRecord->size(); i++) {
			delete (*fPositionRecord)[i]; 
		}

		fPositionRecord->clear(); // Why?
		delete fPositionRecord;
		fPositionRecord = 0;
	}

}


void UserTrajectory::AppendStep(const G4Step * step) {
	fPositionRecord->push_back(new UserTrajectoryPoint(step));
}

void UserTrajectory::MergeTrajectory(G4VTrajectory * trajectory) {
	if(!trajectory) return;

	UserTrajectory * other = (UserTrajectory*)trajectory;
	// Initial point of the other trajectory should not be merged Why???
	for(int i = 1 ; i < other->GetPointEntries() ; i++) {
		fPositionRecord->push_back((*(other->fPositionRecord))[i]);
	}
	delete (*other->fPositionRecord)[0]; // Why only 0??? Memory leak???
	other->fPositionRecord->clear();
}


G4ThreadLocal G4Allocator<UserTrajectory> * fTrajectoryAlloc;
