#include "Chamber.hh"
#include "G4NistManager.hh" 

Chamber::Chamber(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy) : 
	fMotherLogicalVolume(motherLogicalVolume), 
	fPosition(position), 
	fCopy(copy) {


}

Chamber::~Chamber() {
}

G4VPhysicalVolume* Chamber::Construct() {
	std::ostringstream oss;
	oss << fCopy;

	// FIXME: [IMPROVEMENT] move the declaration to the header file and user a GeometryParameter(s) class.
	G4double fModuleXLength = 1.5*cm;
	G4double fModuleYLength = 1.5*cm;
	G4double fModuleZLength = 2.0*mm;

	
	G4String volume_name = "Chamber_Module_"+oss.str();	
	fChamberSolidVolume = new G4Box(volume_name,0.5*fModuleXLength,0.5*fModuleYLength,0.5*fModuleZLength);
	
	fChamberLogicalVolume = new G4LogicalVolume(fChamberSolidVolume,
		G4Material::GetMaterial("G4_SILICON_DIOXIDE"),
		volume_name,
		0, // G4FieldManager * (def. 0)
		0, // G4VSensitiveDetector * (def. 0)
		0, // G4UserLimits * (def. 0)
		true); // optimise (def. true)

	
	fChamberPhysicalVolume = new G4PVPlacement(0,
		fPosition,
		fChamberLogicalVolume,
		volume_name,
		fMotherLogicalVolume,
		false
		,0); 
	
	return fChamberPhysicalVolume;
}

void Chamber::SetProperties() {
	fChamberVisAttributes = new G4VisAttributes(G4Colour(1.0,1.0,0.5));
	fChamberLogicalVolume->SetVisAttributes(fChamberVisAttributes);
}
