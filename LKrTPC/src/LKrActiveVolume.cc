#include "LKrActiveVolume.hh"

LKrActiveVolume::LKrActiveVolume(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy) :
    G4VUserDetectorConstruction(),
	fMotherLogicalVolume(motherLogicalVolume), 
	fPosition(position), 
	fCopy(copy) {

G4double a,z,density,temperature,pressure;

G4Material * lkr_mat = new G4Material("LKr",z=36,a=83.80*g/mole,density=2.41*g/cm3,kStateLiquid,temperature=120.0*kelvin,pressure=1.0*atmosphere);

//G4cout << lkr_mat << G4endl;

fLKrMat = lkr_mat;

// Scintillation
const G4int N = 60;

G4double PhotonEnergy[N] = {6.81*eV,6.90*eV,7.03*eV,7.14*eV,7.26*eV,7.41*eV,7.50*eV,7.61*eV,7.67*eV,7.71*eV,7.77*eV,7.79*eV,7.82*eV,7.86*eV,7.90*eV,7.92*eV,7.94*eV,7.97*eV,8.00*eV,8.02*eV,8.05*eV,8.06*eV,8.10*eV,8.11*eV,8.15*eV,8.18*eV,8.21*eV,8.24*eV,8.27*eV,8.30*eV,8.35*eV,8.42*eV,8.45*eV,8.53*eV,8.54*eV,8.57*eV,8.60*eV,8.62*eV,8.66*eV,8.68*eV,8.70*eV,8.73*eV,8.76*eV,8.80*eV,8.81*eV,8.85*eV,8.87*eV,8.90*eV,8.92*eV,8.96*eV,9.00*eV,9.01*eV,9.08*eV,9.12*eV,9.23*eV,9.32*eV,9.41*eV,9.54*eV,9.66*eV,9.84*eV};

G4double Rindex[N] = {1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32,1.32};

G4double Absorption[N] = {1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m,1.0*m};


G4double Scintillation[N] = {0.011,0.022,0.026,0.031,0.042,0.067,0.089,0.118,0.151,0.182,0.215,0.240,0.275,0.313,0.349,0.388,0.419,0.455,0.497,0.534,0.568,0.606,0.663,0.710,0.754,0.790,0.829,0.872,0.916,0.945,0.984,1.000,0.985,0.947,0.934,0.860,0.818,0.774,0.694,0.652,0.608,0.568,0.524,0.482,0.442,0.405,0.369,0.331,0.301,0.265,0.228,0.199,0.166,0.132,0.099,0.077,0.057,0.048,0.033,0.016};


G4MaterialPropertiesTable * LKrProp = new G4MaterialPropertiesTable();

LKrProp->AddProperty("SLOWCOMPONENT",PhotonEnergy,Scintillation,N);
LKrProp->AddProperty("RINDEX",PhotonEnergy,Rindex,N);
LKrProp->AddProperty("ABSLENGTH",PhotonEnergy,Absorption,N);


LKrProp->AddConstProperty("SCINTILLATIONYIELD",9000.0/MeV); // At 1 kV/cm
//LKrProp->AddConstProperty("SCINTILLATIONYIELD",900.0/MeV); // At 1 kV/cm
LKrProp->AddConstProperty("RESOLUTIONSCALE",1.0); // ??
LKrProp->AddConstProperty("YIELDRATIO",0.18); // Relative strength of the fast component as a fraction of total scintillation yield.
LKrProp->AddConstProperty("FASTTIMECONSTANT",2.0*ns);
LKrProp->AddConstProperty("SLOWTIMECONSTANT",95.0*ns);

fLKrMat->SetMaterialPropertiesTable(LKrProp);
	
}

LKrActiveVolume::~LKrActiveVolume() {
}

G4VPhysicalVolume* LKrActiveVolume::Construct() {
	std::ostringstream oss;
	oss << fCopy;
	
	// FIXME: [IMPROVEMENT] move the declaration to the header file and user a GeometryParameter(s) class.
	G4double fLKrActiveVolumeXLength = 4.0*cm;
	G4double fLKrActiveVolumeYLength = 4.0*cm;
	G4double fLKrActiveVolumeZLength = 4.0*cm;

	G4double fVoxelWidth = 1.0*mm;
	G4int fNVoxel = fLKrActiveVolumeZLength/fVoxelWidth; 

	
	G4String lkr_volume_name = "LKrActiveVolume_"+oss.str();	
	G4String lkr_volume_name_voxel = "LKrActiveVolumeVoxel_"+oss.str();	
	G4String lkr_volume_name_voxel_anode = "LKrActiveVolumeVoxelAnode_"+oss.str();	

	fLKrSolidVolume = new G4Box(lkr_volume_name,0.5*fLKrActiveVolumeXLength,0.5*fLKrActiveVolumeYLength,0.5*fLKrActiveVolumeZLength);

	fLKrLogicalVolume = new G4LogicalVolume(fLKrSolidVolume,
		fLKrMat,
		lkr_volume_name,
		0, // G4FieldManager * (def. 0)
		0, // G4VSensitiveDetector * (def. 0)
		0, // G4UserLimits * (def. 0)
		true); // optimise (def. true)
	
	
	fLKrSolidVolume_Voxel = new G4Box(lkr_volume_name_voxel,0.5*fLKrActiveVolumeXLength,0.5*fLKrActiveVolumeYLength,0.5*fVoxelWidth);

	//This goes into the Voxel "box" 
	fLKrSolidVolume_VoxelAnode = new G4Tubs(lkr_volume_name_voxel_anode,
						0., /* inner radius */
						0.5*cm, /* outer radius*/
						0.5*fVoxelWidth, /* height */
						0., /* start angle*/
						2*CLHEP::pi*rad); /* span angle */

	fLKrLogicalVolume_Voxel = new G4LogicalVolume(fLKrSolidVolume_Voxel,
		fLKrMat,
		lkr_volume_name_voxel,
		0, // G4FieldManager * (def. 0)
		0, // G4VSensitiveDetector * (def. 0)
		0, // G4UserLimits * (def. 0)
		true); // optimise (def. true)

	fLKrLogicalVolume_VoxelAnode = new G4LogicalVolume(fLKrSolidVolume_VoxelAnode,
		fLKrMat,
		lkr_volume_name_voxel_anode,
		0, // G4FieldManager * (def. 0)
		0, // G4VSensitiveDetector * (def. 0)
		0, // G4UserLimits * (def. 0)
		true); // optimise (def. true)
	
	fLKrPhysicalVolume_VoxelAnode = new G4PVPlacement(0,
		G4ThreeVector(0.,0.,0.),
		fLKrLogicalVolume_VoxelAnode,
		lkr_volume_name_voxel_anode,
		fLKrLogicalVolume_Voxel,
		false
		,0); 




	fLKrPhysicalVolume_Voxel = new G4PVReplica(lkr_volume_name_voxel,
		fLKrLogicalVolume_Voxel, 
		fLKrLogicalVolume, 
		kZAxis, 
		fNVoxel, 
		fVoxelWidth);
	
	fLKrPhysicalVolume = new G4PVPlacement(0,
		fPosition,
		fLKrLogicalVolume,
		lkr_volume_name,
		fMotherLogicalVolume,
		false
		,0); 

	



	return fLKrPhysicalVolume_Voxel; // FIXME: Add a function !!! NEED THIS FOR THE OPTICAL SURFACES
}

void LKrActiveVolume::ConstructSDandField() {
    G4cout << "LKrActiveVolume::ConstructSDandField() invoked\n";
    // Setup the sensitive volume
    std::ostringstream oss;
    oss << fCopy;
    G4String sensor_SDname = "/LKr_"+oss.str();

    auto * LKr_SD = new LKrActiveVolumeSD(sensor_SDname,fCopy);
    G4SDManager::GetSDMpointer()->AddNewDetector(LKr_SD);
    SetSensitiveDetector(fLKrLogicalVolume_VoxelAnode->GetName(), LKr_SD);
}

void LKrActiveVolume::SetProperties() {
	fLKrVisAttributes = new G4VisAttributes(G4Colour(1.0,0.0,0.0));
	fLKrLogicalVolume->SetVisAttributes(fLKrVisAttributes);
}
