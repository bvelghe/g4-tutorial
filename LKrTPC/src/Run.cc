#include "Run.hh"

// Worker
Run::Run(RootIO * root_io) : G4Run() {
    m_root_io = root_io;
}

// Main thread
Run::Run() : G4Run(), m_root_io(nullptr) {
}

Run::~Run() {

}

void Run::RecordEvent(const G4Event * aEvent) {
    if(m_root_io) m_root_io->SaveEvent(aEvent);
    G4Run::RecordEvent(aEvent);

}

// Called at the very end, merge sub-runs
void Run::Merge(const G4Run * aRun) {
    G4Run::Merge(aRun);
}


