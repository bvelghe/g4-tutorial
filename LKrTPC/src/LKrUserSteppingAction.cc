#include "LKrUserSteppingAction.hh"
#include "G4SDManager.hh"
#include "SiPMSD.hh"

LKrUserSteppingAction::LKrUserSteppingAction() {}

LKrUserSteppingAction::~LKrUserSteppingAction() {}

void LKrUserSteppingAction::UserSteppingAction(const G4Step *step) {


    G4Track *track = step->GetTrack();
    /* Get the track info */
    auto * usr_trk_info = (UserTrackInformation *) track->GetUserInformation();
    if (usr_trk_info) {
        /* Proceed only if the Compton flag is false */
        if (!usr_trk_info->GetComptonFlag()) {

            G4bool in_passive_volume = false;
            G4VPhysicalVolume *vol_ptr = track->GetVolume();
            if (vol_ptr) {
                // FIXME: Slow?
                in_passive_volume = (vol_ptr->GetName() != "LKrActiveVolumeVoxelAnode_0") ? true : false;
                // FIXME: This can be simplified ...
            }

            // FIXME: Note: See G4SteppingVerbose for inspiration
            if (step->GetPostStepPoint()->GetProcessDefinedStep() != nullptr && in_passive_volume) {
                G4bool is_compton = (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName() == "compt")
                                    ? true : false;
                usr_trk_info->SetComptonFlag(is_compton);

            }
            // Tag the PE interaction everywhere
            if (step->GetPostStepPoint()->GetProcessDefinedStep()) {
                G4bool is_photoelec = (step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName() == "phot")
                                      ? true : false;
                usr_trk_info->SetPhotoElectricFlag(is_photoelec);
            }

        }
    } else {
        G4cerr << "Warning! No track info." << G4endl;
    }

/*
	// Compton II
	// List of processes [...] the particle is sensitive to (!)
	G4ProcessManager * pm = step->GetTrack()->GetDefinition()->GetProcessManager();
	G4int nprocesses = pm->GetProcessListLength();
	G4ProcessVector * pv = pm->GetProcessList();
	G4int i;
	
	for(i = 0; i < nprocesses; i++) {
			// NB: Ok because GetProcessName() returns a std::string which provides a == operator for const char *
		//if((*pv)[i]->GetProcessName()=="OpBoundary") {
		//}
		G4cout << i << " " << (*pv)[i]->GetProcessName() << G4endl;
	}	

	//  physics process that defined the step-length of the current and previous step 
	step->GetDeltaMomentum(); // ???
	*/

    //G4VProcess * cp = track->GetCreatorProcess();
    //cp->GetProcessName()


    // Dynamic particles (G4DynamicParticle)
//	G4cout << step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName() << G4endl;


    G4OpBoundaryProcessStatus boundaryStatus = Undefined;
    static G4ThreadLocal G4OpBoundaryProcess *boundary = nullptr;


    // Find the boundary process only once
    if (!boundary) {
        G4ProcessManager *pm = step->GetTrack()->GetDefinition()->GetProcessManager();
        G4int nprocesses = pm->GetProcessListLength();
        G4ProcessVector *pv = pm->GetProcessList();
        G4int i;
        for (i = 0; i < nprocesses; i++) {
            // NB: Ok because GetProcessName() returns a std::string which provides a == operator for const char *
            if ((*pv)[i]->GetProcessName() == "OpBoundary") {
                boundary = (G4OpBoundaryProcess *) (*pv)[i];
                break;
            }
        }
    }

    G4StepPoint *thePostPoint = step->GetPostStepPoint();
    // FIXME: Check if boundary != nullptr
    G4ParticleDefinition *particleType = track->GetDefinition();
    if (particleType == G4OpticalPhoton::OpticalPhotonDefinition()) {
        // Optical photon only
        boundaryStatus = boundary->GetStatus();

        // This is important!
        if(thePostPoint->GetStepStatus() == fGeomBoundary) {

        } else {
            //G4cout << "Step is not fGeomBoundary!" << G4endl;
            return;
        }
        switch (boundaryStatus) {

            case Detection: {
                G4StepPoint *PostPoint = step->GetPostStepPoint();
                G4VPhysicalVolume *PostPV = PostPoint->GetPhysicalVolume();
                G4int Copy = PostPV->GetCopyNo();
                if (Copy > 8) {
                    //G4cout << "ph det! " << PostPV->GetName() << ", copy: " << PostPV->GetCopyNo() << G4endl;
                    //G4cout << PostPoint->GetPosition() << "\n";
                    break;
                }
                //G4cout << "ph det! " << PostPV->GetName() << ", copy: " << PostPV->GetCopyNo() << G4endl;
                //G4cout << "++ " << PostPoint->GetPosition() << "\n";
                // FIXME Slow --> Use LUT
                std::ostringstream oss;
                oss << Copy;

                // Trigger sensitive detector manually
                G4SDManager *SDman = G4SDManager::GetSDMpointer();
                //FIXME:: RENAME THIS TO SiPM_Module_XXX
                G4String sdName = "/SiPM_" + oss.str();
                SiPMSD *SiPM_SD = (SiPMSD *) SDman->FindSensitiveDetector(sdName);
                if (SiPM_SD) {
                    SiPM_SD->ProcessHits(step, nullptr);
                }

                break;
            }
            case NoRINDEX:
                break;
            default:
                break;

        }
    }
}
