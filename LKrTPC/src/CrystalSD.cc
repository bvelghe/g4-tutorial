#include "CrystalSD.hh"
#include "G4UnitsTable.hh"
#include "G4SDManager.hh"
#include "G4VProcess.hh"
#include "G4Step.hh"

// Call the G4VSensitiveDetector constructor
CrystalSD::CrystalSD(G4String name,G4int icopy) : G4VSensitiveDetector(name), fHitsCollection(nullptr) {

    fCopy = icopy;
	fHCID = -1;
	fCell = -1;
	std::ostringstream os; os << icopy;
  	collectionName.insert("Crystal_HitsCollection_" + os.str());
}	


CrystalSD::~CrystalSD() {
}

//Called at the beginning of every new event
void CrystalSD::Initialize(G4HCofThisEvent* HCTE) {

	fHitsCollection = new CrystalG4HitsCollection(SensitiveDetectorName, collectionName[0]);
	if (fHCID < 0)
    {
        fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
    }
    HCTE->AddHitsCollection(fHCID, fHitsCollection);
    fCell = -1;

}

//Called for every step in the sensitive volume (mandatory)
G4bool CrystalSD::ProcessHits(G4Step* step, G4TouchableHistory*)	{

    // Optical photons do not conserve energy, ignore them
	G4ParticleDefinition * particleType = step->GetTrack()->GetDefinition();
	if(particleType == G4OpticalPhoton::OpticalPhotonDefinition()) return false;
	
	G4double energy = step->GetTotalEnergyDeposit();
	
	if (energy == 0) return false;

	// FIXME Warning: Sum all the energy inside this **event** --> Move this to CrystalRootIO?
	if(fCell == -1) {
		CrystalG4Hit* hit = new CrystalG4Hit();
	
		hit->Energy(energy);
		G4int cell_id = fHitsCollection->insert(hit);
		fCell = cell_id - 1; //Voir exemple N04
	
	} else {
		//G4cout << "Add step (" << energy << ") MeV\n";
		(*fHitsCollection)[fCell]->AddEnergy(energy);
		
		double Esum = (*fHitsCollection)[fCell]->Energy();
	}
	return true;

} 
