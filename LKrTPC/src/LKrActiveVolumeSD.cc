#include "LKrActiveVolumeSD.hh"
#include "G4UnitsTable.hh"
#include "G4SDManager.hh"
#include "G4VProcess.hh"
#include "G4Step.hh"

// Call the superclass (G4VSensitiveDetector) constructor
LKrActiveVolumeSD::LKrActiveVolumeSD(G4String name,G4int icopy) : G4VSensitiveDetector(name) {
	fCopy = icopy;
	fHCID = -1;
	std::ostringstream os; os << icopy;
  	collectionName.insert("LKrActiveVolume_HitsCollection_" + os.str());
}	


LKrActiveVolumeSD::~LKrActiveVolumeSD() {
}

//Called at the beginning of every new event
void LKrActiveVolumeSD::Initialize(G4HCofThisEvent* HCTE) {

    fHitsCollection = new LKrActiveVolumeG4HitsCollection(SensitiveDetectorName, collectionName[0]);
	if (fHCID < 0)
    {
        fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
    }
    fReplicaMap.clear(); 
    HCTE->AddHitsCollection(fHCID, fHitsCollection);
}

//Called for every step in the sensitive volume (mandatory)
G4bool LKrActiveVolumeSD::ProcessHits(G4Step* step, G4TouchableHistory* hist) {
	// Note: hist is filled only if a ReadOutGeometry is instantiated.
	//if(hist) {
	//	G4cout << "Replica #" << hist->GetReplicaNumber() << G4endl;
	//}

	// Optical photons do not conserve energy, ignore them
	G4ParticleDefinition * particleType = step->GetTrack()->GetDefinition();
	if(particleType == G4OpticalPhoton::OpticalPhotonDefinition()) return false;
	
	G4double energy = step->GetTotalEnergyDeposit();

	// FIXME This is not optimal ... energy deposit along the step ...
	G4ThreeVector pre_pos = step->GetPreStepPoint()->GetPosition(); 
	//G4ThreeVector post_pos = step->GetPostStepPoint()->GetPosition(); 



	if (energy == 0) return false;

	G4int replica = step->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber();

	// Found in the hypernews ...
	G4ThreeVector origin(0.,0.,0.);
	G4ThreeVector pos = step->GetPreStepPoint()->GetTouchableHandle()->GetHistory()->GetTopTransform().Inverse().TransformPoint(origin);
	UserTrackInformation * usr_trk_info = (UserTrackInformation*)step->GetTrack()->GetUserInformation();

	G4bool compton_flag = false;
	G4bool pe_flag = false;
	if(usr_trk_info) {
		compton_flag = usr_trk_info->GetComptonFlag();
		pe_flag = usr_trk_info->GetPhotoElectricFlag();
	}

	// Warning: Sum all the energy deposited in a voxel. The user needs to sum the voxel to get the *event* energy.

	// DISABLE ENERGY SUM
	//auto cell_it = std::find(fReplicaMap.begin(),fReplicaMap.end(),replica);
	//if(cell_it == fReplicaMap.end()) {

		LKrActiveVolumeG4Hit* hit = new LKrActiveVolumeG4Hit();

		//hit->Position(pos);
		hit->Position(pre_pos);
		hit->Energy(energy);
		hit->ComptonFlag(compton_flag);
		hit->PhotoElectronFlag(pe_flag);

		G4int idx = fHitsCollection->insert(hit);
		//G4cout << "Replica #" << replica << " (idx = " << idx - 1 << ")" << G4endl;
		// DISABLE ENERGY SUM
		//fReplicaMap.push_back(replica);
	/* DISABLE ENERGY SUM		
	} else {
		G4int idx = std::distance(fReplicaMap.begin(),cell_it);
		//G4cout << "Add to replica #" << replica << " (idx = " << idx << ")" << G4endl;
		(*fHitsCollection)[idx]->AddEnergy(energy);
		//(*fHitsCollection)[idx]->AddPosition(post_pos,energy);
	}
	*/
	return true;
} 
