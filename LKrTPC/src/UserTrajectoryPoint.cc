#include "UserTrajectoryPoint.hh"

UserTrajectoryPoint::UserTrajectoryPoint() {
}

UserTrajectoryPoint::UserTrajectoryPoint(const G4Step * step) {
	fEnergy = step->GetTotalEnergyDeposit();
	fPosition = step->GetPostStepPoint()->GetPosition();
	// NB: Ok because GetProcessName() returns a std::string which provides a == operator for const char *
	// FIXME: Remove this?
	fCompton = step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName() == "compt" ? true : false;
}

UserTrajectoryPoint::~UserTrajectoryPoint() {
}

G4ThreadLocal G4Allocator<UserTrajectoryPoint> * fTrajectoryPointAlloc = 0;
