#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TVector3.h"
#include "Math/Random.h"
#include "Math/MixMaxEngine.h"

#include "CrystalEvent.hh"
#include "EventHeader.hh"
#include "AnodeEvent.hh"
#include "SiPMEvent.hh"
#include "SiPMHit.hh"

int main(int argc, char * argv[]) {
	std::string in_file_name;
	std::string out_file_name;
	if(argc != 3) {
		std::cout << "usage: " << argv[0] << " <input.root> <output.root>" << std::endl;
		return -1;
	} 

	in_file_name = argv[1];
	out_file_name = argv[2];	



	TFile * file = TFile::Open(in_file_name.c_str(),"READ");
	if(file->IsOpen() == false) {
		std::cout << "Unable to open " << in_file_name << ", exiting." << std::endl;
		return -1;
	}

	EventHeader * event_header = new EventHeader();
	
	TTree * tree_header = (TTree*)file->Get("Header");	
	if(tree_header == 0) { 
		std::cout << "No tree named \"Header\"" << std::endl; 
		return -1;
	}
	
	TBranch * branch_meta  = tree_header->GetBranch("Metadata");
	if(branch_meta == 0) {
		std::cout << "No branch named \"(Header) Metadata\"" << std::endl; 
		return -1;
	}
	branch_meta->SetAddress(&event_header);

	///////////////////////
	// LKr Active Volume //
	///////////////////////

	AnodeEvent * lkr_active_vol_event = new AnodeEvent();
	
	TTree * tree_lkr_active_vol = (TTree*)file->Get("LKr_Active_Volume");
	if(tree_lkr_active_vol == 0) {
		std::cout << "No tree named \"LKr_Active_Volume\"" << std::endl; 
		return -1;
	}

	TBranch * branch_lkr_hit  = tree_lkr_active_vol->GetBranch("Hits");
	if(branch_lkr_hit == 0) {
		std::cout << "No branch named \"(LKr_Active_Volume) Hits\"" << std::endl; 
	return -1;
	}
	branch_lkr_hit->SetAddress(&lkr_active_vol_event);
	
	/////////////////
	// NaI Crystal //
	/////////////////
	CrystalEvent * nai_event = new CrystalEvent();
	
	TTree * tree_nai = (TTree*)file->Get("NaI_Crystal");
	if(tree_nai == 0) {
		std::cout << "No tree named \"NaI_Crystal\"" << std::endl; 
		return -1;
	}

	TBranch * branch_nai_hit  = tree_nai->GetBranch("Hits");
	if(branch_nai_hit == 0) {
		std::cout << "No branch named \"(NaI_Crystal) Hits\"" << std::endl; 
	return -1;
	}
	branch_nai_hit->SetAddress(&nai_event);
	
	//////////
	// SiPM //
	//////////
	SiPMEvent * sipm_event = new SiPMEvent();
	
	TTree * tree_sipm = (TTree*)file->Get("SiPMs");
	if(tree_sipm == 0) {
		std::cout << "No tree named \"SiPMs\"" << std::endl; 
		return -1;
	}

	TBranch * branch_sipm_hit  = tree_sipm->GetBranch("Hits");
	if(branch_sipm_hit == 0) {
		std::cout << "No branch named \"(SiPM) Hits\"" << std::endl; 
	return -1;
	}
	branch_sipm_hit->SetAddress(&sipm_event);


	///////////////
	// 
	///////////////

	TH2I * h_Exy = new TH2I("h_Exy",";X (mm); Y (mm)",400,-20,20,400,-20,20); 
	TH2D * h_PhotonCount_vs_Zpos_NotA2_PP = new TH2D("h_PhotonCount_vs_Zpos_NotA2_PP","Photon Count (ALL) vs Z pos;Zpos;N_P",50,0,50,400,0,400);

	long nevents = tree_header->GetEntries();

	if(tree_nai->GetEntries() != nevents || tree_lkr_active_vol->GetEntries() != nevents) {
		std::cout << "Event numbers not are not consistent" << std::endl; 
		return -1;
	}

	TFile * out_file = TFile::Open(out_file_name.c_str(),"RECREATE");// output file default 
	if(out_file->IsOpen() == false) {
		std::cout << "Unable to create " << out_file_name << ", exiting." << std::endl;
		return -1;
	}



	 std::cout << "N events: " << nevents << std::endl;
	 for (int i = 0; i < nevents; i++) {
		                 tree_nai->GetEntry(i);
		                 tree_lkr_active_vol->GetEntry(i);
				 tree_sipm->GetEntry(i);
				 
				 // Crude simulation of the trigger: we require a coincidence
				 if(nai_event->GetNHits() == 0 || lkr_active_vol_event->GetNHits() == 0) 
				 {
					//std::cout << "FAIL TRIGGER" << std::endl; 
				 	continue;
				 }
				
				double Eanode = 0.0;
				double zpos = 0.0;
				double xpos = 0.0;
				double ypos = 0.0;
			        // Note: Note the *!	
				for(auto && hit :  *lkr_active_vol_event) {
					if(!hit.GetComptonFlag()) {
						Eanode += hit.GetEnergy();
						double x = hit.GetPosition().X();
						double y = hit.GetPosition().Y();
						double z = hit.GetPosition().Z();
						xpos += x*(hit.GetEnergy());
						ypos += y*(hit.GetEnergy());
						zpos += z*(hit.GetEnergy());
						//h_Exy->Fill(x,y);
					}
				}
				 zpos /= Eanode;
				 xpos /= Eanode;
				 ypos /= Eanode;
	 			
				 double zpos_anode = 93.0; // mm (TO BE CHECKED)
				 double d_drift = zpos_anode - zpos; // in mm
				 
				 int counter = 0;
				 // FIXME Blend APD1 and APD2 signals
				 for(auto it = sipm_event->begin(); it < sipm_event->end(); ++it) 
				 {
					 counter++;
				 }	


				double r = sqrt(xpos*xpos + ypos*ypos);

			 	// PP peak
				if(Eanode > 0.510 && Eanode < 0.512 && r < 10.0) {
					h_PhotonCount_vs_Zpos_NotA2_PP->Fill(d_drift,counter); 
					h_Exy->Fill(xpos,ypos);
				}

				 

	 }
	// Global

	h_PhotonCount_vs_Zpos_NotA2_PP->FitSlicesY();
	TH1D * h_PhotonCount_vs_Zpos_NotA2_PP_Y_Fit = (TH1D*)gDirectory->Get("h_PhotonCount_vs_Zpos_NotA2_PP_2");
	
	h_PhotonCount_vs_Zpos_NotA2_PP_Y_Fit->Write();
	h_PhotonCount_vs_Zpos_NotA2_PP->Write();
	h_Exy->Write();
	out_file->Close();

}	
