#include <iostream>

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TVector3.h"
#include "Math/Random.h"
#include "Math/MixMaxEngine.h"

#include "CrystalEvent.hh"
#include "EventHeader.hh"
#include "AnodeEvent.hh"
#include "SiPMEvent.hh"
#include "SiPMHit.hh"

int main(int argc, char * argv[]) {
	std::string in_file_name;
	std::string out_file_name;
	if(argc != 3) {
		std::cout << "usage: " << argv[0] << " <input.root> <output.root>" << std::endl;
		return -1;
	} 

	in_file_name = argv[1];
	out_file_name = argv[2];	



	TFile * file = TFile::Open(in_file_name.c_str(),"READ");
	if(file->IsOpen() == false) {
		std::cout << "Unable to open " << in_file_name << ", exiting." << std::endl;
		return -1;
	}

	EventHeader * event_header = new EventHeader();
	
	TTree * tree_header = (TTree*)file->Get("Header");	
	if(tree_header == 0) { 
		std::cout << "No tree named \"Header\"" << std::endl; 
		return -1;
	}
	
	TBranch * branch_meta  = tree_header->GetBranch("Metadata");
	if(branch_meta == 0) {
		std::cout << "No branch named \"(Header) Metadata\"" << std::endl; 
		return -1;
	}
	branch_meta->SetAddress(&event_header);

	///////////////////////
	// LKr Active Volume //
	///////////////////////

	AnodeEvent * lkr_active_vol_event = new AnodeEvent();
	
	TTree * tree_lkr_active_vol = (TTree*)file->Get("LKr_Active_Volume");
	if(tree_lkr_active_vol == 0) {
		std::cout << "No tree named \"LKr_Active_Volume\"" << std::endl; 
		return -1;
	}

	TBranch * branch_lkr_hit  = tree_lkr_active_vol->GetBranch("Hits");
	if(branch_lkr_hit == 0) {
		std::cout << "No branch named \"(LKr_Active_Volume) Hits\"" << std::endl; 
	return -1;
	}
	branch_lkr_hit->SetAddress(&lkr_active_vol_event);
	
	/////////////////
	// NaI Crystal //
	/////////////////
	CrystalEvent * nai_event = new CrystalEvent();
	
	TTree * tree_nai = (TTree*)file->Get("NaI_Crystal");
	if(tree_nai == 0) {
		std::cout << "No tree named \"NaI_Crystal\"" << std::endl; 
		return -1;
	}

	TBranch * branch_nai_hit  = tree_nai->GetBranch("Hits");
	if(branch_nai_hit == 0) {
		std::cout << "No branch named \"(NaI_Crystal) Hits\"" << std::endl; 
	return -1;
	}
	branch_nai_hit->SetAddress(&nai_event);

	///////////////
	// 
	///////////////
	long nevents = tree_header->GetEntries();

	if(tree_nai->GetEntries() != nevents || tree_lkr_active_vol->GetEntries() != nevents) {
		std::cout << "Event numbers not are not consistent" << std::endl; 
		return -1;
	}

	TFile * out_file = TFile::Open(out_file_name.c_str(),"RECREATE");// output file default 
	if(out_file->IsOpen() == false) {
		std::cout << "Unable to create " << out_file_name << ", exiting." << std::endl;
		return -1;
	}


	///////////
	//
	///////////

	TH1D * h_NaI_Energy = new TH1D("h_NaI_Energy","NaI Energy Spectrum;E_{NaI} (MeV);Entries / keV",1000,0,1);
	TH1D * h_LKr_Energy = new TH1D("h_LKr_Energy","LKr Energy Spectrum;E_{LKr} (MeV);Entries / keV",1000,0,1);

	TH1D * h_LKr_DriftTime = new TH1D("h_LKr_DriftTime","NaI Time Spectrum (after PP and C4 cuts);T_{NaI};Entries / 10",2000,0,20000); // 4 ns bins

	 //double zpos_anode = 93.0; // mm (A TO BE CHECKED)
	 //double zpos_anode = 190.5; // mm (B TO BE CHECKED)
	 double zpos_anode = 93.0; // mm (C TO BE CHECKED)
	 
	 ROOT::Math::Random< ROOT::Math::MixMaxEngine<240,0> > random_eng;
	 //ROOT::Math::RandomMixMax random_eng;

	 random_eng.SetSeed(9749); 
	 double lambda = 1/10.0; // mm^-1

	 std::cout << "N events: " << nevents << std::endl;
	 for (int i = 0; i < nevents; i++) {
		                 tree_nai->GetEntry(i);
		                 tree_lkr_active_vol->GetEntry(i);
				 // Crude simulation of the trigger: we require a coincidence
				 if(nai_event->GetNHits() == 0 || lkr_active_vol_event->GetNHits() == 0) 
				 {
					std::cout << "FAIL TRIGGER" << std::endl; 
				 	continue;
				 }
				 // Plot energy spectrums 
				 h_NaI_Energy->Fill(nai_event->GetEnergy());
				
				double Eanode = 0.0;
			        // Note: Note the *!	
				for(auto && hit :  *lkr_active_vol_event) {
					//if(!hit.GetComptonFlag()) {
						Eanode += hit.GetEnergy();
					//}
				}

				 //h_LKr_Energy->Fill(lkr_active_vol_event->GetEnergy());
				 h_LKr_Energy->Fill(Eanode);
	 			 // Select A1 -- TODO
				 
				 double zpos = 0.;
				 double d_drift = 0.;
				 for(auto && hit :  *lkr_active_vol_event) {
					//if(!hit.GetComptonFlag()) {
						Eanode += hit.GetEnergy();
					//}
				}

 
				 
				 //double zpos = lkr_active_vol_event->GetZPos();
	 			 
				 //double d_drift = zpos_anode - zpos; // in mm
				 //double d_drift = zpos_anode - zpos; // in mm
				 
				 double rpt = random_eng.Uniform();
				 
				 //if(rpt < (1.0-TMath::Exp(lambda*-1*d_drift))) {
				 //	 continue;
				 //}
				 
				 double drift_velo = 25e5; // mm/s @ 1 kV
				 double t_drift = (d_drift/drift_velo)*1e9/4; // in "4 ns"
				 h_LKr_DriftTime->Fill(t_drift*-1.0+10000.0);
	 }

	h_LKr_DriftTime->Rebin(16);
	h_NaI_Energy->Write(); 
	h_LKr_Energy->Write();
	h_LKr_DriftTime->Write();
	out_file->Close();

}	
