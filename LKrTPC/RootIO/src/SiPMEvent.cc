#include "SiPMEvent.hh"

SiPMEvent::SiPMEvent() :  m_hits(0) {}

SiPMEvent::~SiPMEvent() {}

int SiPMEvent::GetNHits() const {
	return m_hits.size();
}

void SiPMEvent::Reset() {
	m_hits.clear();
}

void SiPMEvent::AddHit(SiPMHit & hit) {
	m_hits.push_back(hit);
} 
