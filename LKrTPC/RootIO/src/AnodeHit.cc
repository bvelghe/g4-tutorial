#include "AnodeHit.hh"

AnodeHit::AnodeHit() : m_energy(0.0), m_time(0.0), m_compton_flag(false), m_photoelectric_flag(false) {}

AnodeHit::~AnodeHit() {}

double AnodeHit::GetEnergy() const {
	return m_energy;
}

TVector3 AnodeHit::GetPosition() const {
	return m_position;
}

double AnodeHit::GetTime() const {
	return m_time;
}

bool AnodeHit::GetComptonFlag() const {
	return m_compton_flag;
}

bool AnodeHit::GetPhotoElectricFlag() const {
	return m_photoelectric_flag;
}
void AnodeHit::SetEnergy(double energy) {
	m_energy = energy;
}

void AnodeHit::SetPosition(TVector3 position) {
	m_position = position;
}

void AnodeHit::SetTime(double time) {
	m_time = time;
}

void AnodeHit::SetComptonFlag(bool flag) {
	m_compton_flag = flag;
}
void AnodeHit::SetPhotoElectricFlag(bool flag) {
	m_photoelectric_flag = flag;
}
