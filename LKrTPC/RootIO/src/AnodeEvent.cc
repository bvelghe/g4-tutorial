#include "AnodeEvent.hh"

AnodeEvent::AnodeEvent() :  m_hits(0) {}

AnodeEvent::~AnodeEvent() {}

int AnodeEvent::GetNHits() const {
	return m_hits.size();
}

void AnodeEvent::Reset() {
	m_hits.clear();
}

void AnodeEvent::AddHit(AnodeHit & hit) {
	m_hits.push_back(hit);
} 
