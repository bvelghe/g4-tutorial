#include "SiPMHit.hh"

SiPMHit::SiPMHit() : m_energy(0.0), m_time(0.0), m_station_id(0) {}

SiPMHit::~SiPMHit() {}

double SiPMHit::GetEnergy() const {
	return m_energy;
}

double SiPMHit::GetTime() const {
	return m_time;
}

unsigned int SiPMHit::GetStationID() const {
	return m_station_id;
}


void SiPMHit::SetEnergy(double energy) {
	m_energy = energy;
}

void SiPMHit::SetTime(double time) {
	m_time = time;
}

void SiPMHit::SetStationID(unsigned int station_id) {
	m_station_id = station_id;
} 
