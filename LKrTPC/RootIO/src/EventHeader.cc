#include "EventHeader.hh"

EventHeader::EventHeader() : m_event_id(-1) {}

EventHeader::~EventHeader() {}

long EventHeader::GetEventID() const {
	return m_event_id;
}

void EventHeader::SetEventID(long event_id) {
	m_event_id = event_id;
}
