#ifndef SiPMEvent_h
#define SiPMEvent_h 1

#include "TObject.h"
#include "SiPMHit.hh" 

class SiPMEvent : public TObject {
	public:
		SiPMEvent();
		virtual ~SiPMEvent();


		int GetNHits() const;
		void AddHit(SiPMHit & hit);
		void Reset();
		std::vector<SiPMHit>::iterator begin() {
			return m_hits.begin();
		}
		std::vector<SiPMHit>::iterator end() {
			return m_hits.end();
		}


	private:
		std::vector<SiPMHit> m_hits;	

	ClassDef(SiPMEvent,1); // Because we inherit from TObject 
};


#endif // SiPMEvent_h
