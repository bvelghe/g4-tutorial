#ifndef SiPMHit_h
#define SiPMHit_h 1

#include "TObject.h"

class SiPMHit : public TObject {
	public:
		SiPMHit();
		virtual ~SiPMHit();

		double GetEnergy() const;
		double GetTime() const;
		unsigned int GetStationID() const;

		void SetEnergy(double);
		void SetTime(double);
		void SetStationID(unsigned int);

	private:
		double m_energy; // MeV
		double m_time; // ns
		unsigned int m_station_id;  

	ClassDef(SiPMHit,1); // Because we inherit from TObject 
};


#endif // SiPMHit_h
