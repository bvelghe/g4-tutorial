#ifndef CrystalEvent_h
#define CrystalEvent_h 1

#include "TObject.h"

class CrystalEvent : public TObject {
	public:
		CrystalEvent();
		virtual ~CrystalEvent();

		double GetEnergy() const;
		double GetTime() const;
		int GetNHits() const;

		void SetEnergy(double);
		void SetTime(double);
		void SetNHits(int);

	private:
		double m_energy; // MeV
		double m_time; // ns		
		int m_nhits;

	ClassDef(CrystalEvent,1); // Because we inherit from TObject 
};


#endif // CrystalEvent_h
