#ifndef AnodeHit_h
#define AnodeHit_h 1

#include "TObject.h"
#include "TVector3.h" 

class AnodeHit : public TObject {
	public:
		AnodeHit();
		virtual ~AnodeHit();

		double GetEnergy() const;
		double GetTime() const;
		bool GetComptonFlag() const;
		TVector3 GetPosition() const;
		bool GetPhotoElectricFlag() const;


		void SetEnergy(double);
		void SetTime(double);
		void SetComptonFlag(bool);
		void SetPosition(TVector3);
		void SetPhotoElectricFlag(bool);

	private:
		double m_energy; // MeV
		double m_time; // ns
		bool m_compton_flag;
		bool m_photoelectric_flag;
		TVector3 m_position;
	ClassDef(AnodeHit,1); // Because we inherit from TObject 
};


#endif // AnodeHit_h
