#ifndef AnodeEvent_h
#define AnodeEvent_h 1

#include "TObject.h"
#include "AnodeHit.hh" 

class AnodeEvent : public TObject {
	public:
		AnodeEvent();
		virtual ~AnodeEvent();


		int GetNHits() const;
		void AddHit(AnodeHit & hit);
		void Reset();
		std::vector<AnodeHit>::iterator begin() {
			return m_hits.begin();
		}
		std::vector<AnodeHit>::iterator end() {
			return m_hits.end();
		}


	private:
		std::vector<AnodeHit> m_hits;	

	ClassDef(AnodeEvent,1); // Because we inherit from TObject 
};


#endif // AnodeEvent_h
