//#include "G4UIQt.hh"

#include <G4MTRunManager.hh>
#include "G4RunManager.hh"

#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "UserActionInitialization.hh"
//#include "MyUserRunAction.hh"


#include "G4RunManagerFactory.hh"

#include "G4UImanager.hh"

#include "G4UIExecutive.hh"
#include "G4VisExecutive.hh"



int main(int argc, char * argv[]) 
{

	///////////////////////
	// G4 Initialisation //
	///////////////////////

	auto * runManager = G4RunManagerFactory::CreateRunManager(G4RunManagerType::Default);
#ifdef G4MULTITHREADED
    runManager->SetNumberOfThreads(10);
#endif
    // Instances are deleted automatically when G4RunManager is deleted.
    auto * detector = new DetectorConstruction();
    runManager->SetUserInitialization(detector);

    auto * physics = new PhysicsList();
    runManager->SetUserInitialization(physics);

    auto action_initialization = new UserActionInitialization();
    runManager->SetUserInitialization(action_initialization);

	//////////////////////////////////
	// Initialize the Geant4 kernel //
	//////////////////////////////////

	runManager->Initialize();

	//////////////////////////////////////
	// User interface and visualization // 	
	//////////////////////////////////////

	G4VisManager* visManager = new G4VisExecutive;
	visManager->Initialize();

	// Get the pointer to the User Interface manager
	G4UImanager * UImanager = G4UImanager::GetUIpointer(); 
	G4UIExecutive * ui = 0;
	if (argc!=1)   // batch mode  
	{
		G4String command = "/control/execute ";
		G4String fileName = argv[1];
		UImanager->ApplyCommand(command+fileName);
	}
	else           // interactive mode : define UI session  
	{
		ui = new G4UIExecutive(argc,argv);
		ui->SessionStart();
	}
	////////////////////
	// Clean and exit //
	////////////////////

	if(ui != 0) delete ui;	

	delete visManager;
	delete runManager; // Also delete the user initializations/actions


	
	return 0;
}
