#ifndef UserEventAction_h
#define UserEventAction_h 1

#include "G4UserEventAction.hh"

#include "G4Event.hh"
#include "G4TrajectoryContainer.hh"

#include "UserTrajectory.hh"
#include "G4AutoLock.hh"
#include "RootIO.hh"

namespace {
    G4Mutex aMutex=G4MUTEX_INITIALIZER;
}

class UserEventAction : public G4UserEventAction
{
	public:
           UserEventAction();
           ~UserEventAction();
           
	   void BeginOfEventAction(const G4Event*);
           void EndOfEventAction(const G4Event*);
          
	private:
	   ///////////
	   // Debug //
	   ///////////
	   G4int m_hits_collection_ID;
	   LKrActiveVolumeG4HitsCollection * m_hits_collection;
	   G4SDManager * m_SD_manager;
	

};

#endif // UserEventAction_h
