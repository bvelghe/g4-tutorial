#ifndef SiPMSD_h
#define SiPMSD_h 1

#include "G4VSensitiveDetector.hh"
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "SiPMG4Hit.hh"

class SiPMSD : public G4VSensitiveDetector {
	public:
		SiPMSD(G4String,G4int);
		~SiPMSD();
		void Initialize(G4HCofThisEvent*);
		G4bool ProcessHits(G4Step*, G4TouchableHistory*);
        G4bool ProcessHits(const G4Step*, G4TouchableHistory*);

private:
		G4int fCopy;
		G4int fHCID; //Hits collection ID
		SiPMG4HitsCollection * fHitsCollection; // see SiPMG4Hit.hh
};

#endif // SiPMSD_h
