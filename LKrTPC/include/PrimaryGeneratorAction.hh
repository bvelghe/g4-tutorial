#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "GammaSource.hh"
#include "G4SystemOfUnits.hh"

class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    PrimaryGeneratorAction();
    ~PrimaryGeneratorAction();
    //////////////
    // Required //
    //////////////
    void GeneratePrimaries(G4Event* anEvent);
  private:
     GammaSource * fGammaSource;
};

#endif // PrimaryGeneratorAction_h 
