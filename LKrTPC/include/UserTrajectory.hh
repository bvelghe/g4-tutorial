#ifndef UserTrajectory_h
#define UserTrajectory_h 1

#include "G4Allocator.hh"
#include "G4VTrajectory.hh"

#include "G4ParticleDefinition.hh"
#include "UserTrajectoryPoint.hh"
#include "G4Track.hh"

class UserTrajectory : public G4VTrajectory
{
	public:
		UserTrajectory();
		UserTrajectory(const G4Track *);
		UserTrajectory(G4ThreeVector);
		virtual ~UserTrajectory();

		inline void * operator new(size_t);
		inline void operator delete(void*);
		

		inline G4int GetTrackID() const {
			return fTrackID;
		}
		inline G4int GetParentID() const {
			return fParentID;
		}
		inline G4String GetParticleName() const {
			return fParticleName;
		}
		inline G4double GetCharge() const {
			return fPDGCharge;
		}
		inline G4int GetPDGEncoding() const {
			return fPDGEncoding;
		}
		inline G4double GetInitialKineticEnergy() const {
			return fInitialKineticEnergy;
		}
		inline G4ThreeVector GetInitialMomentum() const {
			return fInitialMomentum;
		}

		virtual int GetPointEntries() const {
			return fPositionRecord->size();
		}

		virtual void MergeTrajectory(G4VTrajectory * trajectory);
		virtual void AppendStep(const G4Step * step);
		virtual G4VTrajectoryPoint * GetPoint(G4int i) const {
			return (*fPositionRecord)[i];
		}	

	private:

		std::vector< UserTrajectoryPoint *> * fPositionRecord;
		G4int fTrackID;
		G4int fParentID;
		G4int fPDGEncoding;
		G4double fPDGCharge;
		G4double fInitialKineticEnergy;
		G4String fParticleName;
		G4ThreeVector fInitialMomentum;
		G4ThreeVector fInitialPosition;
};



extern G4ThreadLocal G4Allocator<UserTrajectory> * fTrajectoryAlloc;


inline void * UserTrajectory::operator new(size_t) {
    if(!fTrajectoryAlloc) fTrajectoryAlloc = new G4Allocator<UserTrajectory>;
	void * trajectory = (void *) fTrajectoryAlloc->MallocSingle();
	return trajectory;
}

inline void UserTrajectory::operator delete(void * trajectory) {
	fTrajectoryAlloc->FreeSingle((UserTrajectory *) trajectory);
}



#endif
