#ifndef USERACTIONINITIALIZATION_HH
#define USERACTIONINITIALIZATION_HH

#include "G4VUserActionInitialization.hh"
#include "PrimaryGeneratorAction.hh"
#include "UserEventAction.hh"
#include "UserTrackingAction.hh"
#include "LKrUserSteppingAction.hh" //Rename me
#include "UserRunAction.hh"

#include "RootIO.hh"

class UserActionInitialization : public G4VUserActionInitialization {
public:
    UserActionInitialization();
    ~UserActionInitialization();
    void Build() const;
    void BuildForMaster() const;
private:
    RootIO * m_root_io;

};

#endif //USERACTIONINITIALIZATION_HH
