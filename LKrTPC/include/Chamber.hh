#ifndef __Chamber_HH__
#define __Chamber_HH__

#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Box.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4SystemOfUnits.hh"

#include "G4SDManager.hh"

class Chamber : public G4VUserDetectorConstruction {
	public:  	
		Chamber(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy);
		~Chamber();	
		// Note: Construct is a pure virtual method of G4VUserDetectorConstruction we have to implement it.
		G4VPhysicalVolume * Construct();
		
		void SetProperties(); 
	private:

		G4LogicalVolume* fMotherLogicalVolume; 		
		G4ThreeVector fPosition;
		G4int fCopy;

		G4Box * fChamberSolidVolume; 
		G4LogicalVolume * fChamberLogicalVolume;
		G4VPhysicalVolume * fChamberPhysicalVolume;
		G4VisAttributes * fChamberVisAttributes;

};

#endif // __Chamber_HH___
