#ifndef __UserReadOutGeometry_HH__
#define __UserReadOutGeometry_HH__

#include "G4VReadOutGeometry.hh"

#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4SystemOfUnits.hh"


#include "G4SDManager.hh"
#include "LKrActiveVolumeSD.hh"

class UserReadOutGeometry : public G4VReadOutGeometry {
	public:  	
		UserReadOutGeometry();
		~UserReadOutGeometry();	
		G4VPhysicalVolume * Build();
		
		void SetZPos(G4double);

	private:
		G4Box * fROSolidVolume;
		G4LogicalVolume * fROLogicalVolume;
		G4VPhysicalVolume * fROPhysicalVolume;
	
};

#endif // __UserReadOutGeometry_HH___
