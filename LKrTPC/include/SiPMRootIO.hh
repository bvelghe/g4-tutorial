#ifndef SiPMRootIO_h
#define SiPMRootIO_h 1

#include "SiPMEvent.hh"

#include "TTree.h"
#include "TBranch.h"

#include "G4Event.hh"

#include "SiPMG4Hit.hh" // "G4" Hit Definition != "Physics" Hit
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"


class SiPMRootIO {
	public:
		SiPMRootIO(); 
		~SiPMRootIO();
		
		bool Initialize(TTree *);
		void SaveEvent(const G4Event *);
	private:
		static const int c_n_sipm = 8;
		bool m_ready;
		TTree * m_tree;
		SiPMEvent * m_event;
		TBranch * m_branch;
		
		G4int m_hits_collection_ID[c_n_sipm];
		SiPMG4HitsCollection * m_hits_collection[c_n_sipm];
		G4SDManager * m_SD_manager;
	
};

#endif // SiPMRootIO_h
