#ifndef UserTrajectoryPoint_h
#define UserTrajectoryPoint_h 1

#include "G4Step.hh"
#include "G4VProcess.hh" 
#include "G4Allocator.hh"
#include "G4VTrajectoryPoint.hh"


class UserTrajectoryPoint : public G4VTrajectoryPoint
{
	public:
		UserTrajectoryPoint();
		UserTrajectoryPoint(const G4Step *);
		virtual ~UserTrajectoryPoint();
		//Copy constructor? Will be stored in a std::vector (pointer)

		inline void *operator new(size_t);
		inline void operator delete(void *);
	
		inline const G4ThreeVector GetPosition() const {
			return fPosition;
		}

		inline G4double GetEnergy() const {
			return fEnergy;
		}

		inline G4bool IsCompton() const {
			return fCompton;
		}

	private:
		G4ThreeVector fPosition;	
		G4double fEnergy;
		G4bool fCompton;
};


extern G4ThreadLocal G4Allocator<UserTrajectoryPoint> * fTrajectoryPointAlloc;



inline void * UserTrajectoryPoint::operator new(size_t) {
    if(!fTrajectoryPointAlloc) fTrajectoryPointAlloc = new G4Allocator<UserTrajectoryPoint>;
	void * trajectoryPoint = (void *) fTrajectoryPointAlloc->MallocSingle();
	return trajectoryPoint;
}

inline void UserTrajectoryPoint::operator delete(void * trajectoryPoint) {
	fTrajectoryPointAlloc->FreeSingle((UserTrajectoryPoint *) trajectoryPoint);
}

#endif
