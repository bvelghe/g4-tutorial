#ifndef UserTrackingAction_h
#define UserTrackingAction_h 1

#include "G4TrackingManager.hh"
#include "G4UserTrackingAction.hh"
#include "UserTrackInformation.hh"
#include "G4Track.hh"
#include "UserTrajectory.hh"

class UserTrackingAction : public G4UserTrackingAction
{
	public:

	// Virtual function called through base class pointers/references will be resolved at run-time. =0 --> Pure virtual function, cannot be instantiated, must be implemented in the derived class.

	// Make sure that both the base class and derived class destructor are called
	
		UserTrackingAction();
		virtual ~UserTrackingAction();

		virtual void PreUserTrackingAction(const G4Track *);
		virtual void PostUserTrackingAction(const G4Track *);

	private:
		UserTrajectory * fUserTrajectory;

};

#endif
