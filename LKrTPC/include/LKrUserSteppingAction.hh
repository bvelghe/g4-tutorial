#ifndef LKrUserSteppingAction_h
#define LKrUserSteppingAction_h 1

#include "G4UserSteppingAction.hh" 
#include "G4OpBoundaryProcess.hh" 
#include "G4ProcessManager.hh" 
#include "UserTrackInformation.hh"

// Note: Cannot use UserSteppingAction (--> Method in G4UserSteppingAction)

class LKrUserSteppingAction : public G4UserSteppingAction {
	public:
		LKrUserSteppingAction();
		virtual ~LKrUserSteppingAction();
		virtual void UserSteppingAction(const G4Step *);
};

#endif // LKrUserSteppingAction_h 
