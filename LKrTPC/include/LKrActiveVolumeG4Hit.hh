#ifndef LKrActiveVolumeG4Hit_h
#define LKrActiveVolumeG4Hit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4ThreeVector.hh" 

class LKrActiveVolumeG4Hit : public G4VHit {
	public:
		LKrActiveVolumeG4Hit();
		~LKrActiveVolumeG4Hit();
		//
		LKrActiveVolumeG4Hit(const LKrActiveVolumeG4Hit&);
		const LKrActiveVolumeG4Hit& operator=(const LKrActiveVolumeG4Hit&);
	  	G4int operator==(const LKrActiveVolumeG4Hit&) const;
		//
		//void Draw();
		//void Print();
		inline void Energy(G4double energy) { fEnergy = energy; } 
		inline void AddEnergy(G4double energy) { fEnergy += energy; }
		inline void Position(G4ThreeVector pos) { 
			fPosition = pos;
		}
		inline void ComptonFlag(G4bool flag) { fComptonFlag = flag; }
		inline void PhotoElectronFlag(G4bool flag) { fPhotoElectronFlag = flag; }


		// Return the total energy
		inline G4double Energy(void) { return fEnergy;}
		// Return the voxel position
		inline G4ThreeVector Position() {
			return fPosition; 
		}
		inline G4bool ComptonFlag() { return fComptonFlag; }		
		inline G4bool PhotoElectronFlag() { return fPhotoElectronFlag; }		

	        
	private:
		G4double fEnergy;
		G4ThreeVector fPosition;
		G4bool fComptonFlag;
		G4bool fPhotoElectronFlag;
};

//Hits storage 
typedef G4THitsCollection<LKrActiveVolumeG4Hit> LKrActiveVolumeG4HitsCollection;

#endif // LKrActiveVolumeG4Hit_h
