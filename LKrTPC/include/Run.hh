#ifndef LKRTPC_RUN_HH
#define LKRTPC_RUN_HH

#include "G4Run.hh"
#include "G4SDManager.hh"
#include "RootIO.hh"

class Run : public G4Run {
public:
    Run(RootIO * root_io);
    Run();
    ~Run();

    void RecordEvent(const G4Event *);
    void Merge(const G4Run *);
private:
    RootIO * m_root_io;
};


#endif //LKRTPC_RUN_HH
