#ifndef __SiPM_HH__
#define __SiPM_HH__

#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Box.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4SystemOfUnits.hh"

#include "G4SDManager.hh"
#include "SiPMSD.hh"


enum class Orientation { kLeft, kTop, kRight, kBottom }; 

class SiPM : public G4VUserDetectorConstruction {
	public:  	
		SiPM(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, Orientation orientation ,G4int copy);
		~SiPM();	
		// Note: Construct is a pure virtual method of G4VUserDetectorConstruction we have to implement it.
		G4VPhysicalVolume * Construct();
        void ConstructSDandField();

        void SetProperties();
	private:

		G4LogicalVolume* fMotherLogicalVolume; 		
		G4ThreeVector fPosition;
		G4int fCopy;

		Orientation fOrientation;

		G4Box * fSiPMSolidVolume; 
		G4LogicalVolume * fSiPMLogicalVolume;
	
		G4VPhysicalVolume * fSiPMPhysicalVolume;
		
		G4VisAttributes * fSiPMVisAttributes;

};

#endif // __SiPM_HH___
