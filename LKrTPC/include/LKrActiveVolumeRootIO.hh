#ifndef LKrActiveVolumeRootIO_h
#define LKrActiveVolumeRootIO_h 1

#include "AnodeEvent.hh"

#include "TTree.h"
#include "TBranch.h"

#include "G4Event.hh"

#include "LKrActiveVolumeG4Hit.hh" // "G4" Hit Definition != "Physics" Hit
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4SystemOfUnits.hh"

class LKrActiveVolumeRootIO {
	public:
		LKrActiveVolumeRootIO(); 
		~LKrActiveVolumeRootIO();
		
		bool Initialize(TTree *);
		void SaveEvent(const G4Event *);
	private:
		bool m_ready;
		TTree * m_tree;
		AnodeEvent * m_event;
		TBranch * m_branch;
		
		G4int m_hits_collection_ID;
		LKrActiveVolumeG4HitsCollection * m_hits_collection;
		G4SDManager * m_SD_manager;
	
};

#endif // LKrActiveVolumeRootIO_h
