#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"

#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"

#include "G4ThreeVector.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4OpticalSurface.hh"

#include "Crystal.hh"
#include "LKrActiveVolume.hh"
#include "SiPM.hh" 

class DetectorConstruction : public G4VUserDetectorConstruction 
{
	public:
		DetectorConstruction();
		~DetectorConstruction();
		void SetProperties();
		G4VPhysicalVolume * Construct();
		void ConstructSDandField();

	private:
		G4NistManager * fNistMan;

		G4Box * fWorldSolidVolume;
		G4LogicalVolume * fWorldLogicalVolume;
		G4VPhysicalVolume * fWorldPhysicalVolume;

		G4Box * fExternalWindowSolidVolume;
		G4LogicalVolume * fExternalWindowLogicalVolume; 
		G4VPhysicalVolume * fExternalWindowPhysicalVolume;
		
		G4Box * fInsulationVacuumSolidVolume;
		G4LogicalVolume * fInsulationVacuumLogicalVolume; 
		G4VPhysicalVolume * fInsulationVacuumPhysicalVolume;

		G4Box * fInternalWindowSolidVolume;
		G4LogicalVolume * fInternalWindowLogicalVolume; 
		G4VPhysicalVolume * fInternalWindowPhysicalVolume;

		G4Box * fPassiveLKrSolidVolume;
		G4LogicalVolume * fPassiveLKrLogicalVolume; 
		G4VPhysicalVolume * fPassiveLKrPhysicalVolume;
		
		Crystal * fNaICrystal;
		LKrActiveVolume * fLKrActiveVolume;

		SiPM * fSiPM[8];
		G4LogicalBorderSurface * fSiPMSurface[8];
		G4LogicalBorderSurface * fLKrSurface;
		G4LogicalBorderSurface * fCathodeSurface;

		G4VisAttributes * fExternalWindowVisAttributes;
		G4VisAttributes * fInsulationVacuumVisAttributes;
		G4VisAttributes * fInternalWindowVisAttributes;
		G4VisAttributes * fPassiveLKrVisAttributes;

};

#endif // DetectorConstruction_h
