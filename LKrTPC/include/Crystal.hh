#ifndef __Crystal_HH__
#define __Crystal_HH__

#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Tubs.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4SystemOfUnits.hh"

#include "G4SDManager.hh"

#include "CrystalSD.hh"

class Crystal : public G4VUserDetectorConstruction {
	public:  	
		Crystal(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy);
		~Crystal();	
		// Note: Construct is a pure virtual method of G4VUserDetectorConstruction we have to implement it.
		G4VPhysicalVolume * Construct();
        // For MT
		void ConstructSDandField();

    void SetProperties();
	private:

		G4LogicalVolume* fMotherLogicalVolume; 		
		G4ThreeVector fPosition;
		G4int fCopy;
		
		G4Tubs * fCrystalSolidVolume;

		G4LogicalVolume * fCrystalLogicalVolume;
	
		G4VPhysicalVolume * fCrystalPhysicalVolume;
		
		G4VisAttributes * fCrystalVisAttributes;

};

#endif // __Crystal_HH___
