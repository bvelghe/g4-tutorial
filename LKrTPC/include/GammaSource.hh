#ifndef GammaSource_h
#define GammaSource_h 1

#include "G4VPrimaryGenerator.hh"  
#include "G4RandomDirection.hh" 
#include "G4PrimaryParticle.hh"
#include "G4PrimaryVertex.hh" 
#include "G4SystemOfUnits.hh" 
#include "G4Event.hh"

class GammaSource : public G4VPrimaryGenerator {
	public:
		GammaSource();
		~GammaSource();

		virtual void GeneratePrimaryVertex(G4Event *);	
	private:
};

#endif // GammaSource_h 
