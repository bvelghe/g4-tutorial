#ifndef USERRUNACTION_HH
#define USERRUNACTION_HH

#include "G4UserRunAction.hh"
#include "Run.hh"
#include "G4Threading.hh"

#include "RootIO.hh"

class UserRunAction : public G4UserRunAction{
public:
    UserRunAction();
    ~UserRunAction();
    G4Run * GenerateRun();
    void BeginOfRunAction(const G4Run *aRun);
    void EndOfRunAction(const G4Run *aRun);

private:
    RootIO * m_root_io;
};


#endif //USERRUNACTION_HH
