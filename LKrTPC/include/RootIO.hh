#ifndef RootIO_h
#define RootIO_h 1

#include <iostream>
#include <string>

#include "HeaderRootIO.hh"
#include "CrystalRootIO.hh"
#include "LKrActiveVolumeRootIO.hh"
#include "SiPMRootIO.hh"

#include "TFile.h"
#include "TTree.h"




class RootIO {
	public:
		RootIO();
		virtual ~RootIO();
	
		bool StartRun(long,std::string);
		void EndRun();
		void SaveEvent(const G4Event *);

	private:
		// RootIO is non copyable
		RootIO(const RootIO &); // Disable the copy constructor
		RootIO & operator=(const RootIO &); // Disable the assignment operator

		bool Open(std::string);		
		void Close();
		
		TFile * m_out_file;

		// FIXME This is bad arch. Separate sub-det. Root tree
		TTree * m_header_tree; 
		TTree * m_crystal_tree; 
		TTree * m_lkr_active_volume_tree;
		TTree * m_sipm_tree;

		double m_d;
		HeaderRootIO * m_header_io;
		CrystalRootIO * m_crystal_io;	
		LKrActiveVolumeRootIO * m_lkr_active_volume_io;
		SiPMRootIO * m_sipm_io;

};

#endif // RootIO_h
