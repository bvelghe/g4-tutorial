#ifndef __USERTRACKINFORMATION_HH__
#define __USERTRACKINFORMATION_HH__

#include "G4VUserTrackInformation.hh"
#include "G4Allocator.hh"

class UserTrackInformation : public G4VUserTrackInformation {
	public:
		inline void * operator new(size_t);
		inline void operator delete(void *);
		inline int operator==(const UserTrackInformation & right) const {
			return (this==&right);
		}
		
		UserTrackInformation();
		UserTrackInformation(const UserTrackInformation*);
		UserTrackInformation & operator=(const UserTrackInformation &);
		virtual ~UserTrackInformation();

		inline G4bool GetComptonFlag() { return fComptonScatteringFlag; }
		inline void SetComptonFlag(G4bool flag) { fComptonScatteringFlag = flag; }

		inline G4bool GetPhotoElectricFlag() { return fPhotoElectricFlag; }
		inline void SetPhotoElectricFlag(G4bool flag) { fPhotoElectricFlag = flag; }
		
	private:
		G4bool fComptonScatteringFlag;
		G4bool fPhotoElectricFlag;
};

extern G4ThreadLocal G4Allocator<UserTrackInformation> * aUserTrackInformationAllocator;

inline void * UserTrackInformation::operator new(size_t) {
	if(!aUserTrackInformationAllocator) {
		aUserTrackInformationAllocator = new G4Allocator<UserTrackInformation>;
	}
	return (void*)aUserTrackInformationAllocator->MallocSingle();
}

inline void UserTrackInformation::operator delete(void * aTrackInfo) {
	aUserTrackInformationAllocator->FreeSingle((UserTrackInformation*)aTrackInfo);
}

#endif
