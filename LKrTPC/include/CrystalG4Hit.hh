#ifndef CrystalG4Hit_h
#define CrystalG4Hit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"

class CrystalG4Hit : public G4VHit {
	public:
		CrystalG4Hit();
		~CrystalG4Hit();
		//
		CrystalG4Hit(const CrystalG4Hit&);
		const CrystalG4Hit& operator=(const CrystalG4Hit&);
	  	G4int operator==(const CrystalG4Hit&) const;
		//
		//void Draw();
		//void Print();
		//
		inline void Energy(G4double energy) { fEnergy = energy;} 
		inline void AddEnergy(G4double energy) { fEnergy += energy;}
		//
		G4double Energy(void) { return fEnergy;}
		//

		inline void * operator new(size_t);
		inline void operator delete(void *);

	private:
		G4double fEnergy;
};

//Hits storage 
typedef G4THitsCollection<CrystalG4Hit> CrystalG4HitsCollection;
extern G4ThreadLocal G4Allocator<CrystalG4Hit>* CrystalG4HitAllocator;


inline void* CrystalG4Hit::operator new(size_t)
{
    if (!CrystalG4HitAllocator) {
        CrystalG4HitAllocator = new G4Allocator<CrystalG4Hit>;
    }
    void *hit;
    hit = (void *) CrystalG4HitAllocator->MallocSingle();
    return hit;
}

inline void CrystalG4Hit::operator delete(void *hit)
{
    if (!CrystalG4HitAllocator) {
        CrystalG4HitAllocator = new G4Allocator<CrystalG4Hit>;
    }
    CrystalG4HitAllocator->FreeSingle((CrystalG4Hit*) hit);
}


#endif // CrystalG4Hit_h
