#ifndef SiPMG4Hit_h
#define SiPMG4Hit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"

class SiPMG4Hit : public G4VHit {
	public:
		SiPMG4Hit();
		~SiPMG4Hit();
		//
		SiPMG4Hit(const SiPMG4Hit&);
		const SiPMG4Hit& operator=(const SiPMG4Hit&);
	  	G4int operator==(const SiPMG4Hit&) const;
		//
		//void Draw();
		//void Print();
		//
		inline void WaveLength(G4double wavelength) { fWaveLength = wavelength;} 
		G4double WaveLength(void) { return fWaveLength;}

        inline void * operator new(size_t);
        inline void operator delete(void *);
	private:
		G4double fWaveLength;
};

//Hits storage 
typedef G4THitsCollection<SiPMG4Hit> SiPMG4HitsCollection;
extern G4ThreadLocal G4Allocator<SiPMG4Hit>* SiPMG4HitAllocator;


inline void* SiPMG4Hit::operator new(size_t)
{
    if (!SiPMG4HitAllocator) {
        SiPMG4HitAllocator = new G4Allocator<SiPMG4Hit>;
    }
    void *hit;
    hit = (void *) SiPMG4HitAllocator->MallocSingle();
    return hit;
}

inline void SiPMG4Hit::operator delete(void *hit)
{
    if (!SiPMG4HitAllocator) {
        SiPMG4HitAllocator = new G4Allocator<SiPMG4Hit>;
    }
    SiPMG4HitAllocator->FreeSingle((SiPMG4Hit*) hit);
}


#endif // SiPMG4Hit_h
