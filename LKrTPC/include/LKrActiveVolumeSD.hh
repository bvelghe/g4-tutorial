#ifndef LKrActiveVolumeSD_h
#define LKrActiveVolumeSD_h 1

#include "G4VSensitiveDetector.hh"
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "LKrActiveVolumeG4Hit.hh"
#include "G4OpticalPhoton.hh"
#include <vector>

#include "UserTrackInformation.hh" 

class LKrActiveVolumeSD : public G4VSensitiveDetector {
	public:
		LKrActiveVolumeSD(G4String,G4int);
		~LKrActiveVolumeSD();
		void Initialize(G4HCofThisEvent*);
		G4bool ProcessHits(G4Step*, G4TouchableHistory*);
	private:
		G4int fCopy;
		G4int fHCID; //Hits collection ID
		std::vector<G4int> fReplicaMap;
		LKrActiveVolumeG4HitsCollection * fHitsCollection; // see LKrActiveVolumeG4Hit.hh
};

#endif // LKrActiveVolumeSD_h
