#ifndef __LKrActiveVolume_HH__
#define __LKrActiveVolume_HH__

#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4VisAttributes.hh"
#include "G4SystemOfUnits.hh"


#include "G4SDManager.hh"

#include "LKrActiveVolumeSD.hh"

class LKrActiveVolume : public G4VUserDetectorConstruction {
	public:  	
		LKrActiveVolume(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy);
		~LKrActiveVolume();	
		// Note: Construct is a pure virtual method of G4VUserDetectorConstruction we have to implement it.
		G4VPhysicalVolume * Construct();
        void ConstructSDandField();

        void SetProperties();
	private:

		G4LogicalVolume* fMotherLogicalVolume; 		
		G4ThreeVector fPosition;
		G4int fCopy;
		
		G4Box * fLKrSolidVolume;
		G4LogicalVolume * fLKrLogicalVolume;
		G4VPhysicalVolume * fLKrPhysicalVolume;
		
		G4Box * fLKrSolidVolume_Voxel;
		G4LogicalVolume * fLKrLogicalVolume_Voxel;
		G4VPhysicalVolume * fLKrPhysicalVolume_Voxel;
		
		G4Tubs * fLKrSolidVolume_VoxelAnode;
		G4LogicalVolume * fLKrLogicalVolume_VoxelAnode;
		G4VPhysicalVolume * fLKrPhysicalVolume_VoxelAnode;
	
		G4VisAttributes * fLKrVisAttributes;

		G4Material * fLKrMat; 
};

#endif // __LKrActiveVolume_HH___
