//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// --------------------------------------------------------------
// History:
//
// 2009-03-12 S.Bifani
// - Added the collimator before station 3
//
// 2009-03-03 S.Bifani
// - Removed the alluminum foil
// - Changed the geometrical configuration according to the 2008/11/24 BEATCH file
//   (x -> y deflection and 5th magnet)
//
// 2008-04-22 S.Bifani (Simone.Bifani@cern.ch)
// - Added 3 stations and 4 magnets
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2008-03-30
// --------------------------------------------------------------
//



#include "globals.hh"
#include "GigaTrackerGeometryParameters.hh"
#include "GigaTrackerMaterialParameters.hh"
#include "GigaTrackerDetector.hh"
#include "GigaTrackerSD.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4Material.hh"
#include "G4SDManager.hh"

GigaTrackerDetector::GigaTrackerDetector(G4Material * Material, G4LogicalVolume * MotherVolume) : 
NA62VComponent(Material,MotherVolume), NA62VNamed("GigaTracker")
{

  // Mandatory here to Find or Build the needed materials
  GigaTrackerMaterialParameters::GetInstance();

}

GigaTrackerDetector::~GigaTrackerDetector() {}

void GigaTrackerDetector::ReadGeometryParameters()
{

  // Read all the geometrical parameters and copy them to private members
  GigaTrackerGeometryParameters* GeoPars = GigaTrackerGeometryParameters::GetInstance();

  fGigaTrackerSensitiveDetectorName = GeoPars->GetGigaTrackerSensitiveDetectorName();
  fGigaTrackerCollectionName = GeoPars->GetGigaTrackerCollectionName();

  fXLength = GeoPars->GetGigaTrackerDetectorXLength();
  fYLength = GeoPars->GetGigaTrackerDetectorYLength();
  fZLength = GeoPars->GetGigaTrackerDetectorZLength();

  fZPosition = GeoPars->GetGigaTrackerDetectorZPosition();

  fStation1Position = GeoPars->GetGigaTrackerStation1Position();
  fStation2Position = GeoPars->GetGigaTrackerStation2Position();
  fStation3Position = GeoPars->GetGigaTrackerStation3Position();

  fMagnet1Position = GeoPars->GetGigaTrackerMagnet1Position();
  fMagnet2Position = GeoPars->GetGigaTrackerMagnet2Position();
  fMagnet3Position = GeoPars->GetGigaTrackerMagnet3Position();
  fMagnet4Position = GeoPars->GetGigaTrackerMagnet4Position();
  fMagnet5Position = GeoPars->GetGigaTrackerMagnet5Position();

  fCollimatorPosition = GeoPars->GetGigaTrackerCollimatorPosition();

}

void GigaTrackerDetector::CreateGeometry()
{
  ReadGeometryParameters();

  // Sensitive detector: it would be associated to smaller volume/s inside the global box/es
	
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  GigaTrackerSD * GTKSD = (GigaTrackerSD*)SDman->FindSensitiveDetector(fGigaTrackerSensitiveDetectorName);
  if(!GTKSD) {
    GTKSD = new GigaTrackerSD(fGigaTrackerSensitiveDetectorName, fGigaTrackerCollectionName);
    SDman->AddNewDetector(GTKSD);
  }

  // G4 volumes

  G4double HalfXLength = 0.5 * fXLength;
  G4double HalfYLength = 0.5 * fYLength;
  G4double HalfZLength = 0.5 * fZLength;

  //G4ThreeVector Position(0., 0., fZPosition);

  G4ThreeVector Position(0., 0., 0.);
  fSolidVolume= new G4Box("GigaTracker", HalfXLength, HalfYLength, HalfZLength);
   
  fLogicalVolume= new G4LogicalVolume(fSolidVolume,  // solid
				      fMaterial,     // material
				      "GigaTracker", // name
				      0,             // field manager 
				      0,             // sensitive detector
				      0);            // user limits

  fPhysicalVolume = new G4PVPlacement(0,
				      Position,
				      fLogicalVolume, // its logical volume
				      "GigaTracker",  // its name
				      fMotherVolume,  // its mother  volume
				      false,          // no boolean operations
				      0);             // copy number


  // Create stations
  fStation1 = new GigaTrackerStation(G4Material::GetMaterial("G4_Galactic"), fLogicalVolume, fStation1Position, 0);
  //fStation2 = new GigaTrackerStation(G4Material::GetMaterial("G4_Galactic"), fLogicalVolume, fStation2Position, 1);
  //fStation3 = new GigaTrackerStation(G4Material::GetMaterial("G4_Galactic"), fLogicalVolume, fStation3Position, 2);


  // Create magnets
  //fMagnet1 = new GigaTrackerMCBMagnet(G4Material::GetMaterial("G4_Galactic"), fLogicalVolume, fMagnet1Position, 0,0);
  //fMagnet2 = new GigaTrackerMCBMagnet(G4Material::GetMaterial("G4_Galactic"), fLogicalVolume, fMagnet2Position, 1,0);
  //fMagnet3 = new GigaTrackerMCBMagnet(G4Material::GetMaterial("G4_Galactic"), fLogicalVolume, fMagnet3Position, 2,0);
  //fMagnet4 = new GigaTrackerMCBMagnet(G4Material::GetMaterial("G4_Galactic"), fLogicalVolume, fMagnet4Position, 3,1); //FIXME
  //fMagnet5 = new GigaTrackerMDXMagnet(G4Material::GetMaterial("G4_Galactic"), fLogicalVolume, fMagnet5Position, 0);


  // Add GridCam before the collimator
  //G4ThreeVector GridCamPos = G4ThreeVector(0.0,0.0,+55.0*cm)+fCollimatorPosition;
  //fGridCam1 = new GridCam(fLogicalVolume,GridCamPos,0);
  //fGridCam1->Construct();
  //fGridCam1->SetProperties();
  //fGridCam1->PrintGeometry();
  // Create collimator
  //fCollimator = new GigaTrackerCollimator(G4Material::GetMaterial("G4_Fe"), fLogicalVolume, fCollimatorPosition, 0);

  //G4ThreeVector TracksCamPos = G4ThreeVector(0.0,0.0,0.0)+fStation3Position;
	//fTracksCam1 = new TracksCamera(fLogicalVolume,30.0*cm,TracksCamPos,0);
	//fTracksCam1->Construct();
	//fTracksCam1->SetProperties();

  SetProperties();
}

void GigaTrackerDetector::SetProperties()
{
  // Set visualization properties
  fVisAtt = new G4VisAttributes(G4Colour(1.0, 1.0, 1.0));
  fVisAtt->SetVisibility(false);
  fLogicalVolume->SetVisAttributes(fVisAtt);

}
