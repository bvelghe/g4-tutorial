//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// --------------------------------------------------------------
// History:
//
// 2012-03-09 B.Velghe (bob.velghe@cern.ch)
// - String concatenation : Remplace ROOT Form() method by "std::stringstream" etc.
//
//
// 2009-03-03 S.Bifani
// - Removed the alluminum foil
// - Changed the geometrical configuration according to the 2008/11/24 BEATCH file
//   (x -> y deflection and 5th magnet)
//
// Created by Simone Bifani (Simone.Bifani@cern.ch) 2008-04-22
// --------------------------------------------------------------
//
#include "globals.hh"
#include "GigaTrackerGeometryParameters.hh"
#include "GigaTrackerMaterialParameters.hh"
#include "GigaTrackerMagnet.hh"

#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4Material.hh"
#include "G4Box.hh"

GigaTrackerMagnet::GigaTrackerMagnet(G4Material * Material, G4LogicalVolume * MotherVolume, G4ThreeVector Position, G4int iCopy) : NA62VComponent(Material,MotherVolume)
{

  fiCopy = iCopy;
  fPosition = Position;

  ReadGeometryParameters();

  // Mandatory here to Find or Build the needed materials
  GigaTrackerMaterialParameters::GetInstance();
  CreateGeometry();
  SetProperties();

}

GigaTrackerMagnet::~GigaTrackerMagnet() {}

void GigaTrackerMagnet::ReadGeometryParameters()
{

  // Read all the geometrical parameters and copy them to private members
  GigaTrackerGeometryParameters* GeoPars = GigaTrackerGeometryParameters::GetInstance();

  if((fiCopy == 0) || (fiCopy == 1) || (fiCopy == 2) || (fiCopy == 3)) {
    fXLength = GeoPars->GetGigaTrackerMagnetXLength1();
    fYLength = GeoPars->GetGigaTrackerMagnetYLength1();
    fZLength = GeoPars->GetGigaTrackerMagnetZLength1();  
    fFieldStrength = G4ThreeVector(GeoPars->GetGigaTrackerMagnetFieldStrength(fiCopy), 0., 0.);
  }
  if(fiCopy == 4) {
    fXLength = GeoPars->GetGigaTrackerMagnetXLength2();
    fYLength = GeoPars->GetGigaTrackerMagnetYLength2();
    fZLength = GeoPars->GetGigaTrackerMagnetZLength2();  
    fFieldStrength = G4ThreeVector(0., GeoPars->GetGigaTrackerMagnetFieldStrength(fiCopy), 0.);
  }

}

void GigaTrackerMagnet::CreateGeometry()
{

  // G4 volumes

  G4double HalfZLength = 0.5 * fZLength;
  G4double HalfXLength = 0.5 * fXLength;
  G4double HalfYLength = 0.5 * fYLength;

  std::stringstream s;
  s << "GigaTrackerMagnet" << fiCopy;
  G4String Name = s.str();

  fSolidVolume= new G4Box(Name, HalfXLength, HalfYLength, HalfZLength);

  fLogicalVolume= new G4LogicalVolume(fSolidVolume,        // solid
				      fMaterial,           // material
				      Name, // name
				      0,                   // field manager 
				      0,                   // sensitive detector
				      0);                  // user limits

  fPhysicalVolume = new G4PVPlacement(0,
				      fPosition,
				      fLogicalVolume,      // its logical volume
				      Name, // its name
				      fMotherVolume,       // its mother  volume
				      false,               // no boolean operations
				      fiCopy);             // copy number

  // Uniform magnetic field
  fMagField = new G4UniformMagField(fFieldStrength);
  fFieldMgr = new G4FieldManager(fMagField);
  fFieldMgr->SetDetectorField(fMagField);
  fFieldMgr->CreateChordFinder(fMagField);
  fLogicalVolume->SetFieldManager(fFieldMgr, true);

}

void GigaTrackerMagnet::SetProperties()
{

  // Set visualization properties
  fVisAtt = new G4VisAttributes(G4Colour(0.0, 1.0, 0.0));
  fVisAtt->SetVisibility(true);
  fLogicalVolume->SetVisAttributes(fVisAtt);

}
