//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// --------------------------------------------------------------
// History:
//
// Created by Bob Velghe (bob.velghe@cern.ch) 2012-01-09
// --------------------------------------------------------------
//
#include "GigaTrackerGeometryParameters.hh"
#include "GigaTrackerMaterialParameters.hh"

#include "G4LogicalVolume.hh"
#include "GigaTrackerPCBModule.hh"

GigaTrackerPCBModule::GigaTrackerPCBModule(G4Material * Material, G4LogicalVolume * MotherVolume, G4ThreeVector Position, G4int iCopy) : 
NA62VComponent(Material, MotherVolume)
{

  fPosition = Position;
  fiCopy = iCopy;



  ReadGeometryParameters();
  CreateGeometry();
  SetProperties();
}

GigaTrackerPCBModule::~GigaTrackerPCBModule() {}

void GigaTrackerPCBModule::ReadGeometryParameters()
{
  GigaTrackerGeometryParameters* GeoPars = GigaTrackerGeometryParameters::GetInstance();
  fPCBXLength = GeoPars->GetGigaTrackerPCBXLength();
  fPCBYLength = GeoPars->GetGigaTrackerPCBYLength();
  fPCBZLength = GeoPars->GetGigaTrackerPCBZLength();
  fPCBHoleXOffset = GeoPars->GetGigaTrackerPCBHoleXOffset();
  fPCBHoleXLength = GeoPars->GetGigaTrackerPCBHoleXLength();
  fPCBHoleYLength = GeoPars->GetGigaTrackerPCBHoleYLength();

  fCoolingPlateXLength = GeoPars->GetCoolingPlateXLength(); 
  fCoolingPlateYLength = GeoPars->GetCoolingPlateYLength();

  //FIXME
	fPCBFootSizeVert = 0.5*(71.0*mm - fPCBHoleYLength);
	fPCBFootSizeHoriz = 0.5*(81.0*mm - fPCBHoleXLength);
	
	fPCBFootHeight = (2.0-1.46)*mm;

  //FIXME Remove ?
  //fCoolingPlateZLength = GeoPars->GetCoolingPlateZLength();
  //fSensorAssemblyZLength = GeoPars->GetGigaTrackerSensorAssemblyZLength();
  //fCoolingPlateTopDepth = GeoPars->GetCoolingPlateTopDepth();

}

void GigaTrackerPCBModule::CreateGeometry()
{
  ////////////////
  // PCB module //
  ////////////////
  
  fLogicalVolume = fMotherVolume;
  
  G4double PCBLeftXLength = fPCBHoleXOffset;
  G4double PCBRightXLength = fPCBXLength-PCBLeftXLength-fPCBHoleXLength;
  G4double PCBTopBottomYLength = (fPCBYLength-fPCBHoleYLength)/2;
  
  
  
  
  fPCBLeftSolidVolume = new G4Box("GigaTrackerPCBLeft",0.5*(PCBLeftXLength - fPCBFootSizeHoriz),0.5*fPCBYLength,0.5*fPCBZLength);
  fPCBLeftLogicalVolume = new G4LogicalVolume(fPCBLeftSolidVolume,G4Material::GetMaterial("GTK_PCB"),"GigaTrackerPCBLeft",0,0,0);
		
  fPCBRightSolidVolume = new G4Box("GigaTrackerPCBRight",0.5*(PCBRightXLength - fPCBFootSizeHoriz),0.5*fPCBYLength,0.5*fPCBZLength);
  fPCBRightLogicalVolume = new G4LogicalVolume(fPCBRightSolidVolume,G4Material::GetMaterial("GTK_PCB"),"GigaTrackerPCBRight",0,0,0);
	
  fPCBLeftRightSolidVolume = new G4Box("GigaTrackerPCBLeftRightFoot",0.5*fPCBFootSizeHoriz,0.5*fPCBHoleYLength,0.5*fPCBFootHeight);
	fPCBLeftRightLogicalVolume = new G4LogicalVolume(fPCBLeftRightSolidVolume,G4Material::GetMaterial("GTK_PCB"),"GigaTrackerPCBLeftRightFoot",0,0,0);
		
  //Top and bottom pieces are identical
  fPCBTopBottomSolidVolume = new G4Box("GigaTrackerPCBTopBottom",0.5*fPCBHoleXLength+fPCBFootSizeHoriz,0.5*(PCBTopBottomYLength-fPCBFootSizeVert),0.5*fPCBZLength);
  
  
  fPCBTopBottomLogicalVolume = new G4LogicalVolume(fPCBTopBottomSolidVolume,G4Material::GetMaterial("GTK_PCB"),"GigaTrackerPCBTopBottom",0,0,0);
	
	fPCBTopBottomFootSolidVolume = new G4Box("GigaTrackerPCBTopBottomFoot",0.5*fPCBHoleXLength+fPCBFootSizeHoriz,0.5*fPCBFootSizeVert,0.5*fPCBFootHeight);
	fPCBTopBottomFootLogicalVolume = new G4LogicalVolume(fPCBTopBottomFootSolidVolume,G4Material::GetMaterial("GTK_PCB"),"GigaTrackerPCBTopBottomFoot",0,0,0);
	
  fPCBLeftPhysicalVolume = new G4PVPlacement(0,
					     fPosition+G4ThreeVector(-0.5*(fPCBHoleXLength + PCBRightXLength + fPCBFootSizeHoriz),0,0),
					     fPCBLeftLogicalVolume,
					     "GigaTrackerPCBLeft",
					     fLogicalVolume,
					     false,
					     0);


  fPCBRightPhysicalVolume = new G4PVPlacement(0,
					      fPosition+G4ThreeVector(0.5*(PCBLeftXLength + fPCBHoleXLength + fPCBFootSizeHoriz),0,0),
					      fPCBRightLogicalVolume,
					      "GigaTrackerPCBRight",
					      fLogicalVolume,
					      false,
					      0);

  fPCBTopPhysicalVolume = new G4PVPlacement(0,
					    fPosition+G4ThreeVector(0.5*(PCBLeftXLength - PCBRightXLength),0.5*(fPCBYLength - PCBTopBottomYLength + fPCBFootSizeVert),0),
					    fPCBTopBottomLogicalVolume,
					    "GigaTrackerPCBTop",
					    fLogicalVolume,
					    false,
					    0);

  fPCBBottomPhysicalVolume = new G4PVPlacement(0,
					       fPosition+G4ThreeVector(0.5*(PCBLeftXLength - PCBRightXLength),0.5*(PCBTopBottomYLength - fPCBYLength - fPCBFootSizeVert),0),
					       fPCBTopBottomLogicalVolume,
					       "GigaTrackerPCBBottom",
					       fLogicalVolume,
					       false,
					       0);


  fPCBTopFootPhysicalVolume = new G4PVPlacement(0,
					      fPosition+G4ThreeVector(0.5*(PCBLeftXLength - PCBRightXLength),0.5*fPCBYLength - (PCBTopBottomYLength-fPCBFootSizeVert) - 0.5*fPCBFootSizeVert,0.5*fPCBZLength - 0.5*fPCBFootHeight),
					      fPCBTopBottomFootLogicalVolume,
					      "GigaTrackerPCBTopFoot",
					      fLogicalVolume,
					      false,
					      0);


  fPCBBottomFootPhysicalVolume = new G4PVPlacement(0,
					       fPosition+G4ThreeVector(0.5*(PCBLeftXLength - PCBRightXLength),-0.5*fPCBYLength+(PCBTopBottomYLength-fPCBFootSizeVert)+0.5*fPCBFootSizeVert,0.5*fPCBZLength - 0.5*fPCBFootHeight),
					       fPCBTopBottomFootLogicalVolume,
					       "GigaTrackerPCBBottomFoot",
					       fLogicalVolume,
					       false,
					       0);

  fPCBLeftFootPhysicalVolume = new G4PVPlacement(0,
					       fPosition+G4ThreeVector(-0.5*(PCBLeftXLength + PCBRightXLength+ fPCBHoleXLength) + PCBLeftXLength - 0.5*fPCBFootSizeHoriz,0,0.5*fPCBZLength - 0.5*fPCBFootHeight),
					       fPCBLeftRightLogicalVolume,
					       "GigaTrackerPCBLeftFoot",
					       fLogicalVolume,
					       false,
					       0);

  fPCBRightFootPhysicalVolume = new G4PVPlacement(0,
					       fPosition+G4ThreeVector(0.5*(PCBLeftXLength + PCBRightXLength+ fPCBHoleXLength) - PCBRightXLength +0.5*fPCBFootSizeHoriz,0,0.5*fPCBZLength - 0.5*fPCBFootHeight),
					       fPCBLeftRightLogicalVolume,
					       "GigaTrackerPCBRightFoot",
					       fLogicalVolume,
					       false,
					       0);


}
	

void GigaTrackerPCBModule::SetProperties()
{
  fVisAtt = new G4VisAttributes(G4Colour(0.0,1.0,0.0)); //Green (red,green,blue)
  fVisAtt->SetVisibility(true);
  fPCBLeftLogicalVolume->SetVisAttributes(fVisAtt);
  fPCBRightLogicalVolume->SetVisAttributes(fVisAtt);
  fPCBLeftRightLogicalVolume->SetVisAttributes(fVisAtt);
  fPCBTopBottomLogicalVolume->SetVisAttributes(fVisAtt);
  fPCBTopBottomFootLogicalVolume->SetVisAttributes(fVisAtt);
  
}
