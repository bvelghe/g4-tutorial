//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// --------------------------------------------------------------
// History:
//
// Created by Bob Velghe (bob.velghe@cern.ch) 2012-01-09
// (Based on GigaTrackerStation.cc)
// --------------------------------------------------------------
//
#include "GigaTrackerGeometryParameters.hh"
#include "GigaTrackerMaterialParameters.hh"

#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4Material.hh"
#include "GigaTrackerCoolingPlate.hh"


GigaTrackerCoolingPlate::GigaTrackerCoolingPlate(G4Material * Material, G4LogicalVolume * MotherVolume, G4ThreeVector Position, G4int iCopy) : 
NA62VComponent(Material, MotherVolume)
{

  fPosition = Position;
  fiCopy = iCopy;

  ReadGeometryParameters();
  GigaTrackerMaterialParameters::GetInstance();
  CreateGeometry();
  SetProperties();

}

GigaTrackerCoolingPlate::~GigaTrackerCoolingPlate() {}

void GigaTrackerCoolingPlate::ReadGeometryParameters()
{

  GigaTrackerGeometryParameters* GeoPars = GigaTrackerGeometryParameters::GetInstance();
 
  // Main bloc
  fCoolingPlateXLength = GeoPars->GetCoolingPlateXLength(); 
  fCoolingPlateYLength = GeoPars->GetCoolingPlateYLength();
  fCoolingPlateZLength = GeoPars->GetCoolingPlateZLength(); 

  // Top plate
  fTopShoulderXLength = GeoPars->GetCoolingPlateTopShoulderXLength();
  fTopHollowXLength = GeoPars->GetCoolingPlateTopHollowXLength();
  fTopShoulderYLength = GeoPars->GetCoolingPlateTopShoulderYLength();
  fTopHollowYLength = GeoPars->GetCoolingPlateTopHollowYLength();
  fTopDepth = GeoPars->GetCoolingPlateTopDepth();

  // Bottom plate
  fBottomShoulderXLength = GeoPars->GetCoolingPlateBottomShoulderXLength();
  fBottomHollowXLength = GeoPars->GetCoolingPlateBottomHollowXLength();
  fBottomShoulderYLength = GeoPars->GetCoolingPlateBottomShoulderYLength();
  fBottomHollowYLength = GeoPars->GetCoolingPlateBottomHollowYLength();
  fBottomDepth = GeoPars->GetCoolingPlateBottomDepth();

  //
  fChannelsXOffset = GeoPars->GetCoolingPlateChannelsXOffset();
  fChannelsDepth = GeoPars->GetCoolingPlateChannelsDepth();

  // Envelope
  
  fChannelsEnvelopeXLength = GeoPars->GetCoolingPlateChannelsEnvelopeXLength();
  fChannelsEnvelopeYLength = GeoPars->GetCoolingPlateChannelsEnvelopeYLength();
  fChannelsEnvelopeZLength = GeoPars->GetCoolingPlateChannelsEnvelopeZLength();

  // Channels
  fChannelCoolantXLength = GeoPars->GetCoolingPlateChannelCoolantXLength();
  fChannelCoolantYLength = fChannelsEnvelopeYLength;
  fChannelCoolantZLength = fChannelsEnvelopeZLength;
  
  // Warning *half* wall
  fChannelHalfWallXLength = GeoPars->GetCoolingPlateChannelHalfWallXLength();
  fChannelHalfWallYLength = fChannelsEnvelopeYLength;
  fChannelHalfWallZLength = fChannelsEnvelopeZLength;

  fChannelXLength = 2*fChannelHalfWallXLength+fChannelCoolantXLength;
  fChannelYLength = fChannelsEnvelopeYLength;
  fChannelZLength = fChannelsEnvelopeZLength;

  // Offset ref. top right corner of the cooling plate
  fTopConnectorXOffset = 10*CLHEP::mm;
  fTopConnectorYOffset = 5.1*CLHEP::mm;
  
  // Offset ref. bottom left corner of the cooling plate
  fBottomConnectorXOffset = 10*CLHEP::mm;
  fBottomConnectorYOffset = 5.1*CLHEP::mm;
  

  
}

void GigaTrackerCoolingPlate::CreateGeometry()
{

  //Move me
  G4double A;
  G4double Z;
  A = 28.09*CLHEP::g/CLHEP::mole;
  G4Element* elSi  = new G4Element("Silicon","Si",Z = 14.,A);

  A  =  54.94*CLHEP::g/CLHEP::mole;
  G4Element* elMn   =  new G4Element("Manganese","Mn",Z = 25.,A);
 

  A = 52.00*CLHEP::g/CLHEP::mole;
  G4Element* elCr  = new G4Element("Chromium","Cr",Z = 24.,A);

  A = 58.70*CLHEP::g/CLHEP::mole;
  G4Element* elNi  = new G4Element("Nickel","Ni",Z = 28.,A);
  
  
  A = 55.85*CLHEP::g/CLHEP::mole;
  G4Element* elFe  = new G4Element("Iron","Fe",Z = 26.,A);
 
  
  // Stainless steel (Medical Physics, Vol 25, No 10, Oct 1998)
  G4double d = 8.02*CLHEP::g/CLHEP::cm3 ;
  G4Material * matsteel = new G4Material("Stainless steel",d,5);
  matsteel->AddElement(elMn, 0.02);
  matsteel->AddElement(elSi, 0.01);
  matsteel->AddElement(elCr, 0.19);
  matsteel->AddElement(elNi, 0.10);
  matsteel->AddElement(elFe, 0.68);
 








  // FIXME: KISS
  // "Negative" footprint: these two solids will be removed from the main bloc
  //

  //Top Hollow Footprint
  fTopHollow = new G4Trd("GigaTrackerCoolingPlateTopHollow",
			 0.5*fTopHollowXLength,    	//dx1 (-dz)
			 0.5*fTopShoulderXLength,  	//dx2 (+dz)
			 0.5*fTopHollowYLength,    	//dy1 (-dz)
			 0.5*fTopShoulderYLength,	//dy2 (+dz)
			 0.5*fTopDepth);           	//dz
  //Bottom Hollow Footprint
  fBottomHollow = new G4Trd("GigaTrackerCoolingPlateBottomHollow",  
			    0.5*fBottomShoulderXLength, //dx1 (-dz)
			    0.5*fBottomHollowXLength,   //dx2 (+dz)
			    0.5*fBottomShoulderYLength, //dy1 (-dz)
			    0.5*fBottomHollowYLength,	//dy2 (+dz)
			    0.5*fBottomDepth);          //dz

  fMainBloc = new G4Box("GigaTrackerCoolingMainBloc",0.5*fCoolingPlateXLength,0.5*fCoolingPlateYLength,0.5*fCoolingPlateZLength);
  fUpperWafer = new G4SubtractionSolid("GigaTrackerCoolingPlate",fMainBloc,fTopHollow,0,G4ThreeVector(0.0,0.0,0.5*(fCoolingPlateZLength - fTopDepth)));
  fSolidVolume = new G4SubtractionSolid("GigaTrackerCoolingPlate",fUpperWafer,fBottomHollow,0,G4ThreeVector(0.0,0.0,0.5*(fBottomDepth - fCoolingPlateZLength)));
  
  fLogicalVolume = new G4LogicalVolume(fSolidVolume,fMaterial,"GigaTrackerCoolingPlate",0,0,0);
  fPhysicalVolume = new G4PVPlacement(0,
				      fPosition,
				      fLogicalVolume,
				      "GigaTrackerCoolingPlate",
				      fMotherVolume, //Mother logical volume
				      false,
				      fiCopy);


  ///////////////////////
  // Channels envelope //
  ///////////////////////
 
  G4double channelsEnvelopeZ = 0.5*(fCoolingPlateZLength - fChannelsEnvelopeZLength) - (fTopDepth + fChannelsDepth);
  fChannelsEnvelopeSolidVolume = new G4Box("GigaTrackerCoolingChannelsEnvelope",0.5*fChannelsEnvelopeXLength,0.5*fChannelsEnvelopeYLength,0.5*fChannelsEnvelopeZLength); 
  
  fChannelsEnvelopeLogicalVolume = new G4LogicalVolume(fChannelsEnvelopeSolidVolume,G4Material::GetMaterial("G4_Galactic"),"GigaTrackerCoolingChannelsEnvelope",0,0,0);
    
  fChannelsEnvelopePhysicalVolume = new G4PVPlacement(0,
						      G4ThreeVector(0,0,channelsEnvelopeZ),
						      fChannelsEnvelopeLogicalVolume,
						      "GigaTrackerCoolingChannelsEnvelope",
						      fLogicalVolume, //Mother logical volume
						      false,
						      0);
  
  ///////////////////
  // Channel Model //
  //  ww|cccc|ww   //
  ///////////////////
  
  fChannelSolidVolume = new G4Box("GigaTrackerCoolingChannel",0.5*fChannelXLength,0.5*fChannelYLength,0.5*fChannelZLength); 
  fChannelLogicalVolume = new G4LogicalVolume(fChannelSolidVolume,G4Material::GetMaterial("G4_Galactic"),"GigaTrackerCoolingChannel",0,0,0);

  fChannelHalfWallSolidVolume = new G4Box("GigaTrackerChannelHalfWall",0.5*fChannelHalfWallXLength,0.5*fChannelHalfWallYLength,0.5*fChannelHalfWallZLength);
  fChannelHalfWallLogicalVolume = new G4LogicalVolume(fChannelHalfWallSolidVolume,G4Material::GetMaterial("G4_Si"),"GigaTrackerChannelHalfWall",0,0,0); //Si

  G4double LeftWallPos = -0.5*(fChannelCoolantXLength+fChannelHalfWallXLength);
  G4double RightWallPos =  0.5*(fChannelCoolantXLength+fChannelHalfWallXLength);
  G4double CoolantPos = 0.0;

  fChannelLeftWallPhysicalVolume = new G4PVPlacement(0,
						     G4ThreeVector(LeftWallPos,0,0),
						     fChannelHalfWallLogicalVolume,
						     "GigaTrackerChannelHalfWall",
						     fChannelLogicalVolume, 
						     false,
						     0);

  fChannelRightWallPhysicalVolume = new G4PVPlacement(0,
						      G4ThreeVector(RightWallPos,0,0),
						      fChannelHalfWallLogicalVolume,
						      "GigaTrackerChannelHalfWall",
						      fChannelLogicalVolume, 
						      false,
						      0);

  fChannelCoolantSolidVolume = new G4Box("GigaTrackerChannelCoolant",0.5*fChannelCoolantXLength,0.5*fChannelCoolantYLength,0.5*fChannelCoolantZLength);
  fChannelCollantLogicalVolume = new G4LogicalVolume(fChannelCoolantSolidVolume,G4Material::GetMaterial("GTK_Coolant"),"GigaTrackerChannelCoolant",0,0,0); //C6F14
  fChannelCollantPhysicalVolume = new G4PVPlacement(0,
						    G4ThreeVector(CoolantPos,0,0),
						    fChannelCollantLogicalVolume,
						    "GigaTrackerChannelCoolant",
						    fChannelLogicalVolume,
						    false,
						    0);

  /////////////////////////
  // Channel replication //
  /////////////////////////
 	
  G4int nReplicas = fChannelsEnvelopeXLength/fChannelXLength;
	  
  fChannelPhysicalVolume = new G4PVReplica("GigaTrackerCoolingChannelWalls",
					   fChannelLogicalVolume, //logical volume
					   fChannelsEnvelopeLogicalVolume, //mother logical volume
					   kXAxis,          // replication axis
					   nReplicas,       // nReplicas
					   fChannelXLength, // width,
					   0);              // offset
  
	
	//////////
	// Tubs //
	//////////
	G4double fInterConnLength = 30.8*CLHEP::mm;
	G4double fInnerTubDiam = 1.0*CLHEP::mm;
	G4double fOuterTubDiam = 1.65*CLHEP::mm;
	G4double fTopLeftTubLength = fTopConnectorXOffset+7.75*CLHEP::mm;
  G4double fTopCenterTubLength = fTopConnectorYOffset+7.75*CLHEP::mm;
  G4double fBottomCenterTubLength =  fTopConnectorXOffset+fInterConnLength+7.75*CLHEP::mm;
  G4double fBottomRightTubLength = fBottomConnectorYOffset+7.75*CLHEP::mm;
  G4double fTubsZPos = 5.8*CLHEP::mm; // Relative to cooling plate *surface*

	
	fTopLeftTubSolidVolume = new G4Tubs("GigaTrackerCoolingTopLeftTub",
             0.5*fInnerTubDiam, 
             0.5*fOuterTubDiam , 
             0.5*fTopLeftTubLength,
             0,
             2*CLHEP::pi);
       
       
                    
  fTopCenterTubSolidVolume =  new G4Tubs("GigaTrackerCoolingTopCenterTub",
             0.5*fInnerTubDiam, 
             0.5*fOuterTubDiam , 
             0.5*fTopCenterTubLength,
             0,
             2*CLHEP::pi);
  
  fBottomCenterTubSolidVolume =  new G4Tubs("GigaTrackerCoolingBottomCenterTub",
             0.5*fInnerTubDiam, 
             0.5*fOuterTubDiam , 
             0.5*fBottomCenterTubLength,
             0,
             2*CLHEP::pi); 
             
         
  fBottomRightTubSolidVolume =  new G4Tubs("GigaTrackerCoolingBottomRightTub",
             0.5*fInnerTubDiam, 
             0.5*fOuterTubDiam , 
             0.5*fBottomRightTubLength,
             0,
             2*CLHEP::pi); 
       
       
             
  fTopLeftTubLogicalVolume = new G4LogicalVolume(fTopLeftTubSolidVolume,matsteel,"GigaTrackerCoolingTopLeftTub",0,0,0);
  fTopCenterTubLogicalVolume = new G4LogicalVolume(fTopCenterTubSolidVolume,matsteel,"GigaTrackerCoolingTopCenterTub",0,0,0);
  fBottomCenterTubLogicalVolume = new G4LogicalVolume(fBottomCenterTubSolidVolume,matsteel,"GigaTrackerCoolingBottomCenterTub",0,0,0);
  fBottomRightTubLogicalVolume = new G4LogicalVolume(fBottomRightTubSolidVolume,matsteel,"GigaTrackerCoolingBottomRightTub",0,0,0);
     
  //FIXME FIXME FIXME FIXME !!! Hardcoded values !!!
  G4RotationMatrix * yRot90deg = new G4RotationMatrix();
  yRot90deg->rotateY(CLHEP::pi/2.*CLHEP::rad);
     
  G4RotationMatrix * xRot90deg = new G4RotationMatrix();
  xRot90deg->rotateX(CLHEP::pi/2.*CLHEP::rad);

     
 
  G4double tmp_tl = -0.5*fCoolingPlateXLength+fTopConnectorXOffset- 0.5*fTopLeftTubLength - 2.25*CLHEP::mm; 
  G4double tmp_bc = -0.5*fCoolingPlateXLength+fTopConnectorXOffset- 0.5*fBottomCenterTubLength - 2.25*CLHEP::mm + fInterConnLength ; 
  G4double tmp_tc = 0.5*fCoolingPlateYLength-fTopConnectorYOffset + 0.5*fTopCenterTubLength + 2.25*CLHEP::mm; 
  G4double tmp_br = -0.5*fCoolingPlateYLength+fTopConnectorYOffset - 0.5*fTopCenterTubLength - 2.25*CLHEP::mm; 
 
  G4ThreeVector TopLeftTubPos(tmp_tl,0.5*fCoolingPlateYLength-fTopConnectorYOffset,-0.5*fCoolingPlateZLength-fTubsZPos);
  G4ThreeVector TopCenterTubPos(0.5*fCoolingPlateXLength-fTopConnectorXOffset - fInterConnLength,tmp_tc,-0.5*fCoolingPlateZLength-fTubsZPos);
  
  G4ThreeVector BottomCenterTubPos(tmp_bc,-0.5*fCoolingPlateYLength+fBottomConnectorYOffset,-0.5*fCoolingPlateZLength-fTubsZPos);
  G4ThreeVector BottomRightTubPos(0.5*fCoolingPlateXLength-fBottomConnectorXOffset,tmp_br,-0.5*fCoolingPlateZLength-fTubsZPos);
 
 
  fTopLeftTubPhysicalVolume = new G4PVPlacement(yRot90deg,
				      fPosition+TopLeftTubPos,
				      fTopLeftTubLogicalVolume,
				      "GigaTrackerCoolingTopLeftTub",
				      fMotherVolume, 
				      false,
				      fiCopy);   
	
				      
  fTopCenterTubPhysicalVolume = new G4PVPlacement(xRot90deg,
				      fPosition+TopCenterTubPos,
				      fTopCenterTubLogicalVolume,
				      "GigaTrackerCoolingTopCenterTub",
				      fMotherVolume, 
				      false,
				      fiCopy);   
                    
  fBottomCenterTubPhysicalVolume = new G4PVPlacement(yRot90deg,
				      fPosition+BottomCenterTubPos,
				      fBottomCenterTubLogicalVolume,
				      "GigaTrackerCoolingBottomCenterTub",
				      fMotherVolume, 
				      false,
				      fiCopy);      
               
  fBottomRightTubPhysicalVolume   = new G4PVPlacement(xRot90deg,
				      fPosition+BottomRightTubPos,
				      fBottomRightTubLogicalVolume,
				      "GigaTrackerCoolingBottomRightTub",
				      fMotherVolume, 
				      false,
				      fiCopy);      
				      
	////////////////
	// Connectors //
	////////////////
	
	
	G4ThreeVector ConnTopCenterPos(0.5*fCoolingPlateXLength-fTopConnectorXOffset - fInterConnLength,0.5*fCoolingPlateYLength-fTopConnectorYOffset,-0.5*fCoolingPlateZLength);
	G4ThreeVector ConnTopLeftPos(-0.5*fCoolingPlateXLength+fTopConnectorXOffset,0.5*fCoolingPlateYLength-fTopConnectorYOffset,-0.5*fCoolingPlateZLength);
	
	G4ThreeVector ConnBottomRightPos(0.5*fCoolingPlateXLength-fBottomConnectorXOffset,-0.5*fCoolingPlateYLength+fBottomConnectorYOffset,-0.5*fCoolingPlateZLength);
	G4ThreeVector ConnBottomCenterPos(-0.5*fCoolingPlateXLength+fBottomConnectorXOffset + fInterConnLength,-0.5*fCoolingPlateYLength+fBottomConnectorYOffset,-0.5*fCoolingPlateZLength);


	fKovarConnTopLeft = new GigaTrackerKovarConnector(G4Material::GetMaterial("G4_Galactic"), fMotherVolume,fPosition+ ConnTopLeftPos,2, 0);
	fKovarConnTopCenter = new GigaTrackerKovarConnector(G4Material::GetMaterial("G4_Galactic"), fMotherVolume,fPosition+ ConnTopCenterPos,3, 1);
	fKovarConnBottomCenter = new GigaTrackerKovarConnector(G4Material::GetMaterial("G4_Galactic"), fMotherVolume,fPosition+ ConnBottomCenterPos,2, 2);
	fKovarConnBottomRight = new GigaTrackerKovarConnector(G4Material::GetMaterial("G4_Galactic"), fMotherVolume,fPosition+ ConnBottomRightPos,1, 3);
	


}

void GigaTrackerCoolingPlate::SetProperties() 
{
  fVisAtt = new G4VisAttributes(G4Colour(0.0,0.5,0.5)); // (red,green,blue)
  fVisAtt->SetVisibility(true);
  fLogicalVolume->SetVisAttributes(fVisAtt);

  fChannelLogicalVolume->SetVisAttributes(G4VisAttributes::Invisible);
  fChannelsEnvelopeLogicalVolume->SetVisAttributes(G4VisAttributes::Invisible);
  fChannelHalfWallLogicalVolume->SetVisAttributes(G4VisAttributes::Invisible); 
  
  G4VisAttributes * fVisAttChannels = new G4VisAttributes(G4Colour(0.0,.5,1.0)); // (red,green,blue)
  fVisAttChannels->SetVisibility(true);
  fChannelCollantLogicalVolume->SetVisAttributes(fVisAttChannels);
}
