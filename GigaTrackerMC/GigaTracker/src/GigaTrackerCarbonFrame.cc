#include "GigaTrackerCarbonFrame.hh"

GigaTrackerCarbonFrame::GigaTrackerCarbonFrame(G4Material * Material, G4LogicalVolume * MotherVolume, G4ThreeVector Position, G4int iCopy) : NA62VComponent(Material, MotherVolume) 
{
  fPosition = Position;
  fiCopy = iCopy;

  ReadGeometryParameters();
  CreateGeometry();
  SetProperties();
}

GigaTrackerCarbonFrame::~GigaTrackerCarbonFrame() {
}

void GigaTrackerCarbonFrame::ReadGeometryParameters()
{

  
}

void GigaTrackerCarbonFrame::CreateGeometry()
{
  //////////////////
  // Carbon Frame //
  /////////////////
  
  
    G4Material * fMatFrame = G4Material::GetMaterial("G4_C");
    G4double fFrameZThickness = 5.0*CLHEP::mm;
    G4double fFrameXYThickness = 3.0*CLHEP::mm;
   
    G4double fFrameXLength = 80.5*CLHEP::mm; 
    G4double fFrameYLength = 50.5*CLHEP::mm;
    
    //FIXME
    G4double frameAttachCoreXLength = (4.25-3.0)*CLHEP::mm; 
    G4double fFrameAttachZLength = 4*CLHEP::mm;  
  
   
    fFrameXSolidVolume = new G4Box("GigaTrackerCarbonFrameX",0.5*fFrameXLength,0.5*fFrameXYThickness,0.5*fFrameZThickness);
    fFrameXLogicalVolume = new G4LogicalVolume(fFrameXSolidVolume,fMatFrame,"GigaTrackerCarbonFrameX",0,0,0);

    fFrameYSolidVolume = new G4Box("GigaTrackerCarbonFrameY",0.5*fFrameXYThickness,0.5*(fFrameYLength - 2*fFrameXYThickness),0.5*fFrameZThickness);
    fFrameYLogicalVolume = new G4LogicalVolume(fFrameYSolidVolume,fMatFrame,"GigaTrackerCarbonFrameY",0,0,0);
    
    
    
    G4double zpos = -0.5*fFrameZThickness + (fFrameZThickness - fFrameAttachZLength);
    
    
    G4ThreeVector topFramePos(0.0,+0.5*(fFrameYLength-fFrameXYThickness),zpos);
    G4ThreeVector bottomFramePos(0.0,-0.5*(fFrameYLength-fFrameXYThickness),zpos);
    G4ThreeVector leftFramePos(-0.5*(fFrameXLength-fFrameXYThickness),0.0,zpos);
    G4ThreeVector rightFramePos(+0.5*(fFrameXLength-fFrameXYThickness),0.0,zpos);
    
    fFrameLeftPhysicalVolume = new G4PVPlacement(0,
					       fPosition+leftFramePos,
						     fFrameYLogicalVolume,
						     "GigaTrackerCarbonFrameLeft",
						     fMotherVolume,
						     false,
						     fiCopy);
						     
    fFrameRightPhysicalVolume = new G4PVPlacement(0,
					       fPosition+rightFramePos,
						     fFrameYLogicalVolume,
						     "GigaTrackerCarbonFrameRight",
						     fMotherVolume,
						     false,
						     fiCopy);
						     
    fFrameTopPhysicalVolume = new G4PVPlacement(0,
					       fPosition+topFramePos,
						     fFrameXLogicalVolume,
						     "GigaTrackerCarbonFrameTop",
						     fMotherVolume,
						     false,
						     fiCopy);

    fFrameBottomPhysicalVolume = new G4PVPlacement(0,
					       fPosition+bottomFramePos,
						     fFrameXLogicalVolume,
						     "GigaTrackerCarbonFrameBottom",
						     fMotherVolume,
						     false,
						     fiCopy);



    fFrameAttachCoreVolume = new G4Box("GigaTrackerCarbonFrameAttachCore",0.5*frameAttachCoreXLength,0.5*fFrameXYThickness,0.5*fFrameAttachZLength);
    fFrameAttachCoreLogicalVolume = new G4LogicalVolume(fFrameAttachCoreVolume,fMatFrame,"GigaTrackerCarbonFrameAttachCore",0,0,0);


    G4double attachZpos = -0.5*(fFrameZThickness - fFrameAttachZLength)+zpos;

    
    G4ThreeVector attachCoreTLeft(-0.5*(fFrameXLength+frameAttachCoreXLength),0.5*(fFrameYLength-fFrameXYThickness),attachZpos);
    G4ThreeVector attachCoreTRight(+0.5*(fFrameXLength+frameAttachCoreXLength),0.5*(fFrameYLength-fFrameXYThickness),attachZpos);
    G4ThreeVector attachCoreBLeft(-0.5*(fFrameXLength+frameAttachCoreXLength),-0.5*(fFrameYLength-fFrameXYThickness),attachZpos);
    G4ThreeVector attachCoreBRight(+0.5*(fFrameXLength+frameAttachCoreXLength),-0.5*(fFrameYLength-fFrameXYThickness),attachZpos);
    
    
    fFrameAttachCoreTLeftPhysicalVolume = new G4PVPlacement(0,
					       fPosition+attachCoreTLeft,
						     fFrameAttachCoreLogicalVolume,
						     "GigaTrackerCarbonFrameAttachCoreTLeft",
						     fMotherVolume,
						     false,
						     fiCopy);
    
    fFrameAttachCoreTRightPhysicalVolume = new G4PVPlacement(0,
					       fPosition+attachCoreTRight,
						     fFrameAttachCoreLogicalVolume,
						     "GigaTrackerCarbonFrameAttachCoreTRight",
						     fMotherVolume,
						     false,
						     fiCopy);
						     
		fFrameAttachCoreBLeftPhysicalVolume = new G4PVPlacement(0,
					       fPosition+attachCoreBLeft,
						     fFrameAttachCoreLogicalVolume,
						     "GigaTrackerCarbonFrameAttachCoreBLeft",
						     fMotherVolume,
						     false,
						     fiCopy);
						     
		 fFrameAttachCoreBRightPhysicalVolume = new G4PVPlacement(0,
					       fPosition+attachCoreBRight,
						     fFrameAttachCoreLogicalVolume,
						     "GigaTrackerCarbonFrameAttachCoreBRight",
						     fMotherVolume,
						     false,
						     fiCopy);
    
    
    G4double fFrameAttachHeadDiam = 6*CLHEP::mm;
    G4double fFrameAttachHeadZLength = 4*CLHEP::mm;
    
    fFrameAttachHeadSolidVolume =  new G4Tubs("GigaTrackerCarbonFrameAttachHead",
             0,
             0.5*fFrameAttachHeadDiam,
             0.5*fFrameAttachHeadZLength,
             0,
             2*CLHEP::pi);
  
   
    fFrameAttachHeadLogicalVolume =  new G4LogicalVolume(fFrameAttachHeadSolidVolume,fMatFrame,"GigaTrackerCarbonFrameAttachHead",0,0,0);

  
    G4ThreeVector attachHeadTLeft(-0.5*(fFrameXLength+2*frameAttachCoreXLength+fFrameAttachHeadDiam),0.5*(fFrameYLength-fFrameXYThickness),attachZpos);
    G4ThreeVector attachHeadTRight(+0.5*(fFrameXLength+2*frameAttachCoreXLength+fFrameAttachHeadDiam),0.5*(fFrameYLength-fFrameXYThickness),attachZpos);
    G4ThreeVector attachHeadBLeft(-0.5*(fFrameXLength+2*frameAttachCoreXLength+fFrameAttachHeadDiam),-0.5*(fFrameYLength-fFrameXYThickness),attachZpos);
    G4ThreeVector attachHeadBRight(+0.5*(fFrameXLength+2*frameAttachCoreXLength+fFrameAttachHeadDiam),-0.5*(fFrameYLength-fFrameXYThickness),attachZpos);
    

    fFrameAttachHeadTLeftPhysicalVolume = new G4PVPlacement(0,
					       fPosition+attachHeadTLeft,
						     fFrameAttachHeadLogicalVolume,
						     "GigaTrackerCarbonFrameAttachHeadTLeft",
						     fMotherVolume,
						     false,
						     fiCopy);
    
    fFrameAttachHeadTRightPhysicalVolume = new G4PVPlacement(0,
					       fPosition+attachHeadTRight,
						     fFrameAttachHeadLogicalVolume,
						     "GigaTrackerCarbonFrameAttachHeadTRight",
						     fMotherVolume,
						     false,
						     fiCopy);
						     
    fFrameAttachHeadBLeftPhysicalVolume = new G4PVPlacement(0,
					       fPosition+attachHeadBLeft,
						     fFrameAttachHeadLogicalVolume,
						     "GigaTrackerCarbonFrameAttachHeadBLeft",
						     fMotherVolume,
						     false,
						     fiCopy);
						     
    fFrameAttachHeadBRightPhysicalVolume = new G4PVPlacement(0,
					       fPosition+attachHeadBRight,
						     fFrameAttachHeadLogicalVolume,
						     "GigaTrackerCarbonFrameAttachHeadBRight",
						     fMotherVolume,
						     false,
						     fiCopy);


}

void GigaTrackerCarbonFrame::SetProperties()
{
  fVisAtt = new G4VisAttributes(G4Colour(0.5,0.5,0.5));
  fVisAtt->SetVisibility(true);
//  fTopBottomLipLogicalVolume->SetVisAttributes(fVisAtt);
}
