//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// --------------------------------------------------------------
// History:
//
// 2012-08-13 B. Velghe
// - 
//
// 2012-03-27 B. Velghe
// - Merge the modifications made by G.Ruggiero
// - Magnet and collimator Z position according to the beatch file 2011-02-11
// - Sign of the Trim 5 inverted
//
// 2009-03-12 S.Bifani
// - Added the collimator before station 3
// - Fixed the sign of the magnet fields
//
// 2009-03-03 A.Sergi
// - Added the UpdatePixelSize method to recompute pixel parameters
//
// 2009-03-03 S.Bifani
// - Removed the alluminum foil
// - Changed the geometrical configuration according to the 2008/11/24 BEATCH file
//   (x -> y deflection and 5th magnet)
//
/// 2008-05-06 S.Bifani
// - Fixed a dimension mismatch in X & Y lengths
// - Changed the magnet B fields according to the TRANSPORT file
//   (http://doble.web.cern.ch/doble/transport/k12hika+.txt 2008/02/29)
//
// 2008-04-29 S.Bifani
// - Implemeted the detailed pixel structure 
//   (sensitive silicon sensor, Sn-Pb bump bonding, silicon chip, carbon structure)
//
// 2008-04-22 S.Bifani (Simone.Bifani@cern.ch)
// - Added GTK parameters according to the BEATCH file 
//   (http://doble.web.cern.ch/doble/k12hika+.txt 2008/04/04)
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2008-03-30
// --------------------------------------------------------------
//
#include "GigaTrackerGeometryParameters.hh"
#include "DetectorParameter.hh"
#include "TVector.h"

/////////////////////////
// Geometry Parameters //
/////////////////////////

GigaTrackerGeometryParameters* GigaTrackerGeometryParameters::fInstance = 0;

GigaTrackerGeometryParameters::GigaTrackerGeometryParameters() : NA62VGeometryParameters(G4String("GigaTracker")) 
{

  // Define all the geometrical parameters and build the
  // responsibility region accordingly

  // Sensitive detector
  fGigaTrackerSensitiveDetectorName = "/GigaTracker";
  fGigaTrackerCollectionName = "GigaTrackerCollection";

  // Unused World Parameters for stand-alone configurations
  fWorldZLength = 26.*m;
  fWorldXLength = 10.*m;
  fWorldYLength = 10.*m;

  // Central detector position (G4 frame)
  fGigaTrackerDetectorZPosition = 0.5 * (79.440 + 102.425) *m;

  // Responsability region
  fGigaTrackerDetectorXLength = 4. *m;
  fGigaTrackerDetectorYLength = 4. *m;
  fGigaTrackerDetectorZLength = (102.425 - 79.440) *m;

  fResponsibilityRegion.push_back(new ResponsibilityRegion(fGigaTrackerDetectorZPosition - 0.5 * fGigaTrackerDetectorZLength,
							   fGigaTrackerDetectorZPosition + 0.5 * fGigaTrackerDetectorZLength)
				  );

  // Pixel dimensions
  fGigaTrackerSmallPixelXLength = 300.0*um;
  fGigaTrackerSmallPixelYLength = 300.0*um;
  fGigaTrackerBigPixelXLength = 400.0*um;
  fGigaTrackerBigPixelYLength = 300.0*um;
  
  fGigaTrackerPixelZLength = 200*um;
  fGigaTrackerNumberOfPixels = 18000;

  /////////////////////
  // Sensor Assembly //
  /////////////////////

  // Sensor effective size 
  fGigaTrackerSensorXLength = 63.1 *mm;
  fGigaTrackerSensorYLength = 29.3 *mm;
  fGigaTrackerSensorZLength = 200.0 *um;
  
  // Sensor active area
  fGigaTrackerActiveSensorXLength = 60.8 *mm;
  fGigaTrackerActiveSensorYLength = 27.0 *mm;
  fGigaTrackerActiveSensorZLength = 200.0 *um;
  
  // Bump Bonding dimension
  fGigaTrackerBumpBondingRLength = 10. *um;
  fGigaTrackerBumpBondingZLength = 25. *um;
  // Bump bonds offsets (relative to pixel center)
  fGigaTrackerBumpBondOffset = 100.0 *um;
  fGigaTrackerBumpBondBigOffset = -50.0 *um;

  // Chip dimension
  fGigaTrackerChipXLength = 12.0 *mm;
  fGigaTrackerChipYLength = 19.5 *mm;
  fGigaTrackerChipZLength = 100. *um;
  fGigaTrackerChipXGap = 0.1*mm;

  // Glue layer
  fGigaTrackerGlueLayerZLength = 30.0*um;

  // Sensor assembly dimension 
  fGigaTrackerSensorAssemblyXLength = 63.1*mm;
  fGigaTrackerSensorAssemblyYLength = 39.0*mm; 
  fGigaTrackerSensorAssemblyZLength = fGigaTrackerSensorZLength+fGigaTrackerBumpBondingZLength+fGigaTrackerChipZLength+fGigaTrackerGlueLayerZLength;

  // Pixel matrix configuration
  fGigaTrackerSensorNColumns = 200;
  fGigaTrackerSensorNRows = 90;
  fGigaTrackerChipNColumns = 40;
  fGigaTrackerChipNRows = 45;

  /////////////
  // Station //
  /////////////

  // Station dimension
  fGigaTrackerStationXLength = 435.6*mm; //~Vessel
  fGigaTrackerStationYLength = 146.0 *mm;
  fGigaTrackerStationZLength = 30. *mm;

  // Magnet dimension
  fGigaTrackerMCBMagnetXLength = 1250.0*mm;
  fGigaTrackerMCBMagnetYLength = 1246.0*mm;
  fGigaTrackerMCBMagnetZLength = 2.5 *m;
  fGigaTrackerMDXMagnetXLength = 600.0*mm;
  fGigaTrackerMDXMagnetYLength = 600.0*mm;
  fGigaTrackerMDXMagnetZLength = 0.4 *m;

  // Collimator dimension
  //fGigaTrackerCollimatorOuterXLength = fGigaTrackerDetectorXLength;
  //fGigaTrackerCollimatorOuterYLength = fGigaTrackerDetectorYLength;
  fGigaTrackerCollimatorOuterXLength = 100.*mm;
  fGigaTrackerCollimatorOuterYLength = 100.*mm;
  fGigaTrackerCollimatorInnerXLength = 66. *mm;
  fGigaTrackerCollimatorInnerYLength = 33. *mm;
  fGigaTrackerCollimatorZLength      = 1.0 *m;
  
  // Magnet B field
  fGigaTrackerArchomatMagnetFieldStrength[0] = -1.6678 *tesla;
  fGigaTrackerArchomatMagnetFieldStrength[1] =  1.6678 *tesla;
  fGigaTrackerArchomatMagnetFieldStrength[2] =  1.6678 *tesla;
  fGigaTrackerArchomatMagnetFieldStrength[3] = -1.6678 *tesla;


  fGigaTrackerTRIM5MagnetFieldStrength = -0.7505 *tesla;

  // Cooling plate dimension
  fCoolingPlateXLength = 80.0*mm;
  fCoolingPlateYLength = 70.0*mm;
  //fCoolingPlateZLength = 0.5*mm;
  fCoolingPlateZLength = 0.69*mm;
  
  fCoolingPlateTopShoulderXLength = 65.08*mm;
  fCoolingPlateTopHollowXLength = 64.87*mm;
  fCoolingPlateTopShoulderYLength = 41.50*mm;
  fCoolingPlateTopHollowYLength = 41.29*mm;
  fCoolingPlateTopDepth = 0.08*mm;

  fCoolingPlateBottomShoulderXLength = 65.08*mm;
  fCoolingPlateBottomHollowXLength = 64.27*mm;
  fCoolingPlateBottomShoulderYLength = 32*mm;
  fCoolingPlateBottomHollowYLength = 31.19*mm;
  //fCoolingPlateBottomDepth = 0.29*mm;
  fCoolingPlateBottomDepth = 0.48*mm;

  fCoolingPlateChannelsEnvelopeXLength = 61.6*mm;
  fCoolingPlateChannelsEnvelopeYLength = 41.29*mm;
  fCoolingPlateChannelsEnvelopeZLength = 70.0*um;

  fCoolingPlateChannelsXOffset = 1.73*mm - 100*um; // - Half channel wall !
  fCoolingPlateChannelsDepth = 30*um;

  fCoolingPlateChannelCoolantXLength = 200*um;
  fCoolingPlateChannelHalfWallXLength = 100*um;

  // PCB
  fGigaTrackerPCBXLength = 270.0*mm; 
  fGigaTrackerPCBYLength = 146.0*mm;
  fGigaTrackerPCBZLength = 2.0*mm;
  //fGigaTrackerPCBHoleXLength = 64.4*mm; 
  //fGigaTrackerPCBHoleYLength = 43.0*mm;
  fGigaTrackerPCBHoleXLength = 73.0*mm; 
  fGigaTrackerPCBHoleYLength = 43.0*mm;

  
  //fGigaTrackerPCBHoleXOffset = 185.6*mm;
  fGigaTrackerPCBHoleXOffset = 181.3*mm; // <----

  // --------------------
  // |                  |
  // |         |-----|  |
  // |<------->|  0  |  |
  // |         |-----|  |
  // |                  |
  // --------------------


  ///////////////
  // Positions //
  ///////////////

   // Sensor position
  fGigaTrackerSensorPosition = G4ThreeVector(0., 0., 0.5 * (fGigaTrackerSensorZLength - fGigaTrackerPixelZLength));

  // Bump Bonding position
  fGigaTrackerBumpBondingXOffset  = 50. *um;
  fGigaTrackerBumpBondingYOffset  = 50. *um;
  fGigaTrackerBumpBondingPosition = G4ThreeVector(fGigaTrackerBumpBondingXOffset, fGigaTrackerBumpBondingYOffset, 
						  + 0.5 * (fGigaTrackerBumpBondingZLength - fGigaTrackerPixelZLength) 
						  + fGigaTrackerSensorZLength);

  // Chip position
  fGigaTrackerChipPosition = G4ThreeVector(0., 0., 0.5 * (fGigaTrackerChipZLength - fGigaTrackerPixelZLength) 
					   + fGigaTrackerSensorZLength + fGigaTrackerBumpBondingZLength);

  // Structure position
  fGigaTrackerSupportPosition = G4ThreeVector(0., 0., 0.5 * (fGigaTrackerSupportZLength - fGigaTrackerPixelZLength) 
					      + fGigaTrackerSensorZLength + fGigaTrackerBumpBondingZLength 
					      + fGigaTrackerChipZLength);

  // Sensor position 
  fGigaTrackerSensorPosition = G4ThreeVector(0., 0., 0.5 * fGigaTrackerSensorZLength);

  // Bump Bonding position 
  fGigaTrackerBumpBondingXOffset  = 50. *um;
  fGigaTrackerBumpBondingYOffset  = 50. *um;
  fGigaTrackerBumpBondingPosition = G4ThreeVector(fGigaTrackerBumpBondingXOffset, fGigaTrackerBumpBondingYOffset, 
						  + 0.5 * (fGigaTrackerBumpBondingZLength) 
						  + fGigaTrackerSensorZLength);

  // Chip position
  fGigaTrackerChipPosition = G4ThreeVector(0., 0., 0.5 * (fGigaTrackerChipZLength) 
					   + fGigaTrackerSensorZLength + fGigaTrackerBumpBondingZLength);

  // Station position
  fGigaTrackerStation1Position = G4ThreeVector(0.,       0.,  79.600 *m - 0.5 * fGigaTrackerStationZLength - fGigaTrackerDetectorZPosition);
  fGigaTrackerStation2Position = G4ThreeVector(0., -60. *mm,  92.800 *m - 0.5 * fGigaTrackerStationZLength - fGigaTrackerDetectorZPosition);
  fGigaTrackerStation3Position = G4ThreeVector(0.,       0., 102.400 *m - 0.5 * fGigaTrackerStationZLength - fGigaTrackerDetectorZPosition);

  // Magnet position
  fGigaTrackerMagnet1Position = G4ThreeVector(0., 0.,  82.980 *m - 0.5 * fGigaTrackerMCBMagnetZLength - fGigaTrackerDetectorZPosition);
  fGigaTrackerMagnet2Position = G4ThreeVector(0., 0.,  86.580 *m - 0.5 * fGigaTrackerMCBMagnetZLength - fGigaTrackerDetectorZPosition);
  fGigaTrackerMagnet3Position = G4ThreeVector(0., 0.,  96.180 *m - 0.5 * fGigaTrackerMCBMagnetZLength - fGigaTrackerDetectorZPosition);
  fGigaTrackerMagnet4Position = G4ThreeVector(0., 0.,  99.780 *m - 0.5 * fGigaTrackerMCBMagnetZLength - fGigaTrackerDetectorZPosition);
  fGigaTrackerMagnet5Position = G4ThreeVector(0., 0., 102.000 *m - 0.5 * fGigaTrackerMDXMagnetZLength - fGigaTrackerDetectorZPosition);

  // Collimator position
  fGigaTrackerCollimatorPosition = G4ThreeVector(0., 0., 101.255 *m - 0.5 * fGigaTrackerCollimatorZLength - fGigaTrackerDetectorZPosition);

}

GigaTrackerGeometryParameters::~GigaTrackerGeometryParameters() {}

GigaTrackerGeometryParameters* GigaTrackerGeometryParameters::GetInstance()
{

  if ( fInstance == 0 ) fInstance = new GigaTrackerGeometryParameters();
  return fInstance;

}

TObjArray GigaTrackerGeometryParameters::GetHashTable()
{

  TObjArray GigaTrackerGeometryParameters;
  std::ostringstream Buffer;
  TString Value;
  TObjArray ParameterData;

  Buffer << fWorldZLength;
  Value = Buffer.str();
  Buffer.str("");
  ParameterData.Add(new TVectorT<G4double>(1, &fWorldZLength));
  GigaTrackerGeometryParameters.Add(new DetectorParameter("fWorldZLength", Value.Data(),
							  "World Z Length", ParameterData));
  ParameterData.Clear();

  Buffer << fWorldXLength;
  Value = Buffer.str();
  Buffer.str("");
  ParameterData.Add(new TVectorT<G4double>(1, &fWorldXLength));
  GigaTrackerGeometryParameters.Add(new DetectorParameter("fWorldXLength", Value.Data(),
							  "World X Length", ParameterData));
  ParameterData.Clear();

  Buffer << fWorldYLength;
  Value = Buffer.str();
  Buffer.str("");
  ParameterData.Add(new TVectorT<G4double>(1, &fWorldYLength));
  GigaTrackerGeometryParameters.Add(new DetectorParameter("fWorldYLength", Value.Data(),
							  "World Y Length", ParameterData));
  ParameterData.Clear();

  Buffer << fGigaTrackerDetectorZPosition;
  Value = Buffer.str();
  Buffer.str("");
  ParameterData.Add(new TVectorT<G4double>(1, &fGigaTrackerDetectorZPosition));
  GigaTrackerGeometryParameters.Add(new DetectorParameter("fGigaTrackerDetectorZPosition", Value.Data(),
							  "GigaTracker Detector Z Position", ParameterData));
  ParameterData.Clear();

  Buffer << fGigaTrackerDetectorZLength;
  Value = Buffer.str();
  Buffer.str("");
  ParameterData.Add(new TVectorT<G4double>(1, &fGigaTrackerDetectorZLength));
  GigaTrackerGeometryParameters.Add(new DetectorParameter("fGigaTrackerDetectorZLength", Value.Data(),
							  "GigaTracker Detector Z Length", ParameterData));
  ParameterData.Clear();

  Buffer << fGigaTrackerDetectorXLength;
  Value = Buffer.str();
  Buffer.str("");
  ParameterData.Add(new TVectorT<G4double>(1, &fGigaTrackerDetectorXLength));
  GigaTrackerGeometryParameters.Add(new DetectorParameter("fGigaTrackerDetectorXLength", Value.Data(),
							  "GigaTracker Detector X Length", ParameterData));
  ParameterData.Clear();

  Buffer << fGigaTrackerDetectorYLength;
  Value = Buffer.str();
  Buffer.str("");
  ParameterData.Add(new TVectorT<G4double>(1, &fGigaTrackerDetectorYLength));
  GigaTrackerGeometryParameters.Add(new DetectorParameter("fGigaTrackerDetectorYLength", Value.Data(),
							  "GigaTracker Detector Y Length", ParameterData));

  // Add for Pixel

  return GigaTrackerGeometryParameters;

}
void GigaTrackerGeometryParameters::Print()
{

  G4cout << "fGigaTrackerDetectorZPosition= " << fGigaTrackerDetectorZPosition << G4endl
    
	 << "fGigaTrackerDetectorXLength= " << fGigaTrackerDetectorXLength << G4endl
	 << "fGigaTrackerDetectorYLength= " << fGigaTrackerDetectorYLength << G4endl
	 << "fGigaTrackerDetectorZLength= " << fGigaTrackerDetectorZLength << G4endl

	 << fGigaTrackerSensorXLength << G4endl
	 << fGigaTrackerSensorYLength << G4endl
	 << fGigaTrackerSensorZLength << G4endl

	 << fGigaTrackerBumpBondingRLength << G4endl
	 << fGigaTrackerBumpBondingZLength << G4endl

	 << fGigaTrackerChipXLength << G4endl
	 << fGigaTrackerChipYLength << G4endl
	 << fGigaTrackerChipZLength << G4endl

	 << fGigaTrackerSupportXLength << G4endl
	 << fGigaTrackerSupportYLength << G4endl
	 << fGigaTrackerSupportZLength << G4endl

	 << fGigaTrackerBigPixelXLength << G4endl
	 << fGigaTrackerBigPixelYLength << G4endl
	 << fGigaTrackerSmallPixelXLength << G4endl
	 << fGigaTrackerSmallPixelYLength << G4endl
	 << fGigaTrackerPixelZLength << G4endl

	 << fGigaTrackerStationXLength << G4endl
	 << fGigaTrackerStationYLength << G4endl
	 << fGigaTrackerStationZLength << G4endl

	 << fGigaTrackerMCBMagnetXLength << G4endl
	 << fGigaTrackerMCBMagnetYLength << G4endl
	 << fGigaTrackerMCBMagnetZLength << G4endl
	 << fGigaTrackerMDXMagnetXLength << G4endl
	 << fGigaTrackerMDXMagnetYLength << G4endl
	 << fGigaTrackerMDXMagnetZLength << G4endl

	 << fGigaTrackerCollimatorOuterXLength << G4endl
	 << fGigaTrackerCollimatorOuterYLength << G4endl
	 << fGigaTrackerCollimatorInnerXLength << G4endl
	 << fGigaTrackerCollimatorInnerYLength << G4endl
	 << fGigaTrackerCollimatorZLength << G4endl

	 << fGigaTrackerSensorPosition.getX() << " " << fGigaTrackerSensorPosition.getY() << " " << fGigaTrackerSensorPosition.getZ() << G4endl

	 << fGigaTrackerBumpBondingXOffset << G4endl
	 << fGigaTrackerBumpBondingYOffset << G4endl
	 << fGigaTrackerBumpBondingPosition.getX() << " " << fGigaTrackerBumpBondingPosition.getY() << " " << fGigaTrackerBumpBondingPosition.getZ() << G4endl 

	 << fGigaTrackerChipPosition.getX() << " " << fGigaTrackerChipPosition.getY() << " " << fGigaTrackerChipPosition.getZ() << G4endl

	 << fGigaTrackerSupportPosition.getX() << " " << fGigaTrackerSupportPosition.getY() << " " << fGigaTrackerSupportPosition.getZ() << G4endl

	 << fGigaTrackerStation1Position.getX() << " " << fGigaTrackerStation1Position.getY() << " " << fGigaTrackerStation1Position.getZ() << G4endl
	 << fGigaTrackerStation2Position.getX() << " " << fGigaTrackerStation2Position.getY() << " " << fGigaTrackerStation2Position.getZ() << G4endl
	 << fGigaTrackerStation3Position.getX() << " " << fGigaTrackerStation3Position.getY() << " " << fGigaTrackerStation3Position.getZ() << G4endl

	 << fGigaTrackerMagnet1Position.getX() << " " << fGigaTrackerMagnet1Position.getY() << " " << fGigaTrackerMagnet1Position.getZ() << G4endl
	 << fGigaTrackerMagnet2Position.getX() << " " << fGigaTrackerMagnet2Position.getY() << " " << fGigaTrackerMagnet2Position.getZ() << G4endl
	 << fGigaTrackerMagnet3Position.getX() << " " << fGigaTrackerMagnet3Position.getY() << " " << fGigaTrackerMagnet3Position.getZ() << G4endl
	 << fGigaTrackerMagnet4Position.getX() << " " << fGigaTrackerMagnet4Position.getY() << " " << fGigaTrackerMagnet4Position.getZ() << G4endl
	 << fGigaTrackerMagnet5Position.getX() << " " << fGigaTrackerMagnet5Position.getY() << " " << fGigaTrackerMagnet5Position.getZ() << G4endl

	 << fGigaTrackerCollimatorPosition.getX() << " " << fGigaTrackerCollimatorPosition.getY() << " " << fGigaTrackerCollimatorPosition.getZ() << G4endl

	 << fGigaTrackerArchomatMagnetFieldStrength[0] << G4endl
	 << fGigaTrackerArchomatMagnetFieldStrength[1] << G4endl
	 << fGigaTrackerArchomatMagnetFieldStrength[2] << G4endl
	 << fGigaTrackerArchomatMagnetFieldStrength[3] << G4endl
	 << fGigaTrackerTRIM5MagnetFieldStrength << G4endl;

}

  
//
// Return true if pixel <id> is big, else return false
//
G4bool GigaTrackerGeometryParameters::PixelIsBig(G4int id) const {
  G4int N = (id/fGigaTrackerSensorNColumns)*fGigaTrackerSensorNColumns;
  G4int Xindex = (id-N);
  // 40 / 80 / 120 / 160
  if(Xindex%fGigaTrackerChipNColumns == 0 && Xindex != 0) return true;
  // 39 / 79 / 119 / 159
  if((Xindex+1)%fGigaTrackerChipNColumns == 0 && Xindex != (fGigaTrackerSensorNColumns-1)) return true;
  return false;
}

//
// Return the distance between the sensor edge and the pixel *edge* 
// 
//
//      <N>
//       v
//  sensorRows-1 [0|1|2|...|sensorColumns -1]
//      ...      [...                    ...]
//       0       [0|1|2|...|sensorColumns -1]
//                ^ 
//             <Xindex>
//
G4double GigaTrackerGeometryParameters::PixelXOffset(G4int id) const {
  G4int N = (id/fGigaTrackerSensorNColumns)*fGigaTrackerSensorNColumns;
  G4int Xindex = (id - N);
  //N_big_*: # big pixels *before* pixel <id> 
  G4int N_big_1 = (Xindex-1)/fGigaTrackerChipNColumns; // 41/81/121/161
  G4int N_big_2 = Xindex/fGigaTrackerChipNColumns; // 40/80/120/160 
  return (Xindex-N_big_1-N_big_2)*fGigaTrackerSmallPixelXLength+(N_big_1+N_big_2)*fGigaTrackerBigPixelXLength; 
}

//////////////////////////////
// Pixels position and size //
//////////////////////////////

// ! Positions are relative to the corner of the sensor !

G4double GigaTrackerGeometryParameters::GetPixelXPosition(G4int id) const {
  if(PixelIsBig(id)) return PixelXOffset(id)+0.5*fGigaTrackerBigPixelXLength; 
  else return PixelXOffset(id)+0.5*fGigaTrackerSmallPixelXLength;
 
}

G4double GigaTrackerGeometryParameters::GetPixelYPosition(G4int id) const {
  return 0.5*fGigaTrackerSmallPixelYLength+(id/fGigaTrackerSensorNColumns)*fGigaTrackerSmallPixelYLength;
}

G4double GigaTrackerGeometryParameters::GetPixelXLength(G4int id) const {
  if(PixelIsBig(id)) return fGigaTrackerBigPixelXLength;
  return fGigaTrackerSmallPixelXLength;
}

G4double GigaTrackerGeometryParameters::GetPixelYLength(G4int id) const {
  return fGigaTrackerSmallPixelYLength;
}

/////////////////////////
// Bump bonds position //
/////////////////////////
G4double GigaTrackerGeometryParameters::GetBumpBondXPosition(G4int id) const { 
  G4double offset = 0.0;  
  if(PixelIsBig(id)) offset = fGigaTrackerBumpBondBigOffset;
  if(id%2) return GetPixelXPosition(id)+(fGigaTrackerBumpBondOffset+offset); 
  else return GetPixelXPosition(id)-(fGigaTrackerBumpBondOffset+offset);  
} 

G4double GigaTrackerGeometryParameters::GetBumpBondYPosition(G4int id) const {
  if ( (id/fGigaTrackerSensorNColumns) > (fGigaTrackerChipNRows-1) ) return GetPixelYPosition(id)+fGigaTrackerBumpBondOffset;
  else return GetPixelYPosition(id)-fGigaTrackerBumpBondOffset;
}
