#include "GigaTrackerKovarConnector.hh"

GigaTrackerKovarConnector::GigaTrackerKovarConnector(G4Material * Material, G4LogicalVolume * MotherVolume, G4ThreeVector Position, G4int type, G4int iCopy) : NA62VComponent(Material, MotherVolume) 
{
  //
  // I do not use a global mother volume to allow the placement of a tube.
  // I added the type flag {0,1,2,3} to select the oriantation the the connector. 
  // 
  fPosition = Position;
  fiCopy = iCopy;
  switch (type) {
    case 0:
      break;
    case 1:
      break;
    case 2:
      break;
    case 3:
      break;
    default:
      G4cerr << "GigaTrackerKovarConnector: Wrong orientation, using default value." << G4endl;
      type = 0;
  }
  fType = type;

  ReadGeometryParameters();
  CreateGeometry();
  SetProperties();
}

GigaTrackerKovarConnector::~GigaTrackerKovarConnector() {
}

void GigaTrackerKovarConnector::ReadGeometryParameters()
{  
}

void GigaTrackerKovarConnector::CreateGeometry()
{
  /////////////////////
  // Kovar Connector //
  /////////////////////

  G4double A;
  G4double Z;
  A = 28.09*g/mole;
  G4Element* elSi  = new G4Element("Silicon","Si",Z = 14.,A);

  A  =  54.94*g/mole;
  G4Element* elMn   =  new G4Element("Manganese","Mn",Z = 25.,A);
 
  A = 58.93*g/mole;
  G4Element* elCo  = new G4Element("Cobalt","Co",Z = 27.,A);

  A = 58.70*g/mole;
  G4Element* elNi  = new G4Element("Nickel","Ni",Z = 28.,A);
   
  A = 55.85*g/mole;
  G4Element* elFe  = new G4Element("Iron","Fe",Z = 26.,A);
 
  // Kovar
  //FIXME
  G4double d = 8.36*g/cm3 ;
  fMatKovar = new G4Material("Kovar",d,5);
  fMatKovar->AddElement(elMn, 0.003);
  fMatKovar->AddElement(elSi, 0.001);
  fMatKovar->AddElement(elCo, 0.17);
  fMatKovar->AddElement(elNi, 0.29);
  fMatKovar->AddElement(elFe, 0.536);
  
  //FIXME remove 1.0*
  
  



  // Z = 0 at the base of the connector.

  // Base
  
  fInnerDiameter = 1.0*mm;
  fBaseOuterDiameter = 6.0*mm;
  fBaseHeight = 1.5*mm;
  
  fBaseSolidVolume =  new G4Tubs("GigaTrackerKovarConnBase",
             0.5*fInnerDiameter,
             0.5*fBaseOuterDiameter,
             0.5*fBaseHeight,
             0,
             2*pi);
  
  fBaseLogicalVolume = new G4LogicalVolume(fBaseSolidVolume,fMatKovar,"GigaTrackerKovarConnBase",0,0,0);
  
  G4ThreeVector basePos(0.0,0.0,-0.5*fBaseHeight);
  
  fBasePhysicalVolume = new G4PVPlacement(0,
						     fPosition+basePos,
						     fBaseLogicalVolume,
						     "GigaTrackerKovarConnBase",
						     fMotherVolume,
						     false,
						     fiCopy);
  
  
  // Body
  
  fBodyHeight = 6.7*mm; //FIXME Total height != bodyHeight
  fBodyLength = 4.5*mm;
  fBodyTubeHeight = 4.3*mm; 
  fBodyConnDiam = 1.65*mm;

  // Tube -> Square of same area
  G4double bodyHeight = fBodyTubeHeight - 0.25*fBodyConnDiam*sqrt(pi);
  G4double innerSquareSize = 0.5*fInnerDiameter*sqrt(pi); 
  G4double bodyFBXLength = 0.5*(fBodyLength - innerSquareSize);
  G4double bodySideYLength = 0.5*(fBodyLength - innerSquareSize);

  fBodyBackSolidVolume = new G4Box("GigaTrackerKovarConnBodyBack",0.5*bodyFBXLength,0.5*fBodyLength,0.5*bodyHeight);
  fBodyFrontSolidVolume = new G4Box("GigaTrackerKovarConnBodyFront",0.5*bodyFBXLength,0.5*fBodyLength,0.5*bodyHeight);
  fBodySideSolidVolume = new G4Box("GigaTrackerKovarConnBodySide",0.5*innerSquareSize,0.5*bodySideYLength,0.5*bodyHeight);
  
  fBodyBackLogicalVolume = new G4LogicalVolume(fBodyBackSolidVolume,fMatKovar,"GigaTrackerKovarConnBodyBack",0,0,0);
  fBodyFrontLogicalVolume = new G4LogicalVolume(fBodyFrontSolidVolume,fMatKovar,"GigaTrackerKovarConnBodyFront",0,0,0);
  fBodySideLogicalVolume = new G4LogicalVolume(fBodySideSolidVolume,fMatKovar,"GigaTrackerKovarConnBodySide",0,0,0);
    
  G4ThreeVector bodyBack(-0.5*(fBodyLength - bodyFBXLength),0,-0.5*bodyHeight-fBaseHeight);
  G4ThreeVector bodyFront(0.5*(fBodyLength - bodyFBXLength),0,-0.5*bodyHeight-fBaseHeight);
  G4ThreeVector bodySideA(0,0.5*(fBodyLength - bodySideYLength),-0.5*bodyHeight-fBaseHeight);
  G4ThreeVector bodySideB(0,-0.5*(fBodyLength - bodySideYLength),-0.5*bodyHeight-fBaseHeight);
  fBodyBackPhysicalVolume = new G4PVPlacement(0,
					       fPosition+bodyBack,
						     fBodyBackLogicalVolume,
						     "GigaTrackerKovarConnBodyBack",
						     fMotherVolume,
						     false,
						     fiCopy);
						     
	fBodyFrontPhysicalVolume = new G4PVPlacement(0,
						     fPosition+bodyFront,
						     fBodyFrontLogicalVolume,
						     "GigaTrackerKovarConnBodyFront",
						     fMotherVolume,
						     false,
						     fiCopy);
  
  fBodySideAPhysicalVolume = new G4PVPlacement(0,
						     fPosition+bodySideA,
						     fBodySideLogicalVolume,
						     "GigaTrackerKovarConnBodySideA",
						     fMotherVolume,
						     false,
						     fiCopy);
						     
	 fBodySideBPhysicalVolume = new G4PVPlacement(0,
						     fPosition+bodySideB,
						     fBodySideLogicalVolume,
						     "GigaTrackerKovarConnBodySideB",
						     fMotherVolume,
						     false,
						     fiCopy);
						     
						     
  G4double neckSquare = 0.5*fBodyConnDiam*sqrt(pi);
  G4double bodySideNeckYLength = 0.5*(fBodyLength - neckSquare);
  G4double fNeckConnGap = 0.15*mm; // Horizontal tube changes its diam. before reaching the center of the connector
  
  // One volume for the neck (to allow easier rotation of the connector)
  fBodyNeckSolidVolume = new G4Box("GigaTrackerKovarConnBodyNeck",0.5*fBodyLength,0.5*fBodyLength,0.5*neckSquare);
  fBodyNeckLogicalVolume = new G4LogicalVolume(fBodyNeckSolidVolume,G4Material::GetMaterial("G4_Galactic"),"GigaTrackerKovarConnBodyNeck",0,0,0);
  
  G4ThreeVector bodyNeck(0.0,0.0,(-bodyHeight-fBaseHeight-0.5*neckSquare));
  G4RotationMatrix * neckRot = new G4RotationMatrix();
  switch(fType) {
    case 0:
      neckRot->rotateZ(0*rad);
      break;
    case 1:
      neckRot->rotateZ(0.5*pi*rad);
      break;
    case 2:
      neckRot->rotateZ(pi*rad);
      break;
    case 3:
      neckRot->rotateZ((3.0/2.0)*pi*rad);
      break;
    
  }
  
  
  
  fBodyBackNeckSolidVolume = new G4Box("GigaTrackerKovarConnBodyBackNeck",0.5*bodyFBXLength,0.5*fBodyLength,0.5*neckSquare);
  fBodySideNeckSolidVolume = new G4Box("GigaTrackerKovarConnBodySideNeck",0.5*(innerSquareSize+fNeckConnGap),0.5*bodySideYLength,0.5*neckSquare);
  fBodyCollarNeckSolidVolume = new G4Box("GigaTrackerKovarConnBodyCollarNeck",0.5*(bodyFBXLength-fNeckConnGap),0.5*bodySideNeckYLength,0.5*neckSquare);
  
  G4cout << "Tub pos: " << (bodyHeight+ fBaseHeight + 0.5*neckSquare)/mm << G4endl;
  
  fBodyBackNeckLogicalVolume = new G4LogicalVolume(fBodyBackNeckSolidVolume,fMatKovar,"GigaTrackerKovarConnBodyBackNeck",0,0,0);
  fBodySideNeckLogicalVolume = new G4LogicalVolume(fBodySideNeckSolidVolume,fMatKovar,"GigaTrackerKovarConnBodySideNeck",0,0,0);
  fBodyCollarNeckLogicalVolume = new G4LogicalVolume(fBodyCollarNeckSolidVolume,fMatKovar,"GigaTrackerKovarConnBodyCollarNeck",0,0,0);

  G4ThreeVector bodyBackNeck = G4ThreeVector(-0.5*(fBodyLength - bodyFBXLength),0.0,0.0);
  G4ThreeVector bodySideNeckA = G4ThreeVector(0.5*fNeckConnGap,0.5*(fBodyLength - bodySideYLength),0.0);
  G4ThreeVector bodySideNeckB = G4ThreeVector(0.5*fNeckConnGap,-0.5*(fBodyLength - bodySideYLength),0.0);
  G4ThreeVector bodyCollarNeckA = G4ThreeVector(0.5*(fBodyLength - bodyFBXLength + fNeckConnGap),0.5*(fBodyLength - 0.5*(fBodyLength - neckSquare)),0.0);
  G4ThreeVector bodyCollarNeckB = G4ThreeVector(0.5*(fBodyLength - bodyFBXLength + fNeckConnGap),-0.5*(fBodyLength - 0.5*(fBodyLength - neckSquare)),0.0);
      
  
  fBodyBackNeckPhysicalVolume = new G4PVPlacement(0,
					       bodyBackNeck,
						     fBodyBackNeckLogicalVolume,
						     "GigaTrackerKovarConnBodyBackNeck",
						     fBodyNeckLogicalVolume,
						     false,
						     fiCopy);
	
	fBodySideNeckAPhysicalVolume = new G4PVPlacement(0,
					       bodySideNeckA,
						     fBodySideNeckLogicalVolume,
						     "GigaTrackerKovarConnBodySideNeckA",
						     fBodyNeckLogicalVolume,
						     false,
						     fiCopy);
	
	fBodySideNeckBPhysicalVolume = new G4PVPlacement(0,
					       bodySideNeckB,
						     fBodySideNeckLogicalVolume,
						     "GigaTrackerKovarConnBodySideNeckB",
						     fBodyNeckLogicalVolume,
						     false,
						     fiCopy);
  
   fBodyCollarNeckAPhysicalVolume = new G4PVPlacement(0,
					       bodyCollarNeckA,
						     fBodyCollarNeckLogicalVolume,
						     "GigaTrackerKovarConnBodyCollarNeckA",
						     fBodyNeckLogicalVolume,
						     false,
						     fiCopy);
						     
	fBodyCollarNeckBPhysicalVolume = new G4PVPlacement(0,
					       bodyCollarNeckB,
						     fBodyCollarNeckLogicalVolume,
						     "GigaTrackerKovarConnBodyCollarNeckB",
						     fBodyNeckLogicalVolume,
						     false,
						     fiCopy);

 
  
  fBodyNeckPhysicalVolume = new G4PVPlacement(neckRot,
					       fPosition+bodyNeck,
						     fBodyNeckLogicalVolume,
						     "GigaTrackerKovarConnBodyNeck",
						     fMotherVolume,
						     false,
						     fiCopy);
  
  
  
   G4double coverThickness = fBodyHeight - fBodyTubeHeight - innerSquareSize;
  
  fBodyCoverSolidVolume = new G4Box("GigaTrackerKoverConnBodyCover",0.5*fBodyLength,0.5*fBodyLength,0.5*coverThickness);
  
  fBodyCoverLogicalVolume = new G4LogicalVolume(fBodyCoverSolidVolume,fMatKovar,"GigaTrackerKovarConnBodyCover",0,0,0);
  				
  G4ThreeVector bodyCover(0,0,-1.0*(bodyHeight+fBaseHeight+neckSquare+0.5*coverThickness));
  
  					     
	fBodyCoverPhysicalVolume = new G4PVPlacement(0,
					       fPosition+bodyCover,
						     fBodyCoverLogicalVolume,
						     "GigaTrackerKovarConnBodyCover",
						     fMotherVolume,
						     false,
						     fiCopy);
	  // Front

  
  std::vector<G4TwoVector> vertices;
  
  G4double spire = 1*mm;
      
  G4ThreeVector frontPos;
  
  G4TwoVector vzero;
  G4TwoVector vone;
  G4TwoVector vtwo;
  G4TwoVector vthree;
  
  
  switch (fType) {
    case 0:

  vzero = G4TwoVector(0.0,-0.5*fBodyLength);
  vone =  G4TwoVector(spire,0.0);
  vtwo = G4TwoVector(0.0,+0.5*fBodyLength);
  vthree = G4TwoVector(0.0,0.0);
  frontPos = G4ThreeVector(0.5*fBodyLength,0,0);
    break;
    
    case 1:
 
  vzero = G4TwoVector(0.0,-spire);
  vone = G4TwoVector(+0.5*fBodyLength,0.0);
  vtwo = G4TwoVector(0.0,0.0);
  vthree = G4TwoVector(-0.5*fBodyLength,0.0);
  frontPos = G4ThreeVector(0,-0.5*fBodyLength,0);
    break;
  
    case 2:

  vzero = G4TwoVector(0.0,-0.5*fBodyLength);
  vone =  G4TwoVector(0.0,0.0);
  vtwo = G4TwoVector(0.0,+0.5*fBodyLength);
  vthree = G4TwoVector(-spire,0.0); 
  frontPos = G4ThreeVector(-0.5*fBodyLength,0,0);
    break;
  
    case 3:
    vzero =  G4TwoVector(0.0,0.0);
    vone = G4TwoVector(+0.5*fBodyLength,0.0);
    vtwo = G4TwoVector(0.0,+spire);
    vthree = G4TwoVector(-0.5*fBodyLength,0.0);
    frontPos = G4ThreeVector(0,+0.5*fBodyLength,0);
    break;
    }

  //Bottom
  vertices.push_back(vzero);
  vertices.push_back(vone);
  vertices.push_back(vtwo);
  vertices.push_back(vthree);
  //Top
  vertices.push_back(vzero);
  vertices.push_back(vone);
  vertices.push_back(vtwo);
  vertices.push_back(vthree);
  
   //FIXME
   G4double banana = 0.65*mm;
  
   fFrontBottomSolidVolume = new G4GenericTrap("GigaTrackerKoverConnFrontBottom",
                      0.5*(bodyHeight - 0.5*banana),
                 vertices);
  
     fFrontTopSolidVolume = new G4GenericTrap("GigaTrackerKoverConnFrontTop",
                      0.5*(coverThickness - 0.5*banana),
                 vertices);
  
    fFrontBottomLogicalVolume = new G4LogicalVolume(fFrontBottomSolidVolume,fMatKovar,"GigaTrackerKovarConnFrontBottom",0,0,0);
    fFrontTopLogicalVolume = new G4LogicalVolume(fFrontTopSolidVolume,fMatKovar,"GigaTrackerKovarConnFrontTop",0,0,0);
    
    G4ThreeVector frontBottom(0.0,0.0,-1.0*(0.5*bodyHeight+fBaseHeight - 0.25*banana));
    G4ThreeVector frontTop(0.0,0.0,-1.0*(bodyHeight+fBaseHeight+neckSquare+0.5*coverThickness + 0.25*banana));
    
	  fFrontBottomPhysicalVolume = new G4PVPlacement(0,
					       fPosition+frontBottom+frontPos,
						     fFrontBottomLogicalVolume,
						     "GigaTrackerKovarConnFrontBottom",
						     fMotherVolume,
						     false,
						     fiCopy);

  fFrontTopPhysicalVolume = new G4PVPlacement(0,
					       fPosition+frontTop+frontPos,
						     fFrontTopLogicalVolume,
						     "GigaTrackerKovarConnFrontTop",
						     fMotherVolume,
						     false,
						     fiCopy);

}

void GigaTrackerKovarConnector::SetProperties()
{
  fVisAtt = new G4VisAttributes(G4Colour(0.5,0.5,0.5));
  fVisAtt->SetVisibility(true);
  
  fBodyNeckLogicalVolume->SetVisAttributes(G4VisAttributes::Invisible);
}
