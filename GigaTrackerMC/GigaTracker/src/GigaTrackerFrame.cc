#include "GigaTrackerFrame.hh"

GigaTrackerFrame::GigaTrackerFrame(G4Material * Material, G4LogicalVolume * MotherVolume, G4ThreeVector Position, G4int iCopy) : NA62VComponent(Material, MotherVolume) 
{
  fPosition = Position;
  fiCopy = iCopy;

  ReadGeometryParameters();
  CreateGeometry();
  SetProperties();
}

GigaTrackerFrame::~GigaTrackerFrame() {
}

void GigaTrackerFrame::ReadGeometryParameters()
{
  GigaTrackerGeometryParameters* GeoPars = GigaTrackerGeometryParameters::GetInstance();
  fFrameZLength = 2.0*CLHEP::mm;
  
  fPCBHoleXLength = GeoPars->GetGigaTrackerPCBHoleXLength();
  fPCBHoleYLength = GeoPars->GetGigaTrackerPCBHoleYLength();
  fPCBZLength = GeoPars->GetGigaTrackerPCBZLength();
  
  // FIXME Move this to geom file
  fFrameLipTopBottomLength = 2*CLHEP::mm;
  fFrameLipRightLength = 1*CLHEP::mm;
  fFrameLipLeftXLength = 1*CLHEP::mm;
  fFrameLipLeftYLength = 20*CLHEP::mm;
  fFrameRightXLength = 0.5*CLHEP::cm;
  fFrameLeftXLength = 1.5*CLHEP::cm;
  fFrameTopBottomLeftXLength = 30*CLHEP::mm;
  // ----
  
  // FIXME Move this to geom file
  
  fCoolingPlateXLength = GeoPars->GetCoolingPlateXLength(); 
  fCoolingPlateYLength = GeoPars->GetCoolingPlateYLength();

	
  fPCBFootSizeVert = 0.5*(fCoolingPlateYLength - fPCBHoleYLength);
	fPCBFootSizeHoriz = 0.5*(fCoolingPlateXLength - fPCBHoleXLength);
	//FIXME
	fPCBFootHeight = (2.0-1.46)*CLHEP::mm;
  // ----
}

void GigaTrackerFrame::CreateGeometry()
{
  //FIXME
  double cplate = 0.69*CLHEP::mm;
  ///////////
  // Frame //
  ///////////
  fTopBottomLipSolidVolume = new G4Box("GigaTrackerTopBottomFrameLip",0.5*fPCBHoleXLength + fPCBFootSizeHoriz - 0.5*(fFrameLipRightLength + fFrameLipLeftXLength),0.5*fFrameLipTopBottomLength,0.5*(fFrameZLength + fPCBZLength - fPCBFootHeight - cplate));
  fRightLipSolidVolume = new G4Box("GigaTrackerRightFrameLip",0.5*fFrameLipRightLength,0.5*fPCBHoleYLength + fPCBFootSizeVert ,0.5*(fFrameZLength + fPCBZLength - fPCBFootHeight - cplate));
  
  fLeftLipSolidVolume = new G4Box("GigaTrackerLeftFrameLip",0.5*fFrameLipLeftXLength,0.5*fFrameLipLeftYLength,0.5*(fFrameZLength + fPCBZLength - fPCBFootHeight - cplate));

  fLeftFrameSolidVolume = new G4Box("GigaTrackerLeftFrame",0.5*fFrameLeftXLength,0.5*fPCBHoleYLength + fPCBFootSizeVert,0.5*fFrameZLength);
  fRightFrameSolidVolume =  new G4Box("GigaTrackerRightFrame",0.5*fFrameRightXLength,0.5*fPCBHoleYLength + fPCBFootSizeVert,0.5*fFrameZLength);
  fTopBottomLeftFrameSolidVolume = new G4Box("GigaTrackerTopBottomLeftFrame",0.5*fFrameTopBottomLeftXLength,0.5*fFrameLipLeftYLength,0.5*fFrameZLength);
  
  
  fTopBottomLipLogicalVolume = new G4LogicalVolume(fTopBottomLipSolidVolume ,fMaterial,"GigaTrackerTopBottomFrameLip",0,0,0);
  fRightLipLogicalVolume = new G4LogicalVolume(fRightLipSolidVolume,fMaterial,"GigaTrackerRightFrameLip",0,0,0);
  fLeftLipLogicalVolume = new G4LogicalVolume(fLeftLipSolidVolume,fMaterial,"GigaTrackerLeftFrameLip",0,0,0);
  
  fLeftFrameLogicalVolume = new G4LogicalVolume(fLeftFrameSolidVolume ,fMaterial,"GigaTrackerLeftFrame",0,0,0);
  fRightFrameLogicalVolume = new G4LogicalVolume(fRightFrameSolidVolume ,fMaterial,"GigaTrackerRightFrame",0,0,0);
  fTopBottomLeftFrameLogicalVolume = new G4LogicalVolume(fTopBottomLeftFrameSolidVolume  ,fMaterial,"GigaTrackerTopBottomLeftFrame",0,0,0);
  
  
  
  G4ThreeVector TopLeftLipPos(-0.5*(fPCBHoleXLength+2*fPCBFootSizeHoriz-fFrameLipLeftXLength),0.5*(fPCBHoleYLength + 2*fPCBFootSizeVert - fFrameLipLeftYLength),0.5*(fFrameZLength + fPCBZLength - fPCBFootHeight - cplate) - fFrameZLength);
  G4ThreeVector BottomLeftLipPos(-0.5*(fPCBHoleXLength+2*fPCBFootSizeHoriz-fFrameLipLeftXLength),-0.5*(fPCBHoleYLength + 2*fPCBFootSizeVert - fFrameLipLeftYLength),0.5*(fFrameZLength + fPCBZLength - fPCBFootHeight - cplate) - fFrameZLength);
  
  G4ThreeVector RightLipPos(0.5*(fPCBHoleXLength+2*fPCBFootSizeHoriz-fFrameLipRightLength),0,0.5*(fFrameZLength + fPCBZLength - fPCBFootHeight - cplate) - fFrameZLength);
  G4ThreeVector TopLipPos(0,0.5*(fPCBHoleYLength+2*fPCBFootSizeVert-fFrameLipTopBottomLength),0.5*(fFrameZLength + fPCBZLength - fPCBFootHeight - cplate) - fFrameZLength);
  G4ThreeVector BottomLipPos(0,-0.5*(fPCBHoleYLength+2*fPCBFootSizeVert-fFrameLipTopBottomLength),0.5*(fFrameZLength + fPCBZLength - fPCBFootHeight - cplate) - fFrameZLength);
  
  G4ThreeVector LeftFramePos(-0.5*(fPCBHoleXLength+2*fPCBFootSizeHoriz+ fFrameLeftXLength + 2*fFrameTopBottomLeftXLength),0,-0.5*fFrameZLength); 
  G4ThreeVector RightFramePos(0.5*(fPCBHoleXLength+2*fPCBFootSizeHoriz+ fFrameRightXLength),0,-0.5*fFrameZLength);
  
  G4ThreeVector TopLeftFramePos(-(0.5*fPCBHoleXLength + fPCBFootSizeHoriz - 0.5*(fFrameLipRightLength + fFrameLipLeftXLength)) - fFrameLipLeftXLength - 0.5*fFrameTopBottomLeftXLength,0.5*(fPCBHoleYLength + 2*fPCBFootSizeVert - fFrameLipLeftYLength),-0.5*fFrameZLength);
  G4ThreeVector BottomLeftFramePos(-(0.5*fPCBHoleXLength + fPCBFootSizeHoriz - 0.5*(fFrameLipRightLength + fFrameLipLeftXLength)) - fFrameLipLeftXLength - 0.5*fFrameTopBottomLeftXLength,-0.5*(fPCBHoleYLength + 2*fPCBFootSizeVert - fFrameLipLeftYLength),-0.5*fFrameZLength);
  
  fTopLeftLipPhysicalVolume = new G4PVPlacement(0,
				      fPosition+TopLeftLipPos,
				      fLeftLipLogicalVolume,
				      "GigaTrackerTopLeftFrameLip",
				      fMotherVolume, 
				      false,
				      fiCopy);

 fBottomLeftLipPhysicalVolume = new G4PVPlacement(0,
				      fPosition+BottomLeftLipPos,
				      fLeftLipLogicalVolume,
				      "GigaTrackerBottomLeftFrameLip",
				      fMotherVolume, 
				      false,
				      fiCopy);


 fRightLipPhysicalVolume = new G4PVPlacement(0,
				      fPosition+RightLipPos,
				      fRightLipLogicalVolume,
				      "GigaTrackerRightFrameLip",
				      fMotherVolume, 
				      false,
				      fiCopy);

 fTopLipPhysicalVolume = new G4PVPlacement(0,
				      fPosition+TopLipPos,
				      fTopBottomLipLogicalVolume,
				      "GigaTrackerTopFrameLip",
				      fMotherVolume, 
				      false,
				      fiCopy);

 fBottomLipPhysicalVolume = new G4PVPlacement(0,
				      fPosition+BottomLipPos,
				      fTopBottomLipLogicalVolume,
				      "GigaTrackerBottomFrameLip",
				      fMotherVolume, 
				      false,
				      fiCopy);


  fLeftFramePhysicalVolume = new G4PVPlacement(0,
				      fPosition+LeftFramePos,
				      fLeftFrameLogicalVolume,
				      "GigaTrackerLeftFrame",
				      fMotherVolume, 
				      false,
				      fiCopy);
				      
	fTopLeftFramePhysicalVolume = new G4PVPlacement(0,
				      fPosition+TopLeftFramePos,
				      fTopBottomLeftFrameLogicalVolume,
				      "GigaTrackerTopLeftFrame",
				      fMotherVolume, 
				      false,
				      fiCopy);
				      
	fBottomLeftFramePhysicalVolume = new G4PVPlacement(0,
				      fPosition+BottomLeftFramePos,
				      fTopBottomLeftFrameLogicalVolume,
				      "GigaTrackerBottomLeftFrame",
				      fMotherVolume, 
				      false,
				      fiCopy);
				      
  fRightFramePhysicalVolume = new G4PVPlacement(0,
				      fPosition+RightFramePos,
				      fRightFrameLogicalVolume,
				      "GigaTrackerRightFrame",
				      fMotherVolume, 
				      false,
				      fiCopy);

}

void GigaTrackerFrame::SetProperties()
{
  fVisAtt = new G4VisAttributes(G4Colour(0.5,0.5,0.5));
  fVisAtt->SetVisibility(true);
  fTopBottomLipLogicalVolume->SetVisAttributes(fVisAtt);
  fLeftLipLogicalVolume->SetVisAttributes(fVisAtt);
  fRightLipLogicalVolume->SetVisAttributes(fVisAtt);
  fLeftFrameLogicalVolume->SetVisAttributes(fVisAtt);
  fRightFrameLogicalVolume->SetVisAttributes(fVisAtt);
  fTopBottomLeftFrameLogicalVolume->SetVisAttributes(fVisAtt);
}
