//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// --------------------------------------------------------------
// History:
//
// Modified by Paolo Massarotti 2012-03-08
// Created by Simone Bifani (Simone.Bifani@cern.ch) 2008-03-10
// --------------------------------------------------------------
//
#include "globals.hh"
#include "GigaTrackerGeometryParameters.hh"
#include "GigaTrackerMaterialParameters.hh"
#include "GigaTrackerCollimator.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4Material.hh"

GigaTrackerCollimator::GigaTrackerCollimator(G4Material * Material, G4LogicalVolume * MotherVolume, G4ThreeVector Position, G4int iCopy) : 
NA62VComponent(Material, MotherVolume)
{

  fPosition = Position;
  fiCopy = iCopy;

  ReadGeometryParameters();
  // Mandatory here to Find or Build the needed materials
  GigaTrackerMaterialParameters::GetInstance();
  CreateGeometry();
  SetProperties();

}

GigaTrackerCollimator::~GigaTrackerCollimator() {}

void GigaTrackerCollimator::ReadGeometryParameters()
{

  // Read all the geometrical parameters and copy them to private members
  GigaTrackerGeometryParameters* GeoPars = GigaTrackerGeometryParameters::GetInstance();

  fOuterXLength = GeoPars->GetGigaTrackerCollimatorOuterXLength();
  fOuterYLength = GeoPars->GetGigaTrackerCollimatorOuterYLength();
  fInnerXLength = GeoPars->GetGigaTrackerCollimatorInnerXLength();
  fInnerYLength = GeoPars->GetGigaTrackerCollimatorInnerYLength();
  fZLength = GeoPars->GetGigaTrackerCollimatorZLength();

}

void GigaTrackerCollimator::CreateGeometry()
{

  //
  // G4 volumes
  //
  G4double HalfOuterXLength = 0.5 * fOuterXLength;
  G4double HalfOuterYLength = 0.5 * fOuterYLength;
  G4double HalfInnerXLength = 0.5 * fInnerXLength;
  G4double HalfInnerYLength = 0.5 * fInnerYLength;
  G4double HalfZLength = 0.5 * fZLength;

  // Create collimator

  fSolidVolume = new G4Box("GigaTrackerCollimator1", HalfOuterXLength, HalfOuterYLength, HalfZLength);
  fLogicalVolume = new G4LogicalVolume(fSolidVolume,            // solid
				       fMaterial,               // material
				       "GigaTrackerCollimator", // name
				       0,                       // field manager 
				       0,                       // sensitive detector
				       0);                      // user limits

  fPhysicalVolume = new G4PVPlacement(0,
				      fPosition +
				      G4ThreeVector(1,0,0)*(HalfInnerXLength - HalfOuterXLength)
				      +G4ThreeVector(0,-1,0)*(- HalfInnerYLength - HalfOuterYLength),
				      fLogicalVolume,          // its logical volume
				      "GigaTrackerCollimator", // its name
				      fMotherVolume,           // its mother  volume
				      false,                   // no boolean operations
				      fiCopy);                 // copy number

  fPhysicalVolume = new G4PVPlacement(0,
				      fPosition +
				      G4ThreeVector(1,0,0)*(-HalfInnerXLength - HalfOuterXLength)
				      + G4ThreeVector(0,-1,0)*(- HalfInnerYLength + HalfOuterYLength),
				      fLogicalVolume,          // its logical volume
				      "GigaTrackerCollimator", // its name
				      fMotherVolume,           // its mother  volume
				      false,                   // no boolean operations
				      fiCopy);                 // copy number
  fPhysicalVolume = new G4PVPlacement(0,
				      fPosition +
				      G4ThreeVector(1,0,0)*(-HalfInnerXLength + HalfOuterXLength)
				      + G4ThreeVector(0,-1,0)*(HalfInnerYLength + HalfOuterYLength),
				      fLogicalVolume,          // its logical volume
				      "GigaTrackerCollimator", // its name
				      fMotherVolume,           // its mother  volume
				      false,                   // no boolean operations
				      fiCopy);                 // copy number
  fPhysicalVolume = new G4PVPlacement(0,
				      fPosition +
				      G4ThreeVector(1,0,0)*(HalfInnerXLength + HalfOuterXLength)
				      + G4ThreeVector(0,-1,0)*(HalfInnerYLength - HalfOuterYLength),
				      fLogicalVolume,          // its logical volume
				      "GigaTrackerCollimator", // its name
				      fMotherVolume,           // its mother  volume
				      false,                   // no boolean operations
				      fiCopy);                 // copy number
}

void GigaTrackerCollimator::SetProperties()
{

  // Set visualization properties
  fVisAtt = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
  fVisAtt->SetVisibility(true);
  fLogicalVolume->SetVisAttributes(fVisAtt);
}
