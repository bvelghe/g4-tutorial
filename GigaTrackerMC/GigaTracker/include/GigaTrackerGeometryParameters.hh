//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// --------------------------------------------------------------
// History:
//
// 2012-03-09 B.Velghe (bob.velghe@cern.ch)
//
// 2008-04-22 S.Bifani (Simone.Bifani@cern.ch)
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2008-03-30
// --------------------------------------------------------------
//
#ifndef GigaTrackerGeometryParameters_H
#define GigaTrackerGeometryParameters_H 1

#include "globals.hh"
#include "NA62VGeometryParameters.hh"
#include "TObjArray.h"
#include "G4SystemOfUnits.hh"

#include "G4ThreeVector.hh"


class GigaTrackerGeometryParameters : public NA62VGeometryParameters
{

public:

  ~GigaTrackerGeometryParameters();
  static GigaTrackerGeometryParameters* GetInstance();
  TObjArray GetHashTable();
  void Print();

private:

  static GigaTrackerGeometryParameters* fInstance;

  // Helpers 
  G4bool PixelIsBig(G4int id) const;
  G4double PixelXOffset(G4int id) const;
protected:

  GigaTrackerGeometryParameters();

public:
  G4double GetPixelXPosition(G4int id) const;
  G4double GetPixelYPosition(G4int id) const;
  G4double GetPixelXLength(G4int id) const;
  G4double GetPixelYLength(G4int id) const;
  G4double GetBumpBondXPosition(G4int id) const;
  G4double GetBumpBondYPosition(G4int id) const;

  //////////////
  // Detector //
  //////////////

  G4String             GetGigaTrackerSensitiveDetectorName()              { return fGigaTrackerSensitiveDetectorName;};
  void                 SetGigaTrackerSensitiveDetectorName(G4String value)
                                                                          { fGigaTrackerSensitiveDetectorName = value;};
  G4String             GetGigaTrackerCollectionName()                     { return fGigaTrackerCollectionName;    };
  void                 SetGigaTrackerCollectionName(G4String value)       { fGigaTrackerCollectionName = value;   };

  G4double             GetGigaTrackerDetectorZPosition()                  { return fGigaTrackerDetectorZPosition; };
  void                 SetGigaTrackerDetectorZPosition(G4double value)    { fGigaTrackerDetectorZPosition = value;};
 
  G4double             GetGigaTrackerDetectorZLength()                    { return fGigaTrackerDetectorZLength;   };
  void                 SetGigaTrackerDetectorZLength(G4double value)      { fGigaTrackerDetectorZLength = value;  };
  G4double             GetGigaTrackerDetectorXLength()                    { return fGigaTrackerDetectorXLength;   };
  void                 SetGigaTrackerDetectorXLength(G4double value)      { fGigaTrackerDetectorXLength = value;  };
  G4double             GetGigaTrackerDetectorYLength()                    { return fGigaTrackerDetectorYLength;   };
  void                 SetGigaTrackerDetectorYLength(G4double value)      { fGigaTrackerDetectorYLength = value;  };
 
  /////////////////////
  // Sensor & pixels //
  /////////////////////
  
  // Sensor effective size
  G4double             GetGigaTrackerSensorXLength()                      { return fGigaTrackerSensorXLength;     };
  void                 SetGigaTrackerSensorXLength(G4double value)        { fGigaTrackerSensorXLength = value;    };
  G4double             GetGigaTrackerSensorYLength()                      { return fGigaTrackerSensorYLength;     };
  void                 SetGigaTrackerSensorYLength(G4double value)        { fGigaTrackerSensorYLength = value;    };
  G4double             GetGigaTrackerSensorZLength()                      { return fGigaTrackerSensorZLength;     };
  void                 SetGigaTrackerSensorZLength(G4double value)        { fGigaTrackerSensorZLength = value;    };

  // Sensor active area (pixels)
  G4double 						 GetGigaTrackerActiveSensorXLength()								{ return fGigaTrackerActiveSensorXLength; }
  G4double						 GetGigaTrackerActiveSensorYLength()                { return fGigaTrackerActiveSensorYLength; }
  G4double						 GetGigaTrackerActiveSensorZLength()								{ return fGigaTrackerActiveSensorZLength; }


  ////////////////
  // Bumb bonds //
  ////////////////

  G4double             GetGigaTrackerBumpBondingRLength()                 { return fGigaTrackerBumpBondingRLength;};
  void                 SetGigaTrackerBumpBondingRLength(G4double value)   { fGigaTrackerBumpBondingRLength = value;};
  G4double             GetGigaTrackerBumpBondingZLength()                 { return fGigaTrackerBumpBondingZLength;};
  void                 SetGigaTrackerBumpBondingZLength(G4double value)   { fGigaTrackerBumpBondingZLength = value;};

  ///////////////////
  // Readout chips //
  ///////////////////

  G4double             GetGigaTrackerChipXLength()                        { return fGigaTrackerChipXLength;       };
  void                 SetGigaTrackerChipXLength(G4double value)          { fGigaTrackerChipXLength = value;      };
  G4double             GetGigaTrackerChipYLength()                        { return fGigaTrackerChipYLength;       };
  void                 SetGigaTrackerChipYLength(G4double value)          { fGigaTrackerChipYLength = value;      };
  G4double             GetGigaTrackerChipZLength()                        { return fGigaTrackerChipZLength;       };
  void                 SetGigaTrackerChipZLength(G4double value)          { fGigaTrackerChipZLength = value;      };
  G4double             GetGigaTrackerChipXGap()                           { return fGigaTrackerChipXGap;       };
  void                 SetGigaTrackerChipXGap(G4double value)             { fGigaTrackerChipXGap = value;      };

  /////////////////////
  // Sensor assembly //
  /////////////////////

  G4double             GetGigaTrackerSensorAssemblyXLength() { return fGigaTrackerSensorAssemblyXLength; }; 
  void                 SetGigaTrackerSensorAssemblyXLength(G4double value) { fGigaTrackerSensorAssemblyXLength = value; };
  G4double             GetGigaTrackerSensorAssemblyYLength() { return fGigaTrackerSensorAssemblyYLength; };
  void                 SetGigaTrackerSensorAssemblyYLength(G4double value) { fGigaTrackerSensorAssemblyYLength = value; };
  G4double             GetGigaTrackerSensorAssemblyZLength() { return fGigaTrackerSensorAssemblyZLength; };
  void                 SetGigaTrackerSensorAssemblyZLength(G4double value) { fGigaTrackerSensorAssemblyZLength = value; };

  G4double             GetGigaTrackerSmallPixelXLength()                       { return fGigaTrackerSmallPixelXLength;      };
  void                 SetGigaTrackerSmallPixelXLength(G4double value)         { fGigaTrackerSmallPixelXLength = value;     };
  G4double             GetGigaTrackerSmallPixelYLength()                       { return fGigaTrackerSmallPixelYLength;      };
  void                 SetGigaTrackerSmallPixelYLength(G4double value)         { fGigaTrackerSmallPixelYLength = value;     };
  G4double             GetGigaTrackerBigPixelXLength()                       { return fGigaTrackerBigPixelXLength;      };
  void                 SetGigaTrackerBigPixelXLength(G4double value)         { fGigaTrackerBigPixelXLength = value;     };
  G4double             GetGigaTrackerBigPixelYLength()                       { return fGigaTrackerBigPixelYLength;      };
  void                 SetGigaTrackerBigPixelYLength(G4double value)         { fGigaTrackerBigPixelYLength = value;     };
  G4double             GetGigaTrackerPixelZLength()                       { return fGigaTrackerPixelZLength;      };
  void                 SetGigaTrackerPixelZLength(G4double value)         { fGigaTrackerPixelZLength = value;     };

  G4int                GetGigaTrackerNumberOfPixels()                     { return fGigaTrackerNumberOfPixels; };
  void                 SetGigaTrackerNumberOfPixels(G4int value)          { fGigaTrackerNumberOfPixels = value; };

  /////////
  // PCB //
  /////////
  G4double						GetGigaTrackerPCBXLength() 													{ return fGigaTrackerPCBXLength; }
  G4double						GetGigaTrackerPCBYLength() 													{ return fGigaTrackerPCBYLength; }
  G4double						GetGigaTrackerPCBZLength() 													{ return fGigaTrackerPCBZLength; }
  G4double						GetGigaTrackerPCBHoleXLength()											{ return fGigaTrackerPCBHoleXLength; }
  G4double						GetGigaTrackerPCBHoleYLength()											{ return fGigaTrackerPCBHoleYLength; }
  G4double 						GetGigaTrackerPCBHoleXOffset()											{ return fGigaTrackerPCBHoleXOffset; }
  
  /////////////
  // Station //
  /////////////

  G4double             GetGigaTrackerStationXLength()                     { return fGigaTrackerStationXLength;    };
  void                 SetGigaTrackerStationXLength(G4double value)       { fGigaTrackerStationXLength = value;   };
  G4double             GetGigaTrackerStationYLength()                     { return fGigaTrackerStationYLength;    };
  void                 SetGigaTrackerStationYLength(G4double value)       { fGigaTrackerStationYLength = value;   };
  G4double             GetGigaTrackerStationZLength()                     { return fGigaTrackerStationZLength;    };
  void                 SetGigaTrackerStationZLength(G4double value)       { fGigaTrackerStationZLength = value;   };
 
  /////////////
  // Magnets //
  /////////////

  G4double             GetGigaTrackerMCBMagnetXLength()                     { return fGigaTrackerMCBMagnetXLength;    };
  void                 SetGigaTrackerMCBMagnetXLength(G4double value)       { fGigaTrackerMCBMagnetXLength = value;   };
  G4double             GetGigaTrackerMCBMagnetYLength()                     { return fGigaTrackerMCBMagnetYLength;    };
  void                 SetGigaTrackerMCBMagnetYLength(G4double value)       { fGigaTrackerMCBMagnetYLength = value;   };
  G4double             GetGigaTrackerMCBMagnetZLength()                     { return fGigaTrackerMCBMagnetZLength;    };
  void                 SetGigaTrackerMCBMagnetZLength(G4double value)       { fGigaTrackerMCBMagnetZLength = value;   };
  
  G4double             GetGigaTrackerMDXMagnetXLength()                     { return fGigaTrackerMDXMagnetXLength;    };
  void                 SetGigaTrackerMDXMagnetXLength(G4double value)       { fGigaTrackerMDXMagnetXLength = value;   };
  G4double             GetGigaTrackerMDXMagnetYLength()                     { return fGigaTrackerMDXMagnetYLength;    };
  void                 SetGigaTrackerMDXMagnetYLength(G4double value)       { fGigaTrackerMDXMagnetYLength = value;   };
  G4double             GetGigaTrackerMDXMagnetZLength()                     { return fGigaTrackerMDXMagnetZLength;    };
  void                 SetGigaTrackerMDXMagnetZLength(G4double value)       { fGigaTrackerMDXMagnetZLength = value;   };

 
  /////////////
  // Magnets //
  /////////////

  G4double             GetGigaTrackerMagnetXLength1()                     { return fGigaTrackerMagnetXLength1;    };
  void                 SetGigaTrackerMagnetXLength1(G4double value)       { fGigaTrackerMagnetXLength1 = value;   };
  G4double             GetGigaTrackerMagnetYLength1()                     { return fGigaTrackerMagnetYLength1;    };
  void                 SetGigaTrackerMagnetYLength1(G4double value)       { fGigaTrackerMagnetYLength1 = value;   };
  G4double             GetGigaTrackerMagnetZLength1()                     { return fGigaTrackerMagnetZLength1;    };
  void                 SetGigaTrackerMagnetZLength1(G4double value)       { fGigaTrackerMagnetZLength1 = value;   };
  G4double             GetGigaTrackerMagnetXLength2()                     { return fGigaTrackerMagnetXLength2;    };
  void                 SetGigaTrackerMagnetXLength2(G4double value)       { fGigaTrackerMagnetXLength2 = value;   };
  G4double             GetGigaTrackerMagnetYLength2()                     { return fGigaTrackerMagnetYLength2;    };
  void                 SetGigaTrackerMagnetYLength2(G4double value)       { fGigaTrackerMagnetYLength2 = value;   };
  G4double             GetGigaTrackerMagnetZLength2()                     { return fGigaTrackerMagnetZLength2;    };
  void                 SetGigaTrackerMagnetZLength2(G4double value)       { fGigaTrackerMagnetZLength2 = value;   };

  ////////////////
  // Collimator //
  ////////////////

  G4double             GetGigaTrackerCollimatorOuterXLength()             { return fGigaTrackerCollimatorOuterXLength;};
  void                 SetGigaTrackerCollimatorOuterXLength(G4double value)
                                                                          { fGigaTrackerCollimatorOuterXLength = value;};
  G4double             GetGigaTrackerCollimatorOuterYLength()             { return fGigaTrackerCollimatorOuterYLength; };
  void                 SetGigaTrackerCollimatorOuterYLength(G4double value)
                                                                          { fGigaTrackerCollimatorOuterYLength = value;};
  G4double             GetGigaTrackerCollimatorInnerXLength()             { return fGigaTrackerCollimatorInnerXLength;};
  void                 SetGigaTrackerCollimatorInnerXLength(G4double value)
                                                                          { fGigaTrackerCollimatorInnerXLength = value;};
  G4double             GetGigaTrackerCollimatorInnerYLength()             { return fGigaTrackerCollimatorInnerYLength;};
  void                 SetGigaTrackerCollimatorInnerYLength(G4double value)
                                                                          { fGigaTrackerCollimatorInnerYLength = value;};
  G4double             GetGigaTrackerCollimatorZLength()                  { return fGigaTrackerCollimatorZLength; };
  void                 SetGigaTrackerCollimatorZLength(G4double value)    { fGigaTrackerCollimatorZLength = value;};

  G4ThreeVector        GetGigaTrackerSensorPosition()                     { return fGigaTrackerSensorPosition;    };
  void                 SetGigaTrackerSensorPosition(G4ThreeVector value)  { fGigaTrackerSensorPosition = value;   };
 
  G4ThreeVector        GetGigaTrackerBumpBondingPosition()                { return fGigaTrackerBumpBondingPosition;};
  void                 SetGigaTrackerBumpBondingPosition(G4ThreeVector value)
                                                                          { fGigaTrackerBumpBondingPosition = value;};
  G4ThreeVector        GetGigaTrackerChipPosition()                       { return fGigaTrackerChipPosition;      };
  void                 SetGigaTrackerChipPosition(G4ThreeVector value)    { fGigaTrackerChipPosition = value;     };

  G4ThreeVector        GetGigaTrackerStation1Position()                   { return fGigaTrackerStation1Position;  };
  void                 SetGigaTrackerStation1Position(G4ThreeVector value){ fGigaTrackerStation1Position = value; };
  G4ThreeVector        GetGigaTrackerStation2Position()                   { return fGigaTrackerStation2Position;  };
  void                 SetGigaTrackerStation2Position(G4ThreeVector value){ fGigaTrackerStation2Position = value; };
  G4ThreeVector        GetGigaTrackerStation3Position()                   { return fGigaTrackerStation3Position;  };
  void                 SetGigaTrackerStation3Position(G4ThreeVector value){ fGigaTrackerStation3Position = value; };
 
  G4ThreeVector        GetGigaTrackerMagnet1Position()                    { return fGigaTrackerMagnet1Position;   };
  void                 SetGigaTrackerMagnet1Position(G4ThreeVector value) { fGigaTrackerMagnet1Position = value;  };
  G4ThreeVector        GetGigaTrackerMagnet2Position()                    { return fGigaTrackerMagnet2Position;   };
  void                 SetGigaTrackerMagnet2Position(G4ThreeVector value) { fGigaTrackerMagnet2Position = value;  };
  G4ThreeVector        GetGigaTrackerMagnet3Position()                    { return fGigaTrackerMagnet3Position;   };
  void                 SetGigaTrackerMagnet3Position(G4ThreeVector value) { fGigaTrackerMagnet3Position = value;  };
  G4ThreeVector        GetGigaTrackerMagnet4Position()                    { return fGigaTrackerMagnet4Position;   };
  void                 SetGigaTrackerMagnet4Position(G4ThreeVector value) { fGigaTrackerMagnet4Position = value;  };
  G4ThreeVector        GetGigaTrackerMagnet5Position()                    { return fGigaTrackerMagnet5Position;   };
  void                 SetGigaTrackerMagnet5Position(G4ThreeVector value) { fGigaTrackerMagnet5Position = value;  };

  G4ThreeVector        GetGigaTrackerCollimatorPosition()                 { return fGigaTrackerCollimatorPosition;};
  void                 SetGigaTrackerCollimatorPosition(G4ThreeVector value)
                                                                          { fGigaTrackerCollimatorPosition = value;};

  G4double             GetGigaTrackerArchomatMagnetFieldStrength(G4int i)         { return fGigaTrackerArchomatMagnetFieldStrength[i];}
  void                 SetGigaTrackerArchomatMagnetFieldStrength(G4double value, G4int i) { fGigaTrackerArchomatMagnetFieldStrength[i]=value;}

  G4double             GetGigaTrackerTRIM5MagnetFieldStrength()           { return fGigaTrackerTRIM5MagnetFieldStrength; }
  void                 SetGigaTrackerTRIM5MagnetFieldStrength(G4double value) { fGigaTrackerTRIM5MagnetFieldStrength = value; }


  G4double             GetGigaTrackerMagnetFieldStrength(G4int i)         { return fGigaTrackerMagnetFieldStrength[i];}
  void                 SetGigaTrackerMagnetFieldStrength(G4double value, G4int i) { fGigaTrackerMagnetFieldStrength[i]=value;}
  ///////////////////
  // Cooling Plate //
  ///////////////////
	
  G4double GetCoolingPlateXLength() { return fCoolingPlateXLength; }
  G4double GetCoolingPlateYLength() { return fCoolingPlateYLength; }
  G4double GetCoolingPlateZLength() { return fCoolingPlateZLength; }
  
  G4double GetCoolingPlateTopShoulderXLength() { return fCoolingPlateTopShoulderXLength; }
  G4double GetCoolingPlateTopHollowXLength() { return fCoolingPlateTopHollowXLength; }
  G4double GetCoolingPlateTopShoulderYLength() { return fCoolingPlateTopShoulderYLength; }
  G4double GetCoolingPlateTopHollowYLength() { return fCoolingPlateTopHollowYLength; }
  G4double GetCoolingPlateTopDepth() { return fCoolingPlateTopDepth; }
  
  G4double GetCoolingPlateBottomShoulderXLength() { return fCoolingPlateBottomShoulderXLength; }
  G4double GetCoolingPlateBottomHollowXLength() { return fCoolingPlateBottomHollowXLength; }
  G4double GetCoolingPlateBottomShoulderYLength() { return fCoolingPlateBottomShoulderYLength; }
  G4double GetCoolingPlateBottomHollowYLength() { return fCoolingPlateBottomHollowYLength; }
  G4double GetCoolingPlateBottomDepth() { return fCoolingPlateBottomDepth; }
  
  G4double GetCoolingPlateChannelsEnvelopeXLength() { return fCoolingPlateChannelsEnvelopeXLength;  }
  G4double GetCoolingPlateChannelsEnvelopeYLength() { return fCoolingPlateChannelsEnvelopeYLength;  }
  G4double GetCoolingPlateChannelsEnvelopeZLength() { return fCoolingPlateChannelsEnvelopeZLength;  }
  
  G4double GetCoolingPlateChannelsXOffset() { return fCoolingPlateChannelsXOffset; }
  G4double GetCoolingPlateChannelsDepth() { return fCoolingPlateChannelsDepth;}
  
  G4double GetCoolingPlateChannelHalfWallXLength() { return fCoolingPlateChannelHalfWallXLength; }
  G4double GetCoolingPlateChannelCoolantXLength() { return  fCoolingPlateChannelCoolantXLength; }
  
  ////////////////
  // Glue layer //
  ////////////////
  
  G4double GetGigaTrackerGlueLayerZLength() { return fGigaTrackerGlueLayerZLength; }

private:

  G4String fGigaTrackerSensitiveDetectorName;
  G4String fGigaTrackerCollectionName;

  G4double fWorldZLength;
  G4double fWorldXLength;
  G4double fWorldYLength;

  G4int fGigaTrackerSensorNColumns;
  G4int fGigaTrackerSensorNRows;
  G4int fGigaTrackerChipNColumns;
  G4int fGigaTrackerChipNRows;

  G4double fGigaTrackerDetectorZPosition;

  G4double fGigaTrackerDetectorZLength;
  G4double fGigaTrackerDetectorXLength;
  G4double fGigaTrackerDetectorYLength;

  G4double fGigaTrackerSensorAssemblyXLength; 
  G4double fGigaTrackerSensorAssemblyYLength;
  G4double fGigaTrackerSensorAssemblyZLength;

  G4double fGigaTrackerSensorXLength;
  G4double fGigaTrackerSensorYLength;
  G4double fGigaTrackerSensorZLength;
  
  G4double fGigaTrackerActiveSensorXLength;
  G4double fGigaTrackerActiveSensorYLength;
  G4double fGigaTrackerActiveSensorZLength;

  G4ThreeVector fGigaTrackerSensorPosition;

  G4double fGigaTrackerBumpBondingRLength;
  G4double fGigaTrackerBumpBondingZLength;
  G4double fGigaTrackerBumpBondingXOffset;
  G4double fGigaTrackerBumpBondingYOffset;

  G4double fGigaTrackerBumpBondOffset;
  G4double fGigaTrackerBumpBondBigOffset;

  G4ThreeVector fGigaTrackerBumpBondingPosition;

  G4double fGigaTrackerChipXLength;
  G4double fGigaTrackerChipYLength;
  G4double fGigaTrackerChipZLength;
  G4double fGigaTrackerChipXGap;
  G4double fGigaTrackerSupportXLength;
  G4double fGigaTrackerSupportYLength;
  G4double fGigaTrackerSupportZLength;

  G4double fGigaTrackerSmallPixelXLength;
  G4double fGigaTrackerSmallPixelYLength;
  G4double fGigaTrackerBigPixelXLength;
  G4double fGigaTrackerBigPixelYLength;
  G4double fGigaTrackerPixelZLength;
  G4double fGigaTrackerNumberOfPixels;

  G4double fGigaTrackerStationXLength;
  G4double fGigaTrackerStationYLength;
  G4double fGigaTrackerStationZLength;

  G4double fGigaTrackerMCBMagnetXLength;
  G4double fGigaTrackerMCBMagnetYLength;
  G4double fGigaTrackerMCBMagnetZLength;
  
  G4double fGigaTrackerMDXMagnetXLength;
  G4double fGigaTrackerMDXMagnetYLength;
  G4double fGigaTrackerMDXMagnetZLength;

  G4double fGigaTrackerCollimatorOuterXLength;
  G4double fGigaTrackerCollimatorOuterYLength;
  G4double fGigaTrackerCollimatorInnerXLength;
  G4double fGigaTrackerCollimatorInnerYLength;
  G4double fGigaTrackerCollimatorZLength;

  G4ThreeVector fGigaTrackerChipPosition;


  G4ThreeVector fGigaTrackerSupportPosition;


  G4ThreeVector fGigaTrackerStation1Position;
  G4ThreeVector fGigaTrackerStation2Position;
  G4ThreeVector fGigaTrackerStation3Position;

  G4ThreeVector fGigaTrackerMagnet1Position;
  G4ThreeVector fGigaTrackerMagnet2Position;
  G4ThreeVector fGigaTrackerMagnet3Position;
  G4ThreeVector fGigaTrackerMagnet4Position;
  G4ThreeVector fGigaTrackerMagnet5Position;

  G4ThreeVector fGigaTrackerCollimatorPosition;

  G4double fGigaTrackerArchomatMagnetFieldStrength[4];
  G4double fGigaTrackerTRIM5MagnetFieldStrength;

  G4double fGigaTrackerMagnetXLength1;
  G4double fGigaTrackerMagnetYLength1;
  G4double fGigaTrackerMagnetZLength1;
  G4double fGigaTrackerMagnetXLength2;
  G4double fGigaTrackerMagnetYLength2;
  G4double fGigaTrackerMagnetZLength2;
  G4double fGigaTrackerMagnetFieldStrength[5];


  //Cooling Plate
  G4double fCoolingPlateXLength;
  G4double fCoolingPlateYLength;
  G4double fCoolingPlateZLength;

  G4double fCoolingPlateTopShoulderXLength;
  G4double fCoolingPlateTopHollowXLength;
  G4double fCoolingPlateTopShoulderYLength;
  G4double fCoolingPlateTopHollowYLength;
  G4double fCoolingPlateTopDepth;

  G4double fCoolingPlateBottomShoulderXLength;
  G4double fCoolingPlateBottomHollowXLength;
  G4double fCoolingPlateBottomShoulderYLength;
  G4double fCoolingPlateBottomHollowYLength;
  G4double fCoolingPlateBottomDepth;

  G4double fCoolingPlateChannelsEnvelopeXLength;
  G4double fCoolingPlateChannelsEnvelopeYLength;
  G4double fCoolingPlateChannelsEnvelopeZLength;

  G4double fCoolingPlateChannelsXOffset;
  G4double fCoolingPlateChannelsDepth;

  G4double fCoolingPlateChannelCoolantXLength;
  G4double fCoolingPlateChannelHalfWallXLength;

  // Glue
  G4double fGigaTrackerGlueLayerZLength;

  // PCB
  G4double fGigaTrackerPCBXLength; 
  G4double fGigaTrackerPCBYLength;
  G4double fGigaTrackerPCBZLength;
  G4double fGigaTrackerPCBHoleXLength;
  G4double fGigaTrackerPCBHoleYLength;
  G4double fGigaTrackerPCBHoleXOffset;

};

#endif
