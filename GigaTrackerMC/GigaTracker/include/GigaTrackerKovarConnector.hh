// --------------------------------------------------------------
// History:
//
// Created by Bob Velghe (bob.velghe@cern.ch) 2014-01-09
// --------------------------------------------------------------

#ifndef GigaTrackerKovarConnector_h
#define GigaTrackerKovarConnector_h 1

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4GenericTrap.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

#include "G4TwoVector.hh"

#include "NA62VComponent.hh"
#include "GigaTrackerGeometryParameters.hh"

class GigaTrackerKovarConnector : public NA62VComponent {

  public:
    GigaTrackerKovarConnector(G4Material *, G4LogicalVolume *, G4ThreeVector, G4int,G4int);
    ~GigaTrackerKovarConnector();
    void ReadGeometryParameters();
    void CreateGeometry();
    void SetProperties();
  private:
    G4int fiCopy;
    G4ThreeVector fPosition;
    G4int fType;
    
    
    G4Material * fMatKovar;
    
    G4double fInnerDiameter;
    G4double fBaseOuterDiameter;
    G4double fBaseHeight;
  
    G4Tubs * fBaseSolidVolume;
      
    G4LogicalVolume * fBaseLogicalVolume;
    G4VPhysicalVolume * fBasePhysicalVolume;
  
    G4double fBodyHeight;
    G4double fBodyLength;
    G4double fBodyTubeHeight; 
    G4double fBodyConnDiam; 
    
    G4Box * fBodyBackSolidVolume;
    G4Box * fBodyFrontSolidVolume;
    G4Box * fBodySideSolidVolume;
  
    G4LogicalVolume * fBodyBackLogicalVolume;
    G4LogicalVolume * fBodyFrontLogicalVolume;
    G4LogicalVolume * fBodySideLogicalVolume;
  
    G4VPhysicalVolume * fBodyBackPhysicalVolume;				     
    G4VPhysicalVolume * fBodyFrontPhysicalVolume; 
    G4VPhysicalVolume * fBodySideAPhysicalVolume;
    G4VPhysicalVolume * fBodySideBPhysicalVolume;
    
    G4Box * fBodyNeckSolidVolume;
    G4LogicalVolume * fBodyNeckLogicalVolume;
    G4VPhysicalVolume * fBodyNeckPhysicalVolume;
        
    G4Box * fBodyBackNeckSolidVolume;
    G4Box * fBodySideNeckSolidVolume;
    G4Box * fBodyCollarNeckSolidVolume;
  
    G4LogicalVolume * fBodyBackNeckLogicalVolume;
    G4LogicalVolume * fBodySideNeckLogicalVolume;
    G4LogicalVolume * fBodyCollarNeckLogicalVolume;
  
    G4PVPlacement * fBodyBackNeckPhysicalVolume;
    G4PVPlacement * fBodySideNeckAPhysicalVolume;
  	G4PVPlacement * fBodySideNeckBPhysicalVolume;
    G4PVPlacement * fBodyCollarNeckAPhysicalVolume;
	  G4PVPlacement * fBodyCollarNeckBPhysicalVolume;
  
    G4Box * fBodyCoverSolidVolume;
    G4LogicalVolume * fBodyCoverLogicalVolume;
	  G4PVPlacement * fBodyCoverPhysicalVolume;
    
    G4GenericTrap * fFrontBottomSolidVolume;
    G4LogicalVolume * fFrontBottomLogicalVolume;
	  G4PVPlacement * fFrontBottomPhysicalVolume;
	  
	  G4GenericTrap * fFrontTopSolidVolume;
    G4LogicalVolume * fFrontTopLogicalVolume;
	  G4PVPlacement * fFrontTopPhysicalVolume;
};

#endif // GigaTrackerKovarConnector_h 
