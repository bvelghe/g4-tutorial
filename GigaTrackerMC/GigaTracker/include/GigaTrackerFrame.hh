// --------------------------------------------------------------
// History:
//
// Created by Bob Velghe (bob.velghe@cern.ch) 2013-10-22
// --------------------------------------------------------------

#ifndef GigaTrackerFrame_h
#define GigaTrackerFrame_h 1

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

#include "NA62VComponent.hh"
#include "GigaTrackerGeometryParameters.hh"

class GigaTrackerFrame : public NA62VComponent {

  public:
    GigaTrackerFrame(G4Material *, G4LogicalVolume *, G4ThreeVector, G4int);
    ~GigaTrackerFrame();
    void ReadGeometryParameters();
    void CreateGeometry();
    void SetProperties();
  private:
    G4int fiCopy;
    G4ThreeVector fPosition;
    
    G4double fFrameXLength;
    G4double fFrameYLength;
    G4double fFrameZLength;
    
    G4double fFrameLipTopBottomLength;
    G4double fFrameLipRightLength;
    G4double fFrameLipLeftXLength;
    G4double fFrameLipLeftYLength; //Same YLength for top and bottom
  
    G4double fFrameRightXLength;
    G4double fFrameLeftXLength;
    G4double fFrameTopBottomLeftXLength;
    
    G4double fPCBHoleXLength;
    G4double fPCBHoleYLength;
    G4double fPCBZLength;
  
    // FIXME Move this to geom file
	  G4double fPCBFootSizeVert;
	  G4double fPCBFootSizeHoriz;
	  G4double fPCBFootHeight;
    // ----
    
    //FIXME
    G4double fCoolingPlateYLength;
    G4double fCoolingPlateXLength;
    
    G4Box * fLeftLipSolidVolume;
    G4Box * fRightLipSolidVolume;
    G4Box * fTopBottomLipSolidVolume;
    
    G4Box * fLeftFrameSolidVolume;
    G4Box * fRightFrameSolidVolume;
    G4Box * fTopBottomLeftFrameSolidVolume;
    
    G4LogicalVolume * fLeftLipLogicalVolume;
    G4LogicalVolume * fRightLipLogicalVolume;
    G4LogicalVolume * fTopBottomLipLogicalVolume;
    
    G4LogicalVolume * fLeftFrameLogicalVolume;
    G4LogicalVolume * fRightFrameLogicalVolume;
    G4LogicalVolume * fTopBottomLeftFrameLogicalVolume;
    
    G4VPhysicalVolume * fLeftFramePhysicalVolume;
    G4VPhysicalVolume * fRightFramePhysicalVolume;
    G4VPhysicalVolume * fTopLeftFramePhysicalVolume;
    G4VPhysicalVolume * fBottomLeftFramePhysicalVolume;
    
    G4VPhysicalVolume * fTopLeftLipPhysicalVolume;
    G4VPhysicalVolume * fBottomLeftLipPhysicalVolume;
    G4VPhysicalVolume * fRightLipPhysicalVolume;
    G4VPhysicalVolume * fTopLipPhysicalVolume;
    G4VPhysicalVolume * fBottomLipPhysicalVolume;
  
};

#endif // GigaTrackerFrame_h 
