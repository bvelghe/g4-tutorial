// --------------------------------------------------------------
// History:
//
// Created by Bob Velghe (bob.velghe@cern.ch) 2014-01-09
// --------------------------------------------------------------

#ifndef GigaTrackerCarbonFrame_h
#define GigaTrackerCarbonFrame_h 1

#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

#include "NA62VComponent.hh"
#include "GigaTrackerGeometryParameters.hh"

class GigaTrackerCarbonFrame : public NA62VComponent {

  public:
    GigaTrackerCarbonFrame(G4Material *, G4LogicalVolume *, G4ThreeVector, G4int);
    ~GigaTrackerCarbonFrame();
    void ReadGeometryParameters();
    void CreateGeometry();
    void SetProperties();
  private:
    G4int fiCopy;
    G4ThreeVector fPosition;
   
    
    G4Box * fFrameXSolidVolume;
    G4LogicalVolume * fFrameXLogicalVolume;
       
    G4Box * fFrameYSolidVolume;
    G4LogicalVolume * fFrameYLogicalVolume;
    
    G4VPhysicalVolume * fFrameLeftPhysicalVolume;
    G4VPhysicalVolume * fFrameRightPhysicalVolume;
    G4VPhysicalVolume * fFrameTopPhysicalVolume;
    G4VPhysicalVolume * fFrameBottomPhysicalVolume;
    
    G4Box * fFrameAttachCoreVolume; 
    
    G4LogicalVolume * fFrameAttachCoreLogicalVolume;

    G4VPhysicalVolume * fFrameAttachCoreTLeftPhysicalVolume;
    G4VPhysicalVolume * fFrameAttachCoreTRightPhysicalVolume;
    G4VPhysicalVolume * fFrameAttachCoreBLeftPhysicalVolume;
    G4VPhysicalVolume * fFrameAttachCoreBRightPhysicalVolume;
    
    G4Tubs * fFrameAttachHeadSolidVolume;
    G4LogicalVolume * fFrameAttachHeadLogicalVolume;
    
    G4VPhysicalVolume * fFrameAttachHeadTLeftPhysicalVolume;
    G4VPhysicalVolume * fFrameAttachHeadTRightPhysicalVolume;
    G4VPhysicalVolume * fFrameAttachHeadBLeftPhysicalVolume;
    G4VPhysicalVolume * fFrameAttachHeadBRightPhysicalVolume;
};

#endif // GigaTrackerCarbonFrame_h 
