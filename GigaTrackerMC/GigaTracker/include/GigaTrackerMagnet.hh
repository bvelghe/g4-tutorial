
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// --------------------------------------------------------------
// History:
//
// Created by Simone Bifani (Simone.Bifani@cern.ch) 2008-04-22
// --------------------------------------------------------------
//
#ifndef GigaTrackerMagnet_H
#define GigaTrackerMagnet_H 1

#include "globals.hh"
#include "NA62VComponent.hh"
#include "GigaTrackerGeometryParameters.hh"

#include "G4FieldManager.hh"
#include "G4UniformMagField.hh"

class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;

class GigaTrackerMagnet : public NA62VComponent
{

public:
  
  ~GigaTrackerMagnet();
  GigaTrackerMagnet(G4Material*, G4LogicalVolume*, G4ThreeVector, G4int);
  void ReadGeometryParameters();
  void CreateGeometry();
  void SetProperties();

public:

  G4ThreeVector        GetPosition()                                      { return fPosition;                     };
  void                 SetPosition(G4ThreeVector value)                   { fPosition = value;                    };

  G4int                GetiCopy()                                         { return fiCopy;                        };
  void                 SetiCopy(G4int value)                              { fiCopy = value;                       };

  G4double             GetXLength()                                       { return fXLength;                      };
  void                 SetXLength(G4double value)                         { fXLength = value;                     };
  G4double             GetYLength()                                       { return fYLength;                      };
  void                 SetYLength(G4double value)                         { fYLength = value;                     };
  G4double             GetZLength()                                       { return fZLength;                      };
  void                 SetZLength(G4double value)                         { fZLength = value;                     };
 
  G4ThreeVector        GetFieldStrength()                                 { return fFieldStrength;                };
  void                 SetFieldStrength(G4ThreeVector value)              { fFieldStrength = value;               };
  G4FieldManager *     GetFieldMgr()                                      { return fFieldMgr;                     };
  void                 SetFieldMgr(G4FieldManager * value)                { fFieldMgr = value;                    };
  G4UniformMagField *  GetMagField()                                      { return fMagField;                     };
  void                 SetMagField(G4UniformMagField * value)             { fMagField = value;                    };

private:

  G4ThreeVector fPosition;

  G4int fiCopy;

  G4double fXLength;
  G4double fYLength;
  G4double fZLength; 

  G4ThreeVector fFieldStrength;
  G4FieldManager* fFieldMgr;
  G4UniformMagField * fMagField;

};

#endif
