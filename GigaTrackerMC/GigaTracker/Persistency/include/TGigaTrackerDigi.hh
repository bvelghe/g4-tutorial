// --------------------------------------------------------------
// History:
//
// Created by Massimiliano Fiorini (Massimiliano.Fiorini@cern.ch) 2011-04-11
//
// --------------------------------------------------------------
#ifndef TGigaTrackerDigi_H
#define TGigaTrackerDigi_H

#include "TDCVHit.hh"
#include "GigaTrackerChannelID.hh"
#include "TVector3.h"

class TGigaTrackerDigi : public TDCVHit, public GigaTrackerChannelID {

public:
  
  TGigaTrackerDigi();
  
public:
  
#ifdef GigaTrackerTB
  Int_t                GetHitAddress()                                    { return fHitAddress;                 };
  void                 SetHitAddress(Int_t value)                         { fHitAddress = value;                };
  
  Int_t                GetPileUpAddress()                                 { return fPileUpAddress;              };
  void                 SetPileUpAddress(Int_t value)                      { fPileUpAddress = value;             };

  Int_t                GetPixelFlag()                                     { return fPixelFlag;                  };
  void                 SetPixelFlag(Int_t value)                          { fPixelFlag = value;                 };

  Double_t             GetCAENETTT()                                      { return fCAENETTT;                   };
  void                 SetCAENETTT(Double_t value)                        { fCAENETTT = value;                  };
  Double_t             GetCAENLeading()                                   { return fCAENLeading;                };
  void                 SetCAENLeading(Double_t value)                     { fCAENLeading = value;               };
  Double_t             GetCAENTrailing()                                  { return fCAENTrailing;               };
  void                 SetCAENTrailing(Double_t value)                    { fCAENTrailing = value;              };
  Double_t             GetCAENTimeReference()                             { return fCAENTimeReference;          };
  void                 SetCAENTimeReference(Double_t value)               { fCAENTimeReference = value;         };
#endif

private:
  
#ifdef GigaTrackerTB
  Int_t fHitAddress;  
  Int_t fPileUpAddress;  
  Int_t fPixelFlag;  

  Double_t fCAENETTT;
  Double_t fCAENLeading;
  Double_t fCAENTrailing;
  Double_t fCAENTimeReference;
#endif  
  
  ClassDef(TGigaTrackerDigi,1);

};
#endif
