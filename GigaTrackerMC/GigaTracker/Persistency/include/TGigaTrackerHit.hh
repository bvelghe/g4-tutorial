// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2008-03-30
//
// --------------------------------------------------------------
#ifndef TGigaTrackerHit_H
#define TGigaTrackerHit_H

#include "TDetectorVHit.hh"
#include "GigaTrackerChannelID.hh"

class TGigaTrackerHit : public TDetectorVHit, public GigaTrackerChannelID  {

    public:

        TGigaTrackerHit();
        ~TGigaTrackerHit(){};

    public:

    protected:

        ClassDef(TGigaTrackerHit,1);
};
#endif
