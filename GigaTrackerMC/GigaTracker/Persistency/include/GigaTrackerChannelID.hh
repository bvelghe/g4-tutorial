// --------------------------------------------------------------
// History:
//
// Created by Massimiliano Fiorini (Massimiliano.Fiorini@cern.ch) 2009-10-26
//
// --------------------------------------------------------------
#ifndef GigaTrackerChannelID_H
#define GigaTrackerChannelID_H
#include "Rtypes.h"

class GigaTrackerChannelID {

    public:

        GigaTrackerChannelID();
        GigaTrackerChannelID(Int_t, Int_t);

    public:

        Int_t                GetStationNo()                                     { return fStationNo;                    };
        void                 SetStationNo(Int_t value)                          { fStationNo = value;                   };
        Int_t                GetPixelID()                                       { return fPixelID;                      };
        void                 SetPixelID(Int_t value)                            { fPixelID = value;                     };
        
    private:

        Int_t fStationNo;
        Int_t fPixelID;

        ClassDef(GigaTrackerChannelID,1);
};
#endif
