#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class GigaTrackerChannelID+;
#pragma link C++ class TGigaTrackerHit+;
#pragma link C++ class TGigaTrackerDigi+;
#pragma link C++ class TGigaTrackerEvent+;

#endif

