// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2008-04-24
//
// --------------------------------------------------------------
#include "TGigaTrackerHit.hh"

ClassImp(TGigaTrackerHit)

TGigaTrackerHit::TGigaTrackerHit() : TDetectorVHit(), GigaTrackerChannelID() {
}

