// --------------------------------------------------------------
// History:
//
// Created by Massimiliano Fiorini (Massimiliano.Fiorini@cern.ch) 2011-04-11
//
// --------------------------------------------------------------
#include "TGigaTrackerDigi.hh"

ClassImp(TGigaTrackerDigi)
  
TGigaTrackerDigi::TGigaTrackerDigi() : TDCVHit(), GigaTrackerChannelID() {
    
}
