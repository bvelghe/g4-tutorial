// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2008-04-23
//
// --------------------------------------------------------------
#include "TGigaTrackerEvent.hh"
#include "TGigaTrackerHit.hh"

ClassImp(TGigaTrackerEvent)

TGigaTrackerEvent::TGigaTrackerEvent() : TDetectorVEvent(TGigaTrackerHit::Class()){
}

TGigaTrackerEvent::~TGigaTrackerEvent() {
}
