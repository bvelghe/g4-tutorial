// --------------------------------------------------------------
// History:
//
// Created by Massimiliano Fiorini (Massimiliano.Fiorini@cern.ch) 2009-10-26
//
// --------------------------------------------------------------
#include "GigaTrackerChannelID.hh"

ClassImp(GigaTrackerChannelID)

GigaTrackerChannelID::GigaTrackerChannelID() {
    fStationNo = -1;
    fPixelID = -1;
}

GigaTrackerChannelID::GigaTrackerChannelID(Int_t StationNo, Int_t PixelID) {
    fStationNo = StationNo;
    fPixelID = PixelID;
}

