#ifndef __TTRACK_H__
#define __TTRACK_H__

//
// ROOT includes
// 
#include "TObject.h"
#include "TVector3.h"

class TTrack : public TObject {
  public:
    TTrack();
    virtual ~TTrack();
    // Accessors
    void TrackID(Int_t trkID) { fTrackID = trkID; }
    Int_t TrackID() const { return fTrackID; }
    
    void ParentID(Int_t parID) { fParentID = parID; }
    Int_t ParentID() const { return fParentID; }
    
    void ParticleID(Int_t partID) { fParticleID = partID; }
    Int_t ParticleID() const { return fParticleID; }
    
		void Momentum(TVector3 imom) {fMomentum = imom; }
    TVector3 Momentum() const { return fMomentum; }
		
		void Vertex(TVector3 vtx) {fVertex = vtx; }
    TVector3 Vertex() const { return fVertex; }
    
    void ExitAngle(Double_t angle) {fExitAngle = angle; }
    Double_t ExitAngle() const { return fExitAngle; }
    
		void KineticEnergy(Double_t ke) {fKineticEnergy = ke; }
    Double_t KineticEnergy() const { return fKineticEnergy; }
    
		void TotalEnergy(Double_t te) {fTotalEnergy = te; }
    Double_t TotalEnergy() const { return fTotalEnergy; }
    
    void Charge(Int_t charge) { fCharge = charge; }
    Int_t Charge() const { return fCharge; }
    
    
  private:
    Int_t fTrackID;
    Int_t fParentID;
    Int_t fParticleID;
    TVector3 fMomentum;
    TVector3 fVertex;
		Double_t fKineticEnergy;
    Double_t fTotalEnergy;
    Double_t fExitAngle;
    Int_t fCharge;

    ClassDef(TTrack,1);
};

#endif //__TTRACK_H__

