#include "TTrack.h"

TTrack::TTrack() {
			fTrackID = -1;
    	fParentID = -1;
    	fParticleID = 0;
    	fMomentum = TVector3();
    	fVertex = TVector3();
    	fKineticEnergy = -1.0;
			fTotalEnergy = -1.0;
			fExitAngle = -1.0;
    	fCharge = 0;  
}

TTrack::~TTrack() {}
