#ifndef __TEVENT_H__
#define __TEVENT_H__

#include "TTrack.h"
//
// ROOT includes
//
#include "TObject.h"
#include "TClonesArray.h"

class TEvent : public TObject {
  public:
    TEvent();
    virtual ~TEvent();  
    // Accessors
    Int_t SetID() const { return fSetID; }
    void SetID(Int_t id) { fSetID = id; }
    
		Int_t RunID() const { return fRunID; }
    void RunID(Int_t id) { fRunID = id; }
    
    Int_t EventID() const { return fEventID; }
    void EventID(Int_t id) { fEventID = id; }
        
    Int_t NTracks() const { return fNTracks; }
    void NTracks(Int_t ntrk) {fNTracks = ntrk; }
    
    TClonesArray * TracksArray() { return fTracks; }

  private:
    Int_t fSetID;
		Int_t fEventID;
    Int_t fRunID;
    Int_t fNTracks;
    TClonesArray * fTracks;


    ClassDef(TEvent,1);
};

#endif //__TEVENT_H__



