//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// --------------------------------------------------------------
// History:
//
// Modified by Paolo Massarotti 2012-03-08
// Created by Simone Bifani (Simone.Bifani@cern.ch) 2008-03-10
// --------------------------------------------------------------
//

#include "Collimator.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4Material.hh"

Collimator::Collimator(G4Material * Material, G4LogicalVolume * MotherVolume, G4ThreeVector Position, G4int iCopy)
{

  fPosition = Position;
  fiCopy = iCopy;
  fMaterial = Material;
  fMotherVolume = MotherVolume;
  ReadGeometryParameters();
  CreateGeometry();
  SetProperties();

}

Collimator::~Collimator() {}

void Collimator::ReadGeometryParameters()
{
  
  fOuterXLength = 100.0*mm;
  fOuterYLength = 100.0*mm;
  fInnerXLength = 66.0*mm;
  fInnerYLength = 33.0*mm;
  fZLength = 1.0*m;

}

void Collimator::CreateGeometry()
{

  //
  // G4 volumes
  //
  G4double HalfOuterXLength = 0.5 * fOuterXLength;
  G4double HalfOuterYLength = 0.5 * fOuterYLength;
  G4double HalfInnerXLength = 0.5 * fInnerXLength;
  G4double HalfInnerYLength = 0.5 * fInnerYLength;
  G4double HalfZLength = 0.5 * fZLength;

  // Create collimator

  fCollimatorSolidVolume = new G4Box("Collimator", HalfOuterXLength, HalfOuterYLength, HalfZLength);
  fCollimatorLogicalVolume = new G4LogicalVolume(fCollimatorSolidVolume,            // solid
				       fMaterial,               // material
				       "Collimator", // name
				       0,                       // field manager 
				       0,                       // sensitive detector
				       0);                      // user limits

  fCollimatorAPhysicalVolume = new G4PVPlacement(0,
				      fPosition +
				      G4ThreeVector(1,0,0)*(HalfInnerXLength - HalfOuterXLength)
				      +G4ThreeVector(0,-1,0)*(- HalfInnerYLength - HalfOuterYLength),
				      fCollimatorLogicalVolume,          // its logical volume
				      "Collimator", // its name
				      fMotherVolume,           // its mother  volume
				      false,                   // no boolean operations
				      fiCopy);                 // copy number

  fCollimatorBPhysicalVolume = new G4PVPlacement(0,
				      fPosition +
				      G4ThreeVector(1,0,0)*(-HalfInnerXLength - HalfOuterXLength)
				      + G4ThreeVector(0,-1,0)*(- HalfInnerYLength + HalfOuterYLength),
				      fCollimatorLogicalVolume,          // its logical volume
				      "Collimator", // its name
				      fMotherVolume,           // its mother  volume
				      false,                   // no boolean operations
				      fiCopy);                 // copy number
  fCollimatorCPhysicalVolume = new G4PVPlacement(0,
				      fPosition +
				      G4ThreeVector(1,0,0)*(-HalfInnerXLength + HalfOuterXLength)
				      + G4ThreeVector(0,-1,0)*(HalfInnerYLength + HalfOuterYLength),
				      fCollimatorLogicalVolume,          // its logical volume
				      "Collimator", // its name
				      fMotherVolume,           // its mother  volume
				      false,                   // no boolean operations
				      fiCopy);                 // copy number
  fCollimatorDPhysicalVolume = new G4PVPlacement(0,
				      fPosition +
				      G4ThreeVector(1,0,0)*(HalfInnerXLength + HalfOuterXLength)
				      + G4ThreeVector(0,-1,0)*(HalfInnerYLength - HalfOuterYLength),
				      fCollimatorLogicalVolume,          // its logical volume
				      "Collimator", // its name
				      fMotherVolume,           // its mother  volume
				      false,                   // no boolean operations
				      fiCopy);                 // copy number
}

void Collimator::SetProperties()
{

  // Set visualization properties
  fVisAtt = new G4VisAttributes(G4Colour(0.5, 0.5, 0.5));
  fVisAtt->SetVisibility(true);
  fCollimatorLogicalVolume->SetVisAttributes(fVisAtt);
}
