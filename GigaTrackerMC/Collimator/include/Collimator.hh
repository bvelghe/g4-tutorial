//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// --------------------------------------------------------------
// History:
//
// Created by Simone Bifani (Simone.Bifani@cern.ch) 2008-04-22
// --------------------------------------------------------------
//
#ifndef Collimator_H
#define Collimator_H 1

#include "G4ThreeVector.hh"
#include "G4Box.hh"
#include <vector>

class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class G4VisAttributes;

class Collimator
{

public:
  
  ~Collimator();
  Collimator(G4Material*, G4LogicalVolume*, G4ThreeVector, G4int);
  void ReadGeometryParameters();
  void CreateGeometry();
  void SetProperties();

public:

  G4ThreeVector        GetPosition()                                      { return fPosition;                     };
  void                 SetPosition(G4ThreeVector value)                   { fPosition = value;                    };
 
  G4int                GetiCopy()                                         { return fiCopy;                        };
  void                 SetiCopy(G4int value)                              { fiCopy = value;                       };
 
  G4double             GetOuterXLength()                                  { return fOuterXLength;                 };
  void                 SetOuterXLength(G4double value)                    { fOuterXLength = value;                };
  G4double             GetOuterYLength()                                  { return fOuterYLength;                 };
  void                 SetOuterYLength(G4double value)                    { fOuterYLength = value;                };
  G4double             GetInnerXLength()                                  { return fInnerXLength;                 };
  void                 SetInnerXLength(G4double value)                    { fInnerXLength = value;                };
  G4double             GetInnerYLength()                                  { return fInnerYLength;                 };
  void                 SetInnerYLength(G4double value)                    { fInnerYLength = value;                };
  G4double             GetZLength()                                       { return fZLength;                      };
  void                 SetZLength(G4double value)                         { fZLength = value;                     };

private:

  G4ThreeVector fPosition;

  G4int fiCopy;

  G4double fOuterXLength;
  G4double fOuterYLength;
  G4double fInnerXLength;
  G4double fInnerYLength;
  G4double fZLength;


  G4Box* fCollimatorSolidVolume;
  G4LogicalVolume* fCollimatorLogicalVolume;
  G4VPhysicalVolume* fCollimatorAPhysicalVolume;
  G4VPhysicalVolume* fCollimatorBPhysicalVolume;
  G4VPhysicalVolume* fCollimatorCPhysicalVolume;
  G4VPhysicalVolume* fCollimatorDPhysicalVolume;

  G4Material * fMaterial; 
  G4LogicalVolume * fMotherVolume;

  G4VisAttributes * fVisAtt;

};

#endif
