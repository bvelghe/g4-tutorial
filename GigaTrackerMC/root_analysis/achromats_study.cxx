#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <sstream>
#include <algorithm>
#include <cmath>

#include "TChain.h"
#include "TFile.h"
#include "TH2D.h"

#include "TDetectorVEvent.hh"
#include "TGigaTrackerHit.hh"

#include "TLorentzVector.h"

int main(int argc, char* argv[]) {

  if(argc < 2 || argc > 3) {
    std::cout << "Usage: " << argv[0] << " FILELIST" << std::endl; 
    std::cout << "Achromats magnets analysis" << std::endl;
    std::cout << std::endl;
    std::cout << "FILELIST: ASCII file containing paths to the ROOT data files, separated by newlines." << std::endl;
    return -1;
  }
  
  TChain *tc = new TChain("GigaTracker");
 
	// Read input files
	std::list<std::string> ifiles;
	std::string fname;
	ifstream fh;
	fh.open(argv[1]);
	if(!fh.good()) {
    std::cerr << "Cannot open " << argv[1] << std::endl;
    return -1;
  }
  while(fh >> fname) {
		ifiles.push_back(fname);
	}
	fh.close();
	while(!ifiles.empty()) {
  	// What if the file didn't exist ? I don'k know ... ([...] ALWAYS returns 1 without regard to whether
  	// the file exists or contains the correct tree.) !
  	tc->Add(ifiles.front().c_str()); 
		ifiles.pop_front();
	}

	// Output file
	//FIXME Add a parameter for ofilename
  const char * ofilename = "output.root";
	TFile * ofile = new TFile(ofilename,"RECREATE");	
	if(ofile->IsZombie()) {
		std::cerr << "Cannot open " << ofilename << std::endl;
		return -1;
	}

	TDetectorVEvent * evt = new TDetectorVEvent();

  // Warning ! Pointer-to-pointer !
  tc->SetBranchAddress("Hits",&evt);

 
  unsigned int entries = tc->GetEntries();
	std::cout <<  entries << " events loaded" << std::endl;	
	// Book histo

  TH2D * h1 = new TH2D("h1","Prod. Pos. X-Y",2000,-10,10,1000,-5,5);
  TH2D * h2 = new TH2D("h2","Prod. Pos. X-Z",2000,-100,100,1000,-2000,2000);
  TH2D * h3 = new TH2D("h3","Prod. Pos. Y-Z",2000,-100,100,1000,-2000,2000);
  TH2D * h4 = new TH2D("h4","Prod. Pos. X-Z",2000,-10,10,1000,-10,10);
  TH2D * h5 = new TH2D("h5","Prod. Pos. Y-Z",1000,-5,5,1000,-10,10);
  TH1D * h6 = new TH1D("h6","Run Variation",10,0,10);
	
		int t = 0;
		int c = 0;
	for(unsigned int i = 0;i<entries;i++) {
    if(i%(entries/100) == 0) {
			std::cout << "# "<<  i << "/" << entries << " " << (float)i/entries*100 << "%" << std::endl;  
	  }
		tc->GetEntry(i);
	
		double x[2];
		double y[2];
		bool flag[2] = {false,false};

    TClonesArray * hits = evt->GetHits();
		for(unsigned j = 0; j < hits->GetEntries(); j++) {
			TGigaTrackerHit * hit = (TGigaTrackerHit*)hits->At(j);
			int pid = hit->GetPixelID();
			int row = pid/200;
			int col = pid - 200*row;

			if(hit->GetStationNo() == 0) t++;
			if(col > 39 || row > 44) continue;
			if(hit->GetStationNo() == 0) c++;
			TVector3 pos =  hit->GetPosition();
			if(hit->GetStationNo() == 0) {
				x[0] = col;
				y[0] = row;
				flag[0] = true;
			} 
			if(hit->GetStationNo() == 2) {
				x[1] = col;
				y[1] = row;
				flag[1] = true;
			} 
		
		}
		if (flag[0] && flag[1]) {
			//std::cout << x[0] << " " << y[0] << " " << x[0] - x[1] << " " << y[0] - y[1] << std::endl;
			if(x[0] == 30 && y[0] == 32) {
				std::cout << x[1] << " " << y[1] << std::endl;
			}
		}
	}	
  
	//std::cout << (float)c/(float)t << std::endl;
	h1->Write();
  h2->Write();
  h3->Write();
  h4->Write();
  h5->Write();
	h6->Write();
	ofile->Close();
	
	delete evt;
	return 0;


}
