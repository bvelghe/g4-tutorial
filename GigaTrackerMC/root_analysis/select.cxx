#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <sstream>
#include <algorithm>

#include "TChain.h"
#include "TFile.h"
#include "TH2D.h"
#include "TRandom3.h"

#include "TEvent.h"
#include "TTrack.h"


#include "TLorentzVector.h"

class EventIndex {
  public:
    int setID;
    int runID;
    int eventID;
    
    void print() {
      std::cout << setID << " " << runID << " " << eventID << std::endl;
    }
};

//!!! FIXME use operators and refactor !!!
bool comp(const EventIndex a, const EventIndex b) {
  if(a.setID < b.setID) return true;
  if(a.runID < b.runID && a.setID == b.setID) return true;
  if(a.eventID < b.eventID && a.runID == b.runID && a.setID == b.setID) return true;
  return false;
}

bool equal(const EventIndex a, const EventIndex b) {
  if(a.setID == b.setID && a.runID == b.runID && a.eventID == b.eventID) return true;
  return false;
}

int main(int argc, char* argv[]) {

  if(argc < 2 || argc > 3) {
    std::cout << "Usage: " << argv[0] << " FILELIST [SELECTED EVENTS]" << std::endl; 
    std::cout << "Events selection & analysis. If SELECTED EVENTS is provided, only the selected events in will be analysed." << std::endl;
    std::cout << std::endl;
    std::cout << "FILELIST: ASCII file containing paths to the ROOT data files, separated by newlines." << std::endl;
    std::cout << "SELECTED EVENTS: Optional. ASCII file containing events index (SetID RunID EventID) separated by newlines." << std::endl;
    return -1;
  }
  
  TRandom3 randgen(0);
 
  TChain *tc = new TChain("gtk");
 
	// Read input files
	std::list<std::string> ifiles;
	std::string fname;
	ifstream fh;
	fh.open(argv[1]);
	if(!fh.good()) {
    std::cerr << "Cannot open " << argv[1] << std::endl;
    return -1;
  }
  while(fh >> fname) {
		ifiles.push_back(fname);
	}
	fh.close();
	while(!ifiles.empty()) {
  	// What if the file didn't exist ? I don'k know ... ([...] ALWAYS returns 1 without regard to whether
  	// the file exists or contains the correct tree.) !
  	tc->Add(ifiles.front().c_str()); 
		ifiles.pop_front();
	}

	// Background like events (pre selection)
	// FIXME Generalise
	std::list<EventIndex> bkg_like_events;
  if(argc == 3) {
	  fh.open(argv[2]);
	  if(!fh.good()) {
      std::cerr << "Cannot open " << argv[1] << std::endl;
	    return -1;
    }
    fh.seekg(0);
	  int setID, runID, eventID;
	  while(fh >> setID >> runID >> eventID) {
		  EventIndex ei;
		  ei.setID = setID;
		  ei.runID = runID;
		  ei.eventID = eventID;
		  bkg_like_events.push_back(ei);
	  }
	  fh.close();
  }


	// Output file
	//FIXME Add a parameter for ofilename
  const char * ofilename = "output.root";
	TFile * ofile = new TFile(ofilename,"RECREATE");	
	if(ofile->IsZombie()) {
		std::cerr << "Cannot open " << ofilename << std::endl;
		return -1;
	}

	// Init. containers
  TEvent * evt = new TEvent(); 
  TTrack * trk = new TTrack();  

  // Warning ! Pointer-to-pointer !
  tc->SetBranchAddress("TEvent",&evt);

  if(!bkg_like_events.empty()) {
	  std::cout << "Building events index ..." << std::endl;
    tc->BuildIndex("fSetID","fEventID");
  }

 
  unsigned int entries = tc->GetEntries();
	std::cout <<  entries << " events loaded" << std::endl;	



	// Book histo

  TH2D * h1 = new TH2D("h1","Prod. Pos. X-Y",2000,-10,10,1000,-5,5);
  TH2D * h2 = new TH2D("h2","Prod. Pos. X-Z",2000,-100,100,1000,-2000,2000);
  TH2D * h3 = new TH2D("h3","Prod. Pos. Y-Z",2000,-100,100,1000,-2000,2000);
  TH2D * h4 = new TH2D("h4","Prod. Pos. X-Z",2000,-10,10,1000,-10,10);
  TH2D * h5 = new TH2D("h5","Prod. Pos. Y-Z",1000,-5,5,1000,-10,10);
  TH1D * h6 = new TH1D("h6","Run Variation",10,0,10);
	// Cut-and-Count
  //FIXME Add a parameter for max_entries
	unsigned int max_entries = 10e7; 
  if(entries > max_entries) {
    std::cout << "Warning: only the " << max_entries << " first elements will be analysed." << std::endl;
    entries = max_entries;
  }
  long counter_hz = 0;
  long counter_lz = 0;
	
	// Trk / run
	std::map<int,long> run_trkcount;
	
	
	
	for(unsigned int i = 0;i<entries;i++) {
    if(i%(entries/100) == 0) {
			std::cout << "# "<<  i << "/" << entries << " " << (float)i/entries*100 << "%" << std::endl;  
	  }
		tc->GetEntry(i);
		
		std::vector<TTrack*> tracks;

		tracks.clear();

    TClonesArray * tarray = evt->TracksArray();
		
		unsigned int runID = evt->RunID();
    unsigned int Ntrks = tarray->GetEntries();
		
   	if(run_trkcount.find(runID) == run_trkcount.end()) {
			run_trkcount[runID] = Ntrks;
		} else {
			run_trkcount[runID]+= Ntrks;
		}


		for(unsigned int j = 0; j < Ntrks; j++) {
      TTrack * trk = (TTrack*)(*tarray)[j];
      
			
			if(trk->Vertex().Z() > -10.0) {
				counter_hz++;
				h1->Fill(trk->Vertex().X(),trk->Vertex().Y());
      	h2->Fill(trk->Vertex().X(),trk->Vertex().Z());
      	h3->Fill(trk->Vertex().Y(),trk->Vertex().Z());
      	h4->Fill(trk->Vertex().X(),trk->Vertex().Z());
      	h5->Fill(trk->Vertex().Y(),trk->Vertex().Z());
				h6->Fill(runID);
			}
			else counter_lz++;
        
    }
  }
  std::cout << "If low Z (Z < -10 cm): "<< counter_lz << ", else: " << counter_hz << std::endl;
  // END-OF-LOOP events

	for(std::map<int,long>::iterator it = run_trkcount.begin(); it != run_trkcount.end(); ++it) {
		std::cout << it->first << " " << it->second << std::endl;
	}

  h1->Write();
  h2->Write();
  h3->Write();
  h4->Write();
  h5->Write();
	h6->Write();
	ofile->Close();
	
	delete evt;
  delete trk; 
  delete tc;
	return 0;


}
