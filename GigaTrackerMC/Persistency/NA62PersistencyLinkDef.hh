#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Run+;
#pragma link C++ class DetectorInfo+;
#pragma link C++ class SubDetectorInfo+;
#pragma link C++ class Event+;
#pragma link C++ class EventHeader+;
#pragma link C++ class EventData+;
#pragma link C++ class GenePart+;
#pragma link C++ class KinePart+;
#pragma link C++ class TVChannelID+;
#pragma link C++ class TVEvent+;
#pragma link C++ class TVHit+;
#pragma link C++ class TVDigi+;
#pragma link C++ class TDCVHit+;
#pragma link C++ class TDetectorVEvent+;
#pragma link C++ class TDetectorVHit+;
#pragma link C++ class DetectorParameter+;
#pragma link C++ class TRecoVEvent+;
#pragma link C++ class TRecoVCandidate+;
#pragma link C++ class TRecoVHit+;
#endif

