// --------------------------------------------------------------
// History:
//
// Created by Emanuele Leonardi (Emanuele.Leonardi@cern.ch) 2007-01-10
//
// --------------------------------------------------------------
#include <stdlib.h>
#include "Riostream.h"

#include "EventHeader.hh"

EventHeader::EventHeader()
: fEvtNum(-1), fRunNum(-1), fDate(-1)
{}

void EventHeader::Print(Option_t * /*option*/) const
{
  //cout << "--- Event Header ---" << endl;
				std::cout << "Run " << fRunNum
       << " Event " << fEvtNum
       << " Date " << fDate
       << std::endl;
}
