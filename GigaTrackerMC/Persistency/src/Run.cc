// --------------------------------------------------------------
// History:
//
// Created by Emanuele Leonardi (Emanuele.Leonardi@cern.ch) 2007-01-10
//
// --------------------------------------------------------------
#include <stdlib.h>
#include "Riostream.h"

#include "Run.hh"

ClassImp(Run)
ClassImp(DetectorInfo)
ClassImp(SubDetectorInfo)

Run::Run()
{
  fRunNumber = 0;
}

void Run::Clear(Option_t * /*option*/)
{
  fRunNumber = 0;
  fDetInfo.Clear("C");
}

void Run::Reset(Option_t * /*option*/)
{}

void Run::Print(Option_t * /*option*/) const
{
				std::cout << "Run " << fRunNumber << std::endl;
				std::cout << "Event tree name: " << fEventTreeName << std::endl;
  fDetInfo.Print();
}
