// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-10-04
//
// --------------------------------------------------------------
#include "TRecoVHit.hh"

ClassImp(TRecoVHit)

TRecoVHit::TRecoVHit() : TDetectorVHit() {
    fDigi = 0;
    fDigiOwner = kFALSE;
}

TRecoVHit::TRecoVHit(Int_t iCh) : TDetectorVHit(iCh) {
    fDigi = 0;
    fDigiOwner = kFALSE;
}

TRecoVHit::TRecoVHit(TVDigi* Digi) : fDigi(Digi) {
    (*(TVHit*)this) = (*(TVHit*)Digi);
}

TRecoVHit::TRecoVHit(TDetectorVHit* MCHit) {
    fDigi = new TVDigi(MCHit);
    (*(TDetectorVHit*)this) = (*(TDetectorVHit*)MCHit);
    fDigiOwner = kTRUE;
}

TRecoVHit::~TRecoVHit() {
    Clear();
}

void TRecoVHit::Clear(Option_t* option) {
    if(fDigi && fDigiOwner)
        delete fDigi;
    fDigi = 0;
}

