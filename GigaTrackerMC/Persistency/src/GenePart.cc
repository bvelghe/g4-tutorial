// --------------------------------------------------------------
// History:
//
// Created by Evgueni Goudzovski (eg@hep.ph.bham.ac.uk) 6 Apr 2011
//
// --------------------------------------------------------------
#include <stdlib.h>
#include "Riostream.h"

#include "GenePart.hh"

ClassImp(GenePart)

GenePart::GenePart() {
  fPDGcode = 0;
  fParticleName    = "";
  fInitialEnergy   = 0.0;
  fInitialMomentum = TVector3(0.0, 0.0, 0.0);
  fParticleGroup   = kOriginal;
}

void GenePart::Clear(Option_t*) {
  fParticleName = "";
}

void GenePart::Print(Option_t*) const {
				std::cout << " Code " << fPDGcode
       << " E " << fInitialEnergy << " MeV"
       << " P " << fInitialMomentum.X()<<" "<<fInitialMomentum.Y()<<" "<<fInitialMomentum.Z()<<" MeV"
       << " Group " << fParticleGroup
       << std::endl;
}
