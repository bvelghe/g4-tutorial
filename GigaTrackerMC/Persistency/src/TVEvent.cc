// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-10-04
//
// --------------------------------------------------------------
#include "TVEvent.hh"

ClassImp(TVEvent)

TVEvent::TVEvent(){
    fID = 0;
    fBurstID = 0;
    fIsMC = kFALSE;
    fTimeStamp = 0;
}

TVEvent::~TVEvent(){
}

Int_t TVEvent::Compare(const TObject *obj) const
{
    // Compare two TVEvents objects. 
    if (this == obj) return 0;
    return (Int_t)(fTimeStamp > ((TVEvent*)obj)->GetTimeStamp());
}
       
