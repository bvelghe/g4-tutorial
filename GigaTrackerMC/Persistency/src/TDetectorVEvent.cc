// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-01-08
//
// --------------------------------------------------------------
#include "TDetectorVEvent.hh"
#include "Riostream.h"

ClassImp(TDetectorVEvent)

TDetectorVEvent::TDetectorVEvent() : TVEvent(){
    fNHits = 0;
    fHits = 0;
}

TDetectorVEvent::TDetectorVEvent(TClass * Class) : TVEvent(){
    fNHits = 0;
    fHits = new TClonesArray(Class,1000);
}

TDetectorVEvent::~TDetectorVEvent(){
    if(fHits)
        delete fHits;
}

void TDetectorVEvent::Clear(Option_t * option)
{
    fNHits = 0;
    if(fHits)
        fHits->Clear(option);
}

TVHit * TDetectorVEvent::AddHit(){
    return (TVHit *)(fHits->New(fNHits++));
}

TVHit * TDetectorVEvent::AddHit(Int_t iCh){
    TVHit * hit = (TVHit *)(fHits->New(fNHits++));
    hit->SetChannelID(iCh);
    return hit;
}

TVDigi * TDetectorVEvent::AddHit(TDetectorVHit * MCHit){
    TVDigi * digi = (TVDigi *)(fHits->New(fNHits++));
    digi->SetChannelID(MCHit->GetChannelID());
    digi->SetMCTrackID(MCHit->GetMCTrackID());
    digi->SetMCHit(MCHit);
    return digi;
}

TVHit* TDetectorVEvent::LastHitOnChannel(Int_t iCh) {
    TVHit * hit = 0;
    for(Int_t iHit = 0; iHit < fNHits; iHit++){
        if(((TVHit*)(fHits->At(iHit)))->GetChannelID() == iCh)
            hit = (TVHit*)fHits->At(iHit);
    }
    return hit;
}

TVHit* TDetectorVEvent::Hit(Int_t iHit) {
    return (TVHit*)fHits->At(iHit);
}

TVHit* TDetectorVEvent::LastHit() {
    return (TVHit*)fHits->At(fNHits-1);
}

void TDetectorVEvent::RemoveHit(Int_t iHit){
    fHits->RemoveAt(iHit);
    fHits->Compress();
    fNHits--;
}
