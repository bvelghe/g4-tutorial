// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch)
//            Francesca Bucci (Francesca.Bucci@cern.ch) 2008-01-17
//
// Based on a project by Emanuele Leonardi (Emanuele Leonardi@cern.ch) 2007-01-10
//
// --------------------------------------------------------------
#include <stdlib.h>
#include "Riostream.h"

#include "KinePart.hh"

ClassImp(KinePart)

KinePart::KinePart() : GenePart() {
  fFinalEnergy = 0.;
  fFinalMomentum = TVector3(0.,0.,0.);
  fID = -1;
  fParentID = -1;
  fParentIndex = -1;
  fProdPos = TLorentzVector(0.,0.,0.,0.);
  fEndPos = TLorentzVector(0.,0.,0.,0.);
}

void KinePart::Clear(Option_t*) {
  fProdProcessName = "";
  fEndProcessName = "";
  fParticleName = "";
}

void KinePart::Print(Option_t*) const
{
				std::cout << " PDG "  << fPDGcode << " Name " << fParticleName
       << " Prod " << fProdPos.X() << " " << fProdPos.Y() << " " << fProdPos.Z() << " mm " << fProdProcessName
       << " End "  << fEndPos.X() << " " << fEndPos.Y() << " " << fEndPos.Z() << " mm " << fEndProcessName
       << " E "    << fFinalEnergy << " MeV"
       << " P "    << fFinalMomentum.X() << " " << fFinalMomentum.Y() << " " << fFinalMomentum.Z() << " MeV"
       << std::endl;
}
