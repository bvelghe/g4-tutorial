// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-01-08
//
// --------------------------------------------------------------
#include "Riostream.h"

#include "TDetectorVHit.hh"

ClassImp(TDetectorVHit)

TDetectorVHit::TDetectorVHit() : TVHit() {
  fEnergy=0.;
  fTime=0.;
}

TDetectorVHit::TDetectorVHit(Int_t iCh) : TVHit(iCh) {
  fEnergy=0.;
  fTime=0.;
}

void TDetectorVHit::Print(Option_t *) const {
				std::cout << "HitPosition = (" << fPosition.X() << "," << fPosition.Y() << "," << fPosition.Z() << ")" << std::endl
       << "Energy = " << fEnergy << std::endl
       << "Time = " << fTime << std::endl << std::endl;
}
