// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-09-20
//
// --------------------------------------------------------------
#include "TVDigi.hh"
#include "TVDigi.hh"

ClassImp(TVDigi)

TVDigi::TVDigi() : TVHit() {
  fMCHit = 0;
}

TVDigi::TVDigi(Int_t iCh) : TVHit(iCh) {
  fMCHit = 0;
}

TVDigi::TVDigi(TVHit* MCHit) {
  fMCHit = MCHit;
  (*(TVHit*)this) = (*(TVHit*)MCHit);
}

void TVDigi::Clear(Option_t* option) {
  if(fMCHit) {
      delete fMCHit;
      fMCHit = 0;
  }
}
