// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch)
//            Francesca Bucci (Francesca.Bucci@cern.ch) 2008-01-17
//
// Based on a project by Emanuele Leonardi (Emanuele.Leonardi@cern.ch) 2007-01-10
//
// --------------------------------------------------------------
#include <stdlib.h>
#include "Riostream.h"

#include "EventData.hh"

EventData::EventData() : fNTrajTot(-1) {}

void EventData::Clear(Option_t*) {
}

void EventData::Print(Option_t*) const {
				std::cout << "   NTrajectories Tot " << fNTrajTot << std::endl;
}
