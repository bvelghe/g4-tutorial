// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-01-08
//
// --------------------------------------------------------------
#include "TVHit.hh"

ClassImp(TVHit)

TVHit::TVHit() : TVChannelID() {
  fMCTrackID=-1;
}

TVHit::TVHit(Int_t iCh) : TVChannelID(iCh) {
  fMCTrackID=-1;
}

