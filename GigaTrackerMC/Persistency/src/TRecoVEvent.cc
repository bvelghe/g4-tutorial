// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-10-04
//
// --------------------------------------------------------------
#include "TRecoVEvent.hh"
#include "Riostream.h"
#include "TClass.h"

ClassImp(TRecoVEvent)

TRecoVEvent::TRecoVEvent() : TDetectorVEvent()
{
    fStatus = kFALSE;
    fNCandidates = 0;
    fCandidates = 0;
}

TRecoVEvent::TRecoVEvent(TClass * CandidateClass, TClass * HitClass) : TDetectorVEvent(HitClass){
    fStatus = kFALSE;
    fNCandidates = 0;
    fCandidates = new TClonesArray(CandidateClass,20);
}

TRecoVEvent::~TRecoVEvent()
{
    if(fCandidates){
        fCandidates->Clear("C");
        delete fCandidates;
    }
}

void TRecoVEvent::Clear(Option_t * option)
{
    TDetectorVEvent::Clear(option);
    fNCandidates = 0;
    fCandidates->Clear(option);
}

TRecoVHit * TRecoVEvent::AddHit(TDetectorVHit * MCHit){
    TRecoVHit * hit = (TRecoVHit*)TDetectorVEvent::AddHit(MCHit->GetChannelID());
    (*(TDetectorVHit*)hit) = (*MCHit);
    hit->SetDigi(new TVDigi(MCHit));
    hit->SetDigiOwner(kTRUE);
    return hit;
}

TRecoVHit * TRecoVEvent::AddHit(TVDigi * Digi){
    TRecoVHit * hit = (TRecoVHit*)TDetectorVEvent::AddHit(Digi->GetChannelID());
    TDetectorVHit * MCHit = (TDetectorVHit*)Digi->GetMCHit();
    if(MCHit)
        (*(TDetectorVHit*)hit) = *MCHit;
    hit->SetDigi(Digi);
    return hit;
}

TRecoVCandidate * TRecoVEvent::AddCandidate(){
    TRecoVCandidate * candidate = (TRecoVCandidate*)(fCandidates->New(fNCandidates++));
    candidate->SetEvent(this);
    return candidate;
}

TRecoVCandidate * TRecoVEvent::GetCandidate(Int_t iCandidate){
    return (TRecoVCandidate*)fCandidates->At(iCandidate);
}

void TRecoVEvent::RemoveCandidate(Int_t iCandidate){
    fCandidates->RemoveAt(iCandidate);
    fCandidates->Compress();
    fNCandidates--;
}
