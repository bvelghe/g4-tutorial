// --------------------------------------------------------------
// History:
//
// Created by Emanuele Leonardi (Emanuele Leonardi@cern.ch) 2007-01-10
//// Modified by Sergey Podolsky 2011-01-21
// --------------------------------------------------------------
#include <stdlib.h>
#include "Riostream.h"
#include "Event.hh"

ClassImp(Event)
ClassImp(EventHeader)
ClassImp(EventData)

TClonesArray* Event::fgKineParts = 0;
TClonesArray* Event::fgGeneParts = 0;
TRandom3* Event::fgRandomDecayState = 0;

Event::Event() : fEvtData()
{
  // Create an Event object.
  // When the constructor is invoked for the first time, the class static
  // variable fgVetoDigis is 0 and the TClonesArray fgVetoDigis is created.

  if (!fgGeneParts)
    fgGeneParts = new TClonesArray("GenePart", 100);
  fGeneParts = fgGeneParts;
  fNGeneParts = 0;

  if (!fgKineParts)
    fgKineParts = new TClonesArray("KinePart", 1000);
  fKineParts = fgKineParts;
  fNKineParts = 0;

  if (!fgRandomDecayState) {
    fgRandomDecayState = new TRandom3();
  }
  fRandomDecayState = fgRandomDecayState; 
}

void Event::Clear(Option_t * /*option*/)
{
  fNGeneParts = 0;
  fNKineParts = 0;
  fGeneParts->Clear("C");
  fKineParts->Clear("C"); // will also call KinePart::Clear
  fRandomDecayState->Clear("C");
  fEvtData.Clear();
}

void Event::Reset(Option_t * /*option*/)
{
  // Static function to reset all static objects for this event
  delete fgGeneParts; fgGeneParts = 0;
  delete fgKineParts; fgKineParts = 0;
}

void Event::Print(Option_t * /*option*/) const
{
				std::cout << "Run " << fEvtHdr.GetRunNum()
       << " Event " << fEvtHdr.GetEvtNum()
       << " Date " << fEvtHdr.GetDate() << std::endl;
  fEvtData.Print();
	std::cout << "   NGeneParticles " << fNGeneParts << std::endl;
	std::cout << "   NKineParticles " << fNKineParts << std::endl;
}

void Event::PrintAll() const 
{
  if (fNGeneParts>0) {
					std::cout << "--- Gene Particles ---" << std::endl;
    fGeneParts->Print();
  }
  if (fNKineParts>0) {
					std::cout << "--- Kine Particles ---" << std::endl;
    fKineParts->Print();
  }
}

GenePart* Event::AddGenePart() {
  TClonesArray &parts = *fGeneParts;
  GenePart* part = new(parts[fNGeneParts++]) GenePart();
  return part;
}

KinePart* Event::AddKinePart() {
  TClonesArray &parts = *fKineParts;
  KinePart* part = new(parts[fNKineParts++]) KinePart();
  return part;
}

void Event::EraseGeneParts() {
  // Erase old collection of hits and replace it with a new (empty) one
  delete fgGeneParts;
  fgGeneParts = new TClonesArray("GenePart", 100);
  fGeneParts = fgGeneParts;
  fNGeneParts = 0;
}

void Event::EraseKineParts() {
  // Erase old collection of hits and replace it with a new (empty) one
  delete fgKineParts;
  fgKineParts = new TClonesArray("KinePart", 1000);
  fKineParts = fgKineParts;
  fNKineParts = 0;
}

void Event::StoreRandomState(TRandom3* RandomDecayState, long *RanecuState)
{

  *fgRandomDecayState = *RandomDecayState;
  fRanecuState[0] = RanecuState[0];
  fRanecuState[1] = RanecuState[1];
}
