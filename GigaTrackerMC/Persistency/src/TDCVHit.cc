// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2007-03-30
//
// --------------------------------------------------------------
#include "TDCVHit.hh"
ClassImp(TDCVHit)

TDCVHit::TDCVHit() : TVDigi(){
    fDetectedEdge = 0;
    fLeadingEdge = -300.;
    fTrailingEdge = -300.;
}

TDCVHit::TDCVHit(Int_t iCh) : TVDigi(iCh){
    fDetectedEdge = 0;
    fLeadingEdge = -300.;
    fTrailingEdge = -300.;
}

TDCVHit::TDCVHit(TVHit* MCHit) : TVDigi(MCHit){
    fDetectedEdge = 0;
    fLeadingEdge = -300.;
    fTrailingEdge = -300.;
}
