// --------------------------------------------------------------
// History:
//
// Modified by Giuseppe Ruggiero 2012-03-08
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-10-04
//
// --------------------------------------------------------------
#include "TRecoVCandidate.hh"
#include "TRecoVEvent.hh"
#include "Riostream.h"

ClassImp(TRecoVCandidate)

TRecoVCandidate::TRecoVCandidate()
{
    fEvent = 0;
    fNMaxHits = 200;
    fNHits = 0;
    fHitsIndexes = new Int_t[fNMaxHits];
}

TRecoVCandidate::TRecoVCandidate(Int_t NMaxHits)
{
    fEvent = 0;
    fNMaxHits = NMaxHits;
    fNHits = 0;
    fHitsIndexes = new Int_t[fNMaxHits];
}

TRecoVCandidate::~TRecoVCandidate()
{
    Clear();
}

void TRecoVCandidate::Clear(Option_t * option)
{
    fNHits = 0;
    if(fHitsIndexes)
        delete fHitsIndexes;
    fHitsIndexes = 0;
}

Bool_t TRecoVCandidate::AddHit(Int_t HitIndex){
    fNHits++;
    if(fNHits < fNMaxHits)
        fHitsIndexes[fNHits - 1] = HitIndex;
    else
        return kFALSE;

    return kTRUE;
}

TRecoVHit * TRecoVCandidate::GetHit(Int_t iHit){
    if(fEvent && iHit < fNMaxHits && iHit < fNHits)
        return (TRecoVHit*)fEvent->Hit(fHitsIndexes[iHit]);
    else
        return (TRecoVHit*)0;
}

void TRecoVCandidate::RemoveHit(Int_t iHit){
    fNHits--;
    for(Int_t jHit = iHit; jHit < fNHits && jHit < fNMaxHits; jHit++)
        fHitsIndexes[jHit] = fHitsIndexes[jHit + 1];
}

void TRecoVCandidate::Merge(TRecoVCandidate* Candidate){

  for(Int_t iHit=0; iHit < Candidate->GetNHits() ; iHit++){
    AddHit(Candidate->fHitsIndexes[iHit]);
  }


}
