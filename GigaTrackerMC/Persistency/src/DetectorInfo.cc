// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2008-03-30
//
// --------------------------------------------------------------
#include "DetectorInfo.hh"

DetectorInfo::DetectorInfo()
{;}

void DetectorInfo::Clear(Option_t* opt)
{;}

void DetectorInfo::Print(Option_t * /*option*/) const
{
  Int_t nDetectors = fSubDetectorsInfo.size();
  for(Int_t iDet = 0; iDet < nDetectors; iDet++)
    fSubDetectorsInfo[iDet]->Print();
}

SubDetectorInfo* DetectorInfo::FindSubDetectorInfo(TString name)
{
  Int_t nDetectors = fSubDetectorsInfo.size();
  for(Int_t iDet = 0; iDet < nDetectors; iDet++)
    if(fSubDetectorsInfo[iDet]->GetName() == name)
      return fSubDetectorsInfo[iDet];

  return 0;
}

SubDetectorInfo* DetectorInfo::AddSubDetectorInfo(TString name)
{
  SubDetectorInfo* subDet = FindSubDetectorInfo(name);
  if(!subDet){
    subDet = new SubDetectorInfo(name);
    fSubDetectorsInfo.push_back(subDet);
  }
  return subDet;
}

