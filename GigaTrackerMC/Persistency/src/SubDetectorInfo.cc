// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2008-03-30
//
// --------------------------------------------------------------
#include "SubDetectorInfo.hh"

void SubDetectorInfo::Clear(Option_t*)
{
  fGeometryParameters.Clear();
  fMaterialParameters.Clear();
}

void SubDetectorInfo::Print(Option_t*) const
{
  for(Int_t iPar = 0; iPar < fGeometryParameters.GetEntries(); iPar++)
    fGeometryParameters[iPar]->Print();
  for(Int_t iPar = 0; iPar < fMaterialParameters.GetEntries(); iPar++)
    fMaterialParameters[iPar]->Print();
}


