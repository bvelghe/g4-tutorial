// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-01-08
//
// --------------------------------------------------------------
#ifndef TDetectorVHit_H
#define TDetectorVHit_H

#include "TVector3.h"
#include "TVHit.hh"

class TDetectorVHit : public TVHit {

    public:

        TDetectorVHit();
        TDetectorVHit(Int_t);
        ~TDetectorVHit(){};
        void Print(Option_t* option="") const;

    public:

        TVector3             GetPosition()                                      { return fPosition;                     };
        void                 SetPosition(TVector3 value)                        { fPosition = value;                    };
        Double_t             GetEnergy()                                        { return fEnergy;                       };
        void                 SetEnergy(Double_t value)                          { fEnergy = value;                      };
        Double_t             GetTime()                                          { return fTime;                         };
        void                 SetTime(Double_t value)                            { fTime = value;                        };
        void                 AddEnergy(Double_t value)                          { fEnergy += value;                     };

    private:

        TVector3   fPosition;
        Double_t   fEnergy;
        Double_t   fTime;

        ClassDef(TDetectorVHit,1);
};
#endif
