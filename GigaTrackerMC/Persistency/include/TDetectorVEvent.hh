// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-01-08
//
// --------------------------------------------------------------
#ifndef TDetectorVEvent_H
#define TDetectorVEvent_H

#include "TClass.h"
#include "TClonesArray.h"
#include "TVEvent.hh"
#include "TVHit.hh"
#include "TDetectorVHit.hh"
#include "TVDigi.hh"

class TDetectorVEvent : public TVEvent {

    public:

        TDetectorVEvent();
        TDetectorVEvent(TClass *);
        ~TDetectorVEvent();
        TVHit * AddHit();
        TVHit * AddHit(Int_t iCh);
        TVDigi * AddHit(TDetectorVHit *);
        TVHit * LastHitOnChannel(Int_t iCh);
        TVHit * Hit(Int_t iHit);
        TVHit * LastHit();
        void RemoveHit(Int_t iHit);
        void Clear(Option_t* = "");

    public:

        Int_t                GetNHits()                                         { return fNHits;                        };
        void                 SetNHits(Int_t value)                              { fNHits = value;                       };

        TClonesArray *       GetHits()                                          { return fHits;                         };
        void                 SetHits(TClonesArray * value)                      { fHits = value;                        };

    private:

        Int_t      fNHits;

        TClonesArray * fHits;

        ClassDef(TDetectorVEvent,1);
};
#endif
