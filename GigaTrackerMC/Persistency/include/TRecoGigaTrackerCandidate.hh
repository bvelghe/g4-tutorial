// --------------------------------------------------------------
// History:
//
// Modified by Massimiliano Fiorini (Massimiliano.Fiorini@cern.ch) 2011-05-04
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-10-04
//
// --------------------------------------------------------------
#ifndef TRecoGigaTrackerCandidate_H
#define TRecoGigaTrackerCandidate_H

#include "TRecoVCandidate.hh"
#include "TLorentzVector.h"

class TRecoGigaTrackerCandidate : public TRecoVCandidate {

public:
  
  TRecoGigaTrackerCandidate();
  ~TRecoGigaTrackerCandidate(){};
  
  TLorentzVector GetMomentum(){                                    return fMomentum;                 }
  void           SetMomentum(TLorentzVector value){                fMomentum = value;                }

  Double_t       GetTime(){                                        return fTime;                     }
  void           SetTime(Double_t value){                          fTime = value;                    }

  Double_t       GetTimeStation(Int_t StationID){                  return fTimeStation[StationID];   }
  void           SetTimeStation(Int_t StationID,Double_t value){   fTimeStation[StationID] = value;  }

  TVector3       GetPosition(Int_t StationID){                     return fPosition[StationID];      }
  void           SetPosition(Int_t StationID,TVector3 value){      fPosition[StationID] = value;     }

  Double_t       GetChi2X(){                                       return fChi2X;                    }
  void           SetChi2X(Double_t value){                         fChi2X = value;                   }

  Double_t       GetChi2Y(){                                       return fChi2Y;                    }
  void           SetChi2Y(Double_t value){                         fChi2Y = value;                   }

  Double_t       GetChi2Time(){                                    return fChi2Time;                 }
  void           SetChi2Time(Double_t value){                      fChi2Time = value;                }

  Double_t       GetChi2(){                                        return fChi2;                     }
  void           SetChi2(Double_t value){                          fChi2 = value;                    }

 
public:
  
  
private:
  
  TLorentzVector fMomentum;
  Double_t fTime;
  Double_t fTimeStation[3];
  TVector3 fPosition[3];


  Double_t fChi2X;
  Double_t fChi2Y;
  Double_t fChi2Time;
  Double_t fChi2;
  
  ClassDef(TRecoGigaTrackerCandidate,1);
};
#endif
