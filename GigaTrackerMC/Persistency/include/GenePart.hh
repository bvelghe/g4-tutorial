// --------------------------------------------------------------
// History:
//
// Created by Evgueni Goudzovski (eg@hep.ph.bham.ac.uk) 6 Apr 2011
//
// --------------------------------------------------------------
#ifndef GenePart_h
#define GenePart_h 1

#include "TObject.h"
#include "TVector3.h"
#include "TLorentzVector.h"

enum TGenePartGroup {kOriginal, kPhotosCorrected};

class GenePart : public TObject
{

public:

  GenePart();
  virtual ~GenePart() {Clear();}
  void Clear(Option_t* option="");
  void Print(Option_t* option="") const;

  inline void SetParticleName(TString val)     { fParticleName = val; };
  inline void SetParticleName(char* val)       { fParticleName = TString(val); };
  inline void SetPDGcode(Int_t val)            { fPDGcode = val; };
  inline void SetInitialEnergy(Double_t val)   { fInitialEnergy = val; };
  inline void SetInitialMomentum(TVector3 val) { fInitialMomentum = val; };
  void SetInitial4Momentum(TLorentzVector val)
  { SetInitialMomentum(val.Vect());SetInitialEnergy(val.T()); };
  inline void SetParticleGroup(TGenePartGroup val) { fParticleGroup = val; };

  inline TString GetParticleName()     { return fParticleName; };
  inline Int_t GetPDGcode()            { return fPDGcode; };
  inline Double_t GetInitialEnergy()   { return fInitialEnergy; };
  inline TVector3 GetInitialMomentum() { return fInitialMomentum; };
  inline TLorentzVector GetInitial4Momentum()
  { return TLorentzVector(fInitialMomentum,fInitialEnergy); };
  inline TGenePartGroup GetParticleGroup() { return fParticleGroup; };

protected:

  Int_t          fPDGcode;
  TString        fParticleName;
  Double_t       fInitialEnergy;   // MeV
  TVector3       fInitialMomentum; // MeV
  TGenePartGroup fParticleGroup;

  ClassDef(GenePart,1)
};

#endif
