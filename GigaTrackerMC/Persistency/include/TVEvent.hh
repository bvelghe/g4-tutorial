// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-10-04
//
// --------------------------------------------------------------
#ifndef TVEvent_H
#define TVEvent_H

#include "TObject.h"

class TVEvent : public TObject {

    public:

        TVEvent();
        virtual ~TVEvent();
        Int_t Compare(const TObject *obj) const;
        Bool_t IsSortable() const { return kTRUE; }

    public:

        ULong64_t            GetStartByte()                                     { return fStartByte;                    };
        void                 SetStartByte(ULong64_t value)                      { fStartByte = value;                   };
        Int_t                GetID()                                            { return fID;                           };
        void                 SetID(Int_t value)                                 { fID = value;                          };
        Int_t                GetBurstID()                                       { return fBurstID;                      };
        void                 SetBurstID(Int_t value)                            { fBurstID = value;                     };
        Bool_t               GetIsMC()                                          { return fIsMC;                         };
        void                 SetIsMC(Bool_t value)                              { fIsMC = value;                        };
        ULong_t              GetTimeStamp()                                     { return fTimeStamp;                    };
        void                 SetTimeStamp(ULong_t value)                        { fTimeStamp = value;                   };

    private:

        ULong64_t  fStartByte;
        Int_t      fID;
        Int_t      fBurstID;
        Bool_t     fIsMC;

//        ULong_t fTimeStamp;
        Long_t fTimeStamp;

        ClassDef(TVEvent,1);
};
#endif
