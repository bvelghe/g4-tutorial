// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-10-04
//
// --------------------------------------------------------------
#ifndef TRecoVCandidate_H
#define TRecoVCandidate_H

class TRecoVEvent;
#include "TRecoVHit.hh"

#include "TObject.h"
#include "TClass.h"

#include <vector>

class TRecoVCandidate : public TObject {

    public:

        TRecoVCandidate();
        TRecoVCandidate(Int_t);
        virtual ~TRecoVCandidate();

        Bool_t AddHit(Int_t);
        TRecoVHit * GetHit(Int_t);
        void Clear(Option_t* = "");
        void RemoveHit(Int_t);
        void Merge(TRecoVCandidate*);

    public:

        Int_t                GetNHits()                                         { return fNHits;                        };
        void                 SetNHits(Int_t value)                              { fNHits = value;                       };

        Int_t *              GetHitsIndexes()                                   { return fHitsIndexes;                  };

        TRecoVEvent *        GetEvent()                                         { return fEvent;                        };
        void                 SetEvent(TRecoVEvent * value)                      { fEvent = value;                       };

        Double_t             GetTime()                                          { return fTime;                         };
        void                 SetTime(Double_t value)                            { fTime = value;                        };


    private:

        Int_t fNHits;
        Int_t fNMaxHits;

        Int_t * fHitsIndexes; //[fNMaxHits];

        TRecoVEvent * fEvent; //! Transient data member for simpler manipulation 

        Double_t fTime;

        ClassDef(TRecoVCandidate,1);
};
#endif
