// --------------------------------------------------------------
// History:
//
// Created by Emanuele Leonardi (Emanuele.Leonardi@cern.ch) 2007-01-10
//
// --------------------------------------------------------------
#ifndef EventHeader_h
#define EventHeader_h 1

#include "TROOT.h"

class EventHeader {

public:

  EventHeader();
  virtual ~EventHeader() {}
  void Print(Option_t * option="") const;

  Int_t GetEvtNum() const { return fEvtNum; }
  Int_t GetRunNum() const { return fRunNum; }
  Int_t GetDate()   const { return fDate; }

  void SetEvtNum(Int_t i) { fEvtNum = i; }
  void SetRunNum(Int_t i) { fRunNum = i; }
  void SetDate(Int_t i)   { fDate = i; }

private:

  Int_t fEvtNum;
  Int_t fRunNum;
  Int_t fDate;

  ClassDef(EventHeader,1)
};

#endif
