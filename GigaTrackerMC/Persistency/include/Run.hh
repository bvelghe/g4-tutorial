// --------------------------------------------------------------
// History:
//
// Created by Emanuele Leonardi (Emanuele.Leonardi@cern.ch) 2007-01-10
//
// --------------------------------------------------------------
#ifndef Run_h
#define Run_h 1

#include "TObject.h"
#include "TString.h"

#include "DetectorInfo.hh"

class Run : public TObject
{

public:

  Run();
  virtual ~Run() { Clear(); };
  void Clear(Option_t* option ="");
  static void Reset(Option_t* option ="");
  void Print(Option_t* option="") const;

  DetectorInfo* GetDetectorInfo() { return &fDetInfo; }

  void SetRunNumber(Int_t r) { fRunNumber = r; };
  Int_t GetRunNumber() { return fRunNumber; };
  void SetEventTreeName(TString name) { fEventTreeName = name; };
  TString GetEventTreeName() { return fEventTreeName; };

private:

  DetectorInfo fDetInfo;

  Int_t fRunNumber;
  TString fEventTreeName;

  ClassDef(Run,1)
};

#endif
