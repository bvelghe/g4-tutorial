// --------------------------------------------------------------
// History:
//
// Created by Emanuele Leonardi (Emanuele.Leonardi@cern.ch) 2007-01-10
// Modified by Sergey Podolsky 2011-01-21
// --------------------------------------------------------------
#ifndef Event_h
#define Event_h 1

#include "TObject.h"
#include "TClonesArray.h"

#include "EventHeader.hh"
#include "EventData.hh"

#include "KinePart.hh"
#include "Event.hh"
#include "TRandom3.h"
#include "Riostream.h"


class Event : public TObject
{

public:

  Event();
  virtual ~Event() { Clear(); };
  void Clear(Option_t* option ="");
  static void Reset(Option_t* option ="");
  void Print(Option_t* option="") const;
  void PrintAll() const;

  EventHeader* GetHeader() { return &fEvtHdr; }
  EventData*   GetData()   { return &fEvtData; }

  Int_t         GetNKineParts() const { return fNKineParts; }
  TClonesArray* GetKineParts()  const { return fKineParts; }
  KinePart*     AddKinePart();
  void          EraseKineParts();

  Int_t         GetNGeneParts() const { return fNGeneParts; }
  TClonesArray* GetGeneParts()  const { return fGeneParts; }
  GenePart*     AddGenePart();
  void          EraseGeneParts();

  void StoreRandomState(TRandom3* RandomDecayState,  long *RanecuState);
  TRandom3* GetRandomDecayState () {return fRandomDecayState;}
  long* GetRanecuState() {return fRanecuState;}

private:

  EventHeader fEvtHdr;
  EventData   fEvtData;

  Int_t fNGeneParts;        // Number of generated particles
  Int_t fNKineParts;        // Number of saved particles

  TClonesArray* fKineParts; // Array of saved particles
  TClonesArray* fGeneParts; // Array of generated particles in K rest frame

  static TRandom3* fgRandomDecayState;
  TRandom3* fRandomDecayState;
  Long_t fRanecuState[2];

  static TClonesArray* fgGeneParts;
  static TClonesArray* fgKineParts;

  ClassDef(Event,1)
};

#endif
