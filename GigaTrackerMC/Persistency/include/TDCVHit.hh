// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2007-03-30
//
// --------------------------------------------------------------
#ifndef TDCVHit_H
#define TDCVHit_H
#include "TVDigi.hh"

class TDCVHit : public TVDigi {

    public:

        TDCVHit();
        TDCVHit(Int_t);
        TDCVHit(TVHit* MCHit);
        virtual ~TDCVHit(){};

    public:

        inline Int_t                GetDetectedEdge()                            { return fDetectedEdge;                 };
        inline void                 SetDetectedEdge(Int_t value)                 { fDetectedEdge = value;                };
        inline Double_t              GetLeadingEdge()                             { return fLeadingEdge;                  };
        inline void                 SetLeadingEdge(Double_t value)                { fLeadingEdge = value;                 };
        inline Double_t              GetTrailingEdge()                            { return fTrailingEdge;                 };
        inline void                 SetTrailingEdge(Double_t value)               { fTrailingEdge = value;                };

        inline void                 UpdateDetectedEdge(Int_t e)                  { fDetectedEdge |= e;                   };

    private:

        Int_t   fDetectedEdge; // Bits: 1=Leading,2=Trailing
        Double_t fLeadingEdge;
        Double_t fTrailingEdge;

        ClassDef(TDCVHit,1);
};
#endif
