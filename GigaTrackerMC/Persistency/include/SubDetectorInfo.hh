// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2008-03-30
//
// --------------------------------------------------------------
#ifndef SubDetectorInfo_h
#define SubDetectorInfo_h 1

#include "TString.h"
#include "TObjArray.h"

class SubDetectorInfo {

public:

  SubDetectorInfo() {};
  SubDetectorInfo(TString name) { fName = name; }
  virtual ~SubDetectorInfo() {}
  void Clear(Option_t* option="");
  void Print(Option_t* option="") const;

public:

  TString              GetName()                                          { return fName;                         };
  void                 SetName(TString value)                             { fName = value;                        };

  TObjArray *          GetGeometryParameters()                            { return &fGeometryParameters;          };
  void                 SetGeometryParameters(TObjArray value)             { fGeometryParameters = value;          };
  TObjArray *          GetMaterialParameters()                            { return &fMaterialParameters;          };
  void                 SetMaterialParameters(TObjArray value)             { fMaterialParameters = value;          };

private:

  TString fName;

  TObjArray fGeometryParameters;
  TObjArray fMaterialParameters;

  ClassDef(SubDetectorInfo,1)
};

#endif
