// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch)
//            Francesca Bucci (Francesca.Bucci@cern.ch) 2008-01-17
//
// Based on a project by Emanuele Leonardi (Emanuele Leonardi@cern.ch) 2007-01-10
//
// --------------------------------------------------------------
#ifndef KinePart_h
#define KinePart_h 1

#include "GenePart.hh"
#include "TVector3.h"
#include "TLorentzVector.h"

class KinePart : public GenePart
{

public:

  KinePart();
  virtual ~KinePart() {Clear();}
  void Clear(Option_t* option="");
  void Print(Option_t* option="") const;

  inline void SetFinalEnergy(Double_t val)     { fFinalEnergy = val; };
  inline void SetFinalMomentum(TVector3 val)   { fFinalMomentum = val; };
  inline void SetID(Int_t val)                 { fID = val; };
  inline void SetParentID(Int_t val)           { fParentID = val; };
  inline void SetParentIndex(Int_t val)        { fParentIndex = val; };
  inline void SetProdPos(TLorentzVector val)   { fProdPos = val; };
  inline void SetEndPos(TLorentzVector val)    { fEndPos = val; };
  inline void SetProdProcessName(TString val)  { fProdProcessName = val; };
  inline void SetProdProcessName(char* val)    { fProdProcessName = TString(val); };
  inline void SetEndProcessName(TString val)   { fEndProcessName = val; };
  inline void SetEndProcessName(char* val)     { fEndProcessName = TString(val); };
  void Set4Momentum(TLorentzVector val)        { SetFinalMomentum(val.Vect());SetFinalEnergy(val.T()); };
  inline void SetDirectParent(bool val)        { fDirectParent = val; };

  inline Double_t GetFinalEnergy()    { return fFinalEnergy; };
  inline TVector3 GetFinalMomentum()  { return fFinalMomentum; };
  inline Int_t GetID()                { return fID; };
  inline Int_t GetParentID()          { return fParentID; };
  inline Int_t GetParentIndex()       { return fParentIndex; };
  inline TLorentzVector GetProdPos()  { return fProdPos; };
  inline TLorentzVector GetEndPos()   { return fEndPos; };
  inline TString GetProdProcessName() { return fProdProcessName; };
  inline TString GetEndProcessName()  { return fEndProcessName; };
  inline TLorentzVector Get4Momentum(){ return TLorentzVector(fFinalMomentum,fFinalEnergy); };
  inline bool GetDirectParent()       { return fDirectParent; };

private:

  Double_t fFinalEnergy;      // MeV
  TVector3 fFinalMomentum;    // MeV
  Int_t    fID;
  Int_t    fParentID;
  Int_t    fParentIndex;
  TLorentzVector fProdPos, fEndPos;
  TString  fProdProcessName,fEndProcessName;
  bool     fDirectParent;

  ClassDef(KinePart,1)
};

#endif
