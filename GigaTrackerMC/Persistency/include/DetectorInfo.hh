// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2008-03-30
//
// --------------------------------------------------------------
#ifndef DetectorInfo_h
#define DetectorInfo_h 1

#include "SubDetectorInfo.hh"
#include <vector>

class DetectorInfo {

public:

  DetectorInfo();
  virtual ~DetectorInfo() { }
  void Clear(Option_t* option ="");
  void Print(Option_t* option="") const;

  SubDetectorInfo* AddSubDetectorInfo(TString name);
  SubDetectorInfo* FindSubDetectorInfo(TString name);

private:

  std::vector<SubDetectorInfo*> fSubDetectorsInfo;

  ClassDef(DetectorInfo,1)
};

#endif
