// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-01-08
//
// --------------------------------------------------------------
#ifndef TVChannelID_H
#define TVChannelID_H
#include "TObject.h"

class TVChannelID {

    public:

        TVChannelID() {fChannelID = -1;}
        TVChannelID(Int_t iCh) {fChannelID = iCh;}
        virtual ~TVChannelID(){};

    public:

        Int_t                GetChannelID()                                    { return fChannelID;                    };
        Int_t                SetChannelID(Int_t value)                         { return fChannelID = value;            };

    private:

        Int_t fChannelID;

        ClassDef(TVChannelID,1);
};
#endif
