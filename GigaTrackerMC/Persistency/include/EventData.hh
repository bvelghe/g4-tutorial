// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch)
//            Francesca Bucci (Francesca.Bucci@cern.ch) 2008-01-17
//
// Based on a project by Emanuele Leonardi (Emanuele.Leonardi@cern.ch) 2007-01-10
//
// --------------------------------------------------------------
#ifndef EventData_h
#define EventData_h 1

#include "TROOT.h"

class EventData
{

public:

  EventData();
  virtual ~EventData() {Clear();};
  void Clear(Option_t *option="");
  void Print(Option_t *option="") const;

  void SetNTrajTot(Int_t i) { fNTrajTot = i; }
  Int_t GetNTrajTot()       { return fNTrajTot; }

private:

  Int_t fNTrajTot; // Total number of trajectories in event

  ClassDef(EventData,1)
};

#endif
