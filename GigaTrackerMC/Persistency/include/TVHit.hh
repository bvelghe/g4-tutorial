// --------------------------------------------------------------
// History:
//
// Created by Antonino Sergi (Antonino.Sergi@cern.ch) 2009-01-08
//
// --------------------------------------------------------------
#ifndef TVHit_H
#define TVHit_H

#include "TObject.h"
#include "TVChannelID.hh"

class TVHit : public TObject, public TVChannelID {

    public:

        TVHit();
        TVHit(Int_t);
        virtual ~TVHit(){};

    public:

        Int_t                GetMCTrackID()                                     { return fMCTrackID;                    };
        void                 SetMCTrackID(Int_t value)                          { fMCTrackID = value;                   };

    private:

        Int_t      fMCTrackID; // For MCTruth Association

        ClassDef(TVHit,1);
};
#endif
