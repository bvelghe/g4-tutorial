#include "G4RunManager.hh"

#include "PrimaryGeneratorAction.hh"
#include "G4PhysListFactory.hh"
#include "DetectorConstruction.hh"
#include "UserEventAction.hh"
#include "UserRunAction.hh"
#include "UserTrackingAction.hh"

#include "G4UIExecutive.hh"
#include "G4VisExecutive.hh"
#include "G4UImanager.hh"



int main(int argc, char * argv[]) {
	G4RunManager* runManager = new G4RunManager();

  ////////////////////
  // Initialisation //
  ////////////////////

	DetectorConstruction * detector = new DetectorConstruction();
  runManager->SetUserInitialization(detector);	

 	G4PhysListFactory factory;                                     
  runManager->SetUserInitialization(factory.GetReferencePhysList("QGSP_BERT_EMV"));

	G4VUserPrimaryGeneratorAction* gen_action = new PrimaryGeneratorAction();
  runManager->SetUserAction(gen_action);

	UserRunAction * run_action = new UserRunAction();
 	runManager->SetUserAction(run_action);

	UserEventAction * event_action = new UserEventAction(run_action);
  runManager->SetUserAction(event_action);

  UserTrackingAction * tracking_action = new UserTrackingAction();
  runManager->SetUserAction(tracking_action);

	//////////////////////////
	// Start the event loop //
	//////////////////////////
	
  runManager->Initialize();

  //////////////////////////////////////
  // User interface and visualization // 	
	//////////////////////////////////////
	
	
  G4VisManager* visManager = new G4VisExecutive;
  visManager->Initialize();

 	// Get the pointer to the User Interface manager

	G4UImanager * UImanager = G4UImanager::GetUIpointer(); 


 	if (argc!=1) {  // batch mode  
    G4String command = "/control/execute ";
    G4String fileName = argv[1];
  	UImanager->ApplyCommand(command+fileName);
  } else { // interactive mode : define UI session  
 		G4UIExecutive * ui = new G4UIExecutive(argc,argv);
		ui->SessionStart();
		delete ui;
	}
  
  ////////////////////
  // Clean and exit //
  ////////////////////

  delete visManager;
	delete runManager;
	return 0;
}
