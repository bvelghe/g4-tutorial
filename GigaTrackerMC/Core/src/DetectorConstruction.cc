#include "DetectorConstruction.hh"

//FIXME
#include "GigaTrackerKovarConnector.hh"
#include "GigaTrackerCarbonFrame.hh"

DetectorConstruction::DetectorConstruction() {
	/////////////////////
	// Build materials //
	/////////////////////
    
  fNistMan = G4NistManager::Instance();
  fNistMan->FindOrBuildMaterial("G4_Galactic");
  fNistMan->FindOrBuildMaterial("G4_Si");
  fNistMan->FindOrBuildMaterial("G4_Fe");
  fNistMan->FindOrBuildMaterial("G4_C");
}
DetectorConstruction::~DetectorConstruction() {
	Destroy();
}
  
G4VPhysicalVolume* DetectorConstruction::Construct() {
    ///////////
    // World //
    ///////////
    fworldSolidVolume = new G4Box("World",2.0*CLHEP::m,2.0*CLHEP::m,12.0*CLHEP::m);
    
    fworldLogicalVolume = new G4LogicalVolume(fworldSolidVolume,
                                G4Material::GetMaterial("G4_Galactic"),
                                "World",
                                0,
                                0,
                                0);
                             
    fworldPhysicalVolume = new G4PVPlacement(0,
                                G4ThreeVector(0.,0.,0.),
                                fworldLogicalVolume,
                                "World",
                                0,
                                false
                                ,0);  

    
    //fCollimator = new Collimator(G4Material::GetMaterial("G4_Fe"), fworldLogicalVolume, G4ThreeVector(0.,0.,-1.0*m), 0);
    
    //FIXME Check Construct()
    fGigaTracker = new GigaTrackerDetector(0,0);
    fGigaTracker->SetMaterial(G4Material::GetMaterial("G4_Galactic"));
    fGigaTracker->SetMotherVolume(fworldLogicalVolume);
    fGigaTracker->CreateGeometry();

    //fTracksCamera = new TracksCamera(fworldLogicalVolume,30.0*cm,G4ThreeVector(0.,0.,0.),0);
  	//fTracksCamera->Construct();
  	//fTracksCamera->SetProperties();


    SetProperties();
    return fworldPhysicalVolume;                        
}

void DetectorConstruction::Destroy() {
	delete fworldSolidVolume;
	delete fworldLogicalVolume;
	delete fworldPhysicalVolume;
}

void DetectorConstruction::SetProperties() {	
	 //World volume is invisible!
	fworldLogicalVolume->SetVisAttributes(G4VisAttributes::Invisible);
}
