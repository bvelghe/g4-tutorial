#include "UserTrackingAction.hh"
#include <cmath>

#include "G4TrackingManager.hh"
#include "UserTrajectory.hh"

UserTrackingAction::UserTrackingAction() {}
UserTrackingAction::~UserTrackingAction() {}


void UserTrackingAction::PreUserTrackingAction (const G4Track * fTrack) {  
  // Start a new trajectory 
  fpTrackingManager->SetStoreTrajectory(true);
  UserTrajectory * UTrajectory = new UserTrajectory(fTrack);
  fpTrackingManager->SetTrajectory(UTrajectory);

}

void UserTrackingAction::PostUserTrackingAction (const G4Track * fTrack) {}


