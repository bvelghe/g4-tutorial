#include "UserRunAction.hh"
#include <ctime>



/////////////
// Helpers //
/////////////

uint64_t UserRunAction::UID_encode(uint32_t runID, uint32_t eventID) {
	uint64_t UID = 0x0 ^ eventID;
	return (UID << 32) ^ runID;
}

void UserRunAction::UID_decode(uint64_t UID, uint32_t & runID, uint32_t & eventID) {
	eventID = UID >> 32;
	runID = UID;
}

UserRunAction::UserRunAction() {
    fRunManager = G4RunManager::GetRunManager();
    //(G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction()

    //SQL Buffer
    //f_sqlTracks = new SQLBuffer(1000);
		//f_sqlEvents = new SQLBuffer(1000);    

		/////////////
 		// ROOT IO //
		/////////////
		fIOReady = false;
		fOutputFile = "gtk_output.root"; //Default output file name
		fSetID = 0;

		//CPCV from NA62MC
		// Create tree to hold all events in this run
		fEventTree = new TTree("Generated","Generated events tree");
		fNA62MCEvent = new Event();
    fEventBranch = fEventTree->Branch("event", &fNA62MCEvent, 64000);


		
		// GTK ROOT ID
		
		fGTKIO = new GigaTrackerRootIO();

}

UserRunAction::~UserRunAction() {
    //delete f_sqlTracks;
    //delete f_sqlEvents;
    
		////////////
		// NA62MC //
		////////////

		if(fEventTree) {
			fEventTree->Write();
			delete fEventTree;
		}
		if(fNA62MCEvent) {
			delete fNA62MCEvent;
		}


    /////////////
    // ROOT IO //
    /////////////
    if(fIOReady) {
    	fTree->Write();
      fOutFile->Close();
  		//delete fTree; Don't do that crazy fool !
  		delete fEvent;
  		delete fOutFile;
  	}
}

/////////////////////
// ROOT IO Helpers //
/////////////////////

void UserRunAction::OutputFile(G4String fname) {
	if(fIOReady) {
		//Save and close
		fIOReady = false;
		fTree->Write();
		fOutFile->Close();
	}
	fOutputFile = fname;
}

G4String UserRunAction::OutputFile() const {
	return fOutputFile;
}

void UserRunAction::SetID(G4int setID) {
	fSetID = setID;
}

G4int UserRunAction::SetID() const {
	return fSetID;
}


// Remove me
bool UserRunAction::Clear() {
	if(!fIOReady) return false;
  TClonesArray * tarray = fEvent->TracksArray();
  tarray->Clear();
}

bool UserRunAction::Fill() {
	if(!fIOReady) return false;
	fTree->Fill();
	return true;
}


bool UserRunAction::StoreEvent(G4int eventID, G4int nTracks) {
  if(!fIOReady) return false;
  fTrackIndex = 0; // Cross your fingers ....
	fEvent->SetID(fSetID);
	fEvent->RunID(fRunID);
	fEvent->EventID(eventID);
	fEvent->NTracks(nTracks);
	//uint64_t UID = UID_encode(fRunID,eventID);
	//std::ostringstream oss;
  //G4String query = "INSERT INTO `gtk3inelastic`.`events` (`UID`, `gtkEdepA`, `gtkEdepB`,`nTracks`) VALUES ";  
  //
  //oss << "('" << UID << "','" << gtkEdepA << "','" << gtkEdepB << "','" << nTracks << "');";
  //query += oss.str();
  //return f_sqlEvents->request(query);
	return true;
}

bool UserRunAction::StoreTrack(G4int eventID, G4int trackID, G4int parentID, G4int particleID, G4ThreeVector momentum, G4ThreeVector vertex, G4double kineticEnergy, G4double totalEnergy, G4double exitAngle, G4double charge) {
  if(!fIOReady) return false;
  TClonesArray * tarray = fEvent->TracksArray();
  TTrack * trk = new((*tarray)[fTrackIndex]) TTrack(); // Cross your fingers ....
  fTrackIndex++;
	//ROOT IO
	TVector3 mom, vtx;
 	mom.SetX(momentum.x()); 
 	mom.SetY(momentum.y());
 	mom.SetZ(momentum.z()); 
 	
	vtx.SetX(vertex.x()); 
 	vtx.SetY(vertex.y());
 	vtx.SetZ(vertex.z()); 
	
	trk->TrackID(trackID);
  trk->ParentID(parentID);
  trk->ParticleID(particleID);
	trk->KineticEnergy(kineticEnergy);
	trk->TotalEnergy(totalEnergy);
  trk->Momentum(mom);
  trk->Vertex(vtx);
  trk->ExitAngle(exitAngle);
  trk->Charge(charge);

   
  //uint64_t UID = UID_encode(fRunID,eventID);
	//std::ostringstream oss;
  //G4String query = "INSERT INTO `gtk3inelastic`.`tracks` (`UID`, `trackID`, `parentID`,`particleID`, `initialMomentum`,`exitAngle`,`charge`,`chantiHit`,`strawHit`,`richHit`) VALUES ";  
  
  //oss << "('" << UID << "','" << trackID << "','" << parentID << "','" << particleID <<  "','" << initialMomentum << "','" << exitAngle <<  "','"  << charge << "','" << chantiHit <<  "','" << strawHit << "','" << richHit << "');";
  //query += oss.str();
  //return f_sqlTracks->request(query);
  return true;
}

GigaTrackerRootIO * UserRunAction::GetGTKIO() {
  return fGTKIO;
}

void UserRunAction::BeginOfRunAction(const G4Run* aRun) {
  //fRunManager->GeometryHasBeenModified(); //<<<---  
  
  fRunID = aRun->GetRunID();
  G4cout << "### Run " << fRunID << " start." << G4endl;
  //G4RunManager::GetRunManager()->SetRandomNumberStore(true);
		/////////////
 		// ROOT IO //
		/////////////
		
		if(!fIOReady) {
  		//Output ROOT File
			fOutFile = new TFile(fOutputFile.c_str(),"RECREATE");
			if (fOutFile->IsZombie()) {
				G4cerr << "! Unable to (re)create output ROOT file " << fOutputFile << G4endl;
				delete fOutFile;
			} else {
				fIOReady = true;
  			//Init. containers
  			fEvent = new TEvent(); 
  			//Output TTree
				fTree = new TTree("gtk","GTK Inelastics");
				//fTree->SetMaxTreeSize(600000000); //~600MB
				fTree->Branch("TEvent",&fEvent);
				
				  //GTK Root IO
			  fGTKIO->NewRun(fRunID,fOutFile,0);

			}
		}

}
// G4 Scorer
G4Run* UserRunAction::GenerateRun()
{ 
  return new GridCamRun; 
}

void UserRunAction::EndOfRunAction(const G4Run* aRun) {
  G4cout << "### Run " << fRunID << " end." << G4endl;
  
  
  // MySQL output
  
  //f_sqlTracks->flush();
	//f_sqlEvents->flush();

  //GTK Root IO
  fGTKIO->EndRun();


  // G4 Scorer
  const GridCamRun * GCRun = (const GridCamRun*)aRun;
  GCRun->Print(); 

}
