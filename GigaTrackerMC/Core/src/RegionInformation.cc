//Implementation based on geant4.9.5/examples/extended/runAndEvent/RE01/src/RE01RegionInformation.cc

#include "RegionInformation.hh"
#include "G4ios.hh"

RegionInformation::RegionInformation()
:isGTK3(false),isCHANTI(0),isWorld(false)
{;}

RegionInformation::~RegionInformation()
{;}

G4String RegionInformation::GetName() const {
 if(isGTK3) { return "GTK_3"; }
 else if(isCHANTI) { 
    std::ostringstream oss;
    oss << isCHANTI;
    G4String station_ID = oss.str(); 
   return "CHANTI_"+station_ID; 
 } else if (isWorld) {
   return "World";
  } else { return "Unknown"; }
}

int RegionInformation::GetID() const {
   if(isGTK3) { return 20; }
   else if(isCHANTI) { 
        return 30+isCHANTI; 
   } else if (isWorld) {
        return 10;
  } else { return 0; } 
}

void RegionInformation::Print() const
{
 G4cout << "Region: ";
 G4cout << GetName() << G4endl;
}
