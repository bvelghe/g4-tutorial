#include "UserEventAction.hh"


//FIXME : iCopy ?

UserEventAction::UserEventAction(UserRunAction *runAction) {
  fTracksCamera_HitsCollectionID = -1;
  fTracksCamera_HitsCollection = 0;

  fSDman = G4SDManager::GetSDMpointer();
  fRunAction = runAction;
}
UserEventAction::~UserEventAction() {;}

void UserEventAction::BeginOfEventAction(const G4Event* event) {
  
  /////////////////////////////
  // Display run progression //
  /////////////////////////////	
  G4int eventID = event->GetEventID();
	if(eventID%1000 == 0) {
  	G4cout << "# >>> Event " << event->GetEventID() << G4endl;
  }
  if(fTracksCamera_HitsCollectionID < 0) { 
    fTracksCamera_HitsCollectionID = fSDman->GetCollectionID("TracksCamera_0_HitsCollection");
  }
  

  // ROOT IO
  fRunAction->Clear();
}


void UserEventAction::EndOfEventAction(const G4Event* event) {
		
		G4int NbHits = 0;
	  G4bool VetoDecay = false;
		
		
		
		// Tracks Camera, very basic preselection
	  if(fTracksCamera_HitsCollectionID >= 0) {
 		  G4HCofThisEvent * HCTE = event->GetHCofThisEvent();
		  if(HCTE) {
     	  fTracksCamera_HitsCollection = (TracksCameraHitsCollection*)(HCTE->GetHC(fTracksCamera_HitsCollectionID));
     		NbHits = fTracksCamera_HitsCollection->entries();	
     		for (G4int i = 0; i < NbHits; i++) {
		   	    TracksCameraHit *hit = (*fTracksCamera_HitsCollection)[i];
		   	    
		   	    // Beam Img - Selection output on the console
		   	   // if(hit->TrackID() != 1 && hit->TrackCreatorProcess() == 0) {
						if(hit->TrackID() == 1 && hit->TrackCreatorProcess() == 0) { // Elastic only
		   	      //G4cout << hit->TrackPosition().x() << " " << hit->TrackPosition().y() << " " << hit->TrackPosition().z() << G4endl;
		   	    	G4cout << hit->TrackPosition().z() << " "  << hit->TrackMomentum().theta()/CLHEP::rad << G4endl;
						}
		   	    
		   	    
		   	    if(hit->TrackCreatorProcess() == 1) { 
		   	       VetoDecay = true; //Wrong, if one decay, supress evtg.
							 continue;
		   	    }
     		  }
         
          
		  }
	  }
		
		/*
	  //if(NbHits > 1 && VetoDecay == false) {
		// Look at particles trajectories
		G4TrajectoryContainer* trajectoryContainer = event->GetTrajectoryContainer();
  	G4int n_trajectories = 0;
  	if(trajectoryContainer) {
			n_trajectories = trajectoryContainer->entries();
			// General Event info
    	fRunAction->StoreEvent(event->GetEventID(), n_trajectories);
			for(G4int i = 0; i < n_trajectories; i++) {
  			UserTrajectory * trj = (UserTrajectory*)((*trajectoryContainer)[i]);
   	
   			//G4cout << "** " << trj->GetTrackID() << " **" << G4endl;
   			//G4cout << "parentID: " << trj->GetParentID() << G4endl;
   			//G4cout << "initialMomentum (MeV): " << trj->GetInitialMomentum().mag()/MeV << G4endl;
   			//G4cout << "exitAngle (rad.): " << trj->GetInitialMomentum().theta()/rad << G4endl;
   			//G4cout << "particleID: " << trj->GetPDGEncoding() << G4endl;
   			//G4cout << "charge: " << trj->GetCharge() << G4endl;
        
  
        
       // if(trj->GetParentID() > 0 && trj->GetProcessType() != fDecay ) { //Store only inel
       	if(trj->GetParentID() == 0 && trj->GetProcessType() != fDecay ) { //Store only inel
   			  //fRunAction->StoreTrack(event->GetEventID(),trj->GetTrackID(),trj->GetParentID(),trj->GetPDGEncoding(),trj->GetInitialMomentum()/MeV,trj->GetVertexPosition()/cm,trj->GetInitialKineticEnergy()/MeV, trj->GetInitialTotalEnergy()/MeV, trj->GetInitialMomentum().theta()/rad,trj->GetCharge());    	    
   			  fRunAction->StoreTrack(event->GetEventID(),trj->GetTrackID(),trj->GetParentID(),trj->GetPDGEncoding(),trj->GetInitialMomentum()/MeV,trj->GetVertexPosition()/cm,trj->GetInitialKineticEnergy()/MeV, trj->GetInitialTotalEnergy()/MeV, trj->GetFinalMomentum().theta()/rad,trj->GetCharge());    	    
   			  }
	    }

		
	  
  }	  

	*/
				//FIXME FIXME FIXME
	    fRunAction->GetGTKIO()->SaveEvent(event);
	  
	// ROOT IO
			fRunAction->Fill();		
		

}

