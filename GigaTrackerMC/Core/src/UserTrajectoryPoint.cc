//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// ---------------------------------------------------------------
//
// UserTrajectoryPoint.cc
//
// ---------------------------------------------------------------

#include "UserTrajectoryPoint.hh"

#include "G4AttDefStore.hh"
#include "G4AttDef.hh"
#include "G4AttValue.hh"
#include "G4UnitsTable.hh"

//#define G4ATTDEBUG
#ifdef G4ATTDEBUG
#include "G4AttCheck.hh"
#endif

G4Allocator<UserTrajectoryPoint> aTrajectoryPointAllocator;

UserTrajectoryPoint::UserTrajectoryPoint()
{
  fPosition = G4ThreeVector(0.,0.,0.);
  fMomentum = G4ThreeVector(0.,0.,0.);
}

UserTrajectoryPoint::UserTrajectoryPoint(G4ThreeVector pos, G4ThreeVector mom)
{
  fPosition = pos;
  fMomentum = mom;
}

UserTrajectoryPoint::UserTrajectoryPoint(const UserTrajectoryPoint &right)
 : G4VTrajectoryPoint(),fPosition(right.fPosition),fMomentum(right.fMomentum)
{
}

UserTrajectoryPoint::~UserTrajectoryPoint()
{
}

const std::map<G4String,G4AttDef>* UserTrajectoryPoint::GetAttDefs() const
{
  G4bool isNew;
  std::map<G4String,G4AttDef>* store
    = G4AttDefStore::GetInstance("UserTrajectoryPoint",isNew);
  if (isNew) {
    G4String Pos("Pos");
    (*store)[Pos] =
      G4AttDef(Pos, "Position", "Physics","G4BestUnit","G4ThreeVector");
    G4String Mom("Mom");
    (*store)[Mom] =
      G4AttDef(Mom,"Momentum","Physics","G4BestUnit","G4ThreeVector");
   }
  return store;
}

std::vector<G4AttValue>* UserTrajectoryPoint::CreateAttValues() const
{
  std::vector<G4AttValue>* values = new std::vector<G4AttValue>;
  values->push_back(G4AttValue("Pos",G4BestUnit(fPosition,"Length"),""));
  values->push_back(G4AttValue("Mom",G4BestUnit(fMomentum,"Momemtum"),""));
#ifdef G4ATTDEBUG
  G4cout << G4AttCheck(values,GetAttDefs());
#endif

  return values;
}
