#include "PrimaryGeneratorAction.hh"


//////////////////////////////
//	Minimal Beam Simulation	//
//////////////////////////////

PrimaryGeneratorAction::PrimaryGeneratorAction() {
  G4int n_particle = 1;
	fParticleGun = new G4ParticleGun(n_particle);
	
	//Random engine
	fRandGauss = new CLHEP::RandGauss(CLHEP::HepRandom::getTheEngine());

  G4ParticleTable * particleTable = G4ParticleTable::GetParticleTable();
	
  /////////////////////
	// Beam Parameters //
  /////////////////////

	//fBeamProfileX = 0.5*27.5; //mm
	//fBeamProfileY = 0.5*11.4; //mm
	//fBeamDivergenceX = 0.0; //radian
	//fBeamDivergenceY = 0.0; //radian
	
  G4ParticleDefinition * beam_particle = particleTable->FindParticle("kaon+");
	//G4ParticleDefinition * beam_particle = particleTable->FindParticle("proton");
	fParticleGun->SetParticleDefinition(beam_particle);
	
	fTurtle = true;
	G4cout << "Beam mode: ";
	if(fTurtle) G4cout << "TURTLE (" << fTurtleCard << ")" << G4endl; //FIXME fTurtleCard
	else G4cout << "Gaussian" << G4endl;
	//FIXME Clean me up
	RandomGenerator::GetInstance()->Init(42);
	
	beam = new FastBeam("k12hika+_gtk1_kp"); //FIXME
}

PrimaryGeneratorAction::~PrimaryGeneratorAction() {
   delete fParticleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent) {
	
	// Final beam parameter
	G4double xpos = 0.0;
	G4double ypos = 0.0;
	G4double zpos = 0.0;

	G4double xdir = 0.0;
	G4double ydir = 0.0;
	G4double zdir = 0.0;

	G4double energy = 0.0;
	
	// Call Turtle
	if(fTurtle == true) {

  	G4int Npart = 0; // Not implemented
		beam->SetN(Npart);
		beam->Generate();

		
		// Dummy loop
		for(int j=0; j < beam->GetN(); j++) {
			xpos = beam->GetX(j)*CLHEP::m; 
			ypos = beam->GetY(j)*CLHEP::m;
			zpos = -11500.0*CLHEP::mm;
			
			xdir = beam->GetPx(j)*CLHEP::GeV;
			ydir = beam->GetPy(j)*CLHEP::GeV;
			zdir = beam->GetPz(j)*CLHEP::GeV;
			break;
		}
	} else {
	
		//Beam divergence
		G4double x_kick = atan(fRandGauss->fire(0,fBeamDivergenceX)); //m
		G4double y_kick = atan(fRandGauss->fire(0,fBeamDivergenceY)); //m
	
		//Beam spatial extension (Gaussian)
		G4double x_offset = fRandGauss->fire(0.0,fBeamProfileX); //mm
		G4double y_offset = fRandGauss->fire(0.0,fBeamProfileY); //mm
		
		xpos = x_offset*CLHEP::mm;
		ypos = y_offset*CLHEP::mm;
		zpos = -11500.0*CLHEP::mm;

		xdir = x_kick*CLHEP::m;
		ydir = y_kick*CLHEP::m;
		zdir = +1.0*CLHEP::m;

  	//Beam energy (Gaussian)
		G4double E_base = 75.0*CLHEP::GeV;
  	G4double E_offset = fRandGauss->fire(0.0,0.75*CLHEP::GeV); 
		energy = E_base + E_offset;

	}
	//FIXME rename xdir xmom
  G4double pK = sqrt(xdir*xdir+ydir*ydir+zdir*zdir);
  G4double mk = 493.677*CLHEP::MeV;
	energy =  sqrt(pK*pK + mk*mk);
	//std::cout << xdir/zdir << " " << ydir/zdir << std::endl;
	fParticleGun->SetParticleMomentumDirection(G4ThreeVector(xdir,ydir,zdir)); 
	fParticleGun->SetParticlePosition(G4ThreeVector(xpos,ypos,zpos));
	fParticleGun->SetParticleEnergy(energy);	
  fParticleGun->GeneratePrimaryVertex(anEvent);
}
