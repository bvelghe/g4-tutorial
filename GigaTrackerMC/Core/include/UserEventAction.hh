#ifndef UserEventAction_h
#define UserEventAction_h 1

#include "G4UserEventAction.hh"
#include "TracksCameraHit.hh"
#include "G4Event.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4UnitsTable.hh"
#include "UserTrajectory.hh"
#include "UserRunAction.hh"
#include <map>

class UserEventAction : public G4UserEventAction
{
	public:
		UserEventAction(UserRunAction *runAction);
    ~UserEventAction();
    void BeginOfEventAction(const G4Event*);
    void EndOfEventAction(const G4Event*);

          
	private:

		G4int fTracksCamera_HitsCollectionID;
    TracksCameraHitsCollection * fTracksCamera_HitsCollection;
    
    G4SDManager * fSDman;
    UserRunAction * fRunAction;
};

#endif // UserEventAction_h
