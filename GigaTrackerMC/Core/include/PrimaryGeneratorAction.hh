#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include <CLHEP/Random/RandGaussQ.h>
#include "G4RunManager.hh"
#include "G4Run.hh"

#include "beam.hh"
#include "RandomGenerator.hh"


class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction {
  public:
    PrimaryGeneratorAction();
    virtual ~PrimaryGeneratorAction();
    
    //////////////
    // Required //
    //////////////
    void GeneratePrimaries(G4Event* anEvent);
  private:
  	G4ParticleGun* fParticleGun;
  	CLHEP::RandGauss* fRandGauss;

  	G4double fBeamProfileX; //mm
		G4double fBeamProfileY; //mm
		G4double fBeamDivergenceX; //radian
		G4double fBeamDivergenceY; //radian
		G4String fTurtleCard;
		G4bool fTurtle;
			FastBeam * beam; //FIXME
};

#endif // PrimaryGeneratorAction_h 
