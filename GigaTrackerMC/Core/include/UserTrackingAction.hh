#ifndef UserTrackingAction_h
#define UserTrackingAction_h 1

#include "G4UserTrackingAction.hh"
#include "G4Track.hh"
#include "G4TrackingManager.hh"
#include "G4TrackVector.hh"

class UserTrackingAction : public G4UserTrackingAction
{
	public:
           UserTrackingAction();
           ~UserTrackingAction();
           void PreUserTrackingAction (const G4Track *);
           void PostUserTrackingAction (const G4Track *);

	private:
};

#endif // UserTrackingAction_h
