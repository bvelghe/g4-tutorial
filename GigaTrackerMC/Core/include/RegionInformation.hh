// Implementation based on geant4.9.5/examples/extended/runAndEvent/RE01/include/RE01RegionInformation.hh

#ifndef RegionInformation_H
#define RegionInformation_H 1

#include "globals.hh"
#include "G4VUserRegionInformation.hh"

class RegionInformation : public G4VUserRegionInformation
{
  public:
    RegionInformation(); 
    ~RegionInformation();
    void Print() const; //Pure virtual
    G4String GetName() const;
    G4int GetID() const;

  private:
    G4bool isGTK3;
    G4bool isWorld;
    G4int isCHANTI; // 0 -> False, {1-6} -> True 
    G4bool isSTRAW;

  public:
    inline void SetGTK3(G4bool v=true) {isGTK3 = v;}
    inline void SetCHANTI(G4int v=true) {isCHANTI = v;}
    inline void SetWorld(G4bool v=true) {isWorld = v;}
    inline void SetSTRAW(G4bool v=true) {isSTRAW = v;}
    inline G4bool IsGTK3() const {return isGTK3;}
    inline G4int IsCHANTI() const {return isCHANTI;}
    inline G4int IsWorld() const {return isWorld;}
    inline G4bool IsSTRAW() const {return isSTRAW; }
};

#endif

