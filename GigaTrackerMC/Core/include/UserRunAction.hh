#ifndef UserRunAction_h
#define UserRunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include "G4Run.hh"
#include "G4ThreeVector.hh"
#include "DetectorConstruction.hh"
#include "G4RunManager.hh"
//#include "SimpleBuffer.hh" // Very basic SQL Buffer
#include <fstream>
#include <CLHEP/Random/RandExponential.h>
#include <CLHEP/Random/RandGaussQ.h>
#include <stdint.h> // Future: <cstdint>
// ROOT IO
#include "TEvent.h"
#include "TTrack.h"
#include "TFile.h"
#include "TTree.h"

#include "GigaTrackerRootIO.hh"
#include "Event.hh"

// G4 Scorer
#include "GridCamRun.hh"

class UserRunAction : public G4UserRunAction
{
  public:
    UserRunAction();
    ~UserRunAction();
    void BeginOfRunAction(const G4Run*);
    void EndOfRunAction(const G4Run*);
   
    bool StoreEvent(G4int eventID, G4int nTracks);
    bool StoreTrack(G4int eventID, G4int trackID, G4int parentID, G4int particleID, G4ThreeVector momentum, G4ThreeVector vertex, G4double kineticEnergy, G4double totalEnergy, G4double exitAngle, G4double charge);

    // G4 Scorer
    G4Run * GenerateRun();

	  // Helpers
    uint64_t UID_encode(uint32_t runID, uint32_t eventID);
    void UID_decode(uint64_t UID, uint32_t & runID, uint32_t & eventID);

		// ROOT IO Helpers
		void OutputFile(G4String fname);
		G4String OutputFile() const;
		
		void SetID(G4int setID);
		G4int SetID() const;

		bool Fill();
		bool Clear();

    GigaTrackerRootIO * GetGTKIO();

	private:
    G4int fRunID;
    G4RunManager * fRunManager;
		G4int fTrackIndex;
				
		//////////////////
		// MySQL Output //
		//////////////////
		
    //SQLBuffer * f_sqlTracks;
    //SQLBuffer * f_sqlEvents;

		/////////////
		// ROOT IO //
		/////////////
		bool fIOReady;
		TFile * fOutFile;
		TTree * fTree; 
		G4String fOutputFile;
		G4int fSetID;
		TEvent * fEvent;
		
		// GTK ROOT ID
		GigaTrackerRootIO * fGTKIO;
		TTree * fEventTree;
		Event * fNA62MCEvent;
		TBranch * fEventBranch;

		

};


#endif //UserRunAction_h
