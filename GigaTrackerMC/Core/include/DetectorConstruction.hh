#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"

#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"

#include "G4ThreeVector.hh"

#include "GigaTrackerDetector.hh"
#include "Collimator.hh"
#include "TracksCamera.hh"


class DetectorConstruction : public G4VUserDetectorConstruction 
{
	public:
    DetectorConstruction();
    virtual ~DetectorConstruction();
    void SetProperties();
    void Destroy();
		//////////////
    // Required //
    //////////////
    G4VPhysicalVolume* Construct();
	private:
  	G4NistManager * fNistMan;

		// World

    G4Box * fworldSolidVolume;
    G4LogicalVolume * fworldLogicalVolume;
    G4VPhysicalVolume * fworldPhysicalVolume;

		// Volumes
		Collimator * fCollimator;
    GigaTrackerDetector  * fGigaTracker;
    TracksCamera * fTracksCamera;
};

#endif // DetectorConstruction_h
