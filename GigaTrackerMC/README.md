Stand-alone GigaTracker Geant4 simulation
=========================================
Purpose: 
 * Detector geometry developement
 * Digi developement (FUTURE)

Dependencies
============
 * Geant4 (4.9.6)

Easy Setup
==========
Directory structure:
 * GEANT4_INST_DIR 
 * MC_BUILD_DIR
 * MC_CODE_DIR

Set-up the environnement variables:
 * source $GEANT4_INST_DIR/bin/geant4.sh

Complie the Monte-Carlo simulation: 
 * cd $MC_BUILD_DIR
 * cmake $MC_CODE_DIR/
 * make

Useful G4 vis commands
======================
 * /vis/open OGLIQt
 * /vis/drawVolume
 * /vis/scene/add/trajectories
 * /vis/viewer/set/style surface
 * /vis/scene/endOfEvnetAction accumulate
