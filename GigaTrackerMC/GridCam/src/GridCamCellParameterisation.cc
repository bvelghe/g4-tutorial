//
// --------------------------------------------------------------
// History:
//
// Created by Bob Velghe (bob.velghe@cern.ch) March 17, 2014
// --------------------------------------------------------------
//

#include "GridCamCellParameterisation.hh"


GridCamCellParameterisation::GridCamCellParameterisation(const G4double xLength,const G4double yLength,const G4double zLength, const G4int xDivs, const G4int yDivs) 
{
  fXLength = xLength;
  fYLength = yLength;
  fZLength = zLength;

  fXDivs = xDivs;
  fYDivs = yDivs;
}

void GridCamCellParameterisation::ComputeTransformation(G4int copyNo, G4VPhysicalVolume* physVol) const 
{
  G4double Xpos = (copyNo%fXDivs)*(fXLength/fXDivs)-0.5*(fXLength-fXLength/fXDivs); 
  G4double Ypos = (copyNo/fXDivs)*(fYLength/fYDivs)-0.5*(fYLength-fYLength/fYDivs);
  G4double Zpos = 0;   
  physVol->SetTranslation(G4ThreeVector(Xpos,Ypos,Zpos));
  physVol->SetRotation(0);
}

void GridCamCellParameterisation::ComputeDimensions (G4Box & cell, const G4int copyNo, const G4VPhysicalVolume * physVol) const 
{
  // INOP
}
