//
// Bob Velghe <bob.velghe@cern.ch> - March 17, 2014
//

//FIXME THIS SHOULD BE MOVED AND RENAMED UserRun OR SOMETHING

#include "GridCamRun.hh"

GridCamRun::GridCamRun() {
    fSDman = G4SDManager::GetSDMpointer();

    //FIXME Bug bug bug 
    // if always true
    // factor out
    //////////////////////
    // GigaTracker Hits //
    //////////////////////
    fGigaTracker_CollectionID = -1;
    if(fGigaTracker_CollectionID < 0) {
      fGigaTracker_CollectionID = fSDman->GetCollectionID("GigaTrackerCollection");
    }

    ///////////////
    // G4 Scorer //
    ///////////////
    fGridCam_CollectionID  = -1;

    if(fGridCam_CollectionID < 0) { 
      fGridCam_CollectionID = fSDman->GetCollectionID("GridCam/CellSurfCurr"); 
    }
    
}

GridCamRun::~GridCamRun() {
	//INOP
}

void GridCamRun::Print() const {
  std::map<G4int,G4double>::const_iterator itr = fTmp.begin();
  for(; itr != fTmp.end(); itr++) {
      G4int key = (itr->first);
      G4double val = (itr->second);
      G4cout << key << " " << val << G4endl;
  }
}

void GridCamRun::RecordEvent(const G4Event* aEvent) {

  //////////////////////
  // GigaTracker Hits //
  //////////////////////
  if(fGigaTracker_CollectionID >= 0) {
    G4HCofThisEvent * HCTE = aEvent->GetHCofThisEvent(); //Move out
		if(HCTE) {
		  GigaTrackerHitsCollection* GigaTrackerC = (GigaTrackerHitsCollection*)(HCTE->GetHC(fGigaTracker_CollectionID));
    	G4int n_hit = GigaTrackerC->entries();

		  GigaTrackerHit * Hit;
	    for(G4int i=0;i<n_hit;i++) {
	      // Station / Pixel sum
	      Hit = (*GigaTrackerC)[i];
		    //TGigaTrackerHit * PHit = new TGigaTrackerHit();
		    //PHit->SetTime(Hit->GetTime());
		    //PHit->SetPosition(Hit->GetPosition());
		    //PHit->SetEnergy(Hit->GetEnergy());
		    //PHit->SetMCTrackID(Hit->GetTrackID());
		    //PHit->SetStationID(Hit->GetStationID());
		    //PHit->SetPixelID(Hit->GetPixelID());
		  }
		}  
  }

  ///////////////
  // G4 Scorer //
  ///////////////

  if(fGridCam_CollectionID >= 0) {
 	  G4HCofThisEvent * HCTE = aEvent->GetHCofThisEvent();
		if(HCTE) {
      G4THitsMap<G4double> * evtMap = (G4THitsMap<G4double>*)(HCTE->GetHC(fGridCam_CollectionID));
      std::map<G4int,G4double*>::iterator itr = evtMap->GetMap()->begin();
      for(; itr != evtMap->GetMap()->end(); itr++)
      {
        G4int key = (itr->first);
        G4double val = *(itr->second);
        if(fTmp.find(key) == fTmp.end()) {
          fTmp[key] = val;
        } else {
          fTmp[key] +=val;
        }
      }
    }
  }

	numberOfEvent++; //See G4Run

}

