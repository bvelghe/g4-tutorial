// Bob Velghe - March 27, 2014

#include "GridCam.hh"


GridCam::GridCam(G4LogicalVolume* motherLogicalVol ,G4ThreeVector position, G4int copy) {
  fMotherLogicalVol = motherLogicalVol;
  fPosition = position;
  fCopy = copy;
  
  fXLength = 0.25*m;
  fYLength = 0.25*m;
  fZLength = 1.0*cm;
  fXDivs = 500;
  fYDivs = 500;
  
  fCellXLength = fXLength/fXDivs;
	fCellYLength = fYLength/fYDivs;
	fCellZLength = fZLength;
  
}

GridCam::~GridCam() {
  //FIXME Add Check
	//delete fPhysicalVol;
	//delete fLogicalVol;
	//delete fSolidVol;
}

G4VPhysicalVolume * GridCam::Construct() {

  fSolidVol = new G4Box("GridCam", 0.5*fXLength, 0.5*fYLength, 0.5*fZLength);
	
  fCellSolidVol = new G4Box("GridCamCell", 0.5*(fXLength/fXDivs), 0.5*(fYLength/fYDivs), 0.5*fZLength);    	

	
	fLogicalVol = new G4LogicalVolume(fSolidVol,
                                G4Material::GetMaterial("G4_Galactic"),
                                "GirdCam",
                                0,
                                0,
                                0);

  fCellLogicalVol = new G4LogicalVolume(fCellSolidVol,
                                G4Material::GetMaterial("G4_Galactic"),
                                "GirdCamCell",
                                0,
                                0,
                                0);

	
	fPhysicalVol = new G4PVPlacement(0,
                                fPosition,
                                fLogicalVol,
                                "GridCam",
                                fMotherLogicalVol,
																false
                                ,fCopy);
	
	fCellParameterisation = new GridCamCellParameterisation(fXLength,fYLength,fZLength,fXDivs,fYDivs);
	G4int replicas = fXDivs*fYDivs;
	fCellPhysicalVol = new G4PVParameterised("GirdCamCell",
					  fCellLogicalVol,        
					  fLogicalVol,  
					  kUndefined,                 // axis
					  replicas,            // replicas
					  fCellParameterisation);    //G4VPVParameterisation
	
	G4MultiFunctionalDetector * det = new G4MultiFunctionalDetector("GridCam");
	G4VPrimitiveScorer * primScorer;
	primScorer = new G4PSFlatSurfaceCurrent("CellSurfCurr",0);
	det->RegisterPrimitive(primScorer);
	
	G4SDManager::GetSDMpointer()->AddNewDetector(det);
	fCellLogicalVol->SetSensitiveDetector(det);
	
	return fPhysicalVol;
}

void GridCam::PrintGeometry() {
  G4cout << "**** GirdCam " << fCopy << " ****" <<G4endl;
  G4cout << "Pos. X " << fPosition.x() << " m" << G4endl;
  G4cout << "     Y " << fPosition.y() << " m" << G4endl;
  G4cout << "     Z " << fPosition.z() << " m" << G4endl;
  G4cout << "Grid size X " << fCellXLength/mm << " mm" << G4endl;
  G4cout << "          Y " << fCellYLength/mm << " mm" << G4endl;
  G4cout << "**** END ****" << G4endl; 
}

void GridCam::SetProperties() {
   fVisAtt = new G4VisAttributes(G4Colour(1.0,1.0,0.0,0.2)); //G4Colour (red, green, blue, alpha=1)
   fCellLogicalVol->SetVisAttributes(fVisAtt);
   fLogicalVol->SetVisAttributes(fVisAtt);
   //fLogicalVol->SetVisAttributes(G4VisAttributes::Invisible);
}




