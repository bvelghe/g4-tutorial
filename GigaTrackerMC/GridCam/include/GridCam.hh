#ifndef __GridCam_hh__
#define __GridCam_hh__ 

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "GridCamCellParameterisation.hh"
#include "G4PVParameterised.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSFlatSurfaceCurrent.hh"
#include "G4SDManager.hh"


class GridCam : public G4VUserDetectorConstruction {
	public:
		GridCam(G4LogicalVolume * motherLogicalVol, G4ThreeVector position, G4int copy);
    virtual ~GridCam();
    //Pure virtual 
    G4VPhysicalVolume * Construct();
    		
		void SetProperties();
		
		void PrintGeometry();
		
	private:
		G4LogicalVolume* fMotherLogicalVol;
    G4ThreeVector fPosition;
    G4int fCopy;

    G4double fXLength;
    G4double fYLength;
    G4double fZLength;

	  G4double fCellXLength;
	  G4double fCellYLength;
	  G4double fCellZLength;

    G4int fXDivs;
    G4int fYDivs;

		G4Box * fSolidVol;
   	G4LogicalVolume * fLogicalVol;
    G4VPhysicalVolume * fPhysicalVol;
		
	  GridCamCellParameterisation * fCellParameterisation;
	  G4Box * fCellSolidVol;
   	G4LogicalVolume * fCellLogicalVol;
	  G4PVParameterised * fCellPhysicalVol;
		
		G4VisAttributes * fVisAtt;
};

#endif // __GridCam_hh__
