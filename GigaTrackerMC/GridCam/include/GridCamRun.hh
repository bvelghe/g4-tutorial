#ifndef _GRIDCAMRUN_H_
#define _GRIDCAMRUN_H_ 1

//
// Bob Velghe <bob.velghe@cern.ch> - March 17, 2014
//

#include <G4Run.hh>
#include <G4Event.hh>

//
#include "GigaTrackerHit.hh"

// G4 Scorer
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4THitsMap.hh"


//Move me
#include "TGigaTrackerHit.hh"

class GridCamRun : public G4Run {
	public:
		GridCamRun();
		~GridCamRun();
		// FIXME Copy constructor and = operator must not be used.
		void RecordEvent(const G4Event* aEvent);
    void Print() const;
  private:		

		G4int fGigaTracker_CollectionID;
    G4THitsCollection<GigaTrackerHit> fGigaTracker_HitCollection;


		///////////////
		// G4 Scorer //
		///////////////
		G4int fGridCam_CollectionID;


		
		G4SDManager * fSDman;
		std::map<G4int,G4double> fTmp;


};

#endif // _GRIDCAMRUN_H_
