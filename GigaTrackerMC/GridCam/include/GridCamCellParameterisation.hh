//
// --------------------------------------------------------------
// History:
//
// Created by Bob Velghe (bob.velghe@cern.ch) March 17, 2014
// --------------------------------------------------------------
//

#ifndef GridCamCellParameterisation_H
#define GridCamCellParameterisation_H 1

#include "G4VPVParameterisation.hh"
#include "G4ThreeVector.hh"
#include "G4VPhysicalVolume.hh"
#include "G4Box.hh"
#include "G4ThreeVector.hh"

class GridCamCellParameterisation : public G4VPVParameterisation {
public:
  GridCamCellParameterisation(const G4double, const G4double, const G4double, const G4int, const G4int);
  void ComputeTransformation(const G4int copyNo, G4VPhysicalVolume* physVol) const;
  void ComputeDimensions (G4Box &, const G4int, const G4VPhysicalVolume *) const;
  // DUMMY
  void ComputeDimensions (G4Tubs &, const G4int, const G4VPhysicalVolume *) const {};
  void ComputeDimensions (G4Trd &, const G4int, const G4VPhysicalVolume *) const {};
  void ComputeDimensions (G4Trap &, const G4int, const G4VPhysicalVolume *) const {};
  void ComputeDimensions (G4Cons &, const G4int, const G4VPhysicalVolume *) const {};
  void ComputeDimensions (G4Sphere &, const G4int, const G4VPhysicalVolume *) const {};
  void ComputeDimensions (G4Orb &, const G4int, const G4VPhysicalVolume *) const {};
  void ComputeDimensions (G4Torus &, const G4int, const G4VPhysicalVolume *) const {};
  void ComputeDimensions (G4Para &, const G4int, const G4VPhysicalVolume *) const {};
  void ComputeDimensions (G4Polycone &, const G4int, const G4VPhysicalVolume *) const {};
  void ComputeDimensions (G4Polyhedra &, const G4int, const G4VPhysicalVolume *) const {};
  void ComputeDimensions (G4Hype &, const G4int, const G4VPhysicalVolume *) const {}; 
  
private:
	  G4double fXLength;
	  G4double fYLength;
	  G4double fZLength;

    G4int fXDivs;
    G4int fYDivs;
};

#endif // GridCamCellParameterisation_H
