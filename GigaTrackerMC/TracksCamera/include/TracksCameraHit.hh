#ifndef __TracksCameraHit_hh__
#define __TracksCameraHit_hh__

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4ThreeVector.hh"
#include "G4ProcessType.hh"

class TracksCameraHit : public G4VHit {
	public:
		TracksCameraHit();
		~TracksCameraHit();
		//
		TracksCameraHit(const TracksCameraHit &);
		const TracksCameraHit & operator=(const TracksCameraHit &);
	  	G4int operator==(const TracksCameraHit &) const;
		//
		//void Draw();
		//void Print();
		//
		void TrackID(G4int trackID) {fTrackID = trackID; } 
		G4int TrackID() { return fTrackID; }   
		
		void TrackMomentum(G4ThreeVector trackMom) { fTrackMomentum = trackMom; }
		G4ThreeVector TrackMomentum() { return fTrackMomentum; }
		
		void TrackPosition(G4ThreeVector trackPos) { fTrackPosition = trackPos; }
		G4ThreeVector TrackPosition() { return fTrackPosition; }
		
		void PID(G4int pid) { fPID = pid; }
		G4int PID() { return fPID; }
		
		void TrackEnergy(G4double trackEnergy) { fTrackEnergy = trackEnergy; }
		G4double TrackEnergy() { return fTrackEnergy; }
		
		void TrackCreatorProcess(G4int trackCreatorProcess) { fTrackCreatorProcess = trackCreatorProcess; }
		G4int TrackCreatorProcess() { return fTrackCreatorProcess; }
	private:
		G4int fTrackID;
		G4int fPID; // Particle ID
		G4ThreeVector fTrackMomentum; 
		G4ThreeVector fTrackPosition; 
		G4double fTrackEnergy;
		G4int fTrackCreatorProcess;
};

//Hits storage 
typedef G4THitsCollection<TracksCameraHit> TracksCameraHitsCollection;

#endif // __TracksCameraHit_hh__
