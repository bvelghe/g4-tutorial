#ifndef __TracksCameraSD_hh__
#define __TracksCameraSD_hh__

#include "G4VSensitiveDetector.hh"
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "TracksCameraHit.hh"
#include <map>

class TracksCameraSD : public G4VSensitiveDetector {
	public:
		TracksCameraSD(G4String,G4int);
		~TracksCameraSD();
		void Initialize(G4HCofThisEvent*);
		G4bool ProcessHits(G4Step*, G4TouchableHistory*);
	private:
		G4int fCopy;
		G4int fHCID; //Hits collection ID
		TracksCameraHitsCollection * fHitsCollection; // see TracksCameraHit.hh
		std::map<G4int,G4int> fTrackMap;
};

#endif // __TracksCameraSD_hh__
