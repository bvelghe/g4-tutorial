#ifndef __TracksCamera_hh__
#define __TracksCamera_hh__ 

#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4RotationMatrix.hh"

#include "TracksCameraSD.hh"
#include "G4SDManager.hh"

const G4double thickness = 1.0*CLHEP::mm;

class TracksCamera : public G4VUserDetectorConstruction {
	public:
		TracksCamera(G4LogicalVolume * motherLogicalVol, G4double radius, G4ThreeVector position, G4int copy);
    virtual ~TracksCamera();
    //Pure virtual 
    G4VPhysicalVolume * Construct();
    		
		void SetProperties();
	private:
		G4LogicalVolume* fMotherLogicalVol;
    G4ThreeVector fPosition;
    G4int fCopy;

		G4double fRadius;

		G4String fTracksCameraName;
		TracksCameraSD * fTracksCameraSD;
		
		G4Sphere * fSolidVol;
   	G4LogicalVolume * fLogicalVol;
    G4VPhysicalVolume * fPhysicalVol;
		

		G4VisAttributes * fVisAtt;
};

#endif // __TracksCamera_hh__
