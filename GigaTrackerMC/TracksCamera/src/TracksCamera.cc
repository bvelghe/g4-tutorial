// Bob Velghe - Sep. 20, 2012

//
// For each particle that *enters* the volume, copy the following information 
// - Momentum
// - PID 
// - Track ID (G4 attribute)
//

#include "TracksCamera.hh"


TracksCamera::TracksCamera(G4LogicalVolume* motherLogicalVol, G4double radius,G4ThreeVector position, G4int copy) {
  fMotherLogicalVol = motherLogicalVol;
	fRadius = radius;
  fPosition = position;
  fCopy = copy;
}

TracksCamera::~TracksCamera() {
	//INOP
}

G4VPhysicalVolume * TracksCamera::Construct() {

	//Empty sphere
  fSolidVol = new G4Sphere("TracksCamera",fRadius, 
														fRadius+thickness, //Rout
														0,
														2*CLHEP::pi,
														0,
														CLHEP::pi);
	
    	

	
	fLogicalVol = new G4LogicalVolume(fSolidVol,
                                G4Material::GetMaterial("G4_Galactic"),
                                "TracksCamera",
                                0,
                                0,
                                0);

	
	fPhysicalVol = new G4PVPlacement(0,
                                fPosition,
                                fLogicalVol,
                                "TracksCameraIn",
                                fMotherLogicalVol,
																false
                                ,fCopy);

  /////////////////////
  // Sensitve Volume //
  /////////////////////
 	std::ostringstream os; os << fCopy;
        
	fTracksCameraName = "TracksCamera_" + os.str();
  G4SDManager* SDmanager = G4SDManager::GetSDMpointer();
  fTracksCameraSD = new TracksCameraSD(fTracksCameraName,fCopy);
  fLogicalVol->SetSensitiveDetector(fTracksCameraSD);
  SDmanager->AddNewDetector(fTracksCameraSD);
	
	return fPhysicalVol;
}

void TracksCamera::SetProperties() {
   fVisAtt = new G4VisAttributes(G4Colour(1.0,1.0,0.0,0.2)); //G4Colour (red, green, blue, alpha=1)
   fLogicalVol->SetVisAttributes(fVisAtt);
}




