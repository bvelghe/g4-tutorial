#include "TracksCameraSD.hh"
#include "G4UnitsTable.hh"
#include "G4SDManager.hh"
#include "G4VProcess.hh"
#include "G4Step.hh"

// Call the superclass (G4VSensitiveDetector) constructor
TracksCameraSD::TracksCameraSD(G4String name,G4int icopy) : G4VSensitiveDetector(name) {
	fCopy = icopy;
	fHCID = -1;
  collectionName.insert(name + "_HitsCollection");
}	


TracksCameraSD::~TracksCameraSD() {
	//INOP
}

// Called at the begining of every new event
void TracksCameraSD::Initialize(G4HCofThisEvent* HCTE) {
	// Clean the map
	fTrackMap.clear();
	fHitsCollection = new TracksCameraHitsCollection(SensitiveDetectorName, collectionName[0]);
	if (fHCID < 0)
    	{
        	fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
    	}
	HCTE->AddHitsCollection(fHCID, fHitsCollection);
}

// Called for every step in the sensitve volume (mandatory)
G4bool TracksCameraSD::ProcessHits(G4Step* step, G4TouchableHistory* ROhist)	{  
	G4int trackID = step->GetTrack()->GetTrackID();	
	// Is the track already recored ?
	//if(fTrackMap.find(trackID) == fTrackMap.end() ) { // No
		TracksCameraHit * hit = new TracksCameraHit();
    hit->TrackID(trackID);
    hit->TrackMomentum(step->GetTrack()->GetMomentumDirection());
		hit->TrackPosition(step->GetTrack()->GetPosition());
		hit->TrackEnergy(step->GetTrack()->GetTotalEnergy());
		hit->PID(step->GetTrack()->GetDefinition()->GetPDGEncoding());
		if(step->GetTrack()->GetCreatorProcess()) {
			if(step->GetTrack()->GetCreatorProcess()->GetProcessType() == fDecay) hit->TrackCreatorProcess(1);
			else hit->TrackCreatorProcess(0);
		} else {
			hit->TrackCreatorProcess(0);
		} 
		fHitsCollection->insert(hit);
		fTrackMap.insert( std::pair<G4int,G4int>(trackID,0) ); // Better DS ?
	//} else { // Yes, track is already recored. Do nothing.
	//}
        
	return true;
} 
