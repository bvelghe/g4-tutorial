#include "TracksCameraHit.hh"

TracksCameraHit::TracksCameraHit() {}
TracksCameraHit::~TracksCameraHit() {}

/////////////////////////
// Not yet implemented //
/////////////////////////

//void TracksCameraHit::Draw() {}
//void TracksCameraHit::Print() {}

// Shallow copy is sufficicent
TracksCameraHit::TracksCameraHit(const TracksCameraHit& right)
{
	fTrackID = -1;
	fTrackMomentum = G4ThreeVector(-1.0,-1.0,-1.0);
	fPID = -1;
	fTrackEnergy = -1;
	fTrackCreatorProcess = -1;
}

const TracksCameraHit& TracksCameraHit::operator=(const TracksCameraHit& right)
{
	if (this == &right) return *this; //Check for self-assignment
	fTrackID = right.fTrackID;
	fTrackMomentum = right.fTrackMomentum;
	fPID = right.fPID;
	fTrackEnergy = right.fTrackEnergy;
	fTrackCreatorProcess = right.fTrackCreatorProcess;
	return *this;
}

G4int TracksCameraHit::operator==(const TracksCameraHit& right) const
{
    return (this==&right) ? 1 : 0;
}
