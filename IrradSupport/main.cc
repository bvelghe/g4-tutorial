//#include "G4UIQt.hh"

#include "G4RunManager.hh"

#include "MyDetectorConstruction.hh"
#include "MyPhysicsList.hh"
#include "MyPrimaryGeneratorAction.hh"
#include "MyUserEventAction.hh"
#include "MyUserRunAction.hh"

#include "G4UImanager.hh"

#include "G4UIExecutive.hh"
#include "G4VisExecutive.hh"

int main(int argc, char * argv[]) 
{
    G4RunManager* runManager = new G4RunManager();

    ////////////////////
    // Initialisation //
    ////////////////////

    //FIXME
	//G4VUserDetectorConstruction* detector = new MyDetectorConstruction();
	MyDetectorConstruction * detector = new MyDetectorConstruction();
    runManager->SetUserInitialization(detector);	

  	G4VUserPhysicsList* physics = new MyPhysicsList();
  	runManager->SetUserInitialization(physics);

	G4VUserPrimaryGeneratorAction* gen_action = new MyPrimaryGeneratorAction();
  	runManager->SetUserAction(gen_action);

    //MyUserRunAction dépend de MyDetectorConstruction //FIXME
	MyUserRunAction * run_action = new MyUserRunAction(detector);
  	runManager->SetUserAction(run_action);
	
    //MyUserEventAction dépend de MyUserRunAction
	MyUserEventAction * event_action = new MyUserEventAction(run_action);
    runManager->SetUserAction(event_action);

	//////////////////////////
	// Start the event loop //
	//////////////////////////
	
    runManager->Initialize();

    //////////////////////////////////////
    // User interface and visualization // 	
	//////////////////////////////////////
	
  G4VisManager* visManager = new G4VisExecutive;
  visManager->Initialize();

 	// Get the pointer to the User Interface manager
	G4UImanager * UImanager = G4UImanager::GetUIpointer(); 
 	//
 	if (argc!=1)   // batch mode  
    	{
      		G4String command = "/control/execute ";
      		G4String fileName = argv[1];
      		UImanager->ApplyCommand(command+fileName);
    	}
  	else           // interactive mode : define UI session  
 		{
 		G4UIExecutive * ui = new G4UIExecutive(argc,argv);
		ui->SessionStart();
	 }
    ////////////////////
    // Clean and exit //
    ////////////////////

  delete visManager;
	delete runManager;
	return 0;
}
