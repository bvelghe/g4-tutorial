#include "MyPhysicsList.hh"


MyPhysicsList::MyPhysicsList() {

      // EM physics
  RegisterPhysics(new G4EmStandardPhysics());
}
MyPhysicsList::~MyPhysicsList() {}

/*
void MyPhysicsList::ConstructParticle() {
  // In this method, static member functions should be called
  // for all particles which you want to use.
  // This ensures that objects of these particle types will be
  // created in the program. 
  // G4Geantino::GeantinoDefinition();
  //G4Proton::ProtonDefinition();


}
*/
/*
void MyPhysicsList::ConstructProcess() {
    // Transports particles through the detector geometry
    AddTransportation();
    
    ////////////
    // Proton //
    ////////////    
    G4ParticleTable * particleTable = G4ParticleTable::GetParticleTable();
    G4cout << particleTable << G4endl;
    G4ParticleDefinition * proton = particleTable->FindParticle("proton");
    G4ProcessManager * processManager = proton->GetProcessManager();
    //processManager->AddProcess(new G4MultipleScattering(), -1, 1, 1);
    processManager->AddProcess(new G4hIonisation(),-1, 2, 2);
}
*/
void MyPhysicsList::SetCuts() {
  //  SetCutsWithDefault();
  G4VUserPhysicsList::SetCuts();
}
