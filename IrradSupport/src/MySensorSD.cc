#include "MySensorSD.hh"
#include "G4UnitsTable.hh"
#include "G4SDManager.hh"
#include "G4VProcess.hh"
#include "G4Step.hh"

// Call the superclass (G4VSensitiveDetector) constructor
MySensorSD::MySensorSD(G4String name,G4int icopy) : G4VSensitiveDetector(name) {
	fCopy = icopy;
	fHCID = -1;
    fCell = -1;
	std::ostringstream os; os << icopy;
  	collectionName.insert("Sensor_HitsCollection_" + os.str());
}	


MySensorSD::~MySensorSD() {
}

//Called at the begining of every new event
void MySensorSD::Initialize(G4HCofThisEvent* HCTE) {
	fHitsCollection = new MySensorHitsCollection(SensitiveDetectorName, collectionName[0]);
	if (fHCID < 0)
    {
        fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
    }
    HCTE->AddHitsCollection(fHCID, fHitsCollection);
    fCell = -1;
}

//Called for every step in the sensitve volume (mandatory)
G4bool MySensorSD::ProcessHits(G4Step* step, G4TouchableHistory* ROhist)	{
  	G4double energy = step->GetTotalEnergyDeposit();
	
	if (energy == 0) return false;
	
	if(fCell == -1) {
		MySensorHit* hit = new MySensorHit();
	
		hit->Energy(energy);

		G4int cell_id = fHitsCollection->insert(hit);
		fCell = cell_id - 1; //Voir exemple N04
	
	} else {
		(*fHitsCollection)[fCell]->AddEnergy(energy);
	}
	return true;
} 
