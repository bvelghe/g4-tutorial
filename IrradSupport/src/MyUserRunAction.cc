#include "MyUserRunAction.hh"



MyUserRunAction::MyUserRunAction(MyDetectorConstruction * det)
{
    fDetector = det;
    fRunManager = G4RunManager::GetRunManager();
    fPrimaryGeneratorAction = (MyPrimaryGeneratorAction *)G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction();
    fGenEvents = 0;
    fRecEvents = 0;
}


MyUserRunAction::~MyUserRunAction()
{;}

bool MyUserRunAction::StoreHit(G4double energy) {
    if(fOutFile.is_open()) {
        fOutFile <<  energy/MeV << G4endl;
    }
	return true;
}

void MyUserRunAction::IncGenEvents() {
    fGenEvents++;
}  

void MyUserRunAction::IncRecEvents() {
    fRecEvents++;
} 

void MyUserRunAction::BeginOfRunAction(const G4Run* aRun)
{
  fGenEvents = 0;
  fRecEvents = 0;
  fRunID = aRun->GetRunID();
  G4cout << "### Run " << fRunID << " start." << G4endl;
  G4cout << "# Support thickness: " << fDetector->GetSupportThickness()*mm << " mm" << G4endl;
  G4cout << "# Beam energy: " << fPrimaryGeneratorAction->GetBeamEnergy()/MeV << " MeV" << G4endl;
  //Open output file
  std::ostringstream os; os << fRunID;
  G4String filename = "output_run"+os.str()+".dat";
  G4cout << "# Output in " << filename << G4endl;
  fOutFile.open(filename.c_str());
  if(!fOutFile.is_open()) {
    //FIXME
  } else {
    fOutFile << "### Run " << fRunID << " start." << G4endl;
    fOutFile << "# Support thickness: " << fDetector->GetSupportThickness()*mm << " mm" << G4endl;
    fOutFile << "# Beam energy: " << fPrimaryGeneratorAction->GetBeamEnergy()/MeV << " MeV" << G4endl;
  }
}

void MyUserRunAction::EndOfRunAction(const G4Run*)
{
  G4cout << "### Run " << fRunID << " end." << G4endl;
  //Close output file
  if(fOutFile.is_open()) {
    fOutFile << "# Support Thickness (mm) | Gen. Events | Rec. Events" << G4endl;
    fOutFile << fDetector->GetSupportThickness()*mm << " " << fGenEvents << " " << fRecEvents << G4endl;
    fOutFile << "### Run " << fRunID << " end." << G4endl;
    fOutFile.close(); 
  }
}
