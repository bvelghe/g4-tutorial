#include "MyDetectorConstruction.hh"

MyDetectorConstruction::MyDetectorConstruction() {
	/////////////////////
	// Build materials //
	/////////////////////


	fNistMan = G4NistManager::Instance();
 	fNistMan->FindOrBuildMaterial("G4_Galactic");
    fNistMan->FindOrBuildMaterial("G4_GRAPHITE");
    fNistMan->FindOrBuildMaterial("G4_Si");
    fNistMan->FindOrBuildMaterial("G4_Al");
    
}
MyDetectorConstruction::~MyDetectorConstruction() {

}
  
G4VPhysicalVolume* MyDetectorConstruction::Construct() {
    ///////////
    // World //
    ///////////
    fworldSolidVolume = new G4Box("World",1.0*m,1.0*m,1.0*m);
    
    fworldLogicalVolume = new G4LogicalVolume(fworldSolidVolume,
                                G4Material::GetMaterial("G4_Galactic"),
                                "World",
                                0,
                                0,
                                0);
                                
    fworldPhysicalVolume = new G4PVPlacement(0,
                                G4ThreeVector(0.,0.,0.),
                                fworldLogicalVolume,
                                "World",
                                0,
                                false
                                ,0);  

    /////////////////////////
    // Irradiation Support //
    /////////////////////////
    fIrradSupport = new IrradSupport(fworldLogicalVolume,G4ThreeVector(0.,0.,0.5*m),0);
    fIrradSupport->Construct();
	fIrradSupport->SetProperties();
    
    SetProperties();
    return fworldPhysicalVolume;                        
}

// HACK FIXME FIXME FIXME

G4double MyDetectorConstruction::GetSupportThickness() {
    if(fIrradSupport) return fIrradSupport->GetSupportThickness();
    else return -1.0;
}

void MyDetectorConstruction::SetProperties() {	
	 //World volume is invisible !
	fworldLogicalVolume->SetVisAttributes(G4VisAttributes::Invisible);
}
