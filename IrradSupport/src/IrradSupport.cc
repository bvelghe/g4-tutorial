#include "IrradSupport.hh"

IrradSupport::IrradSupport(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy) {
    fMotherLogicalVolume = motherLogicalVolume;
	fPosition = position;
	fCopy = copy;
    fMessenger = new MyDetectorMessenger(this);
}

IrradSupport::~IrradSupport() {
    delete fMessenger;
}

G4VPhysicalVolume* IrradSupport::Construct() {
    /////////////////////////
    // Irradiation Support //
    /////////////////////////

    fIrradSupportSolidVolume = new G4Box("IrradSupport",15.0*cm,15.0*cm,15.0*cm);
    fSupportSolidVolume = new G4Box("Support",15.0*cm,15.0*cm,0.25*cm);
    fTargetSolidVolume = new G4Box("Target",15.0*cm,15.0*cm,1.25*cm);
    
    fIrradSupportLogicalVolume = new G4LogicalVolume(fIrradSupportSolidVolume,
                                G4Material::GetMaterial("G4_Galactic"),
                                "IrradSupport",
                                0,
                                0,
                                0);

    fSupportLogicalVolume = new G4LogicalVolume(fSupportSolidVolume,
                                G4Material::GetMaterial("G4_Al"),
                                "Support",
                                0,
                                0,
                                0);

    fTargetLogicalVolume =  new G4LogicalVolume(fTargetSolidVolume,
                                G4Material::GetMaterial("G4_GRAPHITE"),
                                "Target",
                                0,
                                0,
                                0);

    fIrradSupportPhysicalVolume = new G4PVPlacement(0,
                                fPosition,
                                fIrradSupportLogicalVolume, 
                                "IrradSupport",
                                fMotherLogicalVolume,
                                false
                                ,0);   

    fSupportPhysicalVolume = new G4PVPlacement(0,
                                G4ThreeVector(0.,0.,-0.5*29*cm),
                                fSupportLogicalVolume,
                                "Support",
                                fIrradSupportLogicalVolume,
                                false
                                ,0); 
 
    fTargetPhysicalVolume = new G4PVPlacement(0,
                                G4ThreeVector(0.,0.,0.5*1*cm), 
                                fTargetLogicalVolume,
                                "Target",
                                fIrradSupportLogicalVolume,
                                false
                                ,0); 
    /////////////////////
    // Sensitve Volume //
    /////////////////////
    //FIXME Clean the code 
    // Name of the detector 
	std::ostringstream oss;
    oss << fCopy;
	G4String sensor_SDname = "/Sensor_"+oss.str();
	
	G4SDManager* SDmanager = G4SDManager::GetSDMpointer();
	MySensorSD* sensor_SD = new MySensorSD(sensor_SDname,fCopy);
    fTargetLogicalVolume->SetSensitiveDetector(sensor_SD);
	SDmanager->AddNewDetector(sensor_SD);

    return fIrradSupportPhysicalVolume;                        
}

bool IrradSupport:: SetSupportThickness(G4double thickness) {
    // Update geometry
    if(thickness >= 0.1*mm && thickness <= 5*mm) {
        fSupportSolidVolume->SetZHalfLength(thickness/2);
        G4RunManager::GetRunManager()->GeometryHasBeenModified();
        return true;                       
    } else return false;
}

G4double IrradSupport::GetSupportThickness() {
    return 2*fSupportSolidVolume->GetZHalfLength();
}

void IrradSupport::SetProperties() {
    fSupportVisAtt = new G4VisAttributes(G4Colour(0.0,1.0,0.0)); //G4Colour (red, green, blue, alpha=1)
    fTargetVisAtt = new G4VisAttributes(G4Colour(0.0,0.0,1.0));

	fSupportLogicalVolume->SetVisAttributes(fSupportVisAtt);
    fTargetLogicalVolume->SetVisAttributes(fTargetVisAtt); 

	fIrradSupportLogicalVolume->SetVisAttributes(G4VisAttributes::Invisible);
}
