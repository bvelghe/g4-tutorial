#include "MyDetectorMessenger.hh"


MyDetectorMessenger::MyDetectorMessenger(IrradSupport* Det) : G4UImessenger(),
   fIrradSupport(Det) {
   
   fDetDirectory = new G4UIdirectory("/support/");
   fDetDirectory->SetGuidance("Support control");

    ///////////////////////////////////
    // Control the support thickness //
    ///////////////////////////////////

   fSetSupportThickness = new G4UIcmdWithADoubleAndUnit("/support/setThickness",this);
   fSetSupportThickness->SetGuidance("Set the support Thickness");
   fSetSupportThickness->SetGuidance("Thickness must be between 0.1mm and 5mm");
   fSetSupportThickness->SetParameterName("Thickness",false);
   fSetSupportThickness->SetUnitCategory("Length");
   fSetSupportThickness->AvailableForStates(G4State_PreInit,G4State_Idle);

}



MyDetectorMessenger::~MyDetectorMessenger() {
    delete fDetDirectory;
    delete fSetSupportThickness;
}

void MyDetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue) {
 if( command == fSetSupportThickness ) {
    fIrradSupport->SetSupportThickness(fSetSupportThickness->GetNewDoubleValue(newValue));
  }
}
