#include "MyPrimaryGeneratorAction.hh"

//////////////////////////////
//	Génération du faisceau	//
//////////////////////////////

MyPrimaryGeneratorAction::MyPrimaryGeneratorAction() {
	fGeneralParticleSource = new G4GeneralParticleSource();
}

MyPrimaryGeneratorAction::~MyPrimaryGeneratorAction() {
    delete fGeneralParticleSource;
}

void MyPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
    fGeneralParticleSource->GeneratePrimaryVertex(anEvent);
}

G4double MyPrimaryGeneratorAction::GetBeamEnergy() {
    return fGeneralParticleSource->GetParticleEnergy();
}
