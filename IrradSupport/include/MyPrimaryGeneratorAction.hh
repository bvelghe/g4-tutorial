#ifndef MyPrimaryGeneratorAction_h
#define MyPrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4GeneralParticleSource.hh"

class MyPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    MyPrimaryGeneratorAction();
    ~MyPrimaryGeneratorAction();
    G4double GetBeamEnergy();
    //////////////
    // Required //
    //////////////
    void GeneratePrimaries(G4Event* anEvent);
  private:
     G4GeneralParticleSource * fGeneralParticleSource;
};

#endif // MyPrimaryGeneratorAction_h 
