#ifndef MyUserRunAction_h
#define MyUserRunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include "G4Run.hh"
#include "G4ThreeVector.hh"
#include "MyDetectorConstruction.hh"
#include "MyPrimaryGeneratorAction.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"
#include <fstream>
#include <CLHEP/Random/RandExponential.h>
#include <CLHEP/Random/RandGaussQ.h>


class MyUserRunAction : public G4UserRunAction
{
  public:
    MyUserRunAction(MyDetectorConstruction * det);
   ~MyUserRunAction();
    void BeginOfRunAction(const G4Run*);
    void EndOfRunAction(const G4Run*);
    bool StoreHit(G4double energy);
    //FIXME Better way of counting transmitted particles ?
    //Record charge instead ?
    void IncGenEvents(); //Increment generated events
    void IncRecEvents(); //Increment recorded events
 private:
    G4int fRunID;
    G4long fGenEvents;
    G4long fRecEvents;
    MyDetectorConstruction * fDetector;
    G4RunManager * fRunManager;
    MyPrimaryGeneratorAction *fPrimaryGeneratorAction;
    std::ofstream fOutFile;
};


#endif //MyUserRunAction_h
