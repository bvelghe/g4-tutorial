#ifndef __IrradSupport_HH__
#define __IrradSupport_HH__

// Materials 
//   - Box: Polymethyl methacrylate (PMMA) 
//   - Sensor: Silicium 

#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Box.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4SystemOfUnits.hh"

#include "MyDetectorMessenger.hh"
#include "G4SDManager.hh"
#include "MySensorSD.hh"
#include "G4RunManager.hh"
class MyDetectorMessenger; //FIXME

class IrradSupport : public G4VUserDetectorConstruction {
  public:  	
    IrradSupport(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy);
    ~IrradSupport();	
    //Pure virtual 
    G4VPhysicalVolume* Construct();
    void SetProperties(); 
    //
    bool SetSupportThickness(G4double thickness);
    G4double GetSupportThickness();
  private:
    MyDetectorMessenger * fMessenger;

    G4LogicalVolume* fMotherLogicalVolume; 
    G4ThreeVector fPosition;
    G4int fCopy;

    ///////////////////
    // Water phantom //
    ///////////////////
    G4Box * fIrradSupportSolidVolume;
    G4Box * fSupportSolidVolume;
    G4Box * fTargetSolidVolume;
    
    G4LogicalVolume * fIrradSupportLogicalVolume;
    G4LogicalVolume * fSupportLogicalVolume;
    G4LogicalVolume * fTargetLogicalVolume;

    G4VPhysicalVolume * fIrradSupportPhysicalVolume;
    G4VPhysicalVolume * fSupportPhysicalVolume;
    G4VPhysicalVolume * fTargetPhysicalVolume;

    G4VisAttributes * fSupportVisAtt;
    G4VisAttributes * fTargetVisAtt;
};

#endif // __IrradSupport_HH__
