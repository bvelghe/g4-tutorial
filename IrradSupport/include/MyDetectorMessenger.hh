#ifndef __MyDetectorMessenger_hh__
#define __MyDetectorMessenger_hh__

#include "G4UImessenger.hh"
#include "IrradSupport.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIdirectory.hh"

class IrradSupport; //FIXME

class MyDetectorMessenger : public G4UImessenger
{
  public:
    MyDetectorMessenger(IrradSupport * Det);
    //MyDetectorMessenger();
    virtual ~MyDetectorMessenger();
    virtual void SetNewValue(G4UIcommand*, G4String);
  private:
    IrradSupport * fIrradSupport;
    G4UIdirectory*           fDetDirectory;
    G4UIcmdWithADoubleAndUnit* fSetSupportThickness;

};
#endif // __MyDetectorMessenger_hh__
