#ifndef MyDetectorConstruction_h
#define MyDetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"

#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"

#include "G4ThreeVector.hh"
#include "IrradSupport.hh"

#include <vector>

class MyDetectorConstruction : public G4VUserDetectorConstruction 
{
	public:
    MyDetectorConstruction();
    ~MyDetectorConstruction();
    void SetProperties();
    //////////////
    // Required //
    //////////////
    G4VPhysicalVolume* Construct();
    //FIXME HACK
    G4double GetSupportThickness();
    private:
      G4NistManager * fNistMan;

      
      G4Box * fworldSolidVolume;
      G4LogicalVolume * fworldLogicalVolume;
      G4VPhysicalVolume * fworldPhysicalVolume;

      IrradSupport * fIrradSupport;
};

#endif // MyDetectorConstruction_h
