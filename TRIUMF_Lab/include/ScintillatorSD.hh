#ifndef ScintillatorSD_h
#define ScintillatorSD_h 1

#include "G4VSensitiveDetector.hh"
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "ScintillatorG4Hit.hh"

class ScintillatorSD : public G4VSensitiveDetector {
	public:
		ScintillatorSD(G4String,G4int);
		~ScintillatorSD();
		void Initialize(G4HCofThisEvent*);
		G4bool ProcessHits(G4Step*, G4TouchableHistory*);
	private:
		G4int fCopy;
		G4int fHCID; //Hits collection ID
		G4int fCell;
		ScintillatorG4HitsCollection * fHitsCollection; // see ScintillatorG4Hit.hh
};

#endif // ScintillatorSD_h
