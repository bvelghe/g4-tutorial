#ifndef SiliconStripRootIO_h
#define SiliconStripRootIO_h 1

#include "SiliconStripEvent.hh"

#include "TTree.h"
#include "TBranch.h"

#include "G4Event.hh"

#include "SiliconStripG4Hit.hh" // "G4" Hit Definition != "Physics" Hit
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

class SiliconStripRootIO {
	public:
		SiliconStripRootIO(); 
		~SiliconStripRootIO();
		
		bool Initialize(TTree *);
		void SaveEvent(const G4Event *);
	private:
		static const int c_n_siliconstrip = 2;
		bool m_ready;
		TTree * m_tree;
		SiliconStripEvent * m_event;
		TBranch * m_branch;
		
		G4int m_g4_hits_collection_ID[c_n_siliconstrip];
		SiliconStripG4HitsCollection * m_g4_hits_collection[c_n_siliconstrip];
		G4SDManager * m_SD_manager;
	
};

#endif // SiliconStripRootIO_h
