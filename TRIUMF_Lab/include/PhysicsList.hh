#ifndef PhysicsList_h
#define PhysicsList_h 1

//#include "G4VUserPhysicsList.hh"
//#include "G4ParticleTypes.hh"
//#include "G4ParticleTable.hh"
//#include "G4ProcessManager.hh"
//#include "G4MultipleScattering.hh"
//#include "G4hIonisation.hh"

#include "G4VModularPhysicsList.hh"
#include "G4EmStandardPhysics.hh"
/// Modular physics list
///
/// It includes the folowing physics builders
/// - G4DecayPhysics
/// - G4RadioactiveDecayPhysics
/// - G4EmStandardPhysics


class PhysicsList : public G4VModularPhysicsList
{
  public:
    PhysicsList();
    virtual ~PhysicsList();
    
    //////////////
    // Required //
    //////////////
    //void ConstructParticle();
    //void ConstructProcess();
    virtual void SetCuts();
};

#endif
