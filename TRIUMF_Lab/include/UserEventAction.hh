#ifndef UserEventAction_h
#define UserEventAction_h 1

#include "G4UserEventAction.hh"
#include "G4Event.hh"


#include "RootIO.hh"

class UserEventAction : public G4UserEventAction
{
	public:
           UserEventAction();
           ~UserEventAction();
           
	   void SetIO(RootIO *);
	   void BeginOfEventAction(const G4Event*);
           void EndOfEventAction(const G4Event*);
          
	private:
	   bool m_io_ready;
	   RootIO * m_root_io;
};

#endif // UserEventAction_h
