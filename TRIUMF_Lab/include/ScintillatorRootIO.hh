#ifndef ScintillatorRootIO_h
#define ScintillatorRootIO_h 1

#include "ScintillatorEvent.hh"

#include "TTree.h"
#include "TBranch.h"

#include "G4Event.hh"

#include "ScintillatorG4Hit.hh" // "G4" Hit Definition != "Physics" Hit
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"


class ScintillatorRootIO {
	public:
		ScintillatorRootIO(); 
		~ScintillatorRootIO();
		
		bool Initialize(TTree *);
		void SaveEvent(const G4Event *);
	private:
		static const int c_n_scintillator = 1;
		bool m_ready;
		TTree * m_tree;
		ScintillatorEvent * m_event;
		TBranch * m_branch;
		
		G4int m_g4_hits_collection_ID[c_n_scintillator];
		ScintillatorG4HitsCollection * m_g4_hits_collection[c_n_scintillator];
		G4SDManager * m_SD_manager;
	
};

#endif // ScintillatorRootIO_h
