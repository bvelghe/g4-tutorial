#ifndef ScintillatorG4Hit_h
#define ScintillatorG4Hit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"

class ScintillatorG4Hit : public G4VHit {
	public:
		ScintillatorG4Hit();
		~ScintillatorG4Hit();
		//
		ScintillatorG4Hit(const ScintillatorG4Hit&);
		const ScintillatorG4Hit& operator=(const ScintillatorG4Hit&);
	  	G4int operator==(const ScintillatorG4Hit&) const;
		//
		//void Draw();
		//void Print();
		//
		inline void Energy(G4double energy) { fEnergy = energy;} 
		inline void AddEnergy(G4double energy) { fEnergy += energy;}
		//
		G4double Energy(void) { return fEnergy;}
		//
	        
	private:
		G4double fEnergy;
};

//Hits storage 
typedef G4THitsCollection<ScintillatorG4Hit> ScintillatorG4HitsCollection;

#endif // ScintillatorG4Hit_h
