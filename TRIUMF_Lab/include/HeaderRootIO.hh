#ifndef HeaderRootIO_h
#define HeaderRootIO_h 1

#include "EventHeader.hh"

#include "TTree.h"
#include "TBranch.h"

#include "G4Event.hh"

class HeaderRootIO {
	public:
		HeaderRootIO(); 
		~HeaderRootIO();
		
		bool Initialize(TTree *);
		void SaveEvent(const G4Event *);
	private:
		bool m_ready;
		TTree * m_tree;
		EventHeader * m_event;
		TBranch * m_branch;
};

#endif // HeaderRootIO_h
