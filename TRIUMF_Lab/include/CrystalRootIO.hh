#ifndef CrystalRootIO_h
#define CrystalRootIO_h 1

#include "CrystalEvent.hh"

#include "TTree.h"
#include "TBranch.h"

#include "G4Event.hh"

#include "CrystalHit.hh" // "G4" Hit Definition != "Physics" Hit
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4SystemOfUnits.hh"

class CrystalRootIO {
	public:
		CrystalRootIO(); 
		~CrystalRootIO();
		
		bool Initialize(TTree *);
		void SaveEvent(const G4Event *);
	private:
		bool m_ready;
		TTree * m_tree;
		CrystalEvent * m_event;
		TBranch * m_branch;
		
		G4int m_hits_collection_ID;
		CrystalHitsCollection * m_hits_collection;
		G4SDManager * m_SD_manager;
	
};

#endif // CrystalRootIO_h
