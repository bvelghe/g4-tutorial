#ifndef CrystalSD_h
#define CrystalSD_h 1

#include "G4VSensitiveDetector.hh"
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "CrystalHit.hh"

class CrystalSD : public G4VSensitiveDetector {
	public:
		CrystalSD(G4String,G4int);
		~CrystalSD();
		void Initialize(G4HCofThisEvent*);
		G4bool ProcessHits(G4Step*, G4TouchableHistory*);
	private:
		G4int fCopy;
		G4int fHCID; //Hits collection ID
		G4int fCell;
		CrystalHitsCollection * fHitsCollection; // see CrystalHit.hh
};

#endif // CrystalSD_h
