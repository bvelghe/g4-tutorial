#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"

#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"

#include "G4ThreeVector.hh"

#include "SiliconStrip.hh"
#include "WireChamber.hh"
#include "Scintillator.hh"
#include "Crystal.hh"


class DetectorConstruction : public G4VUserDetectorConstruction 
{
	public:
		DetectorConstruction();
		~DetectorConstruction();
		void SetProperties();
		G4VPhysicalVolume* Construct();

	private:
		G4NistManager * fNistMan;

		G4Box * fworldSolidVolume;
		G4LogicalVolume * fworldLogicalVolume;
		G4VPhysicalVolume * fworldPhysicalVolume;

		SiliconStrip * fSiliconStrip[2];
		Scintillator * fScintillator;
};

#endif // DetectorConstruction_h
