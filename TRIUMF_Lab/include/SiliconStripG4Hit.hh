#ifndef SiliconStripG4Hit_h
#define SiliconStripG4Hit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"

class SiliconStripG4Hit : public G4VHit {
	public:
		SiliconStripG4Hit();
		~SiliconStripG4Hit();
		//
		SiliconStripG4Hit(const SiliconStripG4Hit&);
		const SiliconStripG4Hit& operator=(const SiliconStripG4Hit&);
	  	G4int operator==(const SiliconStripG4Hit&) const;
		//
		//void Draw();
		//void Print();
		//
		inline void Energy(G4double energy) { fEnergy = energy;} 
		inline void StripID(G4int strip_id) { fStripID = strip_id;} 
		inline void AddEnergy(G4double energy) { fEnergy += energy;}
		//
		G4double Energy(void) { return fEnergy;}
		G4int StripID(void) { return fStripID;}
		//
	        
	private:
		G4double fEnergy;
		G4int fStripID;
};

//Hits storage 
typedef G4THitsCollection<SiliconStripG4Hit> SiliconStripG4HitsCollection;

#endif // SiliconStripG4Hit_h
