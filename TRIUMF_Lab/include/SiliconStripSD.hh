#ifndef SiliconStripSD_h
#define SiliconStripSD_h 1

#include "G4VSensitiveDetector.hh"
#include "G4Step.hh"
#include "G4TouchableHistory.hh"
#include "SiliconStripG4Hit.hh"

class SiliconStripSD : public G4VSensitiveDetector {
	public:
		SiliconStripSD(G4String,G4int);
		~SiliconStripSD();
		void Initialize(G4HCofThisEvent*);
		G4bool ProcessHits(G4Step*, G4TouchableHistory*);
	private:
		G4int fCopy;
		G4int fHCID; //Hits collection ID
		std::map<G4int,G4int> fStripCellMap;
		SiliconStripG4HitsCollection * fHitsCollection; // see SiliconStripG4Hit.hh
};

#endif // SiliconStripSD_h
