#ifndef __Scintillator_HH__
#define __Scintillator_HH__

#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Box.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4SystemOfUnits.hh"

#include "G4SDManager.hh"
#include "ScintillatorSD.hh"

class Scintillator : public G4VUserDetectorConstruction {
	public:  	
		Scintillator(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy);
		~Scintillator();	
		// Note: Construct is a pure virtual method of G4VUserDetectorConstruction we have to implement it.
		G4VPhysicalVolume * Construct();
		
		void SetProperties(); 
	private:

		G4LogicalVolume* fMotherLogicalVolume; 		
		G4ThreeVector fPosition;
		G4int fCopy;

		G4Box * fScintillatorSolidVolume; 
		G4LogicalVolume * fScintillatorLogicalVolume;
	
		G4VPhysicalVolume * fScintillatorPhysicalVolume;
		
		G4VisAttributes * fScintillatorVisAttributes;

};

#endif // __Scintillator_HH___
