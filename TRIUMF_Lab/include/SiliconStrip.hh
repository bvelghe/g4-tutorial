#ifndef __SiliconStrip_HH__
#define __SiliconStrip_HH__

#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Box.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4SystemOfUnits.hh"
#include "G4PVReplica.hh"

#include "G4SDManager.hh"

#include "SiliconStripSD.hh"


class SiliconStrip : public G4VUserDetectorConstruction {
	public:  	
		SiliconStrip(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy);
		~SiliconStrip();	
		// Note: Construct is a pure virtual method of G4VUserDetectorConstruction we have to implement it.
		G4VPhysicalVolume * Construct();
		
		void SetProperties(); 
	private:

		G4LogicalVolume* fMotherLogicalVolume; 		
		G4ThreeVector fPosition;
		G4int fCopy;

	
		G4Box * fSSModuleSolidVolume;
		G4Box * fSStripSolidVolume;
 
		G4LogicalVolume * fSSModuleLogicalVolume;
		G4LogicalVolume * fSStripLogicalVolume;
	
		G4VPhysicalVolume * fSSModulePhysicalVolume;
		G4PVReplica * fSStripReplica;	
	
		G4VisAttributes * fSSModuleVisAttributes;

};

#endif // __SiliconStrip_HH__
