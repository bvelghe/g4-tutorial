#ifndef CrystalHit_h
#define CrystalHit_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"

class CrystalHit : public G4VHit {
	public:
		CrystalHit();
		~CrystalHit();
		//
		CrystalHit(const CrystalHit&);
		const CrystalHit& operator=(const CrystalHit&);
	  	G4int operator==(const CrystalHit&) const;
		//
		//void Draw();
		//void Print();
		//
		inline void Energy(G4double energy) { fEnergy = energy;} 
		inline void AddEnergy(G4double energy) { fEnergy += energy;}
		//
		G4double Energy(void) { return fEnergy;}
		//
	        
	private:
		G4double fEnergy;
};

//Hits storage 
typedef G4THitsCollection<CrystalHit> CrystalHitsCollection;

#endif // CrystalHit_h
