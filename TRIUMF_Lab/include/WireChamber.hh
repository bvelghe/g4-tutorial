#ifndef __WireChamber_HH__
#define __WireChamber_HH__

#include "G4LogicalVolume.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Box.hh"
#include "G4Material.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4SystemOfUnits.hh"

#include "G4SDManager.hh"

//#include "WireChamberSD.hh"

class WireChamber : public G4VUserDetectorConstruction {
	public:  	
		WireChamber(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy);
		~WireChamber();	
		// Note: Construct is a pure virtual method of G4VUserDetectorConstruction we have to implement it.
		G4VPhysicalVolume * Construct();
		
		void SetProperties(); 
	private:

		G4LogicalVolume* fMotherLogicalVolume; 		
		G4ThreeVector fPosition;
		G4int fCopy;

		G4Box * fWCModuleSolidVolume;
		G4Box * fFrontWallSolidVolume;
		G4Box * fBackWallSolidVolume;
		G4Box * fWireBoxSolidVolume;

		G4LogicalVolume * fWCModuleLogicalVolume;
		G4LogicalVolume * fFrontWallLogicalVolume;
		G4LogicalVolume * fBackWallLogicalVolume;
		G4LogicalVolume * fWireBoxLogicalVolume;
		
		G4VPhysicalVolume * fWCModulePhysicalVolume;
		G4VPhysicalVolume * fFrontWallPhysicalVolume;		
		G4VPhysicalVolume * fBackWallPhysicalVolume;
		G4VPhysicalVolume * fWireBoxPhysicalVolume;		

		G4VisAttributes * fWCModuleVisAttributes;
		G4VisAttributes * fFrontWallVisAttributes;
		G4VisAttributes * fBackWallVisAttributes;
		G4VisAttributes * fWireBoxVisAttributes;
	
};

#endif // __WireChamber_HH___
