#ifndef SiliconStripEvent_h
#define SiliconStripEvent_h 1

#include "TObject.h"
#include "SiliconStripHit.hh" 

class SiliconStripEvent : public TObject {
	public:
		SiliconStripEvent();
		virtual ~SiliconStripEvent();

		void AddHit(SiliconStripHit &);
				
		int GetNHits() const;
		void Reset();

	private:
		std::vector<SiliconStripHit> m_hits;

	ClassDef(SiliconStripEvent,1); // Because we inherit from TObject 
};


#endif // SiliconStripEvent_h
