#ifndef ScintillatorEvent_h
#define ScintillatorEvent_h 1

#include "TObject.h"
#include "ScintillatorHit.hh" 

class ScintillatorEvent : public TObject {
	public:
		ScintillatorEvent();
		virtual ~ScintillatorEvent();


		int GetNHits() const;
		void AddHit(ScintillatorHit & hit);
		void Reset();

	private:
		std::vector<ScintillatorHit> m_hits;	

	ClassDef(ScintillatorEvent,1); // Because we inherit from TObject 
};


#endif // ScintillatorEvent_h
