#ifndef EventHeader_h
#define EventHeader_h 1

#include "TObject.h"

class EventHeader : public TObject {
	public:
		EventHeader();
		virtual ~EventHeader();

		long GetEventID() const;
		void SetEventID(long);

	private:
		long m_event_id; 

	ClassDef(EventHeader,1); // Because we inherit from TObject 
};


#endif // EventHeader_h
