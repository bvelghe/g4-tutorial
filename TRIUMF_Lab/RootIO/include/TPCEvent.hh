#ifndef TPCEvent_h
#define TPCEvent_h 1

#include "TObject.h"

class TPCEvent : public TObject {
	public:
		TPCEvent();
		virtual ~TPCEvent();

		double GetEnergy() const;
		double GetTime() const;
		int GetNHits() const;

		void SetEnergy(double);
		void SetTime(double);
		void SetNHits(int);

	private:
		double m_energy; // MeV
		double m_time; // ns		
		int m_nhits;

	ClassDef(TPCEvent,1); // Because we inherit from TObject 
};


#endif // TPCEvent_h
