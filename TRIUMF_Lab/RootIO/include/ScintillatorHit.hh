#ifndef ScintillatorHit_h
#define ScintillatorHit_h 1

#include "TObject.h"

class ScintillatorHit : public TObject {
	public:
		ScintillatorHit();
		virtual ~ScintillatorHit();

		double GetEnergy() const;
		double GetTime() const;
		unsigned int GetStationID() const;

		void SetEnergy(double);
		void SetTime(double);
		void SetStationID(unsigned int);

	private:
		double m_energy; // MeV
		double m_time; // ns
		unsigned int m_station_id;  

	ClassDef(ScintillatorHit,1); // Because we inherit from TObject 
};


#endif // ScintillatorHit_h
