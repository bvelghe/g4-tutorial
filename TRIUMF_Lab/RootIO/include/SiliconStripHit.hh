#ifndef SiliconStripHit_h
#define SiliconStripHit_h 1

#include "TObject.h"

class SiliconStripHit : public TObject {
	public:
		SiliconStripHit();
		virtual ~SiliconStripHit();

		double GetStripID() const;
		double GetTime() const;
		unsigned int GetStationID() const;

		void SetStripID(unsigned int);
		void SetTime(double);
		void SetStationID(unsigned int);

	private:
		unsigned int m_strip_id;
		double m_time; // ns
		unsigned int m_station_id;  

	ClassDef(SiliconStripHit,1); // Because we inherit from TObject 
};


#endif // SiliconStripHit_h
