#include "CrystalEvent.hh"

CrystalEvent::CrystalEvent() : m_time(0.0), m_energy(0.0), m_nhits(0) {}

CrystalEvent::~CrystalEvent() {}

double CrystalEvent::GetEnergy() const {
	return m_energy;
}

double CrystalEvent::GetTime() const {
	return m_time;
}

int CrystalEvent::GetNHits() const {
	return m_nhits;
}

void CrystalEvent::SetEnergy(double energy) {
	m_energy = energy;
}

void CrystalEvent::SetTime(double time) {
	m_time = time;
}

void CrystalEvent::SetNHits(int nhits) {
	m_nhits = nhits;
} 
