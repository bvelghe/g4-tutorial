#include "TPCEvent.hh"

TPCEvent::TPCEvent() : m_time(0.0), m_energy(0.0) {}

TPCEvent::~TPCEvent() {}

double TPCEvent::GetEnergy() const {
	return m_energy;
}

double TPCEvent::GetTime() const {
	return m_time;
}

int TPCEvent::GetNHits() const {
	return m_nhits;
}

void TPCEvent::SetEnergy(double energy) {
	m_energy = energy;
}

void TPCEvent::SetTime(double time) {
	m_time = time;
}

void TPCEvent::SetNHits(int nhits) {
	m_nhits = nhits;
} 
