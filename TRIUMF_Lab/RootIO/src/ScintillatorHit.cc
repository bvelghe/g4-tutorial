#include "ScintillatorHit.hh"

ScintillatorHit::ScintillatorHit() : m_energy(0.0), m_time(0.0), m_station_id(0) {}

ScintillatorHit::~ScintillatorHit() {}

double ScintillatorHit::GetEnergy() const {
	return m_energy;
}

double ScintillatorHit::GetTime() const {
	return m_time;
}

unsigned int ScintillatorHit::GetStationID() const {
	return m_station_id;
}


void ScintillatorHit::SetEnergy(double energy) {
	m_energy = energy;
}

void ScintillatorHit::SetTime(double time) {
	m_time = time;
}

void ScintillatorHit::SetStationID(unsigned int station_id) {
	m_station_id = station_id;
} 
