#include "SiliconStripEvent.hh"

SiliconStripEvent::SiliconStripEvent() : m_hits(0) {}

SiliconStripEvent::~SiliconStripEvent() {}

int SiliconStripEvent::GetNHits() const {
	return m_hits.size();
}

void SiliconStripEvent::Reset() {
	m_hits.clear();
}

void SiliconStripEvent::AddHit(SiliconStripHit & hit) {
	m_hits.push_back(hit);
}
