#include "SiliconStripHit.hh"

SiliconStripHit::SiliconStripHit() : m_strip_id(-1), m_time(0.0), m_station_id(-1) {}

SiliconStripHit::~SiliconStripHit() {}

double SiliconStripHit::GetStripID() const {
	return m_strip_id;
}

double SiliconStripHit::GetTime() const {
	return m_time;
}

unsigned int SiliconStripHit::GetStationID() const {
	return m_station_id;
}


void SiliconStripHit::SetStripID(unsigned int strip_id) {
	m_strip_id = strip_id;
}

void SiliconStripHit::SetTime(double time) {
	m_time = time;
}

void SiliconStripHit::SetStationID(unsigned int station_id) {
	m_station_id = station_id;
} 
