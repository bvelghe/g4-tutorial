#include "ScintillatorEvent.hh"

ScintillatorEvent::ScintillatorEvent() :  m_hits(0) {}

ScintillatorEvent::~ScintillatorEvent() {}

int ScintillatorEvent::GetNHits() const {
	return m_hits.size();
}

void ScintillatorEvent::Reset() {
	m_hits.clear();
}

void ScintillatorEvent::AddHit(ScintillatorHit & hit) {
	m_hits.push_back(hit);
} 
