#include "ScintillatorRootIO.hh" 

ScintillatorRootIO::ScintillatorRootIO() : m_ready(false), m_tree(0), m_event(0), m_branch(0) {
	for(int icopy = 0; icopy < c_n_scintillator; icopy++) {
		m_g4_hits_collection_ID[icopy] = -1;
		m_g4_hits_collection[icopy] = 0;
	}
	m_SD_manager = G4SDManager::GetSDMpointer();

}

bool ScintillatorRootIO::Initialize(TTree * tree) {
	m_tree = tree;
	m_event = new ScintillatorEvent();
	m_branch = m_tree->Branch("Events",&m_event);
	m_ready = true;
	

	return true;
}

void ScintillatorRootIO::SaveEvent(const G4Event * event) {
	if(m_ready == false) return;
	m_event->Reset(); // This is important, clean up the vector

	// FIXME Clean this up ! Do I need to check the m_SD_manager->GetCollectionID at each event?
	for(int icopy = 0; icopy < c_n_scintillator; icopy++) {
		std::ostringstream oss;
		oss << icopy;
		if(m_g4_hits_collection_ID[icopy] < 0) m_g4_hits_collection_ID[icopy] = m_SD_manager->GetCollectionID("Scintillator_G4HitsCollection_"+oss.str());
		if(m_g4_hits_collection_ID[icopy] < 0) return;

		double energy_sum = 0.0;	
		G4HCofThisEvent* HCTE = event->GetHCofThisEvent();
		if(HCTE) {
			m_g4_hits_collection[icopy] = (ScintillatorG4HitsCollection*)(HCTE->GetHC(m_g4_hits_collection_ID[icopy]));
			G4int NbHits = m_g4_hits_collection[icopy]->entries();
			if(NbHits <= 0) continue;
			for(int idx = 0; idx < NbHits; idx++) {
				energy_sum += (*m_g4_hits_collection[icopy])[idx]->Energy()/MeV;
			} 

			// Digitalization goes here
			double time_smearing = G4RandGauss::shoot(0.0,1.0); // sigma = 1 ns
			double energy_smearing = G4RandGauss::shoot(0.0,0.1*energy_sum); // sigma = 10 % of the total energy 
			ScintillatorHit hit;
			hit.SetStationID(icopy);
			hit.SetEnergy(energy_sum + energy_smearing);
			hit.SetTime(0.0 + time_smearing);
			m_event->AddHit(hit);	
		}	
	}
	
	// Store this event
	m_tree->Fill();
}


ScintillatorRootIO::~ScintillatorRootIO() {
	if(m_event != 0) {
		delete m_event;
		m_event = 0;
	}
}
