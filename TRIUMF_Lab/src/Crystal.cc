#include "Crystal.hh"

Crystal::Crystal(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy) : 
	fMotherLogicalVolume(motherLogicalVolume), 
	fPosition(position), 
	fCopy(copy) {
	}

Crystal::~Crystal() {
}

G4VPhysicalVolume* Crystal::Construct() {
	std::ostringstream oss;
	oss << fCopy;
	

	// FIXME: [IMPROVEMENT] move the declaration to the header file and user a GeometryParameter(s) class.
	G4double fCrystalRadius = 10.0*cm;
	G4double fCrystalZLength = 40.0*cm;

	G4String volume_name = "Crystal_"+oss.str();	
	fCrystalSolidVolume = new G4Tubs(volume_name,0.0,fCrystalRadius,0.5*fCrystalZLength,0,2*CLHEP::pi*radian);
	
	fCrystalLogicalVolume = new G4LogicalVolume(fCrystalSolidVolume,
		G4Material::GetMaterial("G4_SODIUM_IODIDE"),
		volume_name,
		0, // G4FieldManager * (def. 0)
		0, // G4VSensitiveDetector * (def. 0)
		0, // G4UserLimits * (def. 0)
		true); // optimise (def. true)
	
	fCrystalPhysicalVolume = new G4PVPlacement(0,
		fPosition,
		fCrystalLogicalVolume,
		volume_name,
		fMotherLogicalVolume,
		false
		,0); 



	// Setup the sensitive volume
	G4String sensor_SDname = "/CR_"+oss.str();

	G4SDManager * SDManager = G4SDManager::GetSDMpointer();
	CrystalSD * CR_SD = new CrystalSD(sensor_SDname,fCopy);
	fCrystalLogicalVolume->SetSensitiveDetector(CR_SD);
	SDManager->AddNewDetector(CR_SD);
	
	return fCrystalPhysicalVolume;
}

void Crystal::SetProperties() {
	fCrystalVisAttributes = new G4VisAttributes(G4Colour(0.0,0.0,1.0));
	fCrystalLogicalVolume->SetVisAttributes(fCrystalVisAttributes);
}
