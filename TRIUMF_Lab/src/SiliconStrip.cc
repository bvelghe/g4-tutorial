#include "SiliconStrip.hh"

SiliconStrip::SiliconStrip(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy) : 
	fMotherLogicalVolume(motherLogicalVolume), 
	fPosition(position), 
	fCopy(copy) {
	}

SiliconStrip::~SiliconStrip() {
}

G4VPhysicalVolume* SiliconStrip::Construct() {
	std::ostringstream oss;
	oss << fCopy;
	

	// FIXME: [IMPROVEMENT] move the declaration to the header file and user a GeometryParameter(s) class.
	G4double fModuleXLength = 10.0*cm;
	G4double fModuleYLength = 10.0*cm;
	G4double fModuleZLength = 1.0*mm;

	// The strips are aligned with the X axis	
	G4int fNStrip = 100;
	G4double fStripXLength = fModuleXLength/fNStrip;

	G4String module_volume_name = "Silicon_Strip_Module_"+oss.str();	
	G4String strip_volume_name = "Silicon_Strip_"+oss.str();	

	fSSModuleSolidVolume = new G4Box(module_volume_name,0.5*fModuleXLength,0.5*fModuleYLength,0.5*fModuleZLength);

	fSStripSolidVolume = new G4Box(strip_volume_name,0.5*fStripXLength,0.5*fModuleYLength,0.5*fModuleZLength);
	
	fSSModuleLogicalVolume = new G4LogicalVolume(fSSModuleSolidVolume,
		G4Material::GetMaterial("G4_Si"),
		module_volume_name,
		0, // G4FieldManager * (def. 0)
		0, // G4VSensitiveDetector * (def. 0)
		0, // G4UserLimits * (def. 0)
		true); // optimise (def. true)
	
	fSStripLogicalVolume = new G4LogicalVolume(fSStripSolidVolume,
		G4Material::GetMaterial("G4_Si"),
		module_volume_name,
		0, // G4FieldManager * (def. 0)
		0, // G4VSensitiveDetector * (def. 0)
		0, // G4UserLimits * (def. 0)
		true); // optimise (def. true)

	fSStripReplica = new G4PVReplica(strip_volume_name,
		fSStripLogicalVolume,
		fSSModuleLogicalVolume,
		kXAxis,
		fNStrip,
		fStripXLength);
	
	fSSModulePhysicalVolume = new G4PVPlacement(0,
		fPosition,
		fSSModuleLogicalVolume,
		module_volume_name,
		fMotherLogicalVolume,
		false
		,0); 

	// Setup the sensitive volume
	G4String sensor_SDname = "/SS_"+oss.str();

	G4SDManager * SDManager = G4SDManager::GetSDMpointer();
	SiliconStripSD * SS_SD = new SiliconStripSD(sensor_SDname,fCopy);
	fSStripLogicalVolume->SetSensitiveDetector(SS_SD);
	SDManager->AddNewDetector(SS_SD);

	return fSSModulePhysicalVolume;
}

void SiliconStrip::SetProperties() {
	fSSModuleVisAttributes = new G4VisAttributes(G4Colour(1.0,0.0,0.0));
	fSSModuleLogicalVolume->SetVisAttributes(fSSModuleVisAttributes);
}
