#include "RootIO.hh"

RootIO::RootIO() : m_out_file(0) {
	m_header_io = new HeaderRootIO();
	m_siliconstrip_io = new SiliconStripRootIO();
	m_scintillator_io = new ScintillatorRootIO();
}

bool RootIO::Open(std::string output_file) {
	m_out_file = new TFile(output_file.c_str(),"NEW");
	if(m_out_file == 0) return false; // Should never happen
	if(m_out_file->IsZombie()) {
		std::cout << "Unable to open the file " << output_file << " for writing" << std::endl;
		return false;
	}
	return true;
}

bool RootIO::StartRun(long run_id,std::string output_file) {
	if(Open(output_file) != true) return false; // Unable to start a run	
	// Create EventHeader tree
	m_header_tree = new TTree("Header","Event Metadata Tree");
	
	// Create sub-detectors tree
	// FIXME: Bad arch. --> Move this to [Detector]RootIO
	m_siliconstrip_tree = new TTree("SiliconStrip","Silicon Strip Hits Tree");
	m_scintillator_tree = new TTree("Scintillator","Scintillator Hits Tree");

	m_header_io->Initialize(m_header_tree);
	m_siliconstrip_io->Initialize(m_siliconstrip_tree);	
	m_scintillator_io->Initialize(m_scintillator_tree);	
	
	return true;
}

void RootIO::EndRun() {
	Close();
}

void RootIO::SaveEvent(const G4Event * event) {
	m_header_io->SaveEvent(event);
	m_siliconstrip_io->SaveEvent(event);	
	m_scintillator_io->SaveEvent(event);	
}

void RootIO::Close() {
	if(m_out_file == 0) return; // INOP
	if(m_out_file->IsOpen()) {
		m_out_file->Write();
		m_out_file->Close(); 	
	}
	delete m_out_file;
	m_out_file = 0;
}

RootIO::~RootIO() {
	Close();
}
