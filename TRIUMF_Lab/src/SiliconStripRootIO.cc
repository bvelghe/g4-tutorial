#include "SiliconStripRootIO.hh" 

SiliconStripRootIO::SiliconStripRootIO() : m_ready(false), m_tree(0), m_event(0), m_branch(0) {
	for(int icopy = 0; icopy < c_n_siliconstrip; icopy++) {
		m_g4_hits_collection_ID[icopy] = -1;
		m_g4_hits_collection[icopy] = 0;
	}
	m_SD_manager = G4SDManager::GetSDMpointer();

}

bool SiliconStripRootIO::Initialize(TTree * tree) {
	// FIXME Events -> Candidates?
	m_tree = tree;
	m_event = new SiliconStripEvent();
	m_branch = m_tree->Branch("Events",&m_event);
	m_ready = true;
	

	return true;
}

void SiliconStripRootIO::SaveEvent(const G4Event * event) {
	if(m_ready == false) return;
	m_event->Reset(); // This is important, clear up the vector

	// FIXME Clean this up ! Do I need to check the m_SD_manager->GetCollectionID at each event? + OSS
	for(int icopy = 0; icopy < c_n_siliconstrip; icopy++) {
		std::ostringstream oss;
		oss << icopy;
		
		if(m_g4_hits_collection_ID[icopy] < 0) m_g4_hits_collection_ID[icopy] = m_SD_manager->GetCollectionID("SiliconStrip_G4HitsCollection_"+oss.str());
		if(m_g4_hits_collection_ID[icopy] < 0) return;

		double energy = 0.0;	
		int strip_id  = -1;
		G4HCofThisEvent* HCTE = event->GetHCofThisEvent();
		if(HCTE) {
        		m_g4_hits_collection[icopy] = (SiliconStripG4HitsCollection*)(HCTE->GetHC(m_g4_hits_collection_ID[icopy]));				      G4int NbHits = m_g4_hits_collection[icopy]->entries();
    			if(NbHits <= 0) continue;
			for(int idx = 0; idx < NbHits; idx++) {
				energy = (*m_g4_hits_collection[icopy])[idx]->Energy()/MeV;
				strip_id = (*m_g4_hits_collection[icopy])[idx]->StripID();
				// Digitalization goes here
				double time_smearing = G4RandGauss::shoot(0.0,1.0); // sigma = 1 ns
				SiliconStripHit hit;
				hit.SetStripID(strip_id);
				hit.SetStationID(icopy);
				hit.SetTime(0.0 + time_smearing);
				m_event->AddHit(hit);	
			}
		}	
	}

	// Store this event
	m_tree->Fill();
}


SiliconStripRootIO::~SiliconStripRootIO() {
	if(m_event != 0) {
		delete m_event;
		m_event = 0;
	}
}
