#include "Scintillator.hh"

Scintillator::Scintillator(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy) : 
	fMotherLogicalVolume(motherLogicalVolume), 
	fPosition(position), 
	fCopy(copy) {
	}

Scintillator::~Scintillator() {
}

G4VPhysicalVolume* Scintillator::Construct() {
	std::ostringstream oss;
	oss << fCopy;
	

	// FIXME: [IMPROVEMENT] move the declaration to the header file and user a GeometryParameter(s) class.
	G4double fModuleXLength = 10.0*cm;
	G4double fModuleYLength = 10.0*cm;
	G4double fModuleZLength = 1.0*cm;

	G4String volume_name = "Scintillator_Module_"+oss.str();	
	fScintillatorSolidVolume = new G4Box(volume_name,0.5*fModuleXLength,0.5*fModuleYLength,0.5*fModuleZLength);
	
	fScintillatorLogicalVolume = new G4LogicalVolume(fScintillatorSolidVolume,
		G4Material::GetMaterial("G4_PLASTIC_SC_VINYLTOLUENE"),
		volume_name,
		0, // G4FieldManager * (def. 0)
		0, // G4VSensitiveDetector * (def. 0)
		0, // G4UserLimits * (def. 0)
		true); // optimise (def. true)
	
	fScintillatorPhysicalVolume = new G4PVPlacement(0,
		fPosition,
		fScintillatorLogicalVolume,
		volume_name,
		fMotherLogicalVolume,
		false
		,0); 



	// Setup the sensitive volume
	G4String sensor_SDname = "/SC_"+oss.str();
	
	G4SDManager * SDManager = G4SDManager::GetSDMpointer();
	ScintillatorSD * CR_SD = new ScintillatorSD(sensor_SDname,fCopy);
	fScintillatorLogicalVolume->SetSensitiveDetector(CR_SD);
	SDManager->AddNewDetector(CR_SD);
	
	return fScintillatorPhysicalVolume;
}

void Scintillator::SetProperties() {
	fScintillatorVisAttributes = new G4VisAttributes(G4Colour(1.0,1.0,0.0));
	fScintillatorLogicalVolume->SetVisAttributes(fScintillatorVisAttributes);
}
