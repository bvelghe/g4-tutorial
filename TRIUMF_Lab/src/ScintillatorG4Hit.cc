#include "ScintillatorG4Hit.hh"

ScintillatorG4Hit::ScintillatorG4Hit() {
	fEnergy = 0;
}
ScintillatorG4Hit::~ScintillatorG4Hit() {}

/////////////////////////
// Not yet implemented //
/////////////////////////

//void ScintillatorG4Hit::Draw() {}
//void ScintillatorG4Hit::Print() {}

// Shallow copy is sufficicent
ScintillatorG4Hit::ScintillatorG4Hit(const ScintillatorG4Hit& right)
{
	fEnergy = right.fEnergy;

}

const ScintillatorG4Hit& ScintillatorG4Hit::operator=(const ScintillatorG4Hit& right)
{
	if (this == &right) return *this; //Check for self-assignment
	fEnergy = right.fEnergy;
	return *this;
}

G4int ScintillatorG4Hit::operator==(const ScintillatorG4Hit& right) const
{
    return (this==&right) ? 1 : 0;
}
