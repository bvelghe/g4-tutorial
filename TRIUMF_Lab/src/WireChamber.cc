// Inspired by the TWIST DC design
// ! Sense wires are parallel to the Y axis !
// The whole module can be rotated if needed. 

#include "WireChamber.hh"

WireChamber::WireChamber(G4LogicalVolume* motherLogicalVolume, G4ThreeVector position, G4int copy) : 
	fMotherLogicalVolume(motherLogicalVolume), 
	fPosition(position), 
	fCopy(copy) {
	}

WireChamber::~WireChamber() {
}

G4VPhysicalVolume* WireChamber::Construct() {
	std::ostringstream oss;
	oss << fCopy;
	
	// Note: Variables are f-prefixed anyway since they should eventually be loaded from a central source (and promoted as class members).
	G4double fModuleWallZLength = 6.4*um;
	G4double fWireRadius = 15.0*um;
	G4double fWirePitch = 4.0*mm;
	G4double fModuleZLength = 4.0*mm + 2*fModuleWallZLength;
	G4double fModuleXLength = 32.2*cm;
	G4double fModuleYLength = 32.2*cm;
	G4int fWireCount = 80; 

	// Define the volume Z positions inside the WCModule volume
	G4double FrontWallZPos = 0.5*(fModuleWallZLength - fModuleZLength);
	G4double BackWallZPos = 0.5*(fModuleZLength - fModuleWallZLength);
	G4double WireBoxZPos = 0.0; // Wire box always at the center of the chamber

	// Die if the parameters are not consistent 
	assert((fWireCount+1)*fWirePitch <= fModuleXLength);

	G4String volume_name = "Wire_Chamber_"+oss.str();	
	
	fWCModuleSolidVolume = new G4Box(volume_name,0.5*fModuleXLength,0.5*fModuleYLength,0.5*fModuleZLength);
	
	fWCModuleLogicalVolume = new G4LogicalVolume(fWCModuleSolidVolume,
		G4Material::GetMaterial("G4_Galactic"),
		volume_name,
		0, // G4FieldManager * (def. 0)
		0, // G4VSensitiveDetector * (def. 0)
		0, // G4UserLimits * (def. 0)
		true); // optimise (def. true)
	
	fWCModulePhysicalVolume = new G4PVPlacement(0,
		fPosition,
		fWCModuleLogicalVolume,
		volume_name,
		fMotherLogicalVolume,
		false
		,0); 



	// Setup the sensitive volume
	G4String sensor_SDname = "/WC_"+oss.str();
	
	/* FUTURE
	G4SDManager * SDManager = G4SDManager::GetSDMpointer();
	WireChamberSD * WC_SD = new WireChamberSD(sensor_SDname,fCopy);
	fWireBoxLogicialVolume->SetSensitiveDetector(WC_SD);
	SDmanager->AddNewDetector(WC_SD);
	*/

	return fWCModulePhysicalVolume;
}

void WireChamber::SetProperties() {
	fWCModuleVisAttributes = new G4VisAttributes(G4Colour(0.0,1.0,0.0));
	fWCModuleLogicalVolume->SetVisAttributes(fWCModuleVisAttributes);
}
