#include "CrystalHit.hh"

CrystalHit::CrystalHit() {
	fEnergy = 0;
}
CrystalHit::~CrystalHit() {}

/////////////////////////
// Not yet implemented //
/////////////////////////

//void CrystalHit::Draw() {}
//void CrystalHit::Print() {}

// Shallow copy is sufficicent
CrystalHit::CrystalHit(const CrystalHit& right)
{
	fEnergy = right.fEnergy;

}

const CrystalHit& CrystalHit::operator=(const CrystalHit& right)
{
	if (this == &right) return *this; //Check for self-assignment
	fEnergy = right.fEnergy;
	return *this;
}

G4int CrystalHit::operator==(const CrystalHit& right) const
{
    return (this==&right) ? 1 : 0;
}
