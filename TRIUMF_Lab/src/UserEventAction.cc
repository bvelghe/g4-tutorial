#include "UserEventAction.hh"


//FIXME : iCopy ?

UserEventAction::UserEventAction() {
	m_root_io = 0;
}

UserEventAction::~UserEventAction() {;}

void UserEventAction::BeginOfEventAction(const G4Event* event) {

 
	/////////////////////////////
	// Display run progression //
	/////////////////////////////	
	G4int eventID = event->GetEventID();
	if(eventID%10000 == 0) {
		G4cout << "# > Event " << event->GetEventID() << G4endl;
	}
}


void UserEventAction::EndOfEventAction(const G4Event* event) {

	if(m_io_ready == true) {
		m_root_io->SaveEvent(event);
	}
}

void UserEventAction::SetIO(RootIO * root_io) {
	m_root_io = root_io;
	m_io_ready = true;
}
