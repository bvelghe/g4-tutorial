#include "ScintillatorSD.hh"
#include "G4UnitsTable.hh"
#include "G4SDManager.hh"
#include "G4VProcess.hh"
#include "G4Step.hh"

// Call the superclass (G4VSensitiveDetector) constructor
ScintillatorSD::ScintillatorSD(G4String name,G4int icopy) : G4VSensitiveDetector(name) {
	fCopy = icopy;
	fHCID = -1;
	fCell = -1;
	std::ostringstream os; os << icopy;
  	collectionName.insert("Scintillator_G4HitsCollection_" + os.str());
}	


ScintillatorSD::~ScintillatorSD() {
}

//Called at the begining of every new event
void ScintillatorSD::Initialize(G4HCofThisEvent* HCTE) {
	fHitsCollection = new ScintillatorG4HitsCollection(SensitiveDetectorName, collectionName[0]);
	if (fHCID < 0)
    {
        fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
    }
    HCTE->AddHitsCollection(fHCID, fHitsCollection);
    fCell = -1;
}

//Called for every step in the sensitve volume (mandatory)
G4bool ScintillatorSD::ProcessHits(G4Step* step, G4TouchableHistory*)	{
  	G4double energy = step->GetTotalEnergyDeposit();
	
	if (energy == 0) return false;

	// FIXME Warning: Sum all the energy inside this **event** --> Move this to ScintillatorRootIO?
	if(fCell == -1) {
		ScintillatorG4Hit* hit = new ScintillatorG4Hit();
	
		hit->Energy(energy);

		G4int cell_id = fHitsCollection->insert(hit);
		fCell = cell_id - 1; //Voir exemple N04
	
	} else {
		(*fHitsCollection)[fCell]->AddEnergy(energy);
	}
	return true;
} 
