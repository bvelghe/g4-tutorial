#include "SiliconStripSD.hh"
#include "G4UnitsTable.hh"
#include "G4SDManager.hh"
#include "G4VProcess.hh"
#include "G4Step.hh"

// Call the superclass (G4VSensitiveDetector) constructor
SiliconStripSD::SiliconStripSD(G4String name,G4int icopy) : G4VSensitiveDetector(name) {
	fCopy = icopy;
	fHCID = -1;
	std::ostringstream os; os << icopy;
  	collectionName.insert("SiliconStrip_G4HitsCollection_" + os.str());
	fStripCellMap = std::map<G4int,G4int>();
}	


SiliconStripSD::~SiliconStripSD() {
}

//Called at the begining of every new event
void SiliconStripSD::Initialize(G4HCofThisEvent* HCTE) {
	fHitsCollection = new SiliconStripG4HitsCollection(SensitiveDetectorName, collectionName[0]);
	if (fHCID < 0)
    {
        fHCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]);
    }
    HCTE->AddHitsCollection(fHCID, fHitsCollection);

    // Don't forget to clean the map
    fStripCellMap.clear();
}

//Called for every step in the sensitve volume (mandatory)
G4bool SiliconStripSD::ProcessHits(G4Step* step, G4TouchableHistory*)	{
  	// NB. the G4TouchableHistory pointer passed to the method is used for Readout geometries 
	G4double energy = step->GetTotalEnergyDeposit();
	if (energy == 0) return false;
	G4TouchableHistory* hist = (G4TouchableHistory*)step->GetPreStepPoint()->GetTouchable();	

	// FIXME Warning: Sum all the energy inside this **event** --> Move this to SiliconStripRootIO?
	int strip_id = hist->GetReplicaNumber();
	if(fStripCellMap.find(strip_id) == fStripCellMap.end()) {
		SiliconStripG4Hit* hit = new SiliconStripG4Hit();
	
		hit->Energy(energy);
		hit->StripID(strip_id);

		G4int cell_idx = fHitsCollection->insert(hit) - 1;
		fStripCellMap.insert(std::make_pair(strip_id,cell_idx));
	
	} else {
		G4int cell_idx = fStripCellMap[strip_id];
		(*fHitsCollection)[cell_idx]->AddEnergy(energy);
	}
	return true;
} 
