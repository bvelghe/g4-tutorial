#include "SiliconStripG4Hit.hh"

SiliconStripG4Hit::SiliconStripG4Hit() {
	fEnergy = 0;
	fStripID = -1;
}
SiliconStripG4Hit::~SiliconStripG4Hit() {}

/////////////////////////
// Not yet implemented //
/////////////////////////

//void SiliconStripG4Hit::Draw() {}
//void SiliconStripG4Hit::Print() {}

// Shallow copy is sufficicent
SiliconStripG4Hit::SiliconStripG4Hit(const SiliconStripG4Hit& right)
{
	fEnergy = right.fEnergy;
	fStripID = right.fStripID;
}

const SiliconStripG4Hit& SiliconStripG4Hit::operator=(const SiliconStripG4Hit& right)
{
	if (this == &right) return *this; //Check for self-assignment
	fEnergy = right.fEnergy;
	fStripID = right.fStripID;
	return *this;
}

G4int SiliconStripG4Hit::operator==(const SiliconStripG4Hit& right) const
{
    return (this==&right) ? 1 : 0;
}
