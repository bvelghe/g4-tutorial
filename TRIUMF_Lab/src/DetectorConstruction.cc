#include "DetectorConstruction.hh"

DetectorConstruction::DetectorConstruction() {
	/////////////////////
	// Build materials //
	/////////////////////
	fNistMan = G4NistManager::Instance();
	fNistMan->FindOrBuildMaterial("G4_Galactic");
	fNistMan->FindOrBuildMaterial("G4_Si");
	fNistMan->FindOrBuildMaterial("G4_AIR");
	fNistMan->FindOrBuildMaterial("G4_STAINLESS-STEEL"); // (density = 8 g/cm^3)
	fNistMan->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
	fNistMan->FindOrBuildMaterial("G4_SODIUM_IODIDE");
}

DetectorConstruction::~DetectorConstruction() {
}

G4VPhysicalVolume* DetectorConstruction::Construct() {
	/////////////////////////////
	// Define the world volume //
	/////////////////////////////
	fworldSolidVolume = new G4Box("World",1.0*m,1.0*m,1.0*m);

	fworldLogicalVolume = new G4LogicalVolume(fworldSolidVolume,
			G4Material::GetMaterial("G4_AIR"),
			"World",
			0,
			0,
			0);

	fworldPhysicalVolume = new G4PVPlacement(0,
			G4ThreeVector(0.,0.,0.),
			fworldLogicalVolume,
			"World",
			0,
			false
			,0);  

	///////////////////////////
	// Instantiate the setup //
	///////////////////////////


	G4double globalZOffset = -1.0*m;
	// Note: Variables are f-prefixed anyway since they should eventually be loaded from a central source (and promoted as class members).
	// All the pos. refer to the *center* of the volume, origin is at -Z pos. of the world volume (see globalZOffset)
		
	G4double fSiliconStripZPos[2] = {10.0*cm,12.0*cm};
	G4double fScintillatorZPos = 36.0*cm;

	for(int i = 0 ; i < 2 ; i++) {
		fSiliconStrip[i] = new SiliconStrip(fworldLogicalVolume,G4ThreeVector(0.0,0.0,fSiliconStripZPos[i]+globalZOffset),i);
		fSiliconStrip[i]->Construct();
		fSiliconStrip[i]->SetProperties();
	}

	fScintillator = new Scintillator(fworldLogicalVolume,G4ThreeVector(0.0,0.0,fScintillatorZPos+globalZOffset),0);
	fScintillator->Construct();
	fScintillator->SetProperties();

	SetProperties();
	return fworldPhysicalVolume;                        
}

void DetectorConstruction::SetProperties() {	
	// Set the world volume invisible
	fworldLogicalVolume->SetVisAttributes(G4VisAttributes::Invisible);
}
