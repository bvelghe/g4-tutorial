# Geant4 Examples

## Introduction
We provide a ready-to-use work environment to the students. It is not a Geant4 course. 

Two examples are provided
 - `AlphaTPC`: TPC with an embedded data source. Use Geant4 `G4ScoringManager` interface to extract physical quantities. ASCII output.
 - `TRIUMF_LAB`: Wire chambers, scintillators, silicon strips and a NaI crystal. Use the more advanced `G4VSensitiveDetector` interface to register physcial quantities. ROOT output. Mostly complete, some work needed on the 'digitalization' part.

## Technical Details

[VirtualBox](https://www.virtualbox.org) Ubuntu virtual machine pre-installed with
 - Geant 4.10
 - ROOT 6.08

VirtualBox runs on Linux, MacOSX and Windows. The image size is 16 GB, distribute it on USB keys.

## Usage
![Screenshot of the desktop](screenshots/work_screen.png)

 - If needed login, username/password are _g4_/_g4_,
 - Navigate to the g4tutorial directory: `cd ~/g4tutorial`, 
 - Setup the environment: `source env.sh`,
 - Create a build directory: `mkdir build`,
 - Navigate inside the build directory: `cd build`,
 - Compile the example: 
   - `cmake ../AlphaTPC`, 
   - `make`,
 - Start the simulation: `./AlphaTPC`.
